package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.AccountTransaction;
import com.mulong.common.domain.pojo.mall.custom.AccountTransactionQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface AccountTransactionMapper extends Mapper<AccountTransaction>, MySqlMapper<AccountTransaction> {

    List<AccountTransaction> selectByParam(AccountTransactionQuery query);

    @Select("SELECT * FROM account_transaction WHERE `id` = #{id}")
    AccountTransaction selectById(@Param("id") Long id);

    @Delete("DELETE FROM account_transaction WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

}