package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.MyFavorite;
import com.mulong.common.domain.pojo.mall.custom.DistributionMyFavorite;
import com.mulong.common.domain.pojo.mall.custom.DistributionMyFavoriteQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface MyFavoriteMapper extends Mapper<MyFavorite>, MySqlMapper<MyFavorite> {

    List<DistributionMyFavorite> selectDistributionMyFavoriteByParam(DistributionMyFavoriteQuery query);

    @Select("SELECT * FROM my_favorite WHERE `id` = #{id}")
    MyFavorite selectById(@Param("id") Long id);

    @Delete("DELETE FROM my_favorite WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

}