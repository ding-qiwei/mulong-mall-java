package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mulong.common.dao.mall.mapper.CustomerOrderDetailMapper;
import com.mulong.common.domain.pojo.mall.CustomerOrderDetail;
import com.mulong.common.domain.pojo.mall.custom.CustomerOrderDetailInfo;
import com.mulong.common.domain.pojo.mall.custom.CustomerOrderDetailInfoQuery;
import com.mulong.common.enums.CustomerOrderStatus;

/**
 * CustomerOrderDao
 * 
 * @author mulong
 * @data 2021-08-18 21:10:56
 */
@Component
public class CustomerOrderDao {
    @Autowired
    private CustomerOrderDetailMapper customerOrderDetailMapper;

    @Transactional(transactionManager = "mallTransactionManager", rollbackFor = {Exception.class, Error.class})
    public void addCustomerOrderDetail(List<CustomerOrderDetail> records) {
        for (CustomerOrderDetail record : records) {
            customerOrderDetailMapper.insert(record);
        }
    }

    public List<CustomerOrderDetailInfo> queryCustomerOrderDetailInfo(CustomerOrderDetailInfoQuery query) {
        return customerOrderDetailMapper.selectInfoByParam(query);
    }

    public CustomerOrderDetail queryCustomerOrderDetailByOrderId(String orderId) {
        return customerOrderDetailMapper.selectByOrderId(orderId);
    }

    public boolean pay(CustomerOrderDetail record) {
        if (!CustomerOrderStatus.ARREARAGE.getCode().equals(record.getStatus())) {
            return false;
        }
        String orderId = record.getOrderId();
        int count = customerOrderDetailMapper.pay(orderId);
        if (count == 0) {
            return false;
        }
        return true;
    }

}
