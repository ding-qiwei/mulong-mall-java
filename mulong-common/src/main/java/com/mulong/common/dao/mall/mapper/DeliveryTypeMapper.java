package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.DeliveryType;
import com.mulong.common.domain.pojo.mall.custom.DeliveryTypeQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface DeliveryTypeMapper extends Mapper<DeliveryType>, MySqlMapper<DeliveryType> {

    List<DeliveryType> selectByParam(DeliveryTypeQuery query);

    @Select("SELECT * FROM delivery_type WHERE `id` = #{id}")
    DeliveryType selectById(@Param("id") Integer id);

    @Delete("DELETE FROM delivery_type WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

}