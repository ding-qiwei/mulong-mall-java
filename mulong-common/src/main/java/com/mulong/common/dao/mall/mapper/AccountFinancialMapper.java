package com.mulong.common.dao.mall.mapper;

import java.math.BigDecimal;
import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.AccountFinancial;
import com.mulong.common.domain.pojo.mall.custom.AccountFinancialQuery;
import com.mulong.common.domain.pojo.mall.custom.CustomAccountFinancial;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface AccountFinancialMapper extends Mapper<AccountFinancial>, MySqlMapper<AccountFinancial> {

    List<CustomAccountFinancial> selectCustomByParam(AccountFinancialQuery query);

    @Select("SELECT * FROM account_financial WHERE `id` = #{id}")
    AccountFinancial selectById(@Param("id") Integer id);

    @Select("SELECT * FROM account_financial WHERE `gateway_account_id` = #{gatewayAccountId}")
    AccountFinancial selectByGatewayAccountId(@Param("gatewayAccountId") Integer gatewayAccountId);

    @Update("UPDATE account_financial SET `account_balance` = `account_balance` + #{change} WHERE `id` = #{id}")
    int increaseBalanceById(@Param("id") Integer id, @Param("change") BigDecimal change);

    @Update("UPDATE account_financial SET `account_balance` = `account_balance` - #{change} WHERE `id` = #{id} AND `account_balance` >= #{change}")
    int decreaseBalanceById(@Param("id") Integer id, @Param("change") BigDecimal change);

    @Update("UPDATE account_financial SET `account_balance` = `account_balance` + #{change} WHERE `gateway_account_id` = #{gatewayAccountId}")
    int increaseBalanceByGatewayAccountId(@Param("gatewayAccountId") Integer gatewayAccountId, @Param("change") BigDecimal change);

    @Update("UPDATE account_financial SET `account_balance` = `account_balance` - #{change} WHERE `gateway_account_id` = #{gatewayAccountId} AND `account_balance` >= #{change}")
    int decreaseBalanceByGatewayAccountId(@Param("gatewayAccountId") Integer gatewayAccountId, @Param("change") BigDecimal change);

    @Delete("DELETE FROM account_financial WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

    @Delete("DELETE FROM account_financial WHERE `gateway_account_id` = #{gatewayAccountId}")
    void deleteByGatewayAccountId(@Param("gatewayAccountId") Integer gatewayAccountId);

}