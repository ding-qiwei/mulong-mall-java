package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.SizeConversion;
import com.mulong.common.domain.pojo.mall.custom.CustomSizeConversion;
import com.mulong.common.domain.pojo.mall.custom.SizeConversionQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface SizeConversionMapper extends Mapper<SizeConversion>, MySqlMapper<SizeConversion> {

    void insertOnDuplicateKeyUpdate(SizeConversion record);

    void insertListOnDuplicateKeyUpdate(List<SizeConversion> records);

    List<SizeConversion> selectByParam(SizeConversionQuery query);

    List<CustomSizeConversion> selectCustomByParam(SizeConversionQuery query);

    List<SizeConversion> selectDuplicateKey(List<SizeConversion> records, @Param("limit") Integer limit);

    @Select("SELECT * FROM size_conversion WHERE `id` = #{id}")
    SizeConversion selectById(@Param("id") Integer id);

    @Delete("DELETE FROM size_conversion WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

}