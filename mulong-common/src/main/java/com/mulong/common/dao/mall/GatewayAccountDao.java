package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.mapper.GatewayAccountMapper;
import com.mulong.common.domain.pojo.mall.custom.CustomGatewayAccount;
import com.mulong.common.domain.pojo.mall.custom.GatewayAccountQuery;

/**
 * GatewayAccountDao
 * 
 * @author mulong
 * @data 2021-03-30 20:50:29
 */
@Component
public class GatewayAccountDao {
    @Autowired
    private GatewayAccountMapper gatewayAccountMapper;

    public CustomGatewayAccount add(CustomGatewayAccount record, String insertBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(insertBy);
        record.setUpdateTime(now);
        record.setUpdateBy(insertBy);
        gatewayAccountMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public CustomGatewayAccount queryById(Integer id) {
        return gatewayAccountMapper.selectById(id);
    }

    public CustomGatewayAccount queryByUsername(String username) {
        return gatewayAccountMapper.selectByUsername(username);
    }

    public List<CustomGatewayAccount> queryAll() {
        return gatewayAccountMapper.selectCustomAll();
    }

    public List<CustomGatewayAccount> query(GatewayAccountQuery query) {
        return gatewayAccountMapper.selectByParam(query);
    }

    public void put(CustomGatewayAccount record, String putBy) {
        record.setUpdateTime(new Date());
        record.setUpdateBy(putBy);
        gatewayAccountMapper.updateByPrimaryKey(record);
    }

    public void delete(CustomGatewayAccount record, String deleteBy) {
        gatewayAccountMapper.deleteById(record.getId());
    }

}
