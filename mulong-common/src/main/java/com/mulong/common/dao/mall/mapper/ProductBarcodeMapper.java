package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.ProductBarcode;
import com.mulong.common.domain.pojo.mall.custom.ProductBarcodeQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface ProductBarcodeMapper extends Mapper<ProductBarcode>, MySqlMapper<ProductBarcode> {

    List<ProductBarcode> selectByParam(ProductBarcodeQuery query);

    List<ProductBarcode> selectDuplicateKey(List<ProductBarcode> records, @Param("limit") Integer limit);

    @Select("SELECT * FROM product_barcode WHERE `id` = #{id}")
    ProductBarcode selectById(@Param("id") Long id);

    @Delete("DELETE FROM product_barcode WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

}