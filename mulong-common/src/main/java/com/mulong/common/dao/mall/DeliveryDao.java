package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.mapper.DeliveryTypeMapper;
import com.mulong.common.dao.mall.mapper.SupplierChannelDeliveryChargeMapper;
import com.mulong.common.domain.pojo.mall.DeliveryType;
import com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelDeliveryChargeQuery;
import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDeliveryCharge;
import com.mulong.common.domain.pojo.mall.custom.DeliveryTypeQuery;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;

/**
 * DeliveryDao
 * 
 * @author mulong
 * @data 2021-06-22 16:59:44
 */
@Component
public class DeliveryDao {
    @Autowired
    private DeliveryTypeMapper deliveryTypeMapper;
    @Autowired
    private SupplierChannelDeliveryChargeMapper supplierChannelDeliveryChargeMapper;

    public DeliveryType addDeliveryType(DeliveryType record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        deliveryTypeMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<DeliveryType> queryDeliveryType(DeliveryTypeQuery query) {
        return deliveryTypeMapper.selectByParam(query);
    }

    public DeliveryType queryDeliveryTypeById(Integer id) {
        return deliveryTypeMapper.selectById(id);
    }

    public void putDeliveryType(DeliveryType record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        deliveryTypeMapper.updateByPrimaryKey(record);
    }

    public void deleteDeliveryType(DeliveryType record, String deleteBy) {
        deliveryTypeMapper.deleteById(record.getId());
    }

    public SupplierChannelDeliveryCharge addDeliveryCharge(SupplierChannelDeliveryCharge record, String addBy) {
        if (record.getChannelId() >= 0) {
            throw new MulongException(ErrorEnums.UNKNOWN_ERROR);
        }
        Date now = new Date();
        record.setId(null);
        record.setChannelId(-1);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        supplierChannelDeliveryChargeMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public void addOrUpdateSupplierChannelDeliveryCharge(SupplierChannelDeliveryCharge record, String addBy) {
        Date now = new Date();
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        supplierChannelDeliveryChargeMapper.insertOnDuplicateKeyUpdate(record);
    }

    public void addOrUpdateSupplierChannelDeliveryCharge(List<SupplierChannelDeliveryCharge> records, String addBy) {
        Date now = new Date();
        records.forEach(item -> {
            item.setCreateTime(now);
            item.setCreateBy(addBy);
            item.setUpdateTime(now);
            item.setUpdateBy(addBy);
        });
        supplierChannelDeliveryChargeMapper.insertListOnDuplicateKeyUpdate(records);
    }

    public List<CustomSupplierChannelDeliveryCharge> queryDeliveryCharge(SupplierChannelDeliveryChargeQuery query) {
        return supplierChannelDeliveryChargeMapper.selectDefaultByParam(query);
    }

    public SupplierChannelDeliveryCharge queryDeliveryChargeById(Long id) {
        return supplierChannelDeliveryChargeMapper.selectDefaultById(id);
    }

    public void putDeliveryCharge(SupplierChannelDeliveryCharge record, String putBy) {
        if (record.getChannelId() >= 0) {
            throw new MulongException(ErrorEnums.UNKNOWN_ERROR);
        }
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        supplierChannelDeliveryChargeMapper.updateByPrimaryKey(record);
    }

    public void deleteDeliveryCharge(SupplierChannelDeliveryCharge record, String deleteBy) {
        supplierChannelDeliveryChargeMapper.deleteDefaultById(record.getId());
    }

}
