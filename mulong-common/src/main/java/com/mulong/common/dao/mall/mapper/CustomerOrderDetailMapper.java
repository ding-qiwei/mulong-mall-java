package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.CustomerOrderDetail;
import com.mulong.common.domain.pojo.mall.custom.CustomerOrderDetailInfo;
import com.mulong.common.domain.pojo.mall.custom.CustomerOrderDetailInfoQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface CustomerOrderDetailMapper extends Mapper<CustomerOrderDetail>, MySqlMapper<CustomerOrderDetail> {

    List<CustomerOrderDetailInfo> selectInfoByParam(CustomerOrderDetailInfoQuery query);

    CustomerOrderDetailInfo selectInfoByOrderId(@Param("orderId") String orderId, @Param("gatewayAccountId") Integer gatewayAccountId);

    @Select("SELECT * FROM customer_order_detail WHERE `order_id` = #{orderId}")
    CustomerOrderDetail selectByOrderId(@Param("orderId") String orderId);

    @Update("UPDATE customer_order_detail SET `status` = 2 WHERE `order_id` = #{orderId} AND `status` = 1")
    int pay(@Param("orderId") String orderId);

    @Delete("DELETE FROM customer_order_detail WHERE `order_id` = #{orderId}")
    void deleteByOrderId(@Param("orderId") String orderId);

}