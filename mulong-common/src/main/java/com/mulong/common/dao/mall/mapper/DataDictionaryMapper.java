package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.DataDictionary;
import com.mulong.common.domain.pojo.mall.custom.CustomDataDictionary;
import com.mulong.common.domain.pojo.mall.custom.DataDictionaryQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface DataDictionaryMapper extends Mapper<DataDictionary>, MySqlMapper<DataDictionary> {

    @Select("SELECT * FROM data_dictionary")
    List<CustomDataDictionary> selectCustomAll();

    List<CustomDataDictionary> selectByParam(DataDictionaryQuery query);

    List<CustomDataDictionary> selectByCodes(List<String> codes);

    @Select("SELECT * FROM data_dictionary WHERE `id` = #{id}")
    CustomDataDictionary selectById(@Param("id") Integer id);

    @Delete("DELETE FROM data_dictionary WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

}