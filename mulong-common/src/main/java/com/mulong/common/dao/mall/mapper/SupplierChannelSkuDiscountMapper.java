package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.SupplierChannelSkuDiscount;
import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelSkuDiscount;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelSkuDiscountQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface SupplierChannelSkuDiscountMapper extends Mapper<SupplierChannelSkuDiscount>, MySqlMapper<SupplierChannelSkuDiscount> {

    List<CustomSupplierChannelSkuDiscount> selectByParam(SupplierChannelSkuDiscountQuery query);

    List<SupplierChannelSkuDiscount> selectDuplicateKey(List<SupplierChannelSkuDiscount> records, @Param("limit") Integer limit);

    @Select("SELECT * FROM supplier_channel_sku_discount WHERE `id` = #{id}")
    SupplierChannelSkuDiscount selectById(@Param("id") Long id);

    @Delete("DELETE FROM supplier_channel_sku_discount WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

}