package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.SupplierChannel;
import com.mulong.common.domain.pojo.mall.custom.InventoryChannelQuery;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface SupplierChannelMapper extends Mapper<SupplierChannel>, MySqlMapper<SupplierChannel> {

    List<SupplierChannel> selectByParam(SupplierChannelQuery query);

    List<SupplierChannel> selectInventoryChannelByParam(InventoryChannelQuery query);

    @Select("SELECT * FROM supplier_channel WHERE `id` = #{id}")
    SupplierChannel selectById(@Param("id") Integer id);

    @Update("UPDATE supplier_channel SET `inventory_update_time` = #{inventoryUpdateTime} WHERE `id` = #{id}")
    void updateInventoryUpdateTimeById(@Param("id") Integer id, @Param("inventoryUpdateTime") Date inventoryUpdateTime);

    @Delete("DELETE FROM supplier_channel WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

}