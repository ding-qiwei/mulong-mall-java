package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.GatewayAccount;
import com.mulong.common.domain.pojo.mall.custom.CustomGatewayAccount;
import com.mulong.common.domain.pojo.mall.custom.GatewayAccountQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface GatewayAccountMapper extends Mapper<GatewayAccount>, MySqlMapper<GatewayAccount> {

    @Select("SELECT * FROM gateway_account")
    List<CustomGatewayAccount> selectCustomAll();

    List<CustomGatewayAccount> selectByParam(GatewayAccountQuery query);

    @Select("SELECT * FROM gateway_account WHERE `id` = #{id}")
    CustomGatewayAccount selectById(@Param("id") Integer id);

    @Select("SELECT * FROM gateway_account WHERE `username` = #{username}")
    CustomGatewayAccount selectByUsername(@Param("username") String username);

    @Delete("DELETE FROM gateway_account WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

}