package com.mulong.common.dao.mall;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.mapper.AccountFinancialMapper;
import com.mulong.common.dao.mall.mapper.AccountRechargeMapper;
import com.mulong.common.dao.mall.mapper.AccountTransactionMapper;
import com.mulong.common.domain.pojo.mall.AccountFinancial;
import com.mulong.common.domain.pojo.mall.AccountRecharge;
import com.mulong.common.domain.pojo.mall.AccountTransaction;
import com.mulong.common.domain.pojo.mall.custom.AccountFinancialQuery;
import com.mulong.common.domain.pojo.mall.custom.AccountRechargeQuery;
import com.mulong.common.domain.pojo.mall.custom.AccountTransactionQuery;
import com.mulong.common.domain.pojo.mall.custom.CustomAccountFinancial;
import com.mulong.common.enums.AccountRechargeStatus;

/**
 * FinanceDao
 * 
 * @author mulong
 * @data 2021-05-30 16:36:52
 */
@Component
public class FinanceDao {
    @Autowired
    private AccountFinancialMapper accountFinancialMapper;
    @Autowired
    private AccountTransactionMapper accountTransactionMapper;
    @Autowired
    private AccountRechargeMapper accountRechargeMapper;

    public AccountFinancial addAccountFinancial(AccountFinancial record) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setUpdateTime(now);
        accountFinancialMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<CustomAccountFinancial> queryCustomAccountFinancial(AccountFinancialQuery query) {
        return accountFinancialMapper.selectCustomByParam(query);
    }

    public AccountFinancial queryAccountFinancialById(Integer id) {
        return accountFinancialMapper.selectById(id);
    }

    public void putAccountFinancial(AccountFinancial record) {
        record.setUpdateTime(new Date());
        accountFinancialMapper.updateByPrimaryKey(record);
    }

    public void deleteAccountFinancial(AccountFinancial record) {
        accountFinancialMapper.deleteById(record.getId());
    }

    public AccountTransaction addTransaction(AccountTransaction record) {
        Date now = new Date();
        record.setCreateTime(now);
        accountTransactionMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<AccountTransaction> queryAccountTransaction(AccountTransactionQuery query) {
        return accountTransactionMapper.selectByParam(query);
    }

    public List<AccountRecharge> queryAccountRecharge(AccountRechargeQuery query) {
        return accountRechargeMapper.selectByParam(query);
    }

    public boolean increaseAccountBalance(Integer gatewayAccountId, BigDecimal change) {
        int count = accountFinancialMapper.increaseBalanceByGatewayAccountId(gatewayAccountId, change);
        if (count == 0) {
            return false;
        }
        return true;
    }

    public boolean decreaseAccountBalance(Integer gatewayAccountId, BigDecimal change) {
        int count = accountFinancialMapper.decreaseBalanceByGatewayAccountId(gatewayAccountId, change);
        if (count == 0) {
            return false;
        }
        return true;
    }

    public boolean rechargeComplete(AccountRecharge recharge) {
        if (!AccountRechargeStatus.WAITING.getCode().equals(recharge.getStatus())) {
            return false;
        }
        int count = accountRechargeMapper.completeById(recharge.getId());
        if (count == 0) {
            return false;
        }
        return true;
    }

}
