package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mulong.common.dao.mall.mapper.InventoryMapper;
import com.mulong.common.dao.mall.mapper.SupplierChannelMapper;
import com.mulong.common.domain.pojo.mall.Inventory;
import com.mulong.common.domain.pojo.mall.custom.InventoryQuery;
import com.mulong.common.domain.pojo.mall.custom.DownloadInventory;
import com.mulong.common.domain.pojo.mall.custom.DownloadInventoryQuery;

/**
 * InventoryDao
 * 
 * @author mulong
 * @data 2021-07-09 13:51:23
 */
@Component
public class InventoryDao {
    @Autowired
    private InventoryMapper inventoryMapper;
    @Autowired
    private SupplierChannelMapper supplierChannelMapper;

    @Transactional(transactionManager = "mallTransactionManager", rollbackFor = {Exception.class, Error.class})
    public void addAll(Integer supplierChannelId, List<Inventory> records, int batchSize, String addBy) {
        inventoryMapper.deleteBySupplierChannelId(supplierChannelId);
        List<Inventory> batchList = new ArrayList<>(batchSize);
        Date now = new Date();
        for (Inventory record : records) {
            record.setId(null);
            record.setCreateTime(now);
            record.setCreateBy(addBy);
            batchList.add(record);
            if (batchList.size() >= batchSize) {
                inventoryMapper.insertList(batchList);
                batchList.clear();
            }
        }
        if (!CollectionUtils.isEmpty(batchList)) {
            inventoryMapper.insertList(batchList);
        }
        supplierChannelMapper.updateInventoryUpdateTimeById(supplierChannelId, now);
    }

    @Transactional(transactionManager = "mallTransactionManager", rollbackFor = {Exception.class, Error.class})
    public void addBatch(List<Inventory> records, int batchSize, String addBy) {
        List<Inventory> batchList = new ArrayList<>(batchSize);
        Date now = new Date();
        for (Inventory record : records) {
            record.setId(null);
            record.setCreateTime(now);
            record.setCreateBy(addBy);
            batchList.add(record);
            if (batchList.size() >= batchSize) {
                inventoryMapper.insertList(batchList);
                batchList.clear();
            }
        }
        if (!CollectionUtils.isEmpty(batchList)) {
            inventoryMapper.insertList(batchList);
        }
    }

    public Inventory add(Inventory record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        inventoryMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<Inventory> query(InventoryQuery query) {
        return inventoryMapper.selectByParam(query);
    }

    public List<DownloadInventory> queryDownloadInventory(DownloadInventoryQuery query) {
        return inventoryMapper.selectDownloadInventoryByParam(query);
    }

    public Inventory queryById(Long id) {
        return inventoryMapper.selectById(id);
    }

    public void put(Inventory record, String putBy) {
        inventoryMapper.updateByPrimaryKey(record);
    }

    public void delete(Inventory record, String deleteBy) {
        inventoryMapper.deleteById(record.getId());
    }

}
