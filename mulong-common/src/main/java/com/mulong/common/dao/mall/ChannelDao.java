package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mulong.common.dao.mall.mapper.SupplierChannelDeliveryChargeMapper;
import com.mulong.common.dao.mall.mapper.SupplierChannelDiscountMapper;
import com.mulong.common.dao.mall.mapper.SupplierChannelMapper;
import com.mulong.common.dao.mall.mapper.SupplierChannelSkuDiscountMapper;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelDiscountQuery;
import com.mulong.common.domain.pojo.mall.SupplierChannel;
import com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge;
import com.mulong.common.domain.pojo.mall.SupplierChannelDiscount;
import com.mulong.common.domain.pojo.mall.SupplierChannelSkuDiscount;
import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDeliveryCharge;
import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDiscount;
import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelSkuDiscount;
import com.mulong.common.domain.pojo.mall.custom.InventoryChannelQuery;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelDeliveryChargeQuery;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelQuery;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelSkuDiscountQuery;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;

/**
 * ChannelDao
 * 
 * @author mulong
 * @data 2021-05-31 15:20:02
 */
@Component
public class ChannelDao {
    @Autowired
    private SupplierChannelMapper supplierChannelMapper;
    @Autowired
    private SupplierChannelDiscountMapper supplierChannelDiscountMapper;
    @Autowired
    private SupplierChannelSkuDiscountMapper supplierChannelSkuDiscountMapper;
    @Autowired
    private SupplierChannelDeliveryChargeMapper supplierChannelDeliveryChargeMapper;

    public SupplierChannel add(SupplierChannel record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        supplierChannelMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<SupplierChannel> query(SupplierChannelQuery query) {
        return supplierChannelMapper.selectByParam(query);
    }

    public List<SupplierChannel> queryInventoryChannel(InventoryChannelQuery query) {
        return supplierChannelMapper.selectInventoryChannelByParam(query);
    }

    public SupplierChannel queryById(Integer id) {
        return supplierChannelMapper.selectById(id);
    }

    public void put(SupplierChannel record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        supplierChannelMapper.updateByPrimaryKey(record);
    }

    public void delete(SupplierChannel record, String deleteBy) {
        supplierChannelMapper.deleteById(record.getId());
    }

    public SupplierChannelDiscount addDiscount(SupplierChannelDiscount record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        supplierChannelDiscountMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<CustomSupplierChannelDiscount> queryDiscount(SupplierChannelDiscountQuery query) {
        return supplierChannelDiscountMapper.selectByParam(query);
    }

    public SupplierChannelDiscount queryDiscountById(Long id) {
        return supplierChannelDiscountMapper.selectById(id);
    }

    public void putDiscount(SupplierChannelDiscount record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        supplierChannelDiscountMapper.updateByPrimaryKey(record);
    }

    public void deleteDiscount(SupplierChannelDiscount record, String deleteBy) {
        supplierChannelDiscountMapper.deleteById(record.getId());
    }

    @Transactional(transactionManager = "mallTransactionManager", rollbackFor = {Exception.class, Error.class})
    public void addSkuDiscountBatch(List<SupplierChannelSkuDiscount> records, int batchSize, String addBy) {
        List<SupplierChannelSkuDiscount> batchList = new ArrayList<>(batchSize);
        Date now = new Date();
        for (SupplierChannelSkuDiscount record : records) {
            record.setId(null);
            record.setCreateTime(now);
            record.setCreateBy(addBy);
            record.setUpdateTime(now);
            record.setUpdateBy(addBy);
            batchList.add(record);
            if (batchList.size() >= batchSize) {
                supplierChannelSkuDiscountMapper.insertList(batchList);
                batchList.clear();
            }
        }
        if (!CollectionUtils.isEmpty(batchList)) {
            supplierChannelSkuDiscountMapper.insertList(batchList);
        }
    }

    public SupplierChannelSkuDiscount addSkuDiscount(SupplierChannelSkuDiscount record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        supplierChannelSkuDiscountMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<CustomSupplierChannelSkuDiscount> querySkuDiscount(SupplierChannelSkuDiscountQuery query) {
        return supplierChannelSkuDiscountMapper.selectByParam(query);
    }

    public List<SupplierChannelSkuDiscount> querySkuDiscountDuplicate(List<SupplierChannelSkuDiscount> records, Integer limit) {
        return supplierChannelSkuDiscountMapper.selectDuplicateKey(records, limit);
    }

    public SupplierChannelSkuDiscount querySkuDiscountById(Long id) {
        return supplierChannelSkuDiscountMapper.selectById(id);
    }

    public void putSkuDiscount(SupplierChannelSkuDiscount record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        supplierChannelSkuDiscountMapper.updateByPrimaryKey(record);
    }

    public void deleteSkuDiscount(SupplierChannelSkuDiscount record, String deleteBy) {
        supplierChannelSkuDiscountMapper.deleteById(record.getId());
    }

    public SupplierChannelDeliveryCharge addDeliveryCharge(SupplierChannelDeliveryCharge record, String addBy) {
        if (record.getChannelId() < 0) {
            throw new MulongException(ErrorEnums.UNKNOWN_ERROR);
        }
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        supplierChannelDeliveryChargeMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<CustomSupplierChannelDeliveryCharge> queryDeliveryCharge(SupplierChannelDeliveryChargeQuery query) {
        return supplierChannelDeliveryChargeMapper.selectChannelByParam(query);
    }

    public SupplierChannelDeliveryCharge queryDeliveryChargeById(Long id) {
        return supplierChannelDeliveryChargeMapper.selectChannelById(id);
    }

    public void putDeliveryCharge(SupplierChannelDeliveryCharge record, String putBy) {
        if (record.getChannelId() < 0) {
            throw new MulongException(ErrorEnums.UNKNOWN_ERROR);
        }
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        supplierChannelDeliveryChargeMapper.updateByPrimaryKey(record);
    }

    public void deleteDeliveryCharge(SupplierChannelDeliveryCharge record, String deleteBy) {
        supplierChannelDeliveryChargeMapper.deleteChannelById(record.getId());
    }

}
