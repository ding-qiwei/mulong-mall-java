package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.mapper.BrandMapper;
import com.mulong.common.domain.pojo.mall.Brand;
import com.mulong.common.domain.pojo.mall.custom.BrandQuery;

/**
 * BrandDao
 * 
 * @author mulong
 * @data 2021-06-21 14:33:53
 */
@Component
public class BrandDao {
    @Autowired
    private BrandMapper brandMapper;

    public Brand add(Brand record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        brandMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<Brand> query(BrandQuery query) {
        return brandMapper.selectByParam(query);
    }

    public Brand queryById(Integer id) {
        return brandMapper.selectById(id);
    }

    public Brand queryByName(String name) {
        return brandMapper.selectByName(name);
    }

    public Brand queryByShortName(String shortName) {
        return brandMapper.selectByShortName(shortName);
    }

    public void put(Brand record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        brandMapper.updateByPrimaryKey(record);
    }

    public void delete(Brand record, String deleteBy) {
        brandMapper.deleteById(record.getId());
    }

}
