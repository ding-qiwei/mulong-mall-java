package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.GoodsDemand;
import com.mulong.common.domain.pojo.mall.custom.GoodsDemandQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface GoodsDemandMapper extends Mapper<GoodsDemand>, MySqlMapper<GoodsDemand> {

    List<GoodsDemand> selectByParam(GoodsDemandQuery query);

    @Select("SELECT * FROM goods_demand WHERE `id` = #{id}")
    GoodsDemand selectById(@Param("id") Long id);

    @Delete("DELETE FROM goods_demand WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

}