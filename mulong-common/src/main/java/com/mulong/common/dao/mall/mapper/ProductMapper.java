package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.Product;
import com.mulong.common.domain.pojo.mall.custom.ProductQuery;
import com.mulong.common.domain.pojo.mall.custom.CustomProduct;
import com.mulong.common.domain.pojo.mall.custom.ProductInventoryProduct;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface ProductMapper extends Mapper<Product>, MySqlMapper<Product> {

    List<Product> selectByParam(ProductQuery query);

    List<CustomProduct> selectCustomByParam(ProductQuery query);

    List<Product> selectDuplicateKey(List<Product> records, @Param("limit") Integer limit);

    List<ProductInventoryProduct> selectProductInventoryProductBySkuPrefix(@Param("skuPrefix") String skuPrefix);

    @Select("SELECT * FROM product WHERE `id` = #{id}")
    Product selectById(@Param("id") Long id);

    @Select("SELECT * FROM product WHERE `sku` = #{sku}")
    Product selectBySku(@Param("sku") String sku);

    @Delete("DELETE FROM product WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

}