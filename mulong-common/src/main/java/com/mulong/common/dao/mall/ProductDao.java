package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mulong.common.dao.mall.mapper.InventoryMapper;
import com.mulong.common.dao.mall.mapper.ProductBarcodeMapper;
import com.mulong.common.dao.mall.mapper.ProductMapper;
import com.mulong.common.domain.pojo.mall.Product;
import com.mulong.common.domain.pojo.mall.ProductBarcode;
import com.mulong.common.domain.pojo.mall.custom.CustomProduct;
import com.mulong.common.domain.pojo.mall.custom.ProductBarcodeQuery;
import com.mulong.common.domain.pojo.mall.custom.ProductInventoryInventory;
import com.mulong.common.domain.pojo.mall.custom.ProductInventoryProduct;
import com.mulong.common.domain.pojo.mall.custom.ProductQuery;

/**
 * ProductDao
 * 
 * @author mulong
 * @data 2021-06-25 21:35:38
 */
@Component
public class ProductDao {
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductBarcodeMapper productBarcodeMapper;
    @Autowired
    private InventoryMapper inventoryMapper;

    @Transactional(transactionManager = "mallTransactionManager", rollbackFor = {Exception.class, Error.class})
    public void addBatch(List<Product> records, int batchSize, String addBy) {
        List<Product> batchList = new ArrayList<>(batchSize);
        Date now = new Date();
        for (Product record : records) {
            record.setId(null);
            record.setCreateTime(now);
            record.setCreateBy(addBy);
            record.setUpdateTime(now);
            record.setUpdateBy(addBy);
            batchList.add(record);
            if (batchList.size() >= batchSize) {
                productMapper.insertList(batchList);
                batchList.clear();
            }
        }
        if (!CollectionUtils.isEmpty(batchList)) {
            productMapper.insertList(batchList);
        }
    }

    public Product add(Product record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        productMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<Product> query(ProductQuery query) {
        return productMapper.selectByParam(query);
    }

    public List<CustomProduct> queryCustom(ProductQuery query) {
        return productMapper.selectCustomByParam(query);
    }

    public List<Product> queryDuplicate(List<Product> records, Integer limit) {
        return productMapper.selectDuplicateKey(records, limit);
    }

    public List<ProductInventoryProduct> queryProductInventoryProductBySkuPrefix(String skuPrefix) {
        List<ProductInventoryProduct> records = productMapper.selectProductInventoryProductBySkuPrefix(skuPrefix);
        if (CollectionUtils.isEmpty(records)) {
            return null;
        }
        records.forEach(item -> {
            String sku = item.getSku();
            List<ProductInventoryInventory> inventoryList = inventoryMapper.selectProductInventoryInventoryBySku(sku);
            item.setInventoryList(inventoryList);
        });
        return records;
    }

    public Product queryById(Long id) {
        return productMapper.selectById(id);
    }

    public Product queryBySku(String sku) {
        return productMapper.selectBySku(sku);
    }

    public void put(Product record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        productMapper.updateByPrimaryKey(record);
    }

    public void delete(Product record, String deleteBy) {
        productMapper.deleteById(record.getId());
    }

    
    
    
    
    
    @Transactional(transactionManager = "mallTransactionManager", rollbackFor = {Exception.class, Error.class})
    public void addBarcodeBatch(List<ProductBarcode> records, int batchSize, String addBy) {
        List<ProductBarcode> batchList = new ArrayList<>(batchSize);
        Date now = new Date();
        for (ProductBarcode record : records) {
            record.setId(null);
            record.setCreateTime(now);
            record.setCreateBy(addBy);
            record.setUpdateTime(now);
            record.setUpdateBy(addBy);
            batchList.add(record);
            if (batchList.size() >= batchSize) {
                productBarcodeMapper.insertList(batchList);
                batchList.clear();
            }
        }
        if (!CollectionUtils.isEmpty(batchList)) {
            productBarcodeMapper.insertList(batchList);
        }
    }

    public ProductBarcode addBarcode(ProductBarcode record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        productBarcodeMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<ProductBarcode> queryBarcode(ProductBarcodeQuery query) {
        return productBarcodeMapper.selectByParam(query);
    }

    public List<ProductBarcode> queryBarcodeDuplicate(List<ProductBarcode> records, Integer limit) {
        return productBarcodeMapper.selectDuplicateKey(records, limit);
    }

    public ProductBarcode queryBarcodeById(Long id) {
        return productBarcodeMapper.selectById(id);
    }

    public void putBarcode(ProductBarcode record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        productBarcodeMapper.updateByPrimaryKey(record);
    }

    public void deleteBarcode(ProductBarcode record, String deleteBy) {
        productBarcodeMapper.deleteById(record.getId());
    }

}
