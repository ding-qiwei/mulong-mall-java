package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.Inventory;
import com.mulong.common.domain.pojo.mall.custom.InventoryQuery;
import com.mulong.common.domain.pojo.mall.custom.ProductInventoryInventory;
import com.mulong.common.domain.pojo.mall.custom.DownloadInventory;
import com.mulong.common.domain.pojo.mall.custom.DownloadInventoryQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface InventoryMapper extends Mapper<Inventory>, MySqlMapper<Inventory> {

    List<Inventory> selectByParam(InventoryQuery query);

    List<DownloadInventory> selectDownloadInventoryByParam(DownloadInventoryQuery query);

    List<ProductInventoryInventory> selectProductInventoryInventoryBySku(@Param("sku") String sku);

    @Select("SELECT * FROM inventory WHERE `id` = #{id}")
    Inventory selectById(@Param("id") Long id);

    @Delete("DELETE FROM inventory WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

    @Delete("DELETE FROM inventory WHERE `supplier_channel_id` = #{supplierChannelId}")
    void deleteBySupplierChannelId(@Param("supplierChannelId") Integer supplierChannelId);

}