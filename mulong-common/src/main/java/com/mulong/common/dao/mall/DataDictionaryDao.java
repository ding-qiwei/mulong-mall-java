package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.mapper.DataDictionaryMapper;
import com.mulong.common.domain.pojo.mall.custom.CustomDataDictionary;
import com.mulong.common.domain.pojo.mall.custom.DataDictionaryQuery;

/**
 * DataDictionaryDao
 * 
 * @author mulong
 * @data 2021-03-30 20:50:06
 */
@Component
public class DataDictionaryDao {
    @Autowired
    private DataDictionaryMapper dataDictionaryMapper;

    public CustomDataDictionary add(CustomDataDictionary record, String insertBy) {
        record.setId(null);
        record.setCreateTime(new Date());
        record.setCreateBy(insertBy);
        record.setUpdateTime(new Date());
        record.setUpdateBy(insertBy);
        dataDictionaryMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public CustomDataDictionary queryById(Integer id) {
        return dataDictionaryMapper.selectById(id);
    }

    public List<CustomDataDictionary> queryAll() {
        return dataDictionaryMapper.selectCustomAll();
    }

    public List<CustomDataDictionary> query(DataDictionaryQuery query) {
        return dataDictionaryMapper.selectByParam(query);
    }

    public List<CustomDataDictionary> queryByCodes(List<String> codes) {
        return dataDictionaryMapper.selectByCodes(codes);
    }

    public void put(CustomDataDictionary record, String putBy) {
        record.setUpdateTime(new Date());
        record.setUpdateBy(putBy);
        dataDictionaryMapper.updateByPrimaryKey(record);
    }

    public void delete(CustomDataDictionary record, String deleteBy) {
        dataDictionaryMapper.deleteById(record.getId());
    }

}
