package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge;
import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDeliveryCharge;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelDeliveryChargeQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface SupplierChannelDeliveryChargeMapper extends Mapper<SupplierChannelDeliveryCharge>, MySqlMapper<SupplierChannelDeliveryCharge> {

    void insertOnDuplicateKeyUpdate(SupplierChannelDeliveryCharge record);

    void insertListOnDuplicateKeyUpdate(List<SupplierChannelDeliveryCharge> records);

    List<CustomSupplierChannelDeliveryCharge> selectByParam(SupplierChannelDeliveryChargeQuery query);

    List<CustomSupplierChannelDeliveryCharge> selectChannelByParam(SupplierChannelDeliveryChargeQuery query);

    List<CustomSupplierChannelDeliveryCharge> selectDefaultByParam(SupplierChannelDeliveryChargeQuery query);

    @Select("SELECT * FROM supplier_channel_delivery_charge WHERE `id` = #{id}")
    SupplierChannelDeliveryCharge selectById(@Param("id") Long id);

    @Select("SELECT * FROM supplier_channel_delivery_charge WHERE `id` = #{id} AND `channel_id` >= 0")
    SupplierChannelDeliveryCharge selectChannelById(@Param("id") Long id);

    @Select("SELECT * FROM supplier_channel_delivery_charge WHERE `id` = #{id} AND `channel_id` < 0")
    SupplierChannelDeliveryCharge selectDefaultById(@Param("id") Long id);

    @Delete("DELETE FROM supplier_channel_delivery_charge WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

    @Delete("DELETE FROM supplier_channel_delivery_charge WHERE `id` = #{id} AND `channel_id` >= 0")
    void deleteChannelById(@Param("id") Long id);

    @Delete("DELETE FROM supplier_channel_delivery_charge WHERE `id` = #{id} AND `channel_id` < 0")
    void deleteDefaultById(@Param("id") Long id);

}