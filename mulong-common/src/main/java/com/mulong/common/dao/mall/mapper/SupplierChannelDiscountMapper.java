package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.SupplierChannelDiscount;
import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDiscount;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelDiscountQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface SupplierChannelDiscountMapper extends Mapper<SupplierChannelDiscount>, MySqlMapper<SupplierChannelDiscount> {

    List<CustomSupplierChannelDiscount> selectByParam(SupplierChannelDiscountQuery query);

    @Select("SELECT * FROM supplier_channel_discount WHERE `id` = #{id}")
    SupplierChannelDiscount selectById(@Param("id") Long id);

    @Delete("DELETE FROM supplier_channel_discount WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

}