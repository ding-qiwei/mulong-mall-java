package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.Brand;
import com.mulong.common.domain.pojo.mall.custom.BrandQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface BrandMapper extends Mapper<Brand>, MySqlMapper<Brand> {

    List<Brand> selectByParam(BrandQuery query);

    @Select("SELECT * FROM brand WHERE `id` = #{id}")
    Brand selectById(@Param("id") Integer id);

    @Select("SELECT * FROM brand WHERE `name` = #{name}")
    Brand selectByName(@Param("name") String name);

    @Select("SELECT * FROM brand WHERE `short_name` = #{shortName}")
    Brand selectByShortName(@Param("shortName") String shortName);

    @Delete("DELETE FROM brand WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

}