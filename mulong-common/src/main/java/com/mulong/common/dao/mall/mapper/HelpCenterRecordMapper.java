package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.HelpCenterRecord;
import com.mulong.common.domain.pojo.mall.custom.HelpCenterRecordQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface HelpCenterRecordMapper extends Mapper<HelpCenterRecord>, MySqlMapper<HelpCenterRecord> {

    List<HelpCenterRecord> selectByParam(HelpCenterRecordQuery query);

    @Select("SELECT * FROM help_center_record WHERE `id` = #{id}")
    HelpCenterRecord selectById(@Param("id") Integer id);

    @Delete("DELETE FROM help_center_record WHERE `id` = #{id}")
    void deleteById(@Param("id") Integer id);

}