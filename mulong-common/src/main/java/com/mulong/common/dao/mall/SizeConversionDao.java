package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.mulong.common.dao.mall.mapper.SizeConversionMapper;
import com.mulong.common.domain.pojo.mall.SizeConversion;
import com.mulong.common.domain.pojo.mall.custom.CustomSizeConversion;
import com.mulong.common.domain.pojo.mall.custom.SizeConversionQuery;

/**
 * SizeConversionDao
 * 
 * @author mulong
 * @data 2021-06-25 21:35:20
 */
@Component
public class SizeConversionDao {
    @Autowired
    private SizeConversionMapper sizeConversionMapper;

    @Transactional(transactionManager = "mallTransactionManager", rollbackFor = {Exception.class, Error.class})
    public void addBatch(List<SizeConversion> records, int batchSize, String addBy) {
        List<SizeConversion> batchList = new ArrayList<>(batchSize);
        Date now = new Date();
        for (SizeConversion record : records) {
            record.setId(null);
            record.setCreateTime(now);
            record.setCreateBy(addBy);
            record.setUpdateTime(now);
            record.setUpdateBy(addBy);
            batchList.add(record);
            if (batchList.size() >= batchSize) {
                sizeConversionMapper.insertList(batchList);
                batchList.clear();
            }
        }
        if (!CollectionUtils.isEmpty(batchList)) {
            sizeConversionMapper.insertList(batchList);
        }
    }

    public SizeConversion add(SizeConversion record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        sizeConversionMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<SizeConversion> queryAll() {
        return sizeConversionMapper.selectAll();
    }

    public List<SizeConversion> query(SizeConversionQuery query) {
        return sizeConversionMapper.selectByParam(query);
    }

    public List<CustomSizeConversion> queryCustom(SizeConversionQuery query) {
        return sizeConversionMapper.selectCustomByParam(query);
    }

    public List<SizeConversion> queryDuplicate(List<SizeConversion> records, Integer limit) {
        return sizeConversionMapper.selectDuplicateKey(records, limit);
    }

    public SizeConversion queryById(Integer id) {
        return sizeConversionMapper.selectById(id);
    }

    public void put(SizeConversion record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        sizeConversionMapper.updateByPrimaryKey(record);
    }

    public void delete(SizeConversion record, String deleteBy) {
        sizeConversionMapper.deleteById(record.getId());
    }

}
