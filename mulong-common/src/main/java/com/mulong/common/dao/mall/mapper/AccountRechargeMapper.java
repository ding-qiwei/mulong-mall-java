package com.mulong.common.dao.mall.mapper;

import java.util.*;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.mulong.common.domain.pojo.mall.AccountRecharge;
import com.mulong.common.domain.pojo.mall.custom.AccountRechargeQuery;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface AccountRechargeMapper extends Mapper<AccountRecharge>, MySqlMapper<AccountRecharge> {

    List<AccountRecharge> selectByParam(AccountRechargeQuery query);

    @Select("SELECT * FROM account_recharge WHERE `id` = #{id}")
    AccountRecharge selectById(@Param("id") Long id);

    @Select("SELECT * FROM account_recharge WHERE `apply_gateway_account_id` = #{applyGatewayAccountId}")
    AccountRecharge selectByApplyGatewayAccountId(@Param("applyGatewayAccountId") Integer applyGatewayAccountId);

    @Update("UPDATE account_recharge SET `status` = 1 WHERE `id` = #{id} AND `status` = 0")
    int completeById(@Param("id") Long id);

    @Delete("DELETE FROM account_recharge WHERE `id` = #{id}")
    void deleteById(@Param("id") Long id);

    @Delete("DELETE FROM account_recharge WHERE `apply_gateway_account_id` = #{applyGatewayAccountId}")
    void deleteByApplyGatewayAccountId(@Param("applyGatewayAccountId") Integer applyGatewayAccountId);

}