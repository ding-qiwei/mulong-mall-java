package com.mulong.common.dao.mall;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.mapper.HelpCenterRecordMapper;
import com.mulong.common.domain.pojo.mall.HelpCenterRecord;
import com.mulong.common.domain.pojo.mall.custom.HelpCenterRecordQuery;
import com.mulong.common.enums.HelpCenterRecordCategory;
import com.mulong.common.enums.HelpCenterRecordStatus;

/**
 * HelpCenterDao
 * 
 * @author mulong
 * @data 2021-05-30 09:19:37
 */
@Component
public class HelpCenterDao {
    @Autowired
    private HelpCenterRecordMapper helpCenterRecordMapper;

    public HelpCenterRecord addHelp(HelpCenterRecord record, String addBy) {
        Date now = new Date();
        record.setId(null);
        record.setCreateTime(now);
        record.setCreateBy(addBy);
        record.setUpdateTime(now);
        record.setUpdateBy(addBy);
        helpCenterRecordMapper.insertUseGeneratedKeys(record);
        return record;
    }

    public List<HelpCenterRecord> queryHelp(HelpCenterRecordQuery query) {
        return helpCenterRecordMapper.selectByParam(query);
    }

    public List<HelpCenterRecord> queryPublishedSystemHelp(String titleKeyword) {
        HelpCenterRecordQuery query = new HelpCenterRecordQuery();
        query.setCategory(HelpCenterRecordCategory.SYSTEM.getCode());
        query.setStatus(HelpCenterRecordStatus.PUBLISHED.getCode());
        query.setTitleKeyword(titleKeyword);
        query.setOrderBy("`published_time` DESC");
        return helpCenterRecordMapper.selectByParam(query);
    }

    public List<HelpCenterRecord> queryPublishedSalesHelp(String titleKeyword) {
        HelpCenterRecordQuery query = new HelpCenterRecordQuery();
        query.setCategory(HelpCenterRecordCategory.SALES.getCode());
        query.setStatus(HelpCenterRecordStatus.PUBLISHED.getCode());
        query.setTitleKeyword(titleKeyword);
        query.setOrderBy("`published_time` DESC");
        return helpCenterRecordMapper.selectByParam(query);
    }

    public List<HelpCenterRecord> queryPublishedPlatformStrategy(String titleKeyword) {
        HelpCenterRecordQuery query = new HelpCenterRecordQuery();
        query.setCategory(HelpCenterRecordCategory.PLATFORM_STRATEGY.getCode());
        query.setStatus(HelpCenterRecordStatus.PUBLISHED.getCode());
        query.setTitleKeyword(titleKeyword);
        query.setOrderBy("`published_time` DESC");
        return helpCenterRecordMapper.selectByParam(query);
    }

    public List<HelpCenterRecord> queryPublishedOthersHelp(String titleKeyword) {
        HelpCenterRecordQuery query = new HelpCenterRecordQuery();
        query.setCategory(HelpCenterRecordCategory.OTHERS.getCode());
        query.setStatus(HelpCenterRecordStatus.PUBLISHED.getCode());
        query.setTitleKeyword(titleKeyword);
        query.setOrderBy("`published_time` DESC");
        return helpCenterRecordMapper.selectByParam(query);
    }

    public HelpCenterRecord queryHelpById(Integer id) {
        return helpCenterRecordMapper.selectById(id);
    }

    public void putHelp(HelpCenterRecord record, String putBy) {
        Date now = new Date();
        record.setUpdateTime(now);
        record.setUpdateBy(putBy);
        helpCenterRecordMapper.updateByPrimaryKey(record);
    }

    public void deleteHelp(HelpCenterRecord record, String deleteBy) {
        helpCenterRecordMapper.deleteById(record.getId());
    }

}
