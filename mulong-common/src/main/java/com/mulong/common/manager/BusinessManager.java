package com.mulong.common.manager;

import java.math.BigDecimal;
import java.util.List;

import com.mulong.common.enums.Province;

/**
 * BusinessManager
 * 
 * @author mulong
 * @data 2021-10-12 17:06:41
 */
public interface BusinessManager {

    void addSupplierChannelDeliveryCharge(
            Integer deliveryTypeId, Province province, BigDecimal firstPrice, BigDecimal addPrice, String addBy);

    void addSupplierChannelDeliveryCharge(
            Integer channelId, Integer deliveryTypeId, Province province, BigDecimal firstPrice, BigDecimal addPrice, String addBy);

    void addSupplierChannelDeliveryCharge(
            Integer deliveryTypeId, List<Province> provinceList, BigDecimal firstPrice, BigDecimal addPrice, String addBy);

    void addSupplierChannelDeliveryCharge(
            Integer channelId, Integer deliveryTypeId, List<Province> provinceList, BigDecimal firstPrice, BigDecimal addPrice, String addBy);

}
