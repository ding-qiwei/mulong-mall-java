package com.mulong.common.manager.impl;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.mulong.common.dao.mall.DeliveryDao;
import com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge;
import com.mulong.common.enums.Province;
import com.mulong.common.manager.BusinessManager;

/**
 * BusinessManagerImpl
 * 
 * @author mulong
 * @data 2021-10-12 17:06:49
 */
@Component
public class BusinessManagerImpl implements BusinessManager {
    @Autowired
    private DeliveryDao deliveryDao;

    @Override
    public void addSupplierChannelDeliveryCharge(
            Integer deliveryTypeId, Province province, BigDecimal firstPrice, BigDecimal addPrice, String addBy) {
        addSupplierChannelDeliveryCharge(-1, deliveryTypeId, province, firstPrice, addPrice, addBy);
    }

    @Override
    public void addSupplierChannelDeliveryCharge(
            Integer channelId, Integer deliveryTypeId, Province province, BigDecimal firstPrice, BigDecimal addPrice, String addBy) {
        SupplierChannelDeliveryCharge record = new SupplierChannelDeliveryCharge();
        record.setChannelId(channelId);
        record.setDeliveryTypeId(deliveryTypeId);
        record.setProvince(province.getCode());
        record.setFirstPrice(firstPrice);
        record.setAddPrice(addPrice);
        deliveryDao.addOrUpdateSupplierChannelDeliveryCharge(record, addBy);
    }

    @Override
    public void addSupplierChannelDeliveryCharge(
            Integer deliveryTypeId, List<Province> provinceList, BigDecimal firstPrice, BigDecimal addPrice, String addBy) {
        addSupplierChannelDeliveryCharge(-1, deliveryTypeId, provinceList, firstPrice, addPrice, addBy);
    }

    @Override
    public void addSupplierChannelDeliveryCharge(
            Integer channelId, Integer deliveryTypeId, List<Province> provinceList, BigDecimal firstPrice, BigDecimal addPrice, String addBy) {
        List<SupplierChannelDeliveryCharge> records = new ArrayList<>(provinceList.size());
        for (Province province : provinceList) {
            SupplierChannelDeliveryCharge record = new SupplierChannelDeliveryCharge();
            record.setChannelId(channelId);
            record.setDeliveryTypeId(deliveryTypeId);
            record.setProvince(province.getCode());
            record.setFirstPrice(firstPrice);
            record.setAddPrice(addPrice);
            records.add(record);
        }
        deliveryDao.addOrUpdateSupplierChannelDeliveryCharge(records, addBy);
    }

}
