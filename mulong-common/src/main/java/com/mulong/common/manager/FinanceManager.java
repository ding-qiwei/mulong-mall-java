package com.mulong.common.manager;

import java.util.*;

import com.mulong.common.domain.pojo.mall.AccountRecharge;
import com.mulong.common.domain.pojo.mall.CustomerOrderDetail;

/**
 * FinanceManager
 * 
 * @author mulong
 * @data 2021-09-23 21:30:10
 */
public interface FinanceManager {

    void rechargeComplete(AccountRecharge recharge);

    void payCustomerOrder(CustomerOrderDetail order);

    void payCustomerOrder(List<CustomerOrderDetail> orders);

}
