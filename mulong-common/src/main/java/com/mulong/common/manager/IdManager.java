package com.mulong.common.manager;

/**
 * IdManager
 * 
 * @author mulong
 * @data 2021-09-23 15:33:05
 */
public interface IdManager {

    /**
     * 下一个号
     *
     * @author mulong
     */
    String nextId();

    /**
     * 下一个用户侧订单号
     *
     * @author mulong
     */
    String nextCustomerOrderId();

    /**
     * 下一个交易流水号
     *
     * @author mulong
     */
    String nextAccountTransactionNo();
 
}
