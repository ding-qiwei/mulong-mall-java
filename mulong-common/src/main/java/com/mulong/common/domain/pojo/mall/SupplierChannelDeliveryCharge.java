package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`supplier_channel_delivery_charge`")
public class SupplierChannelDeliveryCharge implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`channel_id`")
    private Integer channelId;

    @Column(name = "`delivery_type_id`")
    private Integer deliveryTypeId;

    @Column(name = "`province`")
    private String province;

    @Column(name = "`first_price`")
    private BigDecimal firstPrice;

    @Column(name = "`add_price`")
    private BigDecimal addPrice;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    @Column(name = "`update_time`")
    private Date updateTime;

    @Column(name = "`update_by`")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return channel_id
     */
    public Integer getChannelId() {
        return channelId;
    }

    /**
     * @param channelId
     */
    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    /**
     * @return delivery_type_id
     */
    public Integer getDeliveryTypeId() {
        return deliveryTypeId;
    }

    /**
     * @param deliveryTypeId
     */
    public void setDeliveryTypeId(Integer deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }

    /**
     * @return province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * @return first_price
     */
    public BigDecimal getFirstPrice() {
        return firstPrice;
    }

    /**
     * @param firstPrice
     */
    public void setFirstPrice(BigDecimal firstPrice) {
        this.firstPrice = firstPrice;
    }

    /**
     * @return add_price
     */
    public BigDecimal getAddPrice() {
        return addPrice;
    }

    /**
     * @param addPrice
     */
    public void setAddPrice(BigDecimal addPrice) {
        this.addPrice = addPrice;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}