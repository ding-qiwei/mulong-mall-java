package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`account_transaction`")
public class AccountTransaction implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`gateway_account_id`")
    private Integer gatewayAccountId;

    /**
     * 交易流水号
     */
    @Column(name = "`transaction_no`")
    private String transactionNo;

    /**
     * 交易分类
     */
    @Column(name = "`transaction_category`")
    private Integer transactionCategory;

    /**
     * 交易金额
     */
    @Column(name = "`transaction_amount`")
    private BigDecimal transactionAmount;

    /**
     * 交易时间
     */
    @Column(name = "`transaction_time`")
    private Date transactionTime;

    /**
     * 备注
     */
    @Column(name = "`remark`")
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return gateway_account_id
     */
    public Integer getGatewayAccountId() {
        return gatewayAccountId;
    }

    /**
     * @param gatewayAccountId
     */
    public void setGatewayAccountId(Integer gatewayAccountId) {
        this.gatewayAccountId = gatewayAccountId;
    }

    /**
     * 获取交易流水号
     *
     * @return transaction_no - 交易流水号
     */
    public String getTransactionNo() {
        return transactionNo;
    }

    /**
     * 设置交易流水号
     *
     * @param transactionNo 交易流水号
     */
    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo == null ? null : transactionNo.trim();
    }

    /**
     * 获取交易分类
     *
     * @return transaction_category - 交易分类
     */
    public Integer getTransactionCategory() {
        return transactionCategory;
    }

    /**
     * 设置交易分类
     *
     * @param transactionCategory 交易分类
     */
    public void setTransactionCategory(Integer transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    /**
     * 获取交易金额
     *
     * @return transaction_amount - 交易金额
     */
    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * 设置交易金额
     *
     * @param transactionAmount 交易金额
     */
    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    /**
     * 获取交易时间
     *
     * @return transaction_time - 交易时间
     */
    public Date getTransactionTime() {
        return transactionTime;
    }

    /**
     * 设置交易时间
     *
     * @param transactionTime 交易时间
     */
    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}