package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`customer_order_detail`")
public class CustomerOrderDetail implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "`order_id`")
    private String orderId;

    @Column(name = "`gateway_account_id`")
    private Integer gatewayAccountId;

    /**
     * 自定义订单号
     */
    @Column(name = "`customized_order_id`")
    private String customizedOrderId;

    @Column(name = "`sku`")
    private String sku;

    /**
     * 供应商渠道id
     */
    @Column(name = "`supplier_channel_id`")
    private Integer supplierChannelId;

    /**
     * 尺码
     */
    @Column(name = "`normalized_size`")
    private String normalizedSize;

    @Column(name = "`market_price`")
    private BigDecimal marketPrice;

    /**
     * 折扣价
     */
    @Column(name = "`discount_price`")
    private BigDecimal discountPrice;

    /**
     * 运费
     */
    @Column(name = "`delivery_charge`")
    private BigDecimal deliveryCharge;

    /**
     * 付款金额
     */
    @Column(name = "`pay_price`")
    private BigDecimal payPrice;

    @Column(name = "`actual_delivery_charge`")
    private BigDecimal actualDeliveryCharge;

    @Column(name = "`qty`")
    private Integer qty;

    /**
     * 收件人
     */
    @Column(name = "`receiver_name`")
    private String receiverName;

    /**
     * 省
     */
    @Column(name = "`receiver_province`")
    private String receiverProvince;

    /**
     * 市
     */
    @Column(name = "`receiver_city`")
    private String receiverCity;

    /**
     * 区
     */
    @Column(name = "`receiver_district`")
    private String receiverDistrict;

    /**
     * 收件人地址
     */
    @Column(name = "`receiver_address`")
    private String receiverAddress;

    @Column(name = "`receiver_zipcode`")
    private String receiverZipcode;

    @Column(name = "`receiver_mobilephone`")
    private String receiverMobilephone;

    @Column(name = "`receiver_telephone`")
    private String receiverTelephone;

    /**
     * 下单快递
     */
    @Column(name = "`expect_delivery_type_id`")
    private Integer expectDeliveryTypeId;

    /**
     * 实发快递
     */
    @Column(name = "`actual_delivery_type_id`")
    private Integer actualDeliveryTypeId;

    /**
     * 快递单号
     */
    @Column(name = "`express_no`")
    private String expressNo;

    /**
     * 状态
     */
    @Column(name = "`status`")
    private Integer status;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    /**
     * 反馈时间
     */
    @Column(name = "`feedback_time`")
    private Date feedbackTime;

    /**
     * 预计发货日期
     */
    @Column(name = "`on_shipment_date`")
    private Date onShipmentDate;

    /**
     * 备注
     */
    @Column(name = "`remark`")
    private String remark;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return order_id - 主键
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置主键
     *
     * @param orderId 主键
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    /**
     * @return gateway_account_id
     */
    public Integer getGatewayAccountId() {
        return gatewayAccountId;
    }

    /**
     * @param gatewayAccountId
     */
    public void setGatewayAccountId(Integer gatewayAccountId) {
        this.gatewayAccountId = gatewayAccountId;
    }

    /**
     * 获取自定义订单号
     *
     * @return customized_order_id - 自定义订单号
     */
    public String getCustomizedOrderId() {
        return customizedOrderId;
    }

    /**
     * 设置自定义订单号
     *
     * @param customizedOrderId 自定义订单号
     */
    public void setCustomizedOrderId(String customizedOrderId) {
        this.customizedOrderId = customizedOrderId == null ? null : customizedOrderId.trim();
    }

    /**
     * @return sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku
     */
    public void setSku(String sku) {
        this.sku = sku == null ? null : sku.trim();
    }

    /**
     * 获取供应商渠道id
     *
     * @return supplier_channel_id - 供应商渠道id
     */
    public Integer getSupplierChannelId() {
        return supplierChannelId;
    }

    /**
     * 设置供应商渠道id
     *
     * @param supplierChannelId 供应商渠道id
     */
    public void setSupplierChannelId(Integer supplierChannelId) {
        this.supplierChannelId = supplierChannelId;
    }

    /**
     * 获取尺码
     *
     * @return normalized_size - 尺码
     */
    public String getNormalizedSize() {
        return normalizedSize;
    }

    /**
     * 设置尺码
     *
     * @param normalizedSize 尺码
     */
    public void setNormalizedSize(String normalizedSize) {
        this.normalizedSize = normalizedSize == null ? null : normalizedSize.trim();
    }

    /**
     * @return market_price
     */
    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    /**
     * @param marketPrice
     */
    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * 获取折扣价
     *
     * @return discount_price - 折扣价
     */
    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    /**
     * 设置折扣价
     *
     * @param discountPrice 折扣价
     */
    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    /**
     * 获取运费
     *
     * @return delivery_charge - 运费
     */
    public BigDecimal getDeliveryCharge() {
        return deliveryCharge;
    }

    /**
     * 设置运费
     *
     * @param deliveryCharge 运费
     */
    public void setDeliveryCharge(BigDecimal deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    /**
     * 获取付款金额
     *
     * @return pay_price - 付款金额
     */
    public BigDecimal getPayPrice() {
        return payPrice;
    }

    /**
     * 设置付款金额
     *
     * @param payPrice 付款金额
     */
    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    /**
     * @return actual_delivery_charge
     */
    public BigDecimal getActualDeliveryCharge() {
        return actualDeliveryCharge;
    }

    /**
     * @param actualDeliveryCharge
     */
    public void setActualDeliveryCharge(BigDecimal actualDeliveryCharge) {
        this.actualDeliveryCharge = actualDeliveryCharge;
    }

    /**
     * @return qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * @param qty
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    /**
     * 获取收件人
     *
     * @return receiver_name - 收件人
     */
    public String getReceiverName() {
        return receiverName;
    }

    /**
     * 设置收件人
     *
     * @param receiverName 收件人
     */
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName == null ? null : receiverName.trim();
    }

    /**
     * 获取省
     *
     * @return receiver_province - 省
     */
    public String getReceiverProvince() {
        return receiverProvince;
    }

    /**
     * 设置省
     *
     * @param receiverProvince 省
     */
    public void setReceiverProvince(String receiverProvince) {
        this.receiverProvince = receiverProvince == null ? null : receiverProvince.trim();
    }

    /**
     * 获取市
     *
     * @return receiver_city - 市
     */
    public String getReceiverCity() {
        return receiverCity;
    }

    /**
     * 设置市
     *
     * @param receiverCity 市
     */
    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity == null ? null : receiverCity.trim();
    }

    /**
     * 获取区
     *
     * @return receiver_district - 区
     */
    public String getReceiverDistrict() {
        return receiverDistrict;
    }

    /**
     * 设置区
     *
     * @param receiverDistrict 区
     */
    public void setReceiverDistrict(String receiverDistrict) {
        this.receiverDistrict = receiverDistrict == null ? null : receiverDistrict.trim();
    }

    /**
     * 获取收件人地址
     *
     * @return receiver_address - 收件人地址
     */
    public String getReceiverAddress() {
        return receiverAddress;
    }

    /**
     * 设置收件人地址
     *
     * @param receiverAddress 收件人地址
     */
    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress == null ? null : receiverAddress.trim();
    }

    /**
     * @return receiver_zipcode
     */
    public String getReceiverZipcode() {
        return receiverZipcode;
    }

    /**
     * @param receiverZipcode
     */
    public void setReceiverZipcode(String receiverZipcode) {
        this.receiverZipcode = receiverZipcode == null ? null : receiverZipcode.trim();
    }

    /**
     * @return receiver_mobilephone
     */
    public String getReceiverMobilephone() {
        return receiverMobilephone;
    }

    /**
     * @param receiverMobilephone
     */
    public void setReceiverMobilephone(String receiverMobilephone) {
        this.receiverMobilephone = receiverMobilephone == null ? null : receiverMobilephone.trim();
    }

    /**
     * @return receiver_telephone
     */
    public String getReceiverTelephone() {
        return receiverTelephone;
    }

    /**
     * @param receiverTelephone
     */
    public void setReceiverTelephone(String receiverTelephone) {
        this.receiverTelephone = receiverTelephone == null ? null : receiverTelephone.trim();
    }

    /**
     * 获取下单快递
     *
     * @return expect_delivery_type_id - 下单快递
     */
    public Integer getExpectDeliveryTypeId() {
        return expectDeliveryTypeId;
    }

    /**
     * 设置下单快递
     *
     * @param expectDeliveryTypeId 下单快递
     */
    public void setExpectDeliveryTypeId(Integer expectDeliveryTypeId) {
        this.expectDeliveryTypeId = expectDeliveryTypeId;
    }

    /**
     * 获取实发快递
     *
     * @return actual_delivery_type_id - 实发快递
     */
    public Integer getActualDeliveryTypeId() {
        return actualDeliveryTypeId;
    }

    /**
     * 设置实发快递
     *
     * @param actualDeliveryTypeId 实发快递
     */
    public void setActualDeliveryTypeId(Integer actualDeliveryTypeId) {
        this.actualDeliveryTypeId = actualDeliveryTypeId;
    }

    /**
     * 获取快递单号
     *
     * @return express_no - 快递单号
     */
    public String getExpressNo() {
        return expressNo;
    }

    /**
     * 设置快递单号
     *
     * @param expressNo 快递单号
     */
    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo == null ? null : expressNo.trim();
    }

    /**
     * 获取状态
     *
     * @return status - 状态
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取反馈时间
     *
     * @return feedback_time - 反馈时间
     */
    public Date getFeedbackTime() {
        return feedbackTime;
    }

    /**
     * 设置反馈时间
     *
     * @param feedbackTime 反馈时间
     */
    public void setFeedbackTime(Date feedbackTime) {
        this.feedbackTime = feedbackTime;
    }

    /**
     * 获取预计发货日期
     *
     * @return on_shipment_date - 预计发货日期
     */
    public Date getOnShipmentDate() {
        return onShipmentDate;
    }

    /**
     * 设置预计发货日期
     *
     * @param onShipmentDate 预计发货日期
     */
    public void setOnShipmentDate(Date onShipmentDate) {
        this.onShipmentDate = onShipmentDate;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}