package com.mulong.common.domain.pojo.mall.custom;

import java.math.BigDecimal;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * ProductInventoryInventory
 * 
 * @author mulong
 * @data 2021-07-15 14:52:12
 */
@Getter
@Setter
public class ProductInventoryInventory {
    private Long id;
    private Integer supplierChannelId;
    private String supplierChannelShortName;
    private String sku;
    private String originalSize;
    private String normalizedSize;
    private Integer qty;
    private BigDecimal marketPrice;
    private BigDecimal discount;
    private Date createTime;
}
