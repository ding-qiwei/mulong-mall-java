package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * InventoryChannelQuery
 * 
 * @author mulong
 * @data 2021-07-14 11:19:33
 */
@Getter
@Setter
public class InventoryChannelQuery {
    private Integer supplierChannelId;
    private String orderBy;
}
