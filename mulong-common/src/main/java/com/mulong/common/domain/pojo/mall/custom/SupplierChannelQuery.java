package com.mulong.common.domain.pojo.mall.custom;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * ChannelQuery
 * 
 * @author mulong
 * @data 2021-05-31 15:28:37
 */
@Getter
@Setter
public class SupplierChannelQuery {
    private List<String> nameList;
    private String nameKeyword;
    private Integer relatedGatewayAccountId;
    private String orderBy;
}
