package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`supplier_channel`")
public class SupplierChannel implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "`name`")
    private String name;

    @Column(name = "`short_name`")
    private String shortName;

    @Column(name = "`description`")
    private String description;

    @Column(name = "`related_gateway_account_id`")
    private Integer relatedGatewayAccountId;

    @Column(name = "`inventory_update_time`")
    private Date inventoryUpdateTime;

    @Column(name = "`inventory_update_remark`")
    private String inventoryUpdateRemark;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    @Column(name = "`update_time`")
    private Date updateTime;

    @Column(name = "`update_by`")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return short_name
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName
     */
    public void setShortName(String shortName) {
        this.shortName = shortName == null ? null : shortName.trim();
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * @return related_gateway_account_id
     */
    public Integer getRelatedGatewayAccountId() {
        return relatedGatewayAccountId;
    }

    /**
     * @param relatedGatewayAccountId
     */
    public void setRelatedGatewayAccountId(Integer relatedGatewayAccountId) {
        this.relatedGatewayAccountId = relatedGatewayAccountId;
    }

    /**
     * @return inventory_update_time
     */
    public Date getInventoryUpdateTime() {
        return inventoryUpdateTime;
    }

    /**
     * @param inventoryUpdateTime
     */
    public void setInventoryUpdateTime(Date inventoryUpdateTime) {
        this.inventoryUpdateTime = inventoryUpdateTime;
    }

    /**
     * @return inventory_update_remark
     */
    public String getInventoryUpdateRemark() {
        return inventoryUpdateRemark;
    }

    /**
     * @param inventoryUpdateRemark
     */
    public void setInventoryUpdateRemark(String inventoryUpdateRemark) {
        this.inventoryUpdateRemark = inventoryUpdateRemark == null ? null : inventoryUpdateRemark.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}