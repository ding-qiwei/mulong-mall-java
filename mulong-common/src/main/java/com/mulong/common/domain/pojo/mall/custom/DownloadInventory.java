package com.mulong.common.domain.pojo.mall.custom;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * ProductInventoryRecord
 * 
 * @author mulong
 * @data 2021-07-14 22:30:22
 */
@Getter
@Setter
public class DownloadInventory {
    private String sku;
    private Integer brandId;
    private String brandShortName;
    private String productName;
    private String productSeries;
    private Integer goodsCategory;
    private String year;
    private String season;
    private Integer goodsSex;
    private String color;
    private String normalizedSize;
    private BigDecimal weight;
    private BigDecimal productMarketPrice;
    private Integer qty;
    private String saleProps;
    private String material;
    private String remark;
    private String originalSize;
    private BigDecimal salesDiscount;
    private BigDecimal supplierDiscount;
    private Integer supplierChannelId;
    private String supplierChannelShortName;
}
