package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * GoodsDemandQuery
 * 
 * @author mulong
 * @data 2021-06-15 10:21:08
 */
@Getter
@Setter
public class GoodsDemandQuery {
    private String artNo;
    private Integer status;
    private Integer needNotice;
    private String orderBy;
}
