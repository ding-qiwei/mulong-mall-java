package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`product_barcode`")
public class ProductBarcode implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`sku`")
    private String sku;

    @Column(name = "`normalized_size`")
    private String normalizedSize;

    @Column(name = "`barcode`")
    private String barcode;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    @Column(name = "`update_time`")
    private Date updateTime;

    @Column(name = "`update_by`")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku
     */
    public void setSku(String sku) {
        this.sku = sku == null ? null : sku.trim();
    }

    /**
     * @return normalized_size
     */
    public String getNormalizedSize() {
        return normalizedSize;
    }

    /**
     * @param normalizedSize
     */
    public void setNormalizedSize(String normalizedSize) {
        this.normalizedSize = normalizedSize == null ? null : normalizedSize.trim();
    }

    /**
     * @return barcode
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * @param barcode
     */
    public void setBarcode(String barcode) {
        this.barcode = barcode == null ? null : barcode.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}