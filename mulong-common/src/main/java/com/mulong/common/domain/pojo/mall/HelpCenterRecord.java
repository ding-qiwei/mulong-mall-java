package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`help_center_record`")
public class HelpCenterRecord implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 帮助分类 0-系统帮助 1-销售帮助 2-平台策略 99-其它帮助
     */
    @Column(name = "`category`")
    private Integer category;

    /**
     * 状态 0-已发布 1-待编辑 2-已编辑完毕 3-撤销发布
     */
    @Column(name = "`status`")
    private Integer status;

    /**
     * 标题
     */
    @Column(name = "`title`")
    private String title;

    /**
     * 正文
     */
    @Column(name = "`content`")
    private String content;

    /**
     * 发布时间
     */
    @Column(name = "`published_time`")
    private Date publishedTime;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    /**
     * 创建者
     */
    @Column(name = "`create_by`")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "`update_time`")
    private Date updateTime;

    /**
     * 更新者
     */
    @Column(name = "`update_by`")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取帮助分类 0-系统帮助 1-销售帮助 2-平台策略 99-其它帮助
     *
     * @return category - 帮助分类 0-系统帮助 1-销售帮助 2-平台策略 99-其它帮助
     */
    public Integer getCategory() {
        return category;
    }

    /**
     * 设置帮助分类 0-系统帮助 1-销售帮助 2-平台策略 99-其它帮助
     *
     * @param category 帮助分类 0-系统帮助 1-销售帮助 2-平台策略 99-其它帮助
     */
    public void setCategory(Integer category) {
        this.category = category;
    }

    /**
     * 获取状态 0-已发布 1-待编辑 2-已编辑完毕 3-撤销发布
     *
     * @return status - 状态 0-已发布 1-待编辑 2-已编辑完毕 3-撤销发布
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置状态 0-已发布 1-待编辑 2-已编辑完毕 3-撤销发布
     *
     * @param status 状态 0-已发布 1-待编辑 2-已编辑完毕 3-撤销发布
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取标题
     *
     * @return title - 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取正文
     *
     * @return content - 正文
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置正文
     *
     * @param content 正文
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 获取发布时间
     *
     * @return published_time - 发布时间
     */
    public Date getPublishedTime() {
        return publishedTime;
    }

    /**
     * 设置发布时间
     *
     * @param publishedTime 发布时间
     */
    public void setPublishedTime(Date publishedTime) {
        this.publishedTime = publishedTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return create_by - 创建者
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建者
     *
     * @param createBy 创建者
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新者
     *
     * @return update_by - 更新者
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新者
     *
     * @param updateBy 更新者
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}