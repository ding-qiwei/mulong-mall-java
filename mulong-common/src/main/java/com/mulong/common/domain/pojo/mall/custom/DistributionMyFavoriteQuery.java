package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionMyFavoriteQuery
 * 
 * @author mulong
 * @data 2021-06-15 00:25:24
 */
@Getter
@Setter
public class DistributionMyFavoriteQuery {
    private Integer gatewayAccountId;
    private String artNo;
    private String productName;
    private String orderBy;
}
