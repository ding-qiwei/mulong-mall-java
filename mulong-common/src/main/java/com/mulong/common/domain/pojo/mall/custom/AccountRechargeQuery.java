package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * AccountRechargeQuery
 * 
 * @author mulong
 * @data 2021-05-30 21:20:41
 */
@Getter
@Setter
public class AccountRechargeQuery {
    private Integer gatewayAccountId;
    private String transactionSerialNo;
    private Integer status;
    private String orderBy;
}
