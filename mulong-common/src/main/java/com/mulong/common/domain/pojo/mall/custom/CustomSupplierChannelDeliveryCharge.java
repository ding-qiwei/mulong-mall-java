package com.mulong.common.domain.pojo.mall.custom;

import com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomSupplierChannelDeliveryCharge
 * 
 * @author mulong
 * @data 2021-09-30 15:16:24
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class CustomSupplierChannelDeliveryCharge extends SupplierChannelDeliveryCharge {
    private String deliveryTypeTitle;
}
