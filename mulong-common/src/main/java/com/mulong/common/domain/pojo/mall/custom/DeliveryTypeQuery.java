package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * DeliveryTypeQuery
 * 
 * @author mulong
 * @data 2021-05-31 15:28:25
 */
@Getter
@Setter
public class DeliveryTypeQuery {
    private String nameKeyword;
    private String orderBy;
}
