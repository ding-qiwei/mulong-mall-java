package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * ProductBarcodeQuery
 * 
 * @author etiger
 * @data 2021-11-26 14:29:48
 */
@Getter
@Setter
public class ProductBarcodeQuery {
    private String sku;
    private String barcode;
    private String orderBy;
}
