package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * AccountFinancialQuery
 * 
 * @author etiger
 * @data 2022-01-11 13:38:29
 */
@Getter
@Setter
public class AccountFinancialQuery {
    private Integer gatewayAccountId;
    private String gatewayAccountType;
    private Integer gatewayAccountStatus; 
    private String orderBy;
}
