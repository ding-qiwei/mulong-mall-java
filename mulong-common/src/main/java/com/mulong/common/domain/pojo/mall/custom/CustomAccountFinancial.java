package com.mulong.common.domain.pojo.mall.custom;

import com.mulong.common.domain.pojo.mall.AccountFinancial;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomAccountFinancial
 * 
 * @author etiger
 * @data 2022-01-11 13:38:20
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class CustomAccountFinancial extends AccountFinancial {
    private String gatewayAccountName;
    private String gatewayAccountType;
    private Integer gatewayAccountStatus;
    private String gatewayAccountRemark;
}
