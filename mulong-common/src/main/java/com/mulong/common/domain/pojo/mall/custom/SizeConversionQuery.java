package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * SizeConversionQuery
 * 
 * @author mulong
 * @data 2021-06-25 21:29:09
 */
@Getter
@Setter
public class SizeConversionQuery {
    private Integer brandId;
    private Integer goodsCategory;
    private Integer goodsSex;
    private String orderBy;
}
