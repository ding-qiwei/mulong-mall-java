package com.mulong.common.domain.pojo.mall.custom;

import com.mulong.common.domain.pojo.mall.MyFavorite;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionMyFavorite
 * 
 * @author mulong
 * @data 2021-06-15 00:25:45
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class DistributionMyFavorite extends MyFavorite {
    private String productName;
    private String imgUrl;
}
