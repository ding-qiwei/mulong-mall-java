package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomGatewayAccountQuery
 * 
 * @author mulong
 * @data 2021-03-30 20:41:50
 */
@Getter
@Setter
public class GatewayAccountQuery {
    private String usernameKeyword;
    private String username;
    private String type;
    private Integer status;
}
