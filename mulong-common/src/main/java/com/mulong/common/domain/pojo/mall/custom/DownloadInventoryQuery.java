package com.mulong.common.domain.pojo.mall.custom;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * ProductInventoryRecordQuery
 * 
 * @author mulong
 * @data 2021-07-14 22:42:16
 */
@Getter
@Setter
public class DownloadInventoryQuery {
    private Integer supplierChannelId;
    private List<Integer> supplierChannelIdList;
    private String orderBy;
}
