package com.mulong.common.domain.pojo.mall.custom;

import java.util.*;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson2.JSON;
import com.mulong.common.domain.pojo.mall.DataDictionary;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * CustomDataDictionary
 * 
 * @author mulong
 * @data 2021-03-30 20:38:19
 */
@SuppressWarnings("serial")
public class CustomDataDictionary extends DataDictionary {

    public List<Detail> getContentObject() {
        String content = this.getContent();
        if (StringUtils.isBlank(content)) {
            return null;
        }
        return JSON.parseArray(content, Detail.class);
    }

    public void setContentObject(List<Detail> obj) {
        if (obj == null) {
            this.setContent(null);
        } else {
            this.setContent(JSON.toJSONString(obj));
        }
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class Detail {
        private String code;
        private String name;
        private String description;
        private Integer index;
    }

}
