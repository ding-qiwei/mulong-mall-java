package com.mulong.common.domain.pojo.mall.custom;

import com.mulong.common.domain.pojo.mall.SupplierChannelSkuDiscount;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomSupplierChannelSkuDiscount
 * 
 * @author etiger
 * @data 2021-12-03 16:28:36
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class CustomSupplierChannelSkuDiscount extends SupplierChannelSkuDiscount {
    private String channelTitle;
}
