package com.mulong.common.domain.pojo.mall.custom;

import com.mulong.common.domain.pojo.mall.Product;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomProduct
 * 
 * @author etiger
 * @data 2021-12-13 16:46:57
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class CustomProduct extends Product {
    private String brandName;
}
