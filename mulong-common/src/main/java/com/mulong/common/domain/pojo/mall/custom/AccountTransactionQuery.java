package com.mulong.common.domain.pojo.mall.custom;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * AccountTransactionQuery
 * 
 * @author mulong
 * @data 2021-09-22 17:28:33
 */
@Getter
@Setter
public class AccountTransactionQuery {
    private Integer gatewayAccountId;
    private String transactionNo;
    private Integer transactionCategory;
    private Date transactionTimeFrom;
    private Date transactionTimeTo;
    private String orderBy;
}
