package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * SupplierChannelSkuDiscountQuery
 * 
 * @author etiger
 * @data 2021-12-03 16:23:20
 */
@Getter
@Setter
public class SupplierChannelSkuDiscountQuery {
    private Integer channelId;
    private String sku;
    private String orderBy;
}
