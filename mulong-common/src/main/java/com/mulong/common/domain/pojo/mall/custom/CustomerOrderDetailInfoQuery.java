package com.mulong.common.domain.pojo.mall.custom;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomerOrderDetailInfoQuery
 * 
 * @author mulong
 * @data 2021-08-18 17:21:20
 */
@Getter
@Setter
public class CustomerOrderDetailInfoQuery {
    private Integer gatewayAccountId;
    private String customizedOrderId;
    private String sku;
    private Integer supplierChannelId;
    private String receiverName;
    private String expressNo;
    private Integer status;
    private Date createTimeBegin;
    private Date createTimeEnd;
    private Date feedbackTimeBegin;
    private Date feedbackTimeEnd;
    private String orderBy;
}
