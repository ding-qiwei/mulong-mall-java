package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * HelpCenterRecordQuery
 * 
 * @author mulong
 * @data 2021-06-21 16:06:13
 */
@Getter
@Setter
public class HelpCenterRecordQuery {
    private Integer category;
    private Integer status;
    private String titleKeyword;
    private String orderBy;
}
