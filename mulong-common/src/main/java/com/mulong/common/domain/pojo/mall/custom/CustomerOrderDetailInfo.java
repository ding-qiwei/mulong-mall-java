package com.mulong.common.domain.pojo.mall.custom;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.CustomerOrderDetail;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomerOrderDetailInfo
 * 
 * @author mulong
 * @data 2021-08-18 17:20:40
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class CustomerOrderDetailInfo extends CustomerOrderDetail {
    private String gatewayAccountName;
    private String productName;
    private Integer goodsCategory;
    private Integer goodsSex;
    private String productColor;
    private BigDecimal marketPrice;
    private String brandName;
    private String brandShortName;
    private String supplierChannelName;
    private String supplierChannelShortName;
    private String expectDeliveryTypeName;
    private String actualDeliveryTypeName;
}
