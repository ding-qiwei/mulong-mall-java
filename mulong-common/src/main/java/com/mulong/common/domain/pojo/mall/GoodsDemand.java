package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`goods_demand`")
public class GoodsDemand implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`apply_gateway_account_id`")
    private Integer applyGatewayAccountId;

    @Column(name = "`art_no`")
    private String artNo;

    @Column(name = "`size`")
    private String size;

    @Column(name = "`qty`")
    private Integer qty;

    @Column(name = "`max_discount`")
    private BigDecimal maxDiscount;

    @Column(name = "`expect_date`")
    private Date expectDate;

    @Column(name = "`need_notice`")
    private Integer needNotice;

    @Column(name = "`status`")
    private Integer status;

    @Column(name = "`remark`")
    private String remark;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`update_time`")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return apply_gateway_account_id
     */
    public Integer getApplyGatewayAccountId() {
        return applyGatewayAccountId;
    }

    /**
     * @param applyGatewayAccountId
     */
    public void setApplyGatewayAccountId(Integer applyGatewayAccountId) {
        this.applyGatewayAccountId = applyGatewayAccountId;
    }

    /**
     * @return art_no
     */
    public String getArtNo() {
        return artNo;
    }

    /**
     * @param artNo
     */
    public void setArtNo(String artNo) {
        this.artNo = artNo == null ? null : artNo.trim();
    }

    /**
     * @return size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     */
    public void setSize(String size) {
        this.size = size == null ? null : size.trim();
    }

    /**
     * @return qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * @param qty
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    /**
     * @return max_discount
     */
    public BigDecimal getMaxDiscount() {
        return maxDiscount;
    }

    /**
     * @param maxDiscount
     */
    public void setMaxDiscount(BigDecimal maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    /**
     * @return expect_date
     */
    public Date getExpectDate() {
        return expectDate;
    }

    /**
     * @param expectDate
     */
    public void setExpectDate(Date expectDate) {
        this.expectDate = expectDate;
    }

    /**
     * @return need_notice
     */
    public Integer getNeedNotice() {
        return needNotice;
    }

    /**
     * @param needNotice
     */
    public void setNeedNotice(Integer needNotice) {
        this.needNotice = needNotice;
    }

    /**
     * @return status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}