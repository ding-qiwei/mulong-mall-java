package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`my_favorite`")
public class MyFavorite implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`gateway_account_id`")
    private Integer gatewayAccountId;

    @Column(name = "`art_no`")
    private String artNo;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`update_time`")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return gateway_account_id
     */
    public Integer getGatewayAccountId() {
        return gatewayAccountId;
    }

    /**
     * @param gatewayAccountId
     */
    public void setGatewayAccountId(Integer gatewayAccountId) {
        this.gatewayAccountId = gatewayAccountId;
    }

    /**
     * @return art_no
     */
    public String getArtNo() {
        return artNo;
    }

    /**
     * @param artNo
     */
    public void setArtNo(String artNo) {
        this.artNo = artNo == null ? null : artNo.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}