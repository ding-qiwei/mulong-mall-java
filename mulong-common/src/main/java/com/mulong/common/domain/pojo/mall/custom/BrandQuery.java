package com.mulong.common.domain.pojo.mall.custom;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * BrandQuery
 * 
 * @author mulong
 * @data 2021-05-31 09:38:36
 */
@Getter
@Setter
public class BrandQuery {
    private List<String> nameList;
    private String nameKeyword;
    private String orderBy;
}
