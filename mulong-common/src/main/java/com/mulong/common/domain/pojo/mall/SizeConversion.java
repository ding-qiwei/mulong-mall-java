package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`size_conversion`")
public class SizeConversion implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "`brand_id`")
    private Integer brandId;

    @Column(name = "`goods_category`")
    private Integer goodsCategory;

    @Column(name = "`goods_sex`")
    private Integer goodsSex;

    @Column(name = "`original_size`")
    private String originalSize;

    @Column(name = "`normalized_size`")
    private String normalizedSize;

    @Column(name = "`remark`")
    private String remark;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    @Column(name = "`update_time`")
    private Date updateTime;

    @Column(name = "`update_by`")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return brand_id
     */
    public Integer getBrandId() {
        return brandId;
    }

    /**
     * @param brandId
     */
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    /**
     * @return goods_category
     */
    public Integer getGoodsCategory() {
        return goodsCategory;
    }

    /**
     * @param goodsCategory
     */
    public void setGoodsCategory(Integer goodsCategory) {
        this.goodsCategory = goodsCategory;
    }

    /**
     * @return goods_sex
     */
    public Integer getGoodsSex() {
        return goodsSex;
    }

    /**
     * @param goodsSex
     */
    public void setGoodsSex(Integer goodsSex) {
        this.goodsSex = goodsSex;
    }

    /**
     * @return original_size
     */
    public String getOriginalSize() {
        return originalSize;
    }

    /**
     * @param originalSize
     */
    public void setOriginalSize(String originalSize) {
        this.originalSize = originalSize == null ? null : originalSize.trim();
    }

    /**
     * @return normalized_size
     */
    public String getNormalizedSize() {
        return normalizedSize;
    }

    /**
     * @param normalizedSize
     */
    public void setNormalizedSize(String normalizedSize) {
        this.normalizedSize = normalizedSize == null ? null : normalizedSize.trim();
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}