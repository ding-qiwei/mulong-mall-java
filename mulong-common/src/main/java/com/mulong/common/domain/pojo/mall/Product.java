package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`product`")
public class Product implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`sku`")
    private String sku;

    @Column(name = "`brand_id`")
    private Integer brandId;

    @Column(name = "`name`")
    private String name;

    @Column(name = "`series`")
    private String series;

    @Column(name = "`goods_category`")
    private Integer goodsCategory;

    @Column(name = "`year`")
    private String year;

    @Column(name = "`season`")
    private String season;

    @Column(name = "`goods_sex`")
    private Integer goodsSex;

    @Column(name = "`color`")
    private String color;

    @Column(name = "`weight`")
    private BigDecimal weight;

    @Column(name = "`market_price`")
    private BigDecimal marketPrice;

    @Column(name = "`sale_props`")
    private String saleProps;

    @Column(name = "`material`")
    private String material;

    @Column(name = "`remark`")
    private String remark;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    @Column(name = "`update_time`")
    private Date updateTime;

    @Column(name = "`update_by`")
    private String updateBy;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku
     */
    public void setSku(String sku) {
        this.sku = sku == null ? null : sku.trim();
    }

    /**
     * @return brand_id
     */
    public Integer getBrandId() {
        return brandId;
    }

    /**
     * @param brandId
     */
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return series
     */
    public String getSeries() {
        return series;
    }

    /**
     * @param series
     */
    public void setSeries(String series) {
        this.series = series == null ? null : series.trim();
    }

    /**
     * @return goods_category
     */
    public Integer getGoodsCategory() {
        return goodsCategory;
    }

    /**
     * @param goodsCategory
     */
    public void setGoodsCategory(Integer goodsCategory) {
        this.goodsCategory = goodsCategory;
    }

    /**
     * @return year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year
     */
    public void setYear(String year) {
        this.year = year == null ? null : year.trim();
    }

    /**
     * @return season
     */
    public String getSeason() {
        return season;
    }

    /**
     * @param season
     */
    public void setSeason(String season) {
        this.season = season == null ? null : season.trim();
    }

    /**
     * @return goods_sex
     */
    public Integer getGoodsSex() {
        return goodsSex;
    }

    /**
     * @param goodsSex
     */
    public void setGoodsSex(Integer goodsSex) {
        this.goodsSex = goodsSex;
    }

    /**
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color
     */
    public void setColor(String color) {
        this.color = color == null ? null : color.trim();
    }

    /**
     * @return weight
     */
    public BigDecimal getWeight() {
        return weight;
    }

    /**
     * @param weight
     */
    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    /**
     * @return market_price
     */
    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    /**
     * @param marketPrice
     */
    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * @return sale_props
     */
    public String getSaleProps() {
        return saleProps;
    }

    /**
     * @param saleProps
     */
    public void setSaleProps(String saleProps) {
        this.saleProps = saleProps == null ? null : saleProps.trim();
    }

    /**
     * @return material
     */
    public String getMaterial() {
        return material;
    }

    /**
     * @param material
     */
    public void setMaterial(String material) {
        this.material = material == null ? null : material.trim();
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_by
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}