package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`account_recharge`")
public class AccountRecharge implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 交易流水号
     */
    @Column(name = "`transaction_serial_no`")
    private String transactionSerialNo;

    /**
     * 充值金额
     */
    @Column(name = "`amount`")
    private BigDecimal amount;

    /**
     * 充值渠道 0-银行打款充值 1-支付宝转账 2-微信转账
     */
    @Column(name = "`recharge_channel`")
    private Integer rechargeChannel;

    /**
     * 充值时间
     */
    @Column(name = "`recharge_time`")
    private Date rechargeTime;

    /**
     * 处理时间
     */
    @Column(name = "`handle_time`")
    private Date handleTime;

    /**
     * 处理状态 0-待处理 1-充值完成
     */
    @Column(name = "`status`")
    private Integer status;

    /**
     * 申请人id
     */
    @Column(name = "`apply_gateway_account_id`")
    private Integer applyGatewayAccountId;

    /**
     * 充值备注
     */
    @Column(name = "`remark`")
    private String remark;

    /**
     * 提示信息
     */
    @Column(name = "`tips`")
    private String tips;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "`update_time`")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取交易流水号
     *
     * @return transaction_serial_no - 交易流水号
     */
    public String getTransactionSerialNo() {
        return transactionSerialNo;
    }

    /**
     * 设置交易流水号
     *
     * @param transactionSerialNo 交易流水号
     */
    public void setTransactionSerialNo(String transactionSerialNo) {
        this.transactionSerialNo = transactionSerialNo == null ? null : transactionSerialNo.trim();
    }

    /**
     * 获取充值金额
     *
     * @return amount - 充值金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置充值金额
     *
     * @param amount 充值金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取充值渠道 0-银行打款充值 1-支付宝转账 2-微信转账
     *
     * @return recharge_channel - 充值渠道 0-银行打款充值 1-支付宝转账 2-微信转账
     */
    public Integer getRechargeChannel() {
        return rechargeChannel;
    }

    /**
     * 设置充值渠道 0-银行打款充值 1-支付宝转账 2-微信转账
     *
     * @param rechargeChannel 充值渠道 0-银行打款充值 1-支付宝转账 2-微信转账
     */
    public void setRechargeChannel(Integer rechargeChannel) {
        this.rechargeChannel = rechargeChannel;
    }

    /**
     * 获取充值时间
     *
     * @return recharge_time - 充值时间
     */
    public Date getRechargeTime() {
        return rechargeTime;
    }

    /**
     * 设置充值时间
     *
     * @param rechargeTime 充值时间
     */
    public void setRechargeTime(Date rechargeTime) {
        this.rechargeTime = rechargeTime;
    }

    /**
     * 获取处理时间
     *
     * @return handle_time - 处理时间
     */
    public Date getHandleTime() {
        return handleTime;
    }

    /**
     * 设置处理时间
     *
     * @param handleTime 处理时间
     */
    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    /**
     * 获取处理状态 0-待处理 1-充值完成
     *
     * @return status - 处理状态 0-待处理 1-充值完成
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置处理状态 0-待处理 1-充值完成
     *
     * @param status 处理状态 0-待处理 1-充值完成
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取申请人id
     *
     * @return apply_gateway_account_id - 申请人id
     */
    public Integer getApplyGatewayAccountId() {
        return applyGatewayAccountId;
    }

    /**
     * 设置申请人id
     *
     * @param applyGatewayAccountId 申请人id
     */
    public void setApplyGatewayAccountId(Integer applyGatewayAccountId) {
        this.applyGatewayAccountId = applyGatewayAccountId;
    }

    /**
     * 获取充值备注
     *
     * @return remark - 充值备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置充值备注
     *
     * @param remark 充值备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取提示信息
     *
     * @return tips - 提示信息
     */
    public String getTips() {
        return tips;
    }

    /**
     * 设置提示信息
     *
     * @param tips 提示信息
     */
    public void setTips(String tips) {
        this.tips = tips == null ? null : tips.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}