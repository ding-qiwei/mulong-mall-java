package com.mulong.common.domain.pojo.mall.custom;

import com.mulong.common.domain.pojo.mall.SizeConversion;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomSizeConversion
 * 
 * @author etiger
 * @data 2021-11-25 13:46:13
 */
@SuppressWarnings("serial")
@Setter
@Getter
public class CustomSizeConversion extends SizeConversion {
    private String brandName;
    private String brandShortName;
}
