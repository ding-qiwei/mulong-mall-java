package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomDataDictionaryQuery
 * 
 * @author mulong
 * @data 2021-03-30 20:41:17
 */
@Getter
@Setter
public class DataDictionaryQuery {
    private String codeKeyword;
    private String code;
    private String nameKeyword;
    private String name;
}
