package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * ProductQuery
 * 
 * @author mulong
 * @data 2021-06-25 21:29:18
 */
@Getter
@Setter
public class ProductQuery {
    private String sku;
    private Integer brandId;
    private Integer goodsCategory;
    private String year;
    private String season;
    private Integer goodsSex;
    private String orderBy;
}
