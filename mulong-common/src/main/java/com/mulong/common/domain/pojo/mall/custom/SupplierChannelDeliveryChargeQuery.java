package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * ChannelExpressChargeQuery
 * 
 * @author mulong
 * @data 2021-06-01 14:58:07
 */
@Getter
@Setter
public class SupplierChannelDeliveryChargeQuery {
    private Integer channelId;
    private Integer deliveryTypeId;
    private String province;
    private String orderBy;
}
