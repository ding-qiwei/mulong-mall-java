package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * ChannelDiscountQuery
 * 
 * @author mulong
 * @data 2021-06-01 14:57:57
 */
@Getter
@Setter
public class SupplierChannelDiscountQuery {
    private Integer channelId;
    private Integer brandId;
    private Integer goodsCategory;
    private String orderBy;
}
