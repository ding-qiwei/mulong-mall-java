package com.mulong.common.domain.pojo.mall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "`inventory`")
public class Inventory implements Serializable {
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`supplier_channel_id`")
    private Integer supplierChannelId;

    @Column(name = "`sku`")
    private String sku;

    @Column(name = "`original_size`")
    private String originalSize;

    @Column(name = "`normalized_size`")
    private String normalizedSize;

    @Column(name = "`qty`")
    private Integer qty;

    @Column(name = "`market_price`")
    private BigDecimal marketPrice;

    @Column(name = "`discount`")
    private BigDecimal discount;

    @Column(name = "`create_time`")
    private Date createTime;

    @Column(name = "`create_by`")
    private String createBy;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return supplier_channel_id
     */
    public Integer getSupplierChannelId() {
        return supplierChannelId;
    }

    /**
     * @param supplierChannelId
     */
    public void setSupplierChannelId(Integer supplierChannelId) {
        this.supplierChannelId = supplierChannelId;
    }

    /**
     * @return sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku
     */
    public void setSku(String sku) {
        this.sku = sku == null ? null : sku.trim();
    }

    /**
     * @return original_size
     */
    public String getOriginalSize() {
        return originalSize;
    }

    /**
     * @param originalSize
     */
    public void setOriginalSize(String originalSize) {
        this.originalSize = originalSize == null ? null : originalSize.trim();
    }

    /**
     * @return normalized_size
     */
    public String getNormalizedSize() {
        return normalizedSize;
    }

    /**
     * @param normalizedSize
     */
    public void setNormalizedSize(String normalizedSize) {
        this.normalizedSize = normalizedSize == null ? null : normalizedSize.trim();
    }

    /**
     * @return qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * @param qty
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    /**
     * @return market_price
     */
    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    /**
     * @param marketPrice
     */
    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * @return discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * @param discount
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_by
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }
}