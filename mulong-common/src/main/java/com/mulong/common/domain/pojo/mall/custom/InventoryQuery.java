package com.mulong.common.domain.pojo.mall.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * InventoryQuery
 * 
 * @author mulong
 * @data 2021-07-09 13:54:40
 */
@Getter
@Setter
public class InventoryQuery {
    private Integer supplierChannelId;
    private String sku;
    private String orderBy;
}
