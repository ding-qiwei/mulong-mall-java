package com.mulong.common.domain.pojo.mall.custom;

import java.math.BigDecimal;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * ProductInventoryProduct
 * 
 * @author mulong
 * @data 2021-07-15 14:52:01
 */
@Getter
@Setter
public class ProductInventoryProduct {
    private Long id;
    private String sku;
    private Integer brandId;
    private String brandShortName;
    private String name;
    private String series;
    private Integer goodsCategory;
    private String year;
    private String season;
    private Integer goodsSex;
    private String color;
    private BigDecimal weight;
    private BigDecimal marketPrice;
    private String saleProps;
    private String material;
    private String remark;
    private List<ProductInventoryInventory> inventoryList;
}
