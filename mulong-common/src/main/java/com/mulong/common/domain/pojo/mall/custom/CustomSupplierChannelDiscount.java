package com.mulong.common.domain.pojo.mall.custom;

import com.mulong.common.domain.pojo.mall.SupplierChannelDiscount;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomSupplierChannelDiscount
 * 
 * @author mulong
 * @data 2021-09-30 16:00:25
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class CustomSupplierChannelDiscount extends SupplierChannelDiscount {
    private String channelTitle;
    private String brandTitle;
}
