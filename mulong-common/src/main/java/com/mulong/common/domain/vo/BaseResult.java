package com.mulong.common.domain.vo;

import com.mulong.common.constants.Constants;
import com.mulong.common.util.TracerUtil;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * BaseResult
 * 
 * @author mulong
 * @date 2021-03-05 20:39:10
 */
@Getter
@NoArgsConstructor
public class BaseResult<T> {
    private String code = Constants.RESULT_SUCCEED_CODE;
    private String msg = Constants.RESULT_SUCCEED_MSG;
    private String traceId = TracerUtil.getTraceId();
    private T data;

    public BaseResult(T data) {
        this.data = data;
    }

    public BaseResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static <T> BaseResult<T> success() {
        return new BaseResult<>(null);
    }

    public static <T> BaseResult<T> success(T data) {
        return new BaseResult<>(data);
    }

    public static <T> BaseResult<T> failure(String code, String msg) {
        return new BaseResult<>(code, msg);
    }

}
