package com.mulong.common.client;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.jd.open.api.sdk.DefaultJdClient;
import com.jd.open.api.sdk.JdClient;
import com.jd.open.api.sdk.request.order.PopOrderSearchRequest;
import com.jd.open.api.sdk.response.order.PopOrderSearchResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * JdOpenClient
 * 
 * @author mulong
 * @data 2021-10-15 10:30:20
 */
@Component
@Slf4j
public class JdOpenClient {
    @Value("${mulong.jd.open.server-url}")
    private String serverUrl;
    @Value("${mulong.jd.open.app-key}")
    private String appKey;
    @Value("${mulong.jd.open.app-secret}")
    private String appSecret;
    @Value("${mulong.jd.open.access-token}")
    private String accessToken;

    private JdClient client;

    @PostConstruct
    public void init() {
        client = new DefaultJdClient(serverUrl, accessToken, appKey, appSecret);
        log.info("{} inited", JdOpenClient.class.getSimpleName());
    }

    public PopOrderSearchResponse popOrderSearch(PopOrderSearchRequest request) throws Exception {
        return client.execute(request);
    }

}
