package com.mulong.common.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;

import lombok.extern.slf4j.Slf4j;

/**
 * AliyunOssClient
 * 
 * @author mulong
 * @data 2021-07-28 10:38:18
 */
@Slf4j
public class AliyunOssClient {
    private String bucket;
    private String endPoint;
    private String accessKeyId;
    private String accessKeySceret;

    public AliyunOssClient(String bucket, String endPoint, String accessKeyId, String accessKeySceret) {
        this.bucket = bucket;
        this.endPoint = endPoint;
        this.accessKeyId = accessKeyId;
        this.accessKeySceret = accessKeySceret;
    }

    /**
     * 上传文件
     */
    public String upload(MultipartFile file, String key) throws IOException {
        OSS ossClient = null;
        try (InputStream inputStream = file.getInputStream()) {
            ossClient = new OSSClientBuilder().build(endPoint, accessKeyId, accessKeySceret);
            ossClient.putObject(bucket, key, inputStream);
            return String.format("https://%s.%s/%s", bucket, endPoint, key);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    /**
     * 删除文件
     */
    public void remove(String key) {
        OSS ossClient = null;
        try {
            ossClient = new OSSClientBuilder().build(endPoint, accessKeyId, accessKeySceret);
            ossClient.deleteObject(bucket, key);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    /**
     * 从阿里云oss中读取key对应的文件内容为byte[]
     */
    public byte[] downloadToByteArray(String key) throws IOException {
        OSS ossClient = null;
        InputStream inputStream = null;
        ByteArrayOutputStream outputStream = null;
        try {
            ossClient = new OSSClientBuilder().build(endPoint, accessKeyId, accessKeySceret);
            OSSObject object = ossClient.getObject(bucket, key);
            inputStream = object.getObjectContent();
            outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int byteread = 0;
            while ((byteread = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, byteread);
            }
            return outputStream.toByteArray();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                }
            }
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

}
