package com.mulong.common.constants;

/**
 * Constants
 * 
 * @author mulong
 * @date 2021-03-05 20:40:18
 */
public final class Constants {

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    /** 结果 0-成功 */
    public static final String RESULT_SUCCEED_CODE = "0";
    /** 结果 1-失败 */
    public static final String RESULT_FAILED_CODE = "1";
    /** 结果 success-成功 */
    public static final String RESULT_SUCCEED_MSG = "success";
    /** 结果 failed-失败 */
    public static final String RESULT_FAILED_MSG = "抱歉，我们遇到一点问题，请您稍后再试。";

    /** 环境 dev-开发 */
    public static final String ACTIVE_DEV = "dev";
    /** 环境 test-测试 */
    public static final String ACTIVE_TEST = "test";
    /** 环境 prod-生产 */
    public static final String ACTIVE_PROD = "prod";

}
