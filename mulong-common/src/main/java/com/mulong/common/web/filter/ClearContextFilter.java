package com.mulong.common.web.filter;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import org.springframework.core.annotation.Order;

import com.mulong.common.util.RequestContext;
import com.mulong.common.util.ThreadContext;

/**
 * ClearContextFilter
 * 
 * @author mulong
 * @date 2021-03-06 00:52:18
 */
@Order(Integer.MIN_VALUE)
@WebFilter(value = "/*")
public class ClearContextFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
        // Do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            chain.doFilter(request, response);
        } finally {
            remove();
        }
    }

    @Override
    public void destroy() {
        remove();
    }

    private void remove() {
        ThreadContext.remove();
        RequestContext.remove();
    }

}
