package com.mulong.common.web.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;

import com.mulong.common.util.RequestContext;

/**
 * GlobalInterceptor
 * 
 * @author mulong
 * @date 2021-03-06 00:56:10
 */
@Component
public class GlobalInterceptor implements AsyncHandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        RequestContext.init(request, response);
        return true;
    }

}
