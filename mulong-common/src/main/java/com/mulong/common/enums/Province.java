package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Province
 * 
 * @author mulong
 * @data 2021-09-29 18:55:25
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum Province {
    /** 江苏 */
    JIANG_SU("江苏", "江苏", "江苏省"),
    /** 广东 */
    GUANG_DONG("广东", "广东", "广东省"),
    /** 山东 */
    SHAN_DONG("山东", "山东", "山东省"),
    /** 浙江 */
    ZHE_JIANG("浙江", "浙江", "浙江省"),
    /** 四川 */
    SI_CHUAN("四川", "四川", "四川省"),
    /** 北京 */
    BEI_JING("北京", "北京", "北京市"),
    /** 湖北 */
    HU_BEI("湖北", "湖北", "湖北省"),
    /** 辽宁 */
    LIAO_NING("辽宁", "辽宁", "辽宁省"),
    /** 河南 */
    HE_NAN("河南", "河南", "河南省"),
    /** 湖南 */
    HU_NAN("湖南", "湖南", "湖南省"),
    /** 上海 */
    SHANG_HAI("上海", "上海", "上海市"),
    /** 河北 */
    HE_BEI("河北", "河北", "河北省"),
    /** 云南 */
    YUN_NAN("云南", "云南", "云南省"),
    /** 陕西 */
    SHAN_3_XI("陕西", "陕西", "陕西省"),
    /** 重庆 */
    CHONG_QING("重庆", "重庆", "重庆市"),
    /** 福建 */
    FU_JIAN("福建", "福建", "福建省"),
    /** 黑龙江 */
    HEI_LONG_JIANG("黑龙江", "黑龙江", "黑龙江省"),
    /** 安徽 */
    AN_HUI("安徽", "安徽", "安徽省"),
    /** 广西 */
    GUANG_XI("广西", "广西", "广西壮族自治区"),
    /** 内蒙 */
    NEI_MENG("内蒙", "内蒙", "内蒙古自治区"),
    /** 山西 */
    SHAN_1_XI("山西", "山西", "山西省"),
    /** 贵州 */
    GUI_ZHOU("贵州", "贵州", "贵州省"),
    /** 吉林 */
    JI_LIN("吉林", "吉林", "吉林省"),
    /** 江西 */
    JIANG_XI("江西", "江西", "江西省"),
    /** 新疆 */
    XIN_JIANG("新疆", "新疆", "新疆维吾尔自治区"),
    /** 天津 */
    TIAN_JING("天津", "天津", "天津市"),
    /** 海南 */
    HAI_NAN("海南", "海南", "海南省"),
    /** 甘肃 */
    GAN_SU("甘肃", "甘肃", "甘肃省"),
    /** 宁夏 */
    NING_XIA("宁夏", "宁夏", "宁夏回族自治区"),
    /** 青海 */
    QING_HAI("青海", "青海", "青海省"),
    /** 西藏 */
    XI_ZANG("西藏", "西藏", "西藏自治区"),
    /** 台湾 */
    TAI_WAN("台湾", "台湾", "台湾省"),
    /** 香港 */
    XIANG_GANG("香港", "香港", "香港特别行政区"),
    /** 澳门 */
    AO_MEN("澳门", "澳门", "澳门特别行政区"),
    /** 海外 */
    HAI_WAI("海外", "海外", "海外"),

    ;
    
    /**  */
    private String code;
    /**  */
    private String shortName;
    /**  */
    private String name;

    public static synchronized Province getByCode(String code) {
        for (Province e : Province.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

    public static synchronized Province getByName(String name) {
        for (Province e : Province.values()) {
            if (e.getName().equals(name)) {
                return e;
            }
        }
        return null;
    }

}
