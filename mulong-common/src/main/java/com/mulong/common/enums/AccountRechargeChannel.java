package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * AccountRechargeChannel
 * 
 * @author mulong
 * @data 2021-05-30 17:44:44
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum AccountRechargeChannel {
    /** 银行打款充值 */
    BANK(0, "银行打款充值"),
    /** 支付宝转账 */
    ALIPAY(1, "支付宝转账"),
    /** 微信转账 */
    WECHAT(2, "微信转账"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized AccountRechargeChannel getByCode(Integer code) {
        for (AccountRechargeChannel e : AccountRechargeChannel.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
