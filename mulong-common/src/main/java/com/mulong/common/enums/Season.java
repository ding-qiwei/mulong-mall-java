package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Season
 * 
 * @author mulong
 * @data 2021-09-22 13:29:58
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum Season {
    /** 春 */
    Q1("Q1", "Q1", "春"),
    /** 夏 */
    Q2("Q2", "Q2", "夏"),
    /** 秋 */
    Q3("Q3", "Q3", "秋"),
    /** 冬 */
    Q4("Q4", "Q4", "冬"),

    ;

    /**  */
    private String code;
    /**  */
    private String name;
    /**  */
    private String description;

    public static synchronized Season getByCode(String code) {
        for (Season e : Season.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
