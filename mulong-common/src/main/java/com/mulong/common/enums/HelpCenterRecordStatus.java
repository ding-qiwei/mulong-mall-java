package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * HelpCenterRecordStatus
 * 
 * @author mulong
 * @data 2021-05-30 08:57:41
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum HelpCenterRecordStatus {
    /** 已发布 */
    PUBLISHED(0, "已发布"),
    /** 待编辑 */
    EDITING(1, "待编辑"),
    /** 已编辑完毕 */
    EDITED(2, "已编辑完毕"),
    /** 撤销发布 */
    CANCELED(3, "撤销发布"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized HelpCenterRecordStatus getByCode(Integer code) {
        for (HelpCenterRecordStatus e : HelpCenterRecordStatus.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
