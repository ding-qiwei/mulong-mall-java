package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * HelpCenterRecordCategory
 * 
 * @author mulong
 * @data 2021-05-30 08:57:31
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum HelpCenterRecordCategory {
    /** 系统帮助 */
    SYSTEM(0, "系统帮助"),
    /** 销售帮助 */
    SALES(1, "销售帮助"),
    /** 平台策略 */
    PLATFORM_STRATEGY(2, "平台策略"),
    /** 其它帮助 */
    OTHERS(99, "其它帮助"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized HelpCenterRecordCategory getByCode(Integer code) {
        for (HelpCenterRecordCategory e : HelpCenterRecordCategory.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
