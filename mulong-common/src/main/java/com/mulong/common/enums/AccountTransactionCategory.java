package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * AccountTransactionCategory
 * 
 * @author mulong
 * @data 2021-05-30 17:55:12
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum AccountTransactionCategory {
    /** 充值 */
    RECHARGE(0, "充值"),
    /** 扣款 */
    PAYMENT(1, "扣款"),
    /** 退款 */
    REFUND(2, "退款"),
    /** 冻结 */
    FREEZE(3, "冻结"),
    /** 解冻 */
    UNFREEZE(4, "解冻"),
    /** 服务费 */
    SERVICE_FEE(99, "服务费"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized AccountTransactionCategory getByCode(Integer code) {
        for (AccountTransactionCategory e : AccountTransactionCategory.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
