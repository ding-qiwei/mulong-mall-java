package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * GoodsDemandNeedNotice
 * 
 * @author mulong
 * @data 2021-06-15 13:36:48
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum GoodsDemandNeedNotice {
    /** 否 */
    N(0, "否"),
    /** 是 */
    Y(1, "是"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized GoodsDemandNeedNotice getByCode(Integer code) {
        for (GoodsDemandNeedNotice e : GoodsDemandNeedNotice.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
