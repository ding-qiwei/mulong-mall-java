package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * GoodsDemandStatus
 * 
 * @author mulong
 * @data 2021-06-15 13:36:21
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum GoodsDemandStatus {
    /** 待处理 */
    WAITING(0, "待处理"),
    /** 处理中 */
    PROCESSING(1, "处理中"),
    /** 成功 */
    FINISHED(2, "成功"),
    /** 申请取消 */
    APPLY_CANCEL(3, "申请取消"),
    /** 已作废 */
    CANCELED(4, "已作废"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized GoodsDemandStatus getByCode(Integer code) {
        for (GoodsDemandStatus e : GoodsDemandStatus.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
