package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * GatewayAccountType
 * 
 * @author mulong
 * @data 2021-03-06 11:23:27
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum GatewayAccountType {
    /** 分销商 */
    DISTRIBUTOR("distributor", "分销商"),
    /** 供应商 */
    SUPPLIER("supplier", "供应商"),
    /** 零售 */
    RETAIL("retail", "零售"),
    /** 客服 */
    SERVICE("service", "客服"),
    /** 现货仓管理员 */
    GOODS("goods", "现货仓管理员"),
    /** 管理员 */
    MANAGER("manager", "后台管理员"),
    /** 超级管理员 */
    ADMIN("admin", "超级管理员"),

    ;

    /**  */
    private String code;
    /**  */
    private String name;

    public static synchronized GatewayAccountType getByCode(String code) {
        for (GatewayAccountType e : GatewayAccountType.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
