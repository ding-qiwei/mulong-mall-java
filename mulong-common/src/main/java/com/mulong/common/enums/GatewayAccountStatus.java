package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * GatewayAccountStatus
 * 
 * @author mulong
 * @data 2021-07-09 17:56:25
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum GatewayAccountStatus {
    /** 关闭 */
    DISABLED(0, "关闭"),
    /** 开启 */
    ENABLED(1, "开启"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized GatewayAccountStatus getByCode(Integer code) {
        for (GatewayAccountStatus e : GatewayAccountStatus.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
