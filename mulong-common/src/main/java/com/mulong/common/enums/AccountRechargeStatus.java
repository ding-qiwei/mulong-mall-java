package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * AccountRechargeStatus
 * 
 * @author mulong
 * @data 2021-05-30 17:54:29
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum AccountRechargeStatus {
    /** 待处理 */
    WAITING(0, "待处理"),
    /** 充值完成 */
    COMPLETED(1, "充值完成"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized AccountRechargeStatus getByCode(Integer code) {
        for (AccountRechargeStatus e : AccountRechargeStatus.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
