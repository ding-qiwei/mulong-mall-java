package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * GoodsCategory
 * 
 * @author mulong
 * @data 2021-06-22 23:01:28
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum GoodsCategory {
    /** 鞋 */
    SHOES(1, "鞋"),
    /** 服 */
    CLOTHES(2, "服"),
    /** 配 */
    ACCESSORIES(3, "配"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized GoodsCategory getByCode(Integer code) {
        for (GoodsCategory e : GoodsCategory.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

    public static synchronized GoodsCategory getByName(String name) {
        for (GoodsCategory e : GoodsCategory.values()) {
            if (e.getName().equals(name)) {
                return e;
            }
        }
        return null;
    }

}
