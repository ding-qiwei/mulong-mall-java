package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * GoodsSex
 * 
 * @author mulong
 * @data 2021-05-30 23:29:54
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum GoodsSex {
    /** 男 */
    MALE(0, "男"),
    /** 女 */
    FEMALE(1, "女"),
    /** 中性 */
    UNISEX(2, "中性"),
    /** 童 */
    CHILD(3, "童"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized GoodsSex getByCode(Integer code) {
        for (GoodsSex e : GoodsSex.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

    public static synchronized GoodsSex getByName(String name) {
        for (GoodsSex e : GoodsSex.values()) {
            if (e.getName().equals(name)) {
                return e;
            }
        }
        return null;
    }

}
