package com.mulong.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * CustomerOrderStatus
 * 
 * @author mulong
 * @data 2021-08-19 10:47:28
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@AllArgsConstructor
public enum CustomerOrderStatus {
    /** 未付款 */
    ARREARAGE(1, "未付款"),
    /** 已付款 */
    PAID(2, "已付款"),
    /** 配货中 */
    PREPARING(3, "配货中"),
    /** 反馈成功 */
    FEEDBACK_SUCCESS(4, "反馈成功"),
    /** 反馈失败 */
    FEEDBACK_FAIL(5, "反馈失败"),
    /** 已退款 */
    REFUNDED(6, "已退款"),
    /** 待退货 */
    TO_BE_RETURN(7, "待退货"),
    /** 已退货 */
    RETURNED(8, "已退货"),

    ;

    /**  */
    private Integer code;
    /**  */
    private String name;

    public static synchronized CustomerOrderStatus getByCode(Integer code) {
        for (CustomerOrderStatus e : CustomerOrderStatus.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }

}
