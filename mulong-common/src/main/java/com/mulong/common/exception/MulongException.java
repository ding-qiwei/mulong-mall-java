package com.mulong.common.exception;

import lombok.Getter;

/**
 * MulongException
 * 
 * @author mulong
 * @date 2021-03-05 21:44:55
 */
@SuppressWarnings("serial")
public class MulongException extends RuntimeException {
    @Getter
    private final ErrorEnums error;
    @Getter
    private final String code;
    @Getter
    private final String msg;

    public MulongException(ErrorEnums error) {
        super(error.getMsg());
        this.error = error;
        this.code = String.valueOf(error.getCode());
        this.msg = error.getMsg();
    }

    public MulongException(ErrorEnums error, Throwable cause) {
        super(error.getMsg(), cause);
        this.error = error;
        this.code = String.valueOf(error.getCode());
        this.msg = error.getMsg();
    }

    public MulongException(int code, String msg) {
        this(String.valueOf(code), msg);
    }

    public MulongException(String code, String msg) {
        super(msg);
        this.error = null;
        this.code = code;
        this.msg = msg;
    }

}
