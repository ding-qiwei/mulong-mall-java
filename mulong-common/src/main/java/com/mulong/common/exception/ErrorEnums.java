package com.mulong.common.exception;

import com.mulong.common.constants.Constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * ErrorConstants
 * 
 * @author mulong
 * @date 2021-03-05 21:46:03
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ErrorEnums {
    SUCCESS(Constants.RESULT_SUCCEED_CODE, Constants.RESULT_SUCCEED_MSG),
    SUCCESS_TEMPLETE(Constants.RESULT_SUCCEED_CODE, "%s"),

    /**
     * 通用类
     */

    // 系统
    UNKNOWN_ERROR("10001", "未知错误", Constants.RESULT_FAILED_MSG),
    SYSTEM_ERROR("10002", "系统错误", "服务器忙，请稍后再试"),
    ILLEGAL_REQUEST("10003", "非法请求"),
    FORBIDDEN("10004", "拒绝访问"),
    RESOURCE_NOT_EXISTS("10005", "资源不存在"),
    RESOURCE_NOT_EXISTS_TEMPLETE("10006", "资源(%s)不存在"),
    REQUEST_TIMEOUT("10007", "请求超时"),
    UPLOAD_FILE_SERVER_ERROR("10008", "文件上传IO异常"),
    UPLOAD_FILE_OSS_ERROR("10009", "文件上传未知异常"),
    UPLOAD_FILE_EMPTY("10010", "文件上传为空"),

    // 参数
    ILLEGAL_LESS_PARAM_TEMPLETE("10021", "参数(%s)不能为空"),
    ILLEGAL_FORMAT_PARAM_TEMPLETE("10022", "参数(%s)格式错误"),
    ILLEGAL_PARAM("10023", "参数错误"),
    ILLEGAL_PARAM_TEMPLETE("10024", "参数(%s)错误"),

    // 操作失败
    HANDLE_FAILED("10041", "操作失败"),
    HANDLE_FAILED_TEMPLETE("10042", "操作失败，原因：%s"),
 
    /**
     * 业务类
     */
    GET_ACCESS_TOKEN_FAILED("20001", "获取token失败"),
    GET_ACCESS_TOKEN_FAILED_TEMPLETE("20002", "获取token失败，原因：%s"),
    GET_ACCESS_TOKEN_NO_BIND_WECHAT_INFO("20003", "获取token失败，没有绑定的微信信息"),
    VALID_ACCESS_TOKEN_ERROR("20004", "校验token失败"),
    VALID_ACCESS_TOKEN_TEMPLETE("20005", "获取token失败，原因：%s"),
    ACCESS_TOKEN_EXPIRED("20006", "token过期"),
    BOOKING_FAILED("20007", "下单失败"),
    BOOKING_FAILED_TEMPLETE("20008", "下单失败，原因：%s"),
    ORDER_PAY_FAILED("20009", "订单支付失败"),
    ORDER_PAY_FAILED_TEMPLETE("20010", "订单支付失败，原因：%s"),

    CUSTOMER_ORDER_PAY_FAILED("30001", "订单状态无法支付"),
    ACCOUNT_DECREASE_FAILED("30002", "账户余额不足, 无法扣款"),
    ACCOUNT_INCREASE_FAILED("30003", "账户无法充值"),
    ACCOUNT_RECHAGRE_COMPLETE_FAILED("30004", "当前状态无法完成充值"),


    ;

    /** 错误码 */
    private String code;
    /** 服务端错误信息 */
    private String svrMsgTemplete;
    private String svrMsg;
    /** 错误信息 */
    private String msgTemplete;
    private String msg;

    ErrorEnums(String code, String svrMsg, String msg) {
        this.code = code;
        this.svrMsgTemplete = svrMsg;
        this.svrMsg = svrMsg;
        this.msgTemplete = msg;
        this.msg = msg;
    }

    ErrorEnums(String code, String svrMsg) {
        this.code = code;
        this.svrMsgTemplete = svrMsg;
        this.svrMsg = svrMsg;
        this.msgTemplete = svrMsg;
        this.msg = svrMsg;
    }

    public ErrorEnums format(Object... info) {
        this.svrMsg = String.format(svrMsgTemplete, info);
        this.msg = String.format(msgTemplete, info);
        return this;
    }

}
