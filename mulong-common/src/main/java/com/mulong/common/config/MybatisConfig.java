package com.mulong.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MybatisConfig
 * 
 * @author mulong
 * @date 2021-03-06 10:49:19
 */
@Configuration
@EnableTransactionManagement
public class MybatisConfig {

    public static org.apache.ibatis.session.Configuration configuration() {
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        // 设置驼峰
        configuration.setMapUnderscoreToCamelCase(true);
        // 设置超时
        configuration.setDefaultStatementTimeout(20);
        // 延迟加载
        configuration.setLazyLoadingEnabled(true);
        // 积极加载
        configuration.setAggressiveLazyLoading(false);
        return configuration;
    }

}
