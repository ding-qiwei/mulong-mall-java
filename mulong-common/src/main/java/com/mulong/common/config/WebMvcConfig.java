package com.mulong.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.mulong.common.web.interceptor.GlobalInterceptor;

/**
 * WebMvcConfig
 * 
 * @author mulong
 * @data 2021-03-16 10:19:02
 */
@Configuration
@ServletComponentScan(basePackages = "com.mulong.**.*.web")
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    private GlobalInterceptor globalInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(globalInterceptor).addPathPatterns("/**");
    }

}
