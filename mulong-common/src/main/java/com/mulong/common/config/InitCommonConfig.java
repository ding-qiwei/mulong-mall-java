package com.mulong.common.config;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.mulong.common.client.AliyunOssClient;
import com.mulong.common.util.MulongUtil;
import com.mulong.common.util.httpclient.HttpExecutor;
import com.mulong.common.util.httpclient.HttpExecutorBuilder;

import lombok.extern.slf4j.Slf4j;

/**
 * InitConfig
 * 
 * @author mulong
 * @data 2021-03-06 14:35:12
 */
@Slf4j
@Configuration
public class InitCommonConfig {
    @Value("${mulong.aliyun.oss.bucket}")
    private String bucket;
    @Value("${mulong.aliyun.oss.end-point}")
    private String endPoint;
    @Value("${mulong.aliyun.oss.access-key-id}")
    private String accessKeyId;
    @Value("${mulong.aliyun.oss.access-key-secret}")
    private String accessKeySecret;

    @PostConstruct
    public void init() {
        MulongUtil.setDesKey("ABCabc123_321$");
        log.info("{} inited", InitCommonConfig.class.getSimpleName());
    }

    @PreDestroy
    public void destroy() {
        // Do Nothing
    }

    @Primary
    @Bean
    public HttpExecutor httpExecutor() {
        return HttpExecutorBuilder.custom().build();
    }

    @Bean
    public AliyunOssClient aliyunOssClient() {
        return new AliyunOssClient(bucket, endPoint, accessKeyId, accessKeySecret);
    }

}
