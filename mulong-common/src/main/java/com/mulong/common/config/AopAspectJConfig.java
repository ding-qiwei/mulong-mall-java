package com.mulong.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * AopAspectJConfig
 * 
 * @author mulong
 * @data 2021-03-06 10:17:06
 */
@Configuration
@EnableAspectJAutoProxy(exposeProxy = true)
public class AopAspectJConfig {

}
