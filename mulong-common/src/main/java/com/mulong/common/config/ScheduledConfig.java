package com.mulong.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * ScheduledConfig
 * 
 * @author mulong
 * @data 2021-03-22 13:56:03
 */
@Configuration
@EnableScheduling
public class ScheduledConfig {

}
