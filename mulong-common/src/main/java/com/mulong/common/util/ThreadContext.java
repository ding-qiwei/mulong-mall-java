package com.mulong.common.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import static com.alibaba.fastjson2.util.TypeUtils.*;

/**
 * ThreadContext
 * 
 * @author mulong
 * @data 2021-03-05 22:50:24
 */
public class ThreadContext {
    private static final ThreadLocal<Map<String, Object>> CONTENTS = ThreadLocal.withInitial(HashMap::new);

    private ThreadContext() {
        throw new IllegalStateException("Utility class");
    }

    public static void remove() {
        CONTENTS.remove();
    }

    public static void put(String k, Object v) {
        CONTENTS.get().put(k, v);
    }

    public static Object get(String k) {
        return CONTENTS.get().get(k);
    }

    public static <T> T getObject(String key, Class<T> clazz) {
        Object obj = get(key);
        return cast(obj, clazz);
    }

    public static Boolean getBoolean(String key) {
        Object value = get(key);
        if (value == null) {
            return null;
        }
        return toBoolean(value);
    }

    public static boolean getBooleanValue(String key) {
        Object value = get(key);
        if (value == null) {
            return false;
        }
        return toBoolean(value);
    }

    public static Byte getByte(String key) {
        Object value = get(key);
        return toByte(value);
    }

    public static byte getByteValue(String key) {
        Object value = get(key);
        if (value == null) {
            return 0;
        }
        return toByte(value);
    }

    public static Short getShort(String key) {
        Object value = get(key);
        return toShort(value);
    }

    public static short getShortValue(String key) {
        Object value = get(key);
        if (value == null) {
            return 0;
        }
        return toShort(value);
    }

    public static Integer getInteger(String key) {
        Object value = get(key);
        return toInteger(value);
    }

    public static int getIntValue(String key) {
        Object value = get(key);
        if (value == null) {
            return 0;
        }
        return toInteger(value);
    }

    public static Long getLong(String key) {
        Object value = get(key);
        return toLong(value);
    }

    public static long getLongValue(String key) {
        Object value = get(key);
        if (value == null) {
            return 0L;
        }
        return toLong(value);
    }

    public static Float getFloat(String key) {
        Object value = get(key);
        return toFloat(value);
    }

    public static float getFloatValue(String key) {
        Object value = get(key);
        if (value == null) {
            return 0F;
        }
        return toFloat(value);
    }

    public static Double getDouble(String key) {
        Object value = get(key);
        return toDouble(value);
    }

    public static double getDoubleValue(String key) {
        Object value = get(key);
        if (value == null) {
            return 0D;
        }
        return toDouble(value);
    }

    public static BigDecimal getBigDecimal(String key) {
        Object value = get(key);
        return toBigDecimal(value);
    }

    public static BigInteger getBigInteger(String key) {
        Object value = get(key);
        return toBigInteger(value);
    }

    public static String getString(String key) {
        Object value = get(key);
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    public static Date getDate(String key) {
        Object value = get(key);
        return toDate(value);
    }

}
