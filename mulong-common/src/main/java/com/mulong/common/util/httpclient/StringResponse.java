package com.mulong.common.util.httpclient;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.StatusLine;

/**
 * StringResponse
 *
 * @author mulong
 * @date 2020-12-01 13:46:34
 */
@Data
@NoArgsConstructor
public class StringResponse {
    /** 内容字符串 */
    private String contentString;
    /** http状态信息 */
    private StatusLine statusLine;
    /** 异常 */
    private Exception exception;
    /** 耗时 单位：毫秒 */
    private long costTime;

    public StringResponse(String contentString, StatusLine statusLine) {
        this.contentString = contentString;
        this.statusLine = statusLine;
    }

    public StringResponse(Exception exception) {
        this.exception = exception;
    }

}
