package com.mulong.common.util.httpclient;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

/**
 * HttpExecutor
 *
 * @author mulong
 * @date 2020-12-01 13:46:34
 */
public class HttpExecutor {
    private HttpClient httpClient;

    HttpExecutor(final HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public Executor get(String url) {
        return new Executor(httpClient, new HttpGet(url), BodyType.FORM);
    }

    public Executor postForm(String url) {
        return new Executor(httpClient, new HttpPost(url), BodyType.FORM);
    }

    public Executor postJson(String url) {
        return new Executor(httpClient, new HttpPost(url), BodyType.JSON);
    }

    public Executor postXml(String url) {
        return new Executor(httpClient, new HttpPost(url), BodyType.XML);
    }

}
