package com.mulong.common.util;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * RequestContext
 * 
 * @author mulong
 * @date 2021-03-06 01:16:43
 */
public final class RequestContext {
    private static final ThreadLocal<HttpServletRequest> HTTP_SERVLET_REQUEST = new ThreadLocal<>();
    private static final ThreadLocal<HttpServletResponse> HTTP_SERVLET_RESPONSE = new ThreadLocal<>();
    private static final ThreadLocal<Long> BEGIN_TIME = new ThreadLocal<>();

    private RequestContext() {
        throw new IllegalStateException("Utility class");
    }

    public static void init(HttpServletRequest request, HttpServletResponse response) {
        HTTP_SERVLET_REQUEST.set(request);
        HTTP_SERVLET_RESPONSE.set(response);
    }

    public static HttpServletRequest getRequest() {
        return HTTP_SERVLET_REQUEST.get();
    }

    public static HttpServletResponse getResponse() {
        return HTTP_SERVLET_RESPONSE.get();
    }

    public static void setBeginTime(Long timestamp) {
        BEGIN_TIME.set(timestamp);
    }

    public static Long getBeginTime() {
        return BEGIN_TIME.get();
    }

    public static void remove() {
        HTTP_SERVLET_REQUEST.remove();
        HTTP_SERVLET_RESPONSE.remove();
        BEGIN_TIME.remove();
    }

}
