package com.mulong.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.mulong.common.constants.Constants;

import lombok.extern.slf4j.Slf4j;

/**
 * EnvUtil
 * 
 * @author mulong
 * @data 2021-03-06 14:12:12
 */
@Slf4j
@Component
public class EnvUtil {
    private static Environment env;

    @Autowired
    public void setEnv(Environment env) {
        EnvUtil.env = env;
        log.info("{} inited", EnvUtil.class.getSimpleName());
    }

    public static String get(String key) {
        return env.getProperty(key);
    }

    public static String get(String key, String defaultValue) {
        return env.getProperty(key, defaultValue);
    }

    public static Integer getInteger(String key) {
        return env.getProperty(key, Integer.class);
    }

    public static int getInt(String key) {
        return env.getProperty(key, int.class, 0);
    }

    public static int getInt(String key, int defaultValue) {
        return env.getProperty(key, int.class, defaultValue);
    }

    private static String getActive() {
        return get("spring.profiles.active");
    }

    public static boolean isProdMode() {
        return Constants.ACTIVE_PROD.equals(getActive());
    }

    public static boolean isTestMode() {
        return Constants.ACTIVE_TEST.equals(getActive());
    }

    public static boolean isDevMode() {
        return Constants.ACTIVE_DEV.equals(getActive());
    }

}
