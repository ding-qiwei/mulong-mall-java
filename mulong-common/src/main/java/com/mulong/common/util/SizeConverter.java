package com.mulong.common.util;

import java.util.*;

import org.springframework.util.CollectionUtils;

import com.mulong.common.domain.pojo.mall.SizeConversion;

/**
 * SizeConverter
 * 
 * @author mulong
 * @data 2021-07-12 14:37:25
 */
public class SizeConverter {
    private Map<Integer, List<SizeConversion>> mapByBrandId;

    public SizeConverter(List<SizeConversion> list) {
        mapByBrandId = new HashMap<>();
        for (SizeConversion e : list) {
            Integer brandId = e.getBrandId();
            List<SizeConversion> sizeConversionList = mapByBrandId.get(brandId);
            if (sizeConversionList == null) {
                sizeConversionList = new ArrayList<>();
                mapByBrandId.put(brandId, sizeConversionList);
            }
            sizeConversionList.add(e);
        }
    }

    public String getNormalizedSize(Integer brandId, Integer goodsCategory, Integer goodsSex, String originalSize) {
        List<SizeConversion> list = mapByBrandId.get(brandId);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        for (SizeConversion e : list) {
            if (goodsCategory.equals(e.getGoodsCategory()) 
                    && goodsSex.equals(e.getGoodsSex()) 
                    && originalSize.equals(e.getOriginalSize())) {
                return e.getNormalizedSize();
            }
        }
        return null;
    }

}
