package com.mulong.common.util.httpclient;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * BodyType
 * 
 * @author mulong
 * @data 2020-12-01 13:46:34
 */
@Getter
@AllArgsConstructor
public enum BodyType {
    /** form */
    FORM(1),
    /** json */
    JSON(2),
    /** xml */
    XML(3),

    ;

    private int code;

}
