package com.mulong.common.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson2.JSON;
import com.jd.open.api.sdk.request.order.PopOrderSearchRequest;
import com.jd.open.api.sdk.response.order.PopOrderSearchResponse;
import com.mulong.mall.Application;

import lombok.extern.slf4j.Slf4j;

/**
 * JdOpenClientTest
 * 
 * @author mulong
 * @data 2021-10-15 11:33:36
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Slf4j
public class JdOpenClientTest {
    @Autowired
    private JdOpenClient jdOpenClient;

    @Test
    public void testPopOrderSearchTest() throws Exception {
        PopOrderSearchRequest request=new PopOrderSearchRequest();
        request.setStartDate("2021-09-20 10:00:00");
        request.setEndDate("2021-10-15 14:00:00");
        request.setOrderState("FINISHED_L");
        request.setOptionalFields("itemInfoList,orderId,isShipmenttype,scDT,idSopShipmenttype,orderStartTime");
        request.setPage("1");
        request.setPageSize("20");
        request.setSortType(1);
        request.setDateType(0);
        PopOrderSearchResponse response = jdOpenClient.popOrderSearch(request);
        log.info(JSON.toJSONString(response));
    }

}
