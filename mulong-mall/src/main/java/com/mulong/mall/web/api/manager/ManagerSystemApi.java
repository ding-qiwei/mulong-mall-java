package com.mulong.mall.web.api.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.service.ManagerSystemService;

/**
 * ManagerSystemApi
 * 
 * @author mulong
 * @data 2021-06-21 13:59:03
 */
@RestController
@RequestMapping(value = "/api/manager/system")
public class ManagerSystemApi {
    @Autowired
    private ManagerSystemService managerSystemService;

}
