package com.mulong.mall.web.api.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.bo.distributor.*;
import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorAfterSalesService;

/**
 * DistributionAfterSalesApi
 * 
 * @author mulong
 * @data 2021-05-29 17:58:29
 */
@RestController
@RequestMapping(value = "/api/distributor/after-sales")
public class DistributorAfterSalesApi {
    @Autowired
    private DistributorAfterSalesService afterSalesService;

}
