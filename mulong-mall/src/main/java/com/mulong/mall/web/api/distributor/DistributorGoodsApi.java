package com.mulong.mall.web.api.distributor;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorGoodsService;
import com.mulong.common.domain.vo.BaseResult;

/**
 * GoodsApi
 * 
 * @author mulong
 * @data 2021-05-29 17:59:27
 */
@RestController
@RequestMapping(value = "/api/distributor/goods")
public class DistributorGoodsApi {
    @Autowired
    private DistributorGoodsService goodsService;

    /**
     * @author mulong
     */
    @GetMapping(value = "/new")
    public BaseResult<GetGoodsNewByPageResult> getNewByPage(
            @Valid GetGoodsNewByPageParam param) {
        return BaseResult.success(goodsService.getNewByPage(param));
    }

    /**
     * @author mulong 
     */
    @PostMapping(value = "/new/download")
    public ResponseEntity<byte[]> downloadNew() {
        return goodsService.downloadNew();
    }

}
