package com.mulong.mall.web.api.distributor;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.bo.distributor.*;
import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorHelpCenterService;
import com.mulong.common.domain.vo.BaseResult;

/**
 * HelpCenterApi
 * 
 * @author mulong
 * @data 2021-05-29 17:59:41
 */
@RestController
@RequestMapping(value = "/api/distributor/help-center")
public class DistributorHelpCenterApi {
    @Autowired
    private DistributorHelpCenterService helpCenterService;

    /**
     * 说明文档
     * 
     * @author mulong
     */
    @GetMapping(value = "/platform-document")
    public BaseResult<PlatformDocument> getPlatformDocument() {
        return BaseResult.success(helpCenterService.getPlatformDocument());
    }

    /**
     * 系统帮助
     * 
     * @author mulong
     */
    @GetMapping(value = "/system-help")
    public BaseResult<GetSystemHelpByPageResult> getSystemHelpByPage(
            @Valid GetSystemHelpByPageParam param) {
        return BaseResult.success(helpCenterService.getSystemHelpByPage(param));
    }

    /**
     * 系统帮助
     * 
     * @author mulong 
     */
    @GetMapping(value = "/system-help/{id}")
    public BaseResult<SystemHelp> getSystemHelpById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(helpCenterService.getSystemHelpById(id));
    }

    /**
     * 销售帮助
     * 
     * @author mulong
     */
    @GetMapping(value = "/sales-help")
    public BaseResult<GetSalesHelpByPageResult> getSalesHelpByPage(
            @Valid GetSalesHelpByPageParam param) {
        return BaseResult.success(helpCenterService.getSalesHelpByPage(param));
    }

    /**
     * 销售帮助
     * 
     * @author mulong 
     */
    @GetMapping(value = "/sales-help/{id}")
    public BaseResult<SalesHelp> getSalesHelpById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(helpCenterService.getSalesHelpById(id));
    }

    /**
     * 其他帮助
     * 
     * @author mulong
     */
    @GetMapping(value = "/others-help")
    public BaseResult<GetOthersHelpByPageResult> getOthersHelpByPage(
            @Valid GetOthersHelpByPageParam param) {
        return BaseResult.success(helpCenterService.getOthersHelpByPage(param));
    }

    /**
     * 其他帮助
     * 
     * @author mulong 
     */
    @GetMapping(value = "/others-help/{id}")
    public BaseResult<OthersHelp> getOthersHelpById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(helpCenterService.getOthersHelpById(id));
    }
 
    /**
     * 平台策略
     * 
     * @author mulong
     */
    @GetMapping(value = "/platform-strategy")
    public BaseResult<GetPlatformStrategyByPageResult> getPlatformStrategyByPage(
            @Valid GetPlatformStrategyByPageParam param) {
        return BaseResult.success(helpCenterService.getPlatformStrategyByPage(param));
    }

    /**
     * 平台策略
     * 
     * @author mulong 
     */
    @GetMapping(value = "/platform-strategy/{id}")
    public BaseResult<PlatformStrategy> getPlatformStrategyById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(helpCenterService.getPlatformStrategyById(id));
    }

}
