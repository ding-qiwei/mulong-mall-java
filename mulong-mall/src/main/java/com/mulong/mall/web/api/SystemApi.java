package com.mulong.mall.web.api;

import java.util.*;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mulong.mall.domain.bo.*;
import com.mulong.mall.domain.param.*;
import com.mulong.mall.domain.result.*;
import com.mulong.mall.service.SystemService;
import com.mulong.common.domain.vo.BaseResult;

/**
 * SystemApi
 * 
 * @author mulong
 * @data 2021-03-30 21:57:53
 */
@RestController
@RequestMapping(value = "/api/system")
public class SystemApi {
    @Autowired
    private SystemService systemService;

    /**
     * 查询网关账号
     * 
     * @author mulong
     */
    @GetMapping(value = "/gateway-account")
    public BaseResult<GetGatewayAccountByPageResult> getGatewayAccountByPage(
            @Valid GetGatewayAccountByPageParam param) {
        return BaseResult.success(systemService.getGatewayAccountByPage(param));
    }

    /**
     * 添加网关账号
     * 
     * @author mulong 
     */
    @PostMapping(value = "/gateway-account")
    public BaseResult<GatewayAccount> addGatewayAccount(
            @RequestBody GatewayAccount gatewayAccount) {
        return BaseResult.success(systemService.addGatewayAccount(gatewayAccount));
    }

    /**
     * 查询网关账号
     * 
     * @author mulong 
     */
    @GetMapping(value = "/gateway-account/{id}")
    public BaseResult<GatewayAccount> getGatewayAccountById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(systemService.getGatewayAccountById(id));
    }

    /**
     * 更新网关账号
     * 
     * @author mulong 
     */
    @PutMapping(value = "/gateway-account/{id}")
    public BaseResult<GatewayAccount> putGatewayAccountById(
            @PathVariable(name = "id") Integer id,
            @RequestBody GatewayAccount gatewayAccount) {
        return BaseResult.success(systemService.putGatewayAccountById(id, gatewayAccount));
    }

    /**
     * 删除网关账号
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/gateway-account/{id}")
    public BaseResult<GatewayAccount> deleteGatewayAccountById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(systemService.deleteGatewayAccountById(id));
    }

    /**
     * 查询数据字典
     * 
     * @author mulong
     */
    @GetMapping(value = "/data-dict")
    public BaseResult<GetDataDictByPageResult> getDataDictByPage(
            @Valid GetDataDictByPageParam param) {
        return BaseResult.success(systemService.getDataDictByPage(param));
    }

    /**
     * 添加数据字典
     * 
     * @author mulong 
     */
    @PostMapping(value = "/data-dict")
    public BaseResult<DataDict> addDataDict(
            @RequestBody DataDict dataDict) {
        return BaseResult.success(systemService.addDataDict(dataDict));
    }

    /**
     * 查询数据字典
     * 
     * @author mulong 
     */
    @GetMapping(value = "/data-dict/{id}")
    public BaseResult<DataDict> getDataDictById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(systemService.getDataDictById(id));
    }

    /**
     * 更新数据字典
     * 
     * @author mulong 
     */
    @PutMapping(value = "/data-dict/{id}")
    public BaseResult<DataDict> putDataDictById(
            @PathVariable(name = "id") Integer id,
            @RequestBody DataDict dataDict) {
        return BaseResult.success(systemService.putDataDictById(id, dataDict));
    }

    /**
     * 删除数据字典
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/data-dict/{id}")
    public BaseResult<DataDict> deleteDataDictById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(systemService.deleteDataDictById(id));
    }

    /**
     * 获取数据字典
     * 
     * @author mulong
     */
    @PostMapping(value = "/data-dict/fetch")
    public BaseResult<List<DataDict>> fetchDataDicts(
            @RequestBody List<String> dataDictCodes) {
        return BaseResult.success(systemService.fetchDataDicts(dataDictCodes));
    }

    /**
     * 获取网关账号权限信息
     * 
     * @author mulong
     */
    @GetMapping(value = "/gateway-account/info")
    public BaseResult<GetUserInfoResult> getInfo() {
        return BaseResult.success(systemService.getInfo());
    }

    /**
     * 更新网关账号密码
     * 
     * @author mulong
     */
    @PostMapping(value = "/gateway-account/update-password")
    public BaseResult<?> updatePassword(
            @Valid UpdateGetwayAccountPasswordParam param) {
        systemService.updatePassword(param);
        return BaseResult.success(null);
    }

    /**
     * 上传文件
     * 
     * @author mulong
     */
    @PostMapping(value = "/upload/file")
    public BaseResult<UploadFileResult> uploadFile(
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(systemService.uploadFile(file));
    }

    /**
     * 上传图片
     * 
     * @author mulong
     */
    @PostMapping(value = "/upload/img")
    public BaseResult<UploadImgResult> uploadImg(
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(systemService.uploadImg(file));
    }

    /**
     * 获取枚举
     * 
     * @author mulong
     */
    @PostMapping(value = "/fetch-enum")
    public BaseResult<List<SystemEnum>> fetchEnum(
            @RequestBody List<String> enumCodes) {
        return BaseResult.success(systemService.fetchEnum(enumCodes));
    }

    /**
     * EnumBrand
     * 
     * @author mulong
     */
    @GetMapping(value = "/enum/brand")
    public BaseResult<List<EnumBrand>> getEnumBrand() {
        return BaseResult.success(systemService.getEnumBrand());
    }

    /**
     * EnumDeliveryType
     * 
     * @author mulong
     */
    @GetMapping(value = "/enum/delivery-type")
    public BaseResult<List<EnumDeliveryType>> getEnumDeliveryType() {
        return BaseResult.success(systemService.getEnumDeliveryType());
    }

    /**
     * EnumChannel
     * 
     * @author mulong
     */
    @GetMapping(value = "/enum/supplier-channel")
    public BaseResult<List<EnumSupplierChannel>> getEnumSupplierChannel() {
        return BaseResult.success(systemService.getEnumSupplierChannel());
    }

}
