package com.mulong.mall.web.api.distributor;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.bo.distributor.*;
import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.param.manager.DownloadCustomerOrderParam;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorDistributionService;
import com.mulong.common.domain.vo.BaseResult;

/**
 * DistributionApi
 * 
 * @author mulong
 * @data 2021-05-29 17:56:07
 */
@RestController
@RequestMapping(value = "/api/distributor/distribution")
public class DistributorDistributionApi {
    @Autowired
    private DistributorDistributionService distributionService;

    /**
     * 查询商品库存
     * 
     * @author mulong
     */
    @GetMapping(value = "/product-inventory")
    public BaseResult<DistributionProductInventoryResult> getProductInventory(
            @Valid DistributionProductInventoryParam param) {
        return BaseResult.success(distributionService.getProductInventory(param));
    }

    /**
     * 下单
     * 
     * @author mulong 
     */
    @PostMapping(value = "/booking")
    public BaseResult<DistributionBookingResult> booking(
            @RequestBody DistributionBookingParam param) {
        return BaseResult.success(distributionService.booking(param));
    }

    /**
     * @author mulong
     */
    @GetMapping(value = "/groupon")
    public BaseResult<GetDistributionGrouponByPageResult> getGrouponByPage(
            @Valid GetDistributionGrouponByPageParam param) {
        return BaseResult.success(distributionService.getGrouponByPage(param));
    }

    /**
     * @author mulong 
     */
    @GetMapping(value = "/groupon/{id}")
    public BaseResult<DistributionGroupon> getGrouponById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(distributionService.getGrouponById(id));
    }

    /**
     * 订单列表
     * 
     * @author mulong
     */
    @GetMapping(value = "/order")
    public BaseResult<GetDistributionOrderByPageResult> getOrderByPage(
            @Valid GetDistributionOrderByPageParam param) {
        return BaseResult.success(distributionService.getOrderByPage(param));
    }

    /**
     * 下载订单
     * 
     * @author mulong 
     */
    @PostMapping(value = "/order/download")
    public ResponseEntity<byte[]> downloadOrder(
            @RequestBody DownloadOrderParam param) {
        return distributionService.downloadOrder(param);
    }

    /**
     * 付款全部未付款订单
     * 
     * @author mulong 
     */
    @PostMapping(value = "/order/pay-all")
    public BaseResult<DistributionPayAllOrderResult> payAllOrder() {
        return BaseResult.success(distributionService.payAllOrder());
    }

    /**
     * 付款选中订单
     * 
     * @author mulong 
     */
    @PostMapping(value = "/order/pay-selected")
    public BaseResult<DistributionPaySelectedOrderResult> paySelectedOrder(
            @RequestBody DistributionPaySelectedOrderParam param) {
        return BaseResult.success(distributionService.paySelectedOrder(param));
    }

    /**
     * @author mulong 
     */
    @GetMapping(value = "/order/{id}")
    public BaseResult<DistributionOrder> getOrderById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(distributionService.getOrderById(id));
    }

    /**
     * @author mulong 
     */
    @PutMapping(value = "/order/{id}")
    public BaseResult<DistributionOrder> putOrderById(
            @PathVariable(name = "id") Long id,
            @RequestBody DistributionOrder order) {
        return BaseResult.success(distributionService.putOrderById(id, order));
    }

    /**
     * @author mulong
     */
    @GetMapping(value = "/trade")
    public BaseResult<GetDistributionTradeByPageResult> getTradeByPage(
            @Valid GetDistributionTradeByPageParam param) {
        return BaseResult.success(distributionService.getTradeByPage(param));
    }

    /**
     * @author mulong 
     */
    @GetMapping(value = "/trade/{id}")
    public BaseResult<DistributionTrade> getTradeById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(distributionService.getTradeById(id));
    }

    /**
     * @author mulong 
     */
    @PutMapping(value = "/trade/{id}")
    public BaseResult<DistributionTrade> putTradeById(
            @PathVariable(name = "id") Long id,
            @RequestBody DistributionTrade trade) {
        return BaseResult.success(distributionService.putTradeById(id, trade));
    }

    /**
     * @author mulong
     */
    @GetMapping(value = "/discount")
    public BaseResult<GetDistributionDiscountByPageResult> getDiscountByPage(
            @Valid GetDistributionDiscountByPageParam param) {
        return BaseResult.success(distributionService.getDiscountByPage(param));
    }

    /**
     * @author mulong
     */
    @GetMapping(value = "/delivery-charge")
    public BaseResult<GetDistributionDeliveryChargeByPageResult> getDeliveryChargeByPage(
            @Valid GetDistributionDeliveryChargeByPageParam param) {
        return BaseResult.success(distributionService.getDeliveryChargeByPage(param));
    }

    /**
     * @author mulong
     */
    @GetMapping(value = "/goods-demand")
    public BaseResult<GetDistributionGoodsDemandByPageResult> getGoodsDemandByPage(
            @Valid GetDistributionGoodsDemandByPageParam param) {
        return BaseResult.success(distributionService.getGoodsDemandByPage(param));
    }

    /**
     * @author mulong 
     */
    @PostMapping(value = "/goods-demand")
    public BaseResult<DistributionGoodsDemand> addGoodsDemand(
            @RequestBody DistributionGoodsDemand goodsDemand) {
        return BaseResult.success(distributionService.addGoodsDemand(goodsDemand));
    }

    /**
     * @author mulong 
     */
    @GetMapping(value = "/goods-demand/{id}")
    public BaseResult<DistributionGoodsDemand> getGoodsDemandById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(distributionService.getGoodsDemandById(id));
    }

    /**
     * @author mulong 
     */
    @PutMapping(value = "/goods-demand/{id}")
    public BaseResult<DistributionGoodsDemand> putGoodsDemandById(
            @PathVariable(name = "id") Long id,
            @RequestBody DistributionGoodsDemand goodsDemand) {
        return BaseResult.success(distributionService.putGoodsDemandById(id, goodsDemand));
    }

    /**
     * @author mulong
     */
    @GetMapping(value = "/my-favorite")
    public BaseResult<GetDistributionMyFavoriteByPageResult> getMyFavoriteByPage(
            @Valid GetDistributionMyFavoriteByPageParam param) {
        return BaseResult.success(distributionService.getMyFavoriteByPage(param));
    }

    /**
     * @author mulong 
     */
    @PostMapping(value = "/my-favorite")
    public BaseResult<DistributionMyFavorite> addMyFavorite(
            @RequestBody DistributionMyFavorite myFavorite) {
        return BaseResult.success(distributionService.addMyFavorite(myFavorite));
    }

    /**
     * @author mulong 
     */
    @DeleteMapping(value = "/my-favorite/{id}")
    public BaseResult<DistributionMyFavorite> deleteMyFavoriteById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(distributionService.deleteMyFavoriteById(id));
    }

}
