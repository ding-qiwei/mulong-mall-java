package com.mulong.mall.web.api.distributor;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorPresaleService;
import com.mulong.common.domain.vo.BaseResult;

/**
 * PresaleApi
 * 
 * @author mulong
 * @data 2021-05-29 17:59:15
 */
@RestController
@RequestMapping(value = "/api/distributor/presale")
public class DistributorPresaleApi {
    @Autowired
    private DistributorPresaleService presaleService;

    /**
     * 预售订单
     * 
     * @author mulong
     */
    @GetMapping(value = "/order")
    public BaseResult<GetPresaleOrderByPageResult> getOrderByPage(
            @Valid GetPresaleOrderByPageParam param) {
        return BaseResult.success(presaleService.getOrderByPage(param));
    }

    /**
     * 预售下单
     * 
     * @author mulong
     */
    @GetMapping(value = "/booking")
    public BaseResult<GetPresaleBookingByPageResult> getBookingByPage(
            @Valid GetPresaleBookingByPageParam param) {
        return BaseResult.success(presaleService.getBookingByPage(param));
    }

}
