package com.mulong.mall.web.api;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.param.OauthTokenParam;
import com.mulong.mall.domain.result.OauthTokenResult;
import com.mulong.mall.service.OauthService;

/**
 * OauthApi
 * 
 * @author mulong
 * @data 2021-08-10 18:00:15
 */
@RestController
@RequestMapping(value = "/oauth")
public class OauthApi {
    @Autowired
    private OauthService oauthService;

    /**
     * 获取token
     * 
     * @author mulong
     */
    @RequestMapping(value = "/token", method = {RequestMethod.GET, RequestMethod.POST})
    public BaseResult<OauthTokenResult> token(@Valid OauthTokenParam param) {
        return BaseResult.success(oauthService.token(param));
    }

}
