package com.mulong.mall.web.api.manager;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.bo.manager.FinanceAccount;
import com.mulong.mall.domain.param.manager.GetFinanceAccountByPageParam;
import com.mulong.mall.domain.result.manager.GetFinanceAccountByPageResult;
import com.mulong.mall.service.ManagerFinanceService;

/**
 * ManagerFinanceApi
 * 
 * @author mulong
 * @data 2021-06-21 13:57:17
 */
@RestController
@RequestMapping(value = "/api/manager/finance")
public class ManagerFinanceApi {
    @Autowired
    private ManagerFinanceService managerFinanceService;

    /**
     * 查询资金账户
     * 
     * @author mulong
     */
    @GetMapping(value = "/account")
    public BaseResult<GetFinanceAccountByPageResult> getFinanceAccountByPage(
            @Valid GetFinanceAccountByPageParam param) {
        return BaseResult.success(managerFinanceService.getFinanceAccountByPage(param));
    }

    /**
     * 添加资金账户
     * 
     * @author mulong 
     */
    @PostMapping(value = "/account")
    public BaseResult<FinanceAccount> addFinanceAccount(
            @RequestBody FinanceAccount account) {
        return BaseResult.success(managerFinanceService.addFinanceAccount(account));
    }

    /**
     * 查询资金账户
     * 
     * @author mulong 
     */
    @GetMapping(value = "/account/{id}")
    public BaseResult<FinanceAccount> getFinanceAccountById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerFinanceService.getFinanceAccountById(id));
    }

    /**
     * 更新资金账户
     * 
     * @author mulong 
     */
    @PutMapping(value = "/account/{id}")
    public BaseResult<FinanceAccount> putFinanceAccountById(
            @PathVariable(name = "id") Integer id,
            @RequestBody FinanceAccount account) {
        return BaseResult.success(managerFinanceService.putFinanceAccountById(id, account));
    }

    /**
     * 删除资金账户
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/account/{id}")
    public BaseResult<FinanceAccount> deleteFinanceAccountById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerFinanceService.deleteFinanceAccountById(id));
    }

}
