package com.mulong.mall.web.api.manager;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;
import com.mulong.mall.service.ManagerBusinessService;

/**
 * ManagerBusinessApi
 * 
 * @author mulong
 * @data 2021-06-21 13:59:34
 */
@RestController
@RequestMapping(value = "/api/manager/business")
public class ManagerBusinessApi {
    @Autowired
    private ManagerBusinessService managerBusinessService;

    /**
     * 查询快递方式
     * 
     * @author mulong
     */
    @GetMapping(value = "/delivery-type")
    public BaseResult<GetDeliveryTypeByPageResult> getDeliveryTypeByPage(
            @Valid GetDeliveryTypeByPageParam param) {
        return BaseResult.success(managerBusinessService.getDeliveryTypeByPage(param));
    }

    /**
     * 添加快递方式
     * 
     * @author mulong 
     */
    @PostMapping(value = "/delivery-type")
    public BaseResult<DeliveryType> addDeliveryType(
            @RequestBody DeliveryType deliveryType) {
        return BaseResult.success(managerBusinessService.addDeliveryType(deliveryType));
    }

    /**
     * 查询快递方式
     * 
     * @author mulong 
     */
    @GetMapping(value = "/delivery-type/{id}")
    public BaseResult<DeliveryType> getDeliveryTypeById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerBusinessService.getDeliveryTypeById(id));
    }

    /**
     * 更新快递方式
     * 
     * @author mulong 
     */
    @PutMapping(value = "/delivery-type/{id}")
    public BaseResult<DeliveryType> putDeliveryTypeById(
            @PathVariable(name = "id") Integer id,
            @RequestBody DeliveryType deliveryType) {
        return BaseResult.success(managerBusinessService.putDeliveryTypeById(id, deliveryType));
    }

    /**
     * 删除快递方式
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/delivery-type/{id}")
    public BaseResult<DeliveryType> deleteDeliveryTypeById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerBusinessService.deleteDeliveryTypeById(id));
    }

    /**
     * 查询邮费
     * 
     * @author mulong
     */
    @GetMapping(value = "/delivery-charge")
    public BaseResult<GetDeliveryChargeByPageResult> getDeliveryChargeByPage(
            @Valid GetDeliveryChargeByPageParam param) {
        return BaseResult.success(managerBusinessService.getDeliveryChargeByPage(param));
    }

    /**
     * 添加邮费
     * 
     * @author mulong 
     */
    @PostMapping(value = "/delivery-charge")
    public BaseResult<DeliveryCharge> addDeliveryCharge(
            @RequestBody DeliveryCharge deliveryCharge) {
        return BaseResult.success(managerBusinessService.addDeliveryCharge(deliveryCharge));
    }

    /**
     * 查询邮费
     * 
     * @author mulong 
     */
    @GetMapping(value = "/delivery-charge/{id}")
    public BaseResult<DeliveryCharge> getDeliveryChargeById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerBusinessService.getDeliveryChargeById(id));
    }

    /**
     * 更新邮费
     * 
     * @author mulong 
     */
    @PutMapping(value = "/delivery-charge/{id}")
    public BaseResult<DeliveryCharge> putDeliveryChargeById(
            @PathVariable(name = "id") Long id,
            @RequestBody DeliveryCharge deliveryCharge) {
        return BaseResult.success(managerBusinessService.putDeliveryChargeById(id, deliveryCharge));
    }

    /**
     * 删除邮费
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/delivery-charge/{id}")
    public BaseResult<DeliveryCharge> deleteDeliveryChargeById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerBusinessService.deleteDeliveryChargeById(id));
    }

    /**
     * 批量添加邮费
     * 
     * @author mulong 
     */
    @PostMapping(value = "/delivery-charge/add-batch")
    public BaseResult<AddBatchDeliveryChargeResult> addBatchDeliveryCharge(
            @RequestBody AddBatchDeliveryChargeParam param) {
        return BaseResult.success(managerBusinessService.addBatchDeliveryCharge(param));
    }

    /**
     * 查询品牌
     * 
     * @author mulong
     */
    @GetMapping(value = "/brand")
    public BaseResult<GetBrandByPageResult> getBrandByPage(
            @Valid GetBrandByPageParam param) {
        return BaseResult.success(managerBusinessService.getBrandByPage(param));
    }

    /**
     * 添加品牌
     * 
     * @author mulong 
     */
    @PostMapping(value = "/brand")
    public BaseResult<Brand> addBrand(
            @RequestBody Brand brand) {
        return BaseResult.success(managerBusinessService.addBrand(brand));
    }

    /**
     * 查询品牌
     * 
     * @author mulong 
     */
    @GetMapping(value = "/brand/{id}")
    public BaseResult<Brand> getBrandById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerBusinessService.getBrandById(id));
    }

    /**
     * 更新品牌
     * 
     * @author mulong 
     */
    @PutMapping(value = "/brand/{id}")
    public BaseResult<Brand> putBrandById(
            @PathVariable(name = "id") Integer id,
            @RequestBody Brand brand) {
        return BaseResult.success(managerBusinessService.putBrandById(id, brand));
    }

    /**
     * 删除品牌
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/brand/{id}")
    public BaseResult<Brand> deleteBrandById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerBusinessService.deleteBrandById(id));
    }

}
