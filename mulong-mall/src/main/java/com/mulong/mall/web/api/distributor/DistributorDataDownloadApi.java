package com.mulong.mall.web.api.distributor;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorDataDownloadService;
import com.mulong.common.domain.vo.BaseResult;

/**
 * DataDownloadApi
 * 
 * @author mulong
 * @data 2021-05-29 17:58:57
 */
@RestController
@RequestMapping(value = "/api/distributor/data-download")
public class DistributorDataDownloadApi {
    @Autowired
    private DistributorDataDownloadService dataDownloadService;

    /**
     * 库存渠道列表
     * 
     * @author mulong
     */
    @GetMapping(value = "/inventory/channel")
    public BaseResult<GetDataDownloadInventoryChannelByPageResult> getInventoryChannelByPage(
            @Valid GetDataDownloadInventoryChannelByPageParam param) {
        return BaseResult.success(dataDownloadService.getInventoryChannelByPage(param));
    }

    /**
     * 库存下载
     * 
     * @author mulong
     */
    @PostMapping(value = "/inventory/download")
    public ResponseEntity<byte[]> downloadInventory(@RequestBody DownloadDataDownloadInventoryParam param) {
        return dataDownloadService.downloadInventory(param);
    }

    /**
     * 新增款式推荐包列表
     * 
     * @author mulong
     */
    @GetMapping(value = "/recommendation")
    public BaseResult<GetDataDownloadRecommendationByPageResult> getRecommendationByPage(
            @Valid GetDataDownloadRecommendationByPageParam param) {
        return BaseResult.success(dataDownloadService.getRecommendationByPage(param));
    }

    /**
     * 新增款式推荐包下载
     * 
     * @author mulong
     */
    @PostMapping(value = "/recommendation/download")
    public ResponseEntity<byte[]> downloadRecommendation(@RequestBody DownloadDataDownloadRecommendationParam param) {
        return dataDownloadService.downloadRecommendation(param);
    }

    /**
     * 数据包列表
     * 
     * @author mulong
     */
    @GetMapping(value = "/data-package")
    public BaseResult<GetDataDownloadDataPackageByPageResult> getDataPackageByPage(
            @Valid GetDataDownloadDataPackageByPageParam param) {
        return BaseResult.success(dataDownloadService.getDataPackageByPage(param));
    }

    /**
     * 数据包下载
     * 
     * @author mulong
     */
    @PostMapping(value = "/data-package/download")
    public ResponseEntity<byte[]> downloadDataPackage(@RequestBody DownloadDataDownloadDataPackageParam param) {
        return dataDownloadService.downloadDataPackage(param);
    }

    /**
     * 商品数据列表
     * 
     * @author mulong
     */
    @GetMapping(value = "/goods")
    public BaseResult<GetDataDownloadGoodsByPageResult> getGoodsByPage(
            @Valid GetDataDownloadGoodsByPageParam param) {
        return BaseResult.success(dataDownloadService.getGoodsByPage(param));
    }

    /**
     * 商品数据下载
     * 
     * @author mulong
     */
    @PostMapping(value = "/goods/download")
    public ResponseEntity<byte[]> downloadGoods(@RequestBody DownloadDataDownloadGoodsParam param) {
        return dataDownloadService.downloadGoods(param);
    }

}
