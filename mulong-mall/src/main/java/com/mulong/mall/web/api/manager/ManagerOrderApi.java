package com.mulong.mall.web.api.manager;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;
import com.mulong.mall.service.ManagerOrderService;

/**
 * ManagerOrderApi
 * 
 * @author mulong
 * @data 2021-06-21 13:56:58
 */
@RestController
@RequestMapping(value = "/api/manager/order")
public class ManagerOrderApi {
    @Autowired
    private ManagerOrderService managerOrderService;

    /**
     * 客户订单列表
     * 
     * @author mulong
     */
    @GetMapping(value = "/customer-order")
    public BaseResult<GetCustomerOrderByPageResult> getCustomerOrderByPage(
            @Valid GetCustomerOrderByPageParam param) {
        return BaseResult.success(managerOrderService.getCustomerOrderByPage(param));
    }

    /**
     * 下载客户订单
     * 
     * @author mulong 
     */
    @PostMapping(value = "/customer-order/download")
    public ResponseEntity<byte[]> downloadCustomerOrder(
            @RequestBody DownloadCustomerOrderParam param) {
        return managerOrderService.downloadCustomerOrder(param);
    }

    /**
     * 下载客户已付款订单
     * 
     * @author mulong 
     */
    @PostMapping(value = "/customer-order/paid/download")
    public ResponseEntity<byte[]> downloadCustomerPaidOrder() {
        return managerOrderService.downloadCustomerPaidOrder();
    }

}
