package com.mulong.mall.web.api.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.service.ManagerCustomerService;

/**
 * ManagerCustomerApi
 * 
 * @author mulong
 * @data 2021-06-21 13:57:50
 */
@RestController
@RequestMapping(value = "/api/manager/customer")
public class ManagerCustomerApi {
    @Autowired
    private ManagerCustomerService managerCustomerService;

}
