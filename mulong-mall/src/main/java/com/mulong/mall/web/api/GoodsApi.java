package com.mulong.mall.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.service.GoodsService;

/**
 * GoodsApi
 * 
 * @author mulong
 * @data 2021-06-21 13:40:35
 */
@RestController
@RequestMapping(value = "/api/goods")
public class GoodsApi {
    @Autowired
    private GoodsService goodsService;

}
