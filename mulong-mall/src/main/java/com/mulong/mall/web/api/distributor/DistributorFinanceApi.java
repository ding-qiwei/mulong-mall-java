package com.mulong.mall.web.api.distributor;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorFinanceService;
import com.mulong.common.domain.vo.BaseResult;

/**
 * FinanceApi
 * 
 * @author mulong
 * @data 2021-05-29 17:58:43
 */
@RestController
@RequestMapping(value = "/api/distributor/finance")
public class DistributorFinanceApi {
    @Autowired
    private DistributorFinanceService financeService;

    /**
     * @author mulong
     */
    @GetMapping(value = "/account-recharge")
    public BaseResult<GetFinanceAccountRechargeByPageResult> getAccountRechargeByPage(
            @Valid GetFinanceAccountRechargeByPageParam param) {
        return BaseResult.success(financeService.getAccountRechargeByPage(param));
    }

    /**
     * @author mulong 
     */
    @PostMapping(value = "/account-recharge/download")
    public ResponseEntity<byte[]> downloadAccountRecharge() {
        return financeService.downloadAccountRecharge();
    }

    /**
     * @author mulong
     */
    @GetMapping(value = "/account-transaction")
    public BaseResult<GetFinanceAccountTransactionByPageResult> getAccountTransactionByPage(
            @Valid GetFinanceAccountTransactionByPageParam param) {
        return BaseResult.success(financeService.getAccountTransactionByPage(param));
    }

    /**
     * @author mulong 
     */
    @PostMapping(value = "/account-transaction/download")
    public ResponseEntity<byte[]> downloadAccountTransaction() {
        return financeService.downloadAccountTransaction();
    }

}
