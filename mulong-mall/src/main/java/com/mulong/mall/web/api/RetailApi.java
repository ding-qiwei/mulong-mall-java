package com.mulong.mall.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.service.RetailService;

/**
 * RetailApi
 * 
 * @author mulong
 * @data 2021-06-21 13:46:42
 */
@RestController
@RequestMapping(value = "/api/retail")
public class RetailApi {
    @Autowired
    private RetailService retailService;

}
