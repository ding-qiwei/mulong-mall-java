package com.mulong.mall.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.service.ServiceService;

/**
 * ServiceApi
 * 
 * @author mulong
 * @data 2021-06-21 13:46:58
 */
@RestController
@RequestMapping(value = "/api/service")
public class ServiceApi {
    @Autowired
    private ServiceService serviceService;

}
