package com.mulong.mall.web.api.manager;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;
import com.mulong.mall.service.ManagerInfoService;

/**
 * ManagerInfoApi
 * 
 * @author mulong
 * @data 2021-06-21 13:58:35
 */
@RestController
@RequestMapping(value = "/api/manager/info")
public class ManagerInfoApi {
    @Autowired
    private ManagerInfoService managerInfoService;

    /**
     * 查询帮助
     * 
     * @author mulong
     */
    @GetMapping(value = "/help")
    public BaseResult<GetHelpByPageResult> getHelpByPage(
            @Valid GetHelpByPageParam param) {
        return BaseResult.success(managerInfoService.getHelpByPage(param));
    }

    /**
     * 添加帮助
     * 
     * @author mulong 
     */
    @PostMapping(value = "/help")
    public BaseResult<HelpCenterRecord> addHelp(
            @RequestBody HelpCenterRecord help) {
        return BaseResult.success(managerInfoService.addHelp(help));
    }

    /**
     * 查询帮助
     * 
     * @author mulong 
     */
    @GetMapping(value = "/help/{id}")
    public BaseResult<HelpCenterRecord> getHelpById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerInfoService.getHelpById(id));
    }

    /**
     * 更新帮助
     * 
     * @author mulong 
     */
    @PutMapping(value = "/help/{id}")
    public BaseResult<HelpCenterRecord> putHelpById(
            @PathVariable(name = "id") Integer id,
            @RequestBody HelpCenterRecord help) {
        return BaseResult.success(managerInfoService.putHelpById(id, help));
    }

    /**
     * 删除帮助
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/help/{id}")
    public BaseResult<HelpCenterRecord> deleteHelpById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerInfoService.deleteHelpById(id));
    }

}
