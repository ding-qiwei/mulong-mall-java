package com.mulong.mall.web.api;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.bo.supplier.*;
import com.mulong.mall.domain.param.supplier.*;
import com.mulong.mall.domain.result.supplier.*;
import com.mulong.mall.service.SupplierService;

/**
 * SupplierApi
 * 
 * @author mulong
 * @data 2021-06-21 13:44:05
 */
@RestController
@RequestMapping(value = "/api/supplier")
public class SupplierApi {
    @Autowired
    private SupplierService supplierService;

    /**
     * 查询库存
     * 
     * @author mulong
     */
    @GetMapping(value = "/inventory")
    public BaseResult<GetInventoryByPageResult> getInventoryByPage(
            @Valid GetInventoryByPageParam param) {
        return BaseResult.success(supplierService.getInventoryByPage(param));
    }

    /**
     * 添加库存
     * 
     * @author mulong 
     */
    @PostMapping(value = "/inventory")
    public BaseResult<Inventory> addInventory(
            @RequestBody Inventory inventory) {
        return BaseResult.success(supplierService.addInventory(inventory));
    }

    /**
     * 查询库存
     * 
     * @author mulong 
     */
    @GetMapping(value = "/inventory/{id}")
    public BaseResult<Inventory> getInventoryById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(supplierService.getInventoryById(id));
    }

    /**
     * 更新库存
     * 
     * @author mulong 
     */
    @PutMapping(value = "/inventory/{id}")
    public BaseResult<Inventory> putInventoryById(
            @PathVariable(name = "id") Long id,
            @RequestBody Inventory inventory) {
        return BaseResult.success(supplierService.putInventoryById(id, inventory));
    }

    /**
     * 删除库存
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/inventory/{id}")
    public BaseResult<Inventory> deleteInventoryById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(supplierService.deleteInventoryById(id));
    }

    /**
     * 上传库存
     * 
     * @author mulong
     */
    @PostMapping(value = "/inventory/upload")
    public BaseResult<UploadInventoryResult> uploadInventory(
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(supplierService.uploadInventory(file));
    }

}
