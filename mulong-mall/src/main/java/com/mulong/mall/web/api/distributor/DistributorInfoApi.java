package com.mulong.mall.web.api.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mulong.mall.domain.bo.distributor.*;
import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorInfoService;

/**
 * DistributorInfoApi
 * 
 * @author mulong
 * @data 2021-06-21 18:06:23
 */
@RestController
@RequestMapping(value = "/api/distributor/info")
public class DistributorInfoApi {
    @Autowired
    private DistributorInfoService infoService;

}
