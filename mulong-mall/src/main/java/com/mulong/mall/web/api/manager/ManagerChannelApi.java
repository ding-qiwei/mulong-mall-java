package com.mulong.mall.web.api.manager;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;
import com.mulong.mall.service.ManagerChannelService;

/**
 * ManagerChannelApi
 * 
 * @author mulong
 * @data 2021-06-21 13:57:31
 */
@RestController
@RequestMapping(value = "/api/manager/channel")
public class ManagerChannelApi {
    @Autowired
    private ManagerChannelService managerChannelService;

    /**
     * 查询渠道
     * 
     * @author mulong
     */
    @GetMapping(value = "/supplier")
    public BaseResult<GetChannelSupplierByPageResult> getSupplierByPage(
            @Valid GetChannelSupplierByPageParam param) {
        return BaseResult.success(managerChannelService.getSupplierByPage(param));
    }

    /**
     * 添加渠道
     * 
     * @author mulong 
     */
    @PostMapping(value = "/supplier")
    public BaseResult<ChannelSupplier> addSupplier(
            @RequestBody ChannelSupplier supplier) {
        return BaseResult.success(managerChannelService.addSupplier(supplier));
    }

    /**
     * 查询渠道
     * 
     * @author mulong 
     */
    @GetMapping(value = "/supplier/{id}")
    public BaseResult<ChannelSupplier> getSupplierById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerChannelService.getSupplierById(id));
    }

    /**
     * 更新渠道
     * 
     * @author mulong 
     */
    @PutMapping(value = "/supplier/{id}")
    public BaseResult<ChannelSupplier> putSupplierById(
            @PathVariable(name = "id") Integer id,
            @RequestBody ChannelSupplier supplier) {
        return BaseResult.success(managerChannelService.putSupplierById(id, supplier));
    }

    /**
     * 删除渠道
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/supplier/{id}")
    public BaseResult<ChannelSupplier> deleteSupplierById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerChannelService.deleteSupplierById(id));
    }

    /**
     * 查询渠道折扣
     * 
     * @author mulong
     */
    @GetMapping(value = "/supplier/discount")
    public BaseResult<GetChannelSupplierDiscountByPageResult> getSupplierDiscountByPage(
            @Valid GetChannelSupplierDiscountByPageParam param) {
        return BaseResult.success(managerChannelService.getSupplierDiscountByPage(param));
    }

    /**
     * 添加渠道折扣
     * 
     * @author mulong 
     */
    @PostMapping(value = "/supplier/discount")
    public BaseResult<ChannelSupplierDiscount> addSupplierDiscount(
            @RequestBody ChannelSupplierDiscount supplierDiscount) {
        return BaseResult.success(managerChannelService.addSupplierDiscount(supplierDiscount));
    }

    /**
     * 查询渠道折扣
     * 
     * @author mulong 
     */
    @GetMapping(value = "/supplier/discount/{id}")
    public BaseResult<ChannelSupplierDiscount> getSupplierDiscountById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerChannelService.getSupplierDiscountById(id));
    }

    /**
     * 更新渠道折扣
     * 
     * @author mulong 
     */
    @PutMapping(value = "/supplier/discount/{id}")
    public BaseResult<ChannelSupplierDiscount> putSupplierDiscountById(
            @PathVariable(name = "id") Long id,
            @RequestBody ChannelSupplierDiscount supplierDiscount) {
        return BaseResult.success(managerChannelService.putSupplierDiscountById(id, supplierDiscount));
    }

    /**
     * 删除渠道折扣
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/supplier/discount/{id}")
    public BaseResult<ChannelSupplierDiscount> deleteSupplierDiscountById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerChannelService.deleteSupplierDiscountById(id));
    }

    /**
     * 查询渠道货号折扣
     * 
     * @author mulong
     */
    @GetMapping(value = "/supplier/sku-discount")
    public BaseResult<GetChannelSupplierSkuDiscountByPageResult> getSupplierSkuDiscountByPage(
            @Valid GetChannelSupplierSkuDiscountByPageParam param) {
        return BaseResult.success(managerChannelService.getSupplierSkuDiscountByPage(param));
    }

    /**
     * 添加渠道货号折扣
     * 
     * @author mulong 
     */
    @PostMapping(value = "/supplier/sku-discount")
    public BaseResult<ChannelSupplierSkuDiscount> addSupplierSkuDiscount(
            @RequestBody ChannelSupplierSkuDiscount supplierSkuDiscount) {
        return BaseResult.success(managerChannelService.addSupplierSkuDiscount(supplierSkuDiscount));
    }

    /**
     * 查询渠道货号折扣
     * 
     * @author mulong 
     */
    @GetMapping(value = "/supplier/sku-discount/{id}")
    public BaseResult<ChannelSupplierSkuDiscount> getSupplierSkuDiscountById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerChannelService.getSupplierSkuDiscountById(id));
    }

    /**
     * 更新渠道货号折扣
     * 
     * @author mulong 
     */
    @PutMapping(value = "/supplier/sku-discount/{id}")
    public BaseResult<ChannelSupplierSkuDiscount> putSupplierSkuDiscountById(
            @PathVariable(name = "id") Long id,
            @RequestBody ChannelSupplierSkuDiscount supplierSkuDiscount) {
        return BaseResult.success(managerChannelService.putSupplierSkuDiscountById(id, supplierSkuDiscount));
    }

    /**
     * 删除渠道货号折扣
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/supplier/sku-discount/{id}")
    public BaseResult<ChannelSupplierSkuDiscount> deleteSupplierSkuDiscountById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerChannelService.deleteSupplierSkuDiscountById(id));
    }

    /**
     * 下载渠道货号折扣
     * 
     * @author mulong
     */
    @GetMapping(value = "/supplier/sku-discount/download")
    public ResponseEntity<byte[]> downloadSupplierSkuDiscount(
            @Valid DownloadSupplierSkuDiscountParam param) {
        return managerChannelService.downloadSupplierSkuDiscount(param);
    }

    /**
     * 上传渠道货号折扣
     * 
     * @author mulong
     */
    @PostMapping(value = "/supplier/sku-discount/upload")
    public BaseResult<UploadSupplierSkuDiscountResult> uploadSupplierSkuDiscount(
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(managerChannelService.uploadSupplierSkuDiscount(file));
    }

    /**
     * 下载渠道货号折扣模板
     * 
     * @author mulong
     */
    @GetMapping(value = "/supplier/sku-discount/template")
    public ResponseEntity<byte[]> downloadSupplierSkuDiscountTemplate() {
        return managerChannelService.downloadSupplierSkuDiscountTemplate();
    }

    /**
     * 查询渠道邮费
     * 
     * @author mulong
     */
    @GetMapping(value = "/supplier/delivery-charge")
    public BaseResult<GetChannelSupplierDeliveryChargeByPageResult> getSupplierDeliveryChargeByPage(
            @Valid GetChannelSupplierDeliveryChargeByPageParam param) {
        return BaseResult.success(managerChannelService.getSupplierDeliveryChargeByPage(param));
    }

    /**
     * 添加渠道邮费
     * 
     * @author mulong 
     */
    @PostMapping(value = "/supplier/delivery-charge")
    public BaseResult<ChannelSupplierDeliveryCharge> addSupplierDeliveryCharge(
            @RequestBody ChannelSupplierDeliveryCharge supplierDeliveryCharge) {
        return BaseResult.success(managerChannelService.addSupplierDeliveryCharge(supplierDeliveryCharge));
    }

    /**
     * 查询渠道邮费
     * 
     * @author mulong 
     */
    @GetMapping(value = "/supplier/delivery-charge/{id}")
    public BaseResult<ChannelSupplierDeliveryCharge> getSupplierDeliveryChargeById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerChannelService.getSupplierDeliveryChargeById(id));
    }

    /**
     * 更新渠道邮费
     * 
     * @author mulong 
     */
    @PutMapping(value = "/supplier/delivery-charge/{id}")
    public BaseResult<ChannelSupplierDeliveryCharge> putSupplierDeliveryChargeById(
            @PathVariable(name = "id") Long id,
            @RequestBody ChannelSupplierDeliveryCharge supplierDeliveryCharge) {
        return BaseResult.success(managerChannelService.putSupplierDeliveryChargeById(id, supplierDeliveryCharge));
    }

    /**
     * 删除渠道邮费
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/supplier/delivery-charge/{id}")
    public BaseResult<ChannelSupplierDeliveryCharge> deleteSupplierDeliveryChargeById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerChannelService.deleteSupplierDeliveryChargeById(id));
    }

}
