package com.mulong.mall.web.api.manager;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mulong.common.domain.vo.BaseResult;
import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;
import com.mulong.mall.service.ManagerGoodsService;

/**
 * ManagerGoodsApi
 * 
 * @author mulong
 * @data 2021-06-21 13:56:12
 */
@RestController
@RequestMapping(value = "/api/manager/goods")
public class ManagerGoodsApi {
    @Autowired
    private ManagerGoodsService managerGoodsService;

    /**
     * 上传库存
     * 
     * @author mulong
     */
    @PostMapping(value = "/inventory/upload")
    public BaseResult<UploadInventoryResult> uploadInventory(
            @RequestParam("supplierChannelId") Integer supplierChannelId, 
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(managerGoodsService.uploadInventory(supplierChannelId, file));
    }

    /**
     * 下载库存导入模板
     * 
     * @author mulong
     */
    @GetMapping(value = "/inventory/template")
    public ResponseEntity<byte[]> downloadInventoryTemplate() {
        return managerGoodsService.downloadInventoryTemplate();
    }

    /**
     * 下载库存
     * 
     * @author mulong 
     */
    @PostMapping(value = "/inventory/download")
    public ResponseEntity<byte[]> downloadInventory(@RequestBody DownloadInventoryParam param) {
        return managerGoodsService.downloadInventory(param);
    }

    /**
     * 查询商品库存
     * 
     * @author mulong
     */
    @GetMapping(value = "/product-inventory")
    public BaseResult<GetProductInventoryResult> getProductInventory(
            @Valid GetProductInventoryParam param) {
        return BaseResult.success(managerGoodsService.getProductInventory(param));
    }

    /**
     * 下单
     * 
     * @author mulong
     */
    /*@PostMapping(value = "/booking")
    public BaseResult<BookingResult> booking(
            @RequestBody BookingParam param) {
        return BaseResult.success(managerGoodsService.booking(param));
    }*/

    /**
     * 查询商品
     * 
     * @author mulong
     */
    @GetMapping(value = "/product")
    public BaseResult<GetProductByPageResult> getProductByPage(
            @Valid GetProductByPageParam param) {
        return BaseResult.success(managerGoodsService.getProductByPage(param));
    }

    /**
     * 添加商品
     * 
     * @author mulong 
     */
    @PostMapping(value = "/product")
    public BaseResult<Product> addProduct(
            @RequestBody Product product) {
        return BaseResult.success(managerGoodsService.addProduct(product));
    }

    /**
     * 查询商品
     * 
     * @author mulong 
     */
    @GetMapping(value = "/product/{id}")
    public BaseResult<Product> getProductById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerGoodsService.getProductById(id));
    }

    /**
     * 更新商品
     * 
     * @author mulong 
     */
    @PutMapping(value = "/product/{id}")
    public BaseResult<Product> putProductById(
            @PathVariable(name = "id") Long id,
            @RequestBody Product product) {
        return BaseResult.success(managerGoodsService.putProductById(id, product));
    }

    /**
     * 删除商品
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/product/{id}")
    public BaseResult<Product> deleteProductById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerGoodsService.deleteProductById(id));
    }

    /**
     * 下载商品
     * 
     * @author mulong
     */
    @GetMapping(value = "/product/download")
    public ResponseEntity<byte[]> downloadProduct(
            @Valid DownloadProductParam param) {
        return managerGoodsService.downloadProduct(param);
    }

    /**
     * 上传商品
     * 
     * @author mulong
     */
    @PostMapping(value = "/product/upload")
    public BaseResult<UploadProductResult> uploadProduct(
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(managerGoodsService.uploadProduct(file));
    }

    /**
     * 下载产品信息模板
     * 
     * @author mulong
     */
    @GetMapping(value = "/product/template")
    public ResponseEntity<byte[]> downloadProductTemplate() {
        return managerGoodsService.downloadProductTemplate();
    }

    /**
     * 查询尺码
     * 
     * @author mulong
     */
    @GetMapping(value = "/size")
    public BaseResult<GetSizeConversionByPageResult> getSizeByPage(
            @Valid GetSizeConversionByPageParam param) {
        return BaseResult.success(managerGoodsService.getSizeByPage(param));
    }

    /**
     * 添加尺码
     * 
     * @author mulong 
     */
    @PostMapping(value = "/size")
    public BaseResult<SizeConversion> addSize(
            @RequestBody SizeConversion sizeConversion) {
        return BaseResult.success(managerGoodsService.addSize(sizeConversion));
    }

    /**
     * 查询尺码
     * 
     * @author mulong 
     */
    @GetMapping(value = "/size/{id}")
    public BaseResult<SizeConversion> getSizeById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerGoodsService.getSizeById(id));
    }

    /**
     * 更新尺码
     * 
     * @author mulong 
     */
    @PutMapping(value = "/size/{id}")
    public BaseResult<SizeConversion> putSizeById(
            @PathVariable(name = "id") Integer id,
            @RequestBody SizeConversion sizeConversion) {
        return BaseResult.success(managerGoodsService.putSizeById(id, sizeConversion));
    }

    /**
     * 删除尺码
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/size/{id}")
    public BaseResult<SizeConversion> deleteSizeById(
            @PathVariable(name = "id") Integer id) {
        return BaseResult.success(managerGoodsService.deleteSizeById(id));
    }

    /**
     * 下载尺码
     * 
     * @author mulong
     */
    @GetMapping(value = "/size/download")
    public ResponseEntity<byte[]> downloadSize(
            @Valid DownloadSizeConversionParam param) {
        return managerGoodsService.downloadSize(param);
    }

    /**
     * 上传尺码
     * 
     * @author mulong
     */
    @PostMapping(value = "/size/upload")
    public BaseResult<UploadSizeResult> uploadSize(
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(managerGoodsService.uploadSize(file));
    }

    /**
     * 下载尺码转换模板
     * 
     * @author mulong
     */
    @GetMapping(value = "/size/template")
    public ResponseEntity<byte[]> downloadSizeTemplate() {
        return managerGoodsService.downloadSizeTemplate();
    }

    /**
     * 查询条形码
     * 
     * @author mulong
     */
    @GetMapping(value = "/barcode")
    public BaseResult<GetBarcodeByPageResult> getBarcodeByPage(
            @Valid GetBarcodeByPageParam param) {
        return BaseResult.success(managerGoodsService.getBarcodeByPage(param));
    }

    /**
     * 添加条形码
     * 
     * @author mulong 
     */
    @PostMapping(value = "/barcode")
    public BaseResult<Barcode> addBarcode(
            @RequestBody Barcode barcode) {
        return BaseResult.success(managerGoodsService.addBarcode(barcode));
    }

    /**
     * 查询条形码
     * 
     * @author mulong 
     */
    @GetMapping(value = "/barcode/{id}")
    public BaseResult<Barcode> getBarcodeById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerGoodsService.getBarcodeById(id));
    }

    /**
     * 更新条形码
     * 
     * @author mulong 
     */
    @PutMapping(value = "/barcode/{id}")
    public BaseResult<Barcode> putBarcodeById(
            @PathVariable(name = "id") Long id,
            @RequestBody Barcode barcode) {
        return BaseResult.success(managerGoodsService.putBarcodeById(id, barcode));
    }

    /**
     * 删除条形码
     * 
     * @author mulong 
     */
    @DeleteMapping(value = "/barcode/{id}")
    public BaseResult<Barcode> deleteBarcodeById(
            @PathVariable(name = "id") Long id) {
        return BaseResult.success(managerGoodsService.deleteBarcodeById(id));
    }

    /**
     * 下载条形码
     * 
     * @author mulong
     */
    @GetMapping(value = "/barcode/download")
    public ResponseEntity<byte[]> downloadBarcode(
            @Valid DownloadBarcodeParam param) {
        return managerGoodsService.downloadBarcode(param);
    }

    /**
     * 上传条形码
     * 
     * @author mulong
     */
    @PostMapping(value = "/barcode/upload")
    public BaseResult<UploadBarcodeResult> uploadBarcode(
            @RequestParam("file") MultipartFile file) {
        return BaseResult.success(managerGoodsService.uploadBarcode(file));
    }

    /**
     * 下载条形码信息模板
     * 
     * @author mulong
     */
    @GetMapping(value = "/barcode/template")
    public ResponseEntity<byte[]> downloadBarcodeTemplate() {
        return managerGoodsService.downloadBarcodeTemplate();
    }

}
