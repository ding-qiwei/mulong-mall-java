package com.mulong.mall.constants;

import java.util.*;

/**
 * Constants
 * 
 * @author mulong
 * @data 2021-03-30 16:08:23
 */
public final class Constants {

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    /** 分页查询默认PageNo */
    public static final int DEFAULT_PAGE_NO = 1;
    /** 分页查询默认PageSize */
    public static final int DEFAULT_PAGE_SIZE = 20;

    public static final String MANAGER_UPLOAD_INVENTORY_TEMPLATE_FILENAME = "库存导入模板.xlsx";
    public static final String MANAGER_UPLOAD_INVENTORY_TEMPLATE_OSS_KEY = "template/manager/库存导入模板.xlsx";
    public static final String MANAGER_UPLOAD_PRODUCT_TEMPLATE_FILENAME = "产品信息模板.xlsx";
    public static final String MANAGER_UPLOAD_PRODUCT_TEMPLATE_OSS_KEY = "template/manager/产品信息模板.xlsx";
    public static final String MANAGER_UPLOAD_SIZE_CONVERSION_TEMPLATE_FILENAME = "尺码转换模板.xlsx";
    public static final String MANAGER_UPLOAD_SIZE_CONVERSION_TEMPLATE_OSS_KEY = "template/manager/尺码转换模板.xlsx";
    public static final String MANAGER_UPLOAD_BARCODE_TEMPLATE_FILENAME = "条形码信息模板.xlsx";
    public static final String MANAGER_UPLOAD_BARCODE_TEMPLATE_OSS_KEY = "template/manager/条形码信息模板.xlsx";
    public static final String MANAGER_UPLOAD_SUPPLIER_CHANNEL_SKY_DISCOUNT_TEMPLATE_FILENAME = "渠道货号折扣模板.xlsx";
    public static final String MANAGER_UPLOAD_SUPPLIER_CHANNEL_SKY_DISCOUNT_TEMPLATE_OSS_KEY = "template/manager/渠道货号折扣模板.xlsx";

    public static final String MANAGER_UPLOAD_INVENTORY_SHEET_NAME = "库存导入表";
    public static final String MANAGER_DOWNLOAD_INVENTORY_FILENAME = "库存信息.xlsx";
    public static final String MANAGER_DOWNLOAD_INVENTORY_SHEET_NAME = "库存导出表（管理人员）";
    public static final List<String> MANAGER_DOWNLOAD_INVENTORY_SHEET_TITLE = Arrays.asList(
            "货号（型号）", "品牌", "产品名称", "系列", "大类", "年份", "季节", "性别", "颜色", "尺码（尺寸、规格）", 
            "重量", "吊牌价", "数量", "物编（条形码）", "卖点", "材质", "细节备注", "原尺码", "销售折扣", "成本折扣", 
            "仓库（渠道）名称");
    public static final String MANAGER_UPLOAD_PRODUCT_SHEET_NAME = "产品基础信息表";
    public static final String MANAGER_DOWNLOAD_PRODUCT_FILENAME = "产品基础信息.xlsx";
    public static final String MANAGER_DOWNLOAD_PRODUCT_SHEET_NAME = "产品基础信息导出表";
    public static final List<String> MANAGER_DOWNLOAD_PRODUCT_SHEET_TITLE = Arrays.asList(
            "货号（型号）", "品牌", "产品名称", "大类", "中类", "年份", "季节", "性别", "颜色", "重量",
            "吊牌价", "卖点", "材质", "细节备注");
    public static final String MANAGER_UPLOAD_SIZE_CONVERSION_SHEET_NAME = "产品尺码转换表";
    public static final String MANAGER_DOWNLOAD_SIZE_CONVERSION_FILENAME = "产品尺码转换信息.xlsx";
    public static final String MANAGER_DOWNLOAD_SIZE_CONVERSION_SHEET_NAME = "产品尺码转换导出表";
    public static final List<String> MANAGER_DOWNLOAD_SIZE_CONVERSION_SHEET_TITLE = Arrays.asList(
            "品牌", "大类", "性别", "原尺码", "转换尺码", "尺码备注");
    public static final String MANAGER_UPLOAD_BARCODE_SHEET_NAME = "产品条形码表";
    public static final String MANAGER_DOWNLOAD_BARCODE_FILENAME = "产品条形码信息.xlsx";
    public static final String MANAGER_DOWNLOAD_BARCODE_SHEET_NAME = "产品条形码表导出表";
    public static final List<String> MANAGER_DOWNLOAD_BARCODE_SHEET_TITLE = Arrays.asList(
            "货号（型号）", "尺码", "条形码");

    /** 后台管理员导出客户订单表名 */
    public static final String MANAGER_DOWNLOAD_CUSTOMER_ORDER_SHEET_NAME = "订单导出表（分销客户）";
    /** 后台管理员导出客户订单表标题名 */
    public static final List<String> MANAGER_DOWNLOAD_CUSTOMER_ORDER_SHEET_TITLE = Arrays.asList(
            "订单日期", "订单状态", "系统订单号", "平台订单号（自定义）", "店铺（客户）名称", "品牌", "产品名称", "大类", "性别", "颜色",
            "货号（型号）", "尺码（尺寸、规格）", "物编（条形码）", "收货人", "联系电话", "省", "市", "区", "详细地址", "订单备注",
            "反馈日期", "发货快递", "快递单号", "是否安装", "客户下单渠道", "下单数量", "实发数量", "吊牌价", "销售折扣", "销售额",
            "预收快递费", "实收快递费", "销售总额");

    public static final String MANAGER_UPLOAD_SUPPLIER_SKU_DISCOUNT_SHEET_NAME = "渠道货号折扣表";
    public static final String MANAGER_DOWNLOAD_SUPPLIER_SKU_DISCOUNT_FILENAME = "渠道货号折扣信息.xlsx";
    public static final String MANAGER_DOWNLOAD_SUPPLIER_SKU_DISCOUNT_SHEET_NAME = "渠道货号折扣导出表";
    public static final List<String> MANAGER_DOWNLOAD_SUPPLIER_SKU_DISCOUNT_SHEET_TITLE = Arrays.asList(
            "渠道", "货号（型号）", "折扣");

    /** 供应商上传库存信息表名 */
    public static final String SUPPLIER_UPLOAD_INVENTORY_SHEET_NAME = "库存导入表";

    /** 分销商导出库存信息表名 */
    public static final String DISTRIBUTOR_DOWNLOAD_INVENTORY_SHEET_NAME = "库存导出表（客户）";
    /** 分销商导出库存信息表标题名 */
    public static final List<String> DISTRIBUTOR_DOWNLOAD_INVENTORY_SHEET_TITLE = Arrays.asList(
            "货号（型号）", "品牌", "产品名称", "系列", "大类", "年份", "季节", "性别", "颜色", "尺码（尺寸、规格）",
            "重量", "吊牌价", "数量", "物编（条形码）", "卖点", "材质", "细节备注", "原尺码", "销售折扣", "仓库（渠道）名称");
    /** 分销商上传订单表名 */
    public static final String DISTRIBUTOR_UPLOAD_ORDER_SHEET_NAME = "订单导入表（分销客户）";
    /** 分销商导出订单表名 */
    public static final String DISTRIBUTOR_DOWNLOAD_ORDER_SHEET_NAME = "订单导出表（分销客户）";
    /** 分销商导出订单表标题名 */
    public static final List<String> DISTRIBUTOR_DOWNLOAD_ORDER_SHEET_TITLE = Arrays.asList(
            "订单日期", "订单状态", "系统订单号", "平台订单号（自定义）", "店铺（客户）名称", "品牌", "产品名称", "大类", "性别", "颜色",
            "货号（型号）", "尺码（尺寸、规格）", "物编（条形码）", "收货人", "联系电话", "省", "市", "区", "详细地址", "订单备注",
            "反馈日期", "发货快递", "快递单号", "是否安装", "客户下单渠道", "下单数量", "实发数量", "吊牌价", "销售折扣", "销售额",
            "预收快递费", "实收快递费", "销售总额");
    /** 分销商导出充值表名 */
    public static final String DISTRIBUTOR_DOWNLOAD_RECHARGE_SHEET_NAME = "充值导出表（分销客户）";
    /** 分销商导出充值表标题名 */
    public static final List<String> DISTRIBUTOR_DOWNLOAD_RECHARGE_SHEET_TITLE = Arrays.asList(
            "交易流水号", "充值金额", "充值渠道", "充值时间", "处理时间", "处理状态", "充值备注");
    /** 分销商导出交易流水表名 */
    public static final String DISTRIBUTOR_DOWNLOAD_TRANSACTION_SHEET_NAME = "交易流水导出表（分销客户）";
    /** 分销商导出交易流水表标题名 */
    public static final List<String> DISTRIBUTOR_DOWNLOAD_TRANSACTION_SHEET_TITLE = Arrays.asList(
            "交易流水号", "交易分类", "交易金额", "交易时间", "备注");

}
