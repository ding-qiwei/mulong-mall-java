package com.mulong.mall.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mulong.common.dao.mall.FinanceDao;
import com.mulong.common.domain.pojo.mall.custom.AccountFinancialQuery;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;
import com.mulong.mall.constants.Constants;
import com.mulong.mall.domain.bo.manager.FinanceAccount;
import com.mulong.mall.domain.param.manager.GetFinanceAccountByPageParam;
import com.mulong.mall.domain.result.manager.GetFinanceAccountByPageResult;
import com.mulong.mall.service.ManagerFinanceService;
import lombok.extern.slf4j.Slf4j;

/**
 * ManagerFinanceServiceImpl
 * 
 * @author mulong
 * @data 2021-06-21 14:09:02
 */
@Slf4j
@Service
public class ManagerFinanceServiceImpl implements ManagerFinanceService {
    @Autowired
    private FinanceDao financeDao;

    /**
     * 查询资金账户
     * 
     * @author mulong
     */
    @Override
    public GetFinanceAccountByPageResult getFinanceAccountByPage(GetFinanceAccountByPageParam param) {
        AccountFinancialQuery query = new AccountFinancialQuery();
        query.setGatewayAccountId(param.getGatewayAccountId());
        query.setGatewayAccountType(param.getGatewayAccountType());
        query.setGatewayAccountStatus(param.getGatewayAccountStatus());
        query.setOrderBy("af.`id` ASC");
        int pageNo = param.getPageNo() == null ? Constants.DEFAULT_PAGE_NO : param.getPageNo();
        int pageSize = param.getPageSize() == null ? Constants.DEFAULT_PAGE_SIZE : param.getPageSize();
        PageHelper.startPage(pageNo, pageSize, true);
        Page<com.mulong.common.domain.pojo.mall.custom.CustomAccountFinancial> page = (Page<com.mulong.common.domain.pojo.mall.custom.CustomAccountFinancial>) financeDao.queryCustomAccountFinancial(query);
        GetFinanceAccountByPageResult getFinanceAccountByPageResult = new GetFinanceAccountByPageResult(page.getPageNum(), page.getPageSize(), (int)page.getTotal());
        List<com.mulong.common.domain.pojo.mall.custom.CustomAccountFinancial> list = page.getResult();
        if (!CollectionUtils.isEmpty(list)) {
            getFinanceAccountByPageResult.setResult(list.stream().map(FinanceAccount::new).collect(Collectors.toList()));
        }
        return getFinanceAccountByPageResult;
    }

    /**
     * 添加资金账户
     * 
     * @author mulong 
     */
    @Override
    public FinanceAccount addFinanceAccount(FinanceAccount account) {
        account.setId(null);
        com.mulong.common.domain.pojo.mall.AccountFinancial newRecord = account.buildAccountFinancial();
        newRecord = financeDao.addAccountFinancial(newRecord);
        return new FinanceAccount(newRecord);
    }

    /**
     * 查询资金账户
     * 
     * @author mulong 
     */
    @Override
    public FinanceAccount getFinanceAccountById(Integer id) {
        com.mulong.common.domain.pojo.mall.AccountFinancial record = financeDao.queryAccountFinancialById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        return new FinanceAccount(record);
    }

    /**
     * 更新资金账户
     * 
     * @author mulong 
     */
    @Override
    public FinanceAccount putFinanceAccountById(Integer id, FinanceAccount account) {
        if (!id.equals(account.getId())) {
            log.error("id mismatch");
            throw new MulongException(ErrorEnums.ILLEGAL_PARAM);
        }
        com.mulong.common.domain.pojo.mall.AccountFinancial record = financeDao.queryAccountFinancialById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        com.mulong.common.domain.pojo.mall.AccountFinancial newRecord = account.buildAccountFinancial();
        record.setAccountBalance(newRecord.getAccountBalance());
        financeDao.putAccountFinancial(record);
        return new FinanceAccount(record);
    }

    /**
     * 删除资金账户
     * 
     * @author mulong 
     */
    @Override
    public FinanceAccount deleteFinanceAccountById(Integer id) {
        com.mulong.common.domain.pojo.mall.AccountFinancial record = financeDao.queryAccountFinancialById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        financeDao.deleteAccountFinancial(record);
        return new FinanceAccount(record);
    }

}
