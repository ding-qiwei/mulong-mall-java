package com.mulong.mall.service.support;

import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.springframework.stereotype.Component;

import com.mulong.common.domain.pojo.mall.AccountRecharge;
import com.mulong.common.domain.pojo.mall.AccountTransaction;
import com.mulong.common.enums.AccountRechargeChannel;
import com.mulong.common.enums.AccountRechargeStatus;
import com.mulong.common.enums.AccountTransactionCategory;

import cn.hutool.core.date.DateUtil;

/**
 * DistributorFinanceSupport
 * 
 * @author mulong
 * @data 2021-09-26 18:20:55
 */
@Component
public class DistributorFinanceSupport {

    public void addRow(HSSFRow row, AccountRecharge record, HSSFCellStyle defaultStyle) {
        // 交易流水号
        HSSFCell transactionSerialNoCell = row.createCell(0);
        transactionSerialNoCell.setCellStyle(defaultStyle);
        transactionSerialNoCell.setCellValue(record.getTransactionSerialNo());
        // 充值金额
        HSSFCell amountCell = row.createCell(1);
        amountCell.setCellStyle(defaultStyle);
        amountCell.setCellValue(record.getAmount() == null ? null : record.getAmount().toString());
        // 充值渠道
        HSSFCell rechargeChannelCell = row.createCell(2);
        rechargeChannelCell.setCellStyle(defaultStyle);
        AccountRechargeChannel rechargeChannel = AccountRechargeChannel.getByCode(record.getRechargeChannel());
        rechargeChannelCell.setCellValue(rechargeChannel == null ? null : rechargeChannel.getName());
        // 充值时间
        HSSFCell rechargeTimeCell = row.createCell(3);
        rechargeTimeCell.setCellStyle(defaultStyle);
        Date rechargeTime = record.getRechargeTime();
        rechargeTimeCell.setCellValue(rechargeTime == null ? null : DateUtil.format(rechargeTime, "yyyy-MM-dd HH:mm:ss"));
        // 处理时间
        HSSFCell handleTimeCell = row.createCell(4);
        handleTimeCell.setCellStyle(defaultStyle);
        Date handleTime = record.getHandleTime();
        handleTimeCell.setCellValue(handleTime == null ? null : DateUtil.format(handleTime, "yyyy-MM-dd HH:mm:ss"));
        // 处理状态
        HSSFCell statusCell = row.createCell(5);
        statusCell.setCellStyle(defaultStyle);
        AccountRechargeStatus status = AccountRechargeStatus.getByCode(record.getStatus());
        statusCell.setCellValue(status == null ? null : status.getName());
        // 充值备注
        HSSFCell remarkCell = row.createCell(6);
        remarkCell.setCellStyle(defaultStyle);
        remarkCell.setCellValue(record.getRemark());
    }

    public void addRow(HSSFRow row, AccountTransaction record, HSSFCellStyle defaultStyle) {
        // 交易流水号
        HSSFCell transactionNoCell = row.createCell(0);
        transactionNoCell.setCellStyle(defaultStyle);
        transactionNoCell.setCellValue(record.getTransactionNo());
        // 交易分类
        HSSFCell transactionCategoryCell = row.createCell(1);
        transactionCategoryCell.setCellStyle(defaultStyle);
        AccountTransactionCategory transactionCategory = AccountTransactionCategory.getByCode(record.getTransactionCategory());
        transactionCategoryCell.setCellValue(transactionCategory == null ? null : transactionCategory.getName());
        // 交易金额
        HSSFCell transactionAmountCell = row.createCell(2);
        transactionAmountCell.setCellStyle(defaultStyle);
        transactionAmountCell.setCellValue(record.getTransactionAmount() == null ? null : record.getTransactionAmount().toString());
        // 交易时间
        HSSFCell transactionTimeCell = row.createCell(3);
        transactionTimeCell.setCellStyle(defaultStyle);
        Date transactionTime = record.getTransactionTime();
        transactionTimeCell.setCellValue(transactionTime == null ? null : DateUtil.format(transactionTime, "yyyy-MM-dd HH:mm:ss"));
        // 备注
        HSSFCell remarkCell = row.createCell(4);
        remarkCell.setCellStyle(defaultStyle);
        remarkCell.setCellValue(record.getRemark());
    }

}
