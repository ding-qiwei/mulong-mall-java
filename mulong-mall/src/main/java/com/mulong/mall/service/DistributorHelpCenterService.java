package com.mulong.mall.service;

import com.mulong.mall.domain.bo.distributor.*;
import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;

/**
 * HelpCenterService
 * 
 * @author mulong
 * @data 2021-05-29 21:37:52
 */
public interface DistributorHelpCenterService {

    /**
     * 说明文档
     * 
     * @author mulong
     */
    PlatformDocument getPlatformDocument();

    /**
     * 系统帮助
     * 
     * @author mulong
     */
    GetSystemHelpByPageResult getSystemHelpByPage(GetSystemHelpByPageParam param);
    
    /**
     * 系统帮助
     * 
     * @author mulong
     */
    SystemHelp getSystemHelpById(Integer id);

    /**
     * 销售帮助
     * 
     * @author mulong
     */
    GetSalesHelpByPageResult getSalesHelpByPage(GetSalesHelpByPageParam param);

    /**
     * 销售帮助
     * 
     * @author mulong
     */
    SalesHelp getSalesHelpById(Integer id);

    /**
     * 其他帮助
     * 
     * @author mulong
     */
    GetOthersHelpByPageResult getOthersHelpByPage(GetOthersHelpByPageParam param);

    /**
     * 其他帮助
     * 
     * @author mulong
     */
    OthersHelp getOthersHelpById(Integer id);

    /**
     * 平台策略
     * 
     * @author mulong
     */
    GetPlatformStrategyByPageResult getPlatformStrategyByPage(GetPlatformStrategyByPageParam param);

    /**
     * 平台策略
     * 
     * @author mulong
     */
    PlatformStrategy getPlatformStrategyById(Integer id);

}
