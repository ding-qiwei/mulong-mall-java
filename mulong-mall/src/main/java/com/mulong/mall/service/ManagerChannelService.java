package com.mulong.mall.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;

/**
 * ManagerChannelService
 * 
 * @author mulong
 * @data 2021-06-21 14:07:15
 */
public interface ManagerChannelService {

    /**
     * 查询渠道
     * 
     * @author mulong
     */
    GetChannelSupplierByPageResult getSupplierByPage(GetChannelSupplierByPageParam param);

    /**
     * 添加渠道
     * 
     * @author mulong 
     */
    ChannelSupplier addSupplier(ChannelSupplier supplier);

    /**
     * 查询渠道
     * 
     * @author mulong 
     */
    ChannelSupplier getSupplierById(Integer id);

    /**
     * 更新渠道
     * 
     * @author mulong 
     */
    ChannelSupplier putSupplierById(Integer id, ChannelSupplier supplier);

    /**
     * 删除渠道
     * 
     * @author mulong 
     */
    ChannelSupplier deleteSupplierById(Integer id);

    /**
     * 查询渠道折扣
     * 
     * @author mulong
     */
    GetChannelSupplierDiscountByPageResult getSupplierDiscountByPage(GetChannelSupplierDiscountByPageParam param);

    /**
     * 添加渠道折扣
     * 
     * @author mulong 
     */
    ChannelSupplierDiscount addSupplierDiscount(ChannelSupplierDiscount supplierDiscount);

    /**
     * 查询渠道折扣
     * 
     * @author mulong 
     */
    ChannelSupplierDiscount getSupplierDiscountById(Long id);

    /**
     * 更新渠道折扣
     * 
     * @author mulong 
     */
    ChannelSupplierDiscount putSupplierDiscountById(Long id, ChannelSupplierDiscount supplierDiscount);

    /**
     * 删除渠道折扣
     * 
     * @author mulong 
     */
    ChannelSupplierDiscount deleteSupplierDiscountById(Long id);

    /**
     * 查询渠道货号折扣
     * 
     * @author mulong
     */
    GetChannelSupplierSkuDiscountByPageResult getSupplierSkuDiscountByPage(GetChannelSupplierSkuDiscountByPageParam param);

    /**
     * 添加渠道货号折扣
     * 
     * @author mulong 
     */
    ChannelSupplierSkuDiscount addSupplierSkuDiscount(ChannelSupplierSkuDiscount supplierSkuDiscount);

    /**
     * 查询渠道货号折扣
     * 
     * @author mulong 
     */
    ChannelSupplierSkuDiscount getSupplierSkuDiscountById(Long id);

    /**
     * 更新渠道货号折扣
     * 
     * @author mulong 
     */
    ChannelSupplierSkuDiscount putSupplierSkuDiscountById(Long id, ChannelSupplierSkuDiscount supplierSkuDiscount);

    /**
     * 删除渠道货号折扣
     * 
     * @author mulong 
     */
    ChannelSupplierSkuDiscount deleteSupplierSkuDiscountById(Long id);

    /**
     * 下载渠道货号折扣
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadSupplierSkuDiscount(DownloadSupplierSkuDiscountParam param);

    /**
     * 上传渠道货号折扣
     * 
     * @author mulong
     */
    UploadSupplierSkuDiscountResult uploadSupplierSkuDiscount(MultipartFile file);

    /**
     * 下载渠道货号折扣模板
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadSupplierSkuDiscountTemplate();

    /**
     * 查询渠道邮费
     * 
     * @author mulong
     */
    GetChannelSupplierDeliveryChargeByPageResult getSupplierDeliveryChargeByPage(GetChannelSupplierDeliveryChargeByPageParam param);

    /**
     * 添加渠道邮费
     * 
     * @author mulong 
     */
    ChannelSupplierDeliveryCharge addSupplierDeliveryCharge(ChannelSupplierDeliveryCharge supplierDeliveryCharge);

    /**
     * 查询渠道邮费
     * 
     * @author mulong 
     */
    ChannelSupplierDeliveryCharge getSupplierDeliveryChargeById(Long id);

    /**
     * 更新渠道邮费
     * 
     * @author mulong 
     */
    ChannelSupplierDeliveryCharge putSupplierDeliveryChargeById(Long id, ChannelSupplierDeliveryCharge supplierDeliveryCharge);

    /**
     * 删除渠道邮费
     * 
     * @author mulong 
     */
    ChannelSupplierDeliveryCharge deleteSupplierDeliveryChargeById(Long id);

}
