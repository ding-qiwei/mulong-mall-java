package com.mulong.mall.service;

import org.springframework.web.multipart.MultipartFile;

import com.mulong.mall.domain.bo.supplier.Inventory;
import com.mulong.mall.domain.param.supplier.GetInventoryByPageParam;
import com.mulong.mall.domain.result.supplier.GetInventoryByPageResult;
import com.mulong.mall.domain.result.supplier.UploadInventoryResult;

/**
 * SupplierService
 * 
 * @author mulong
 * @data 2021-06-21 13:44:44
 */
public interface SupplierService {

    /**
     * 查询库存
     * 
     * @author mulong
     */
    GetInventoryByPageResult getInventoryByPage(GetInventoryByPageParam param);

    /**
     * 添加库存
     * 
     * @author mulong 
     */
    Inventory addInventory(Inventory inventory);

    /**
     * 查询库存
     * 
     * @author mulong 
     */
    Inventory getInventoryById(Long id);

    /**
     * 更新库存
     * 
     * @author mulong 
     */
    Inventory putInventoryById(Long id, Inventory inventory);

    /**
     * 删除库存
     * 
     * @author mulong 
     */
    Inventory deleteInventoryById(Long id);

    /**
     * 上传库存
     * 
     * @author mulong
     */
    UploadInventoryResult uploadInventory(MultipartFile file);

}
