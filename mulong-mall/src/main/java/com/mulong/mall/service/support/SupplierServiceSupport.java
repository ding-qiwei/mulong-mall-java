package com.mulong.mall.service.support;

import java.math.BigDecimal;
import java.util.*;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.ChannelDao;
import com.mulong.common.dao.mall.ProductDao;
import com.mulong.common.domain.pojo.mall.Product;
import com.mulong.common.domain.pojo.mall.SupplierChannel;
import com.mulong.common.domain.pojo.mall.custom.SupplierChannelQuery;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;
import com.mulong.common.util.MulongUtil;
import com.mulong.common.util.SizeConverter;
import com.mulong.mall.util.MallRequestContext;

import cn.hutool.poi.excel.ExcelReader;
import lombok.extern.slf4j.Slf4j;

/**
 * SupplierServiceSupport
 * 
 * @author mulong
 * @data 2021-07-09 15:10:26
 */
@Slf4j
@Component
public class SupplierServiceSupport {
    @Autowired
    private ChannelDao channelDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private MallSupport mallSupport;

    public SupplierChannel getSupplierChannel() {
        Integer gatewayAccountId = MallRequestContext.getGatewayAccountId();
        if (gatewayAccountId == null) {
            log.error("gatewayAccountId is null");
            throw new MulongException(ErrorEnums.ILLEGAL_REQUEST);
        }
        SupplierChannelQuery supplierChannelQuery = new SupplierChannelQuery();
        supplierChannelQuery.setRelatedGatewayAccountId(gatewayAccountId);
        List<SupplierChannel> list = channelDao.query(supplierChannelQuery);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        if (list.size() > 1) {
            log.error("more than one SupplierChannel");
            throw new MulongException(ErrorEnums.ILLEGAL_REQUEST);
        }
        return list.get(0);
    }

    public void richInventory(com.mulong.common.domain.pojo.mall.Inventory record) {
        SizeConverter sizeConverter = mallSupport.getSizeConverter();
        richInventory(record, sizeConverter);
    }

    private void richInventory(com.mulong.common.domain.pojo.mall.Inventory record, SizeConverter sizeConverter) {
        String sku = record.getSku();
        if (StringUtils.isBlank(sku)) {
            throw new MulongException(ErrorEnums.HANDLE_FAILED_TEMPLETE.format("存在没有填写货号的记录"));
        }
        String originalSize = record.getOriginalSize();
        if (StringUtils.isBlank(originalSize)) {
            throw new MulongException(ErrorEnums.HANDLE_FAILED_TEMPLETE.format("存在没有填写尺码的记录"));
        }
        Product product = productDao.queryBySku(sku);
        if (product == null) {
            return;
        }
        String normalizedSize = sizeConverter.getNormalizedSize(product.getBrandId(), product.getGoodsCategory(), product.getGoodsSex(), originalSize);
        if (StringUtils.isBlank(normalizedSize)) {
            return;
        }
        record.setNormalizedSize(normalizedSize);
    }

    public List<com.mulong.common.domain.pojo.mall.Inventory> buildInventoryList(
            ExcelReader reader, SupplierChannel supplierChannel) {
        int rowCount = reader.getRowCount();
        if (rowCount <= 1) {
            log.error("upload file is empty");
            throw new MulongException(ErrorEnums.UPLOAD_FILE_EMPTY);
        }
        SizeConverter sizeConverter = mallSupport.getSizeConverter();
        List<com.mulong.common.domain.pojo.mall.Inventory> inventoryRecords = new ArrayList<>(rowCount - 1);
        for (int y = 1; y < rowCount; ++y) {
            com.mulong.common.domain.pojo.mall.Inventory inventory = buildInventory(reader, y, supplierChannel, sizeConverter);
            if (inventory != null) {
                inventoryRecords.add(inventory);
            }
        }
        return inventoryRecords;
    }

    private com.mulong.common.domain.pojo.mall.Inventory buildInventory(
            ExcelReader reader, int rowIndex, SupplierChannel supplierChannel, SizeConverter sizeConverter) {
        Cell skuCell = reader.getCell(0, rowIndex);
        Cell originalSizeCell = reader.getCell(1, rowIndex);
        Cell qtyCell = reader.getCell(2, rowIndex);
        Cell marketPriceCell = reader.getCell(3, rowIndex);
        Cell discountCell = reader.getCell(4, rowIndex);
        String sku = MulongUtil.getStringCellValue(skuCell);
        if (StringUtils.isBlank(sku)) {
            throw new MulongException(ErrorEnums.HANDLE_FAILED_TEMPLETE.format("存在没有填写货号的记录"));
        }
        String originalSize = MulongUtil.getStringCellValue(originalSizeCell);
        if (StringUtils.isBlank(originalSize)) {
            throw new MulongException(ErrorEnums.HANDLE_FAILED_TEMPLETE.format("存在没有填写尺码的记录"));
        }
        String qtyCellValue = MulongUtil.getStringCellValue(qtyCell);
        if (StringUtils.isBlank(qtyCellValue)) {
            throw new MulongException(ErrorEnums.HANDLE_FAILED_TEMPLETE.format("存在没有填写数量的记录"));
        }
        Integer qty = Integer.parseInt(qtyCellValue);
        String marketPriceCellValue = MulongUtil.getStringCellValue(marketPriceCell);
        if (StringUtils.isBlank(marketPriceCellValue)) {
            throw new MulongException(ErrorEnums.HANDLE_FAILED_TEMPLETE.format("存在没有填写吊牌价的记录"));
        }
        BigDecimal marketPrice = new BigDecimal(marketPriceCellValue);
        String discountCellValue = MulongUtil.getStringCellValue(discountCell);
        if (StringUtils.isBlank(discountCellValue)) {
            throw new MulongException(ErrorEnums.HANDLE_FAILED_TEMPLETE.format("存在没有填写成本折扣的记录"));
        }
        BigDecimal discount = new BigDecimal(discountCellValue);
        com.mulong.common.domain.pojo.mall.Inventory record = new com.mulong.common.domain.pojo.mall.Inventory();
        record.setSupplierChannelId(supplierChannel.getId());
        record.setSku(sku);
        record.setOriginalSize(originalSize);
        record.setQty(qty);
        record.setMarketPrice(marketPrice);
        record.setDiscount(discount);
        richInventory(record, sizeConverter);
        return record;
    }

}
