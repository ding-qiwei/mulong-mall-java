package com.mulong.mall.service.support;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.springframework.stereotype.Component;

import com.mulong.common.domain.pojo.mall.custom.DownloadInventory;
import com.mulong.common.enums.GoodsCategory;
import com.mulong.common.enums.GoodsSex;

/**
 * DistributorDataDownloadSupport
 * 
 * @author mulong
 * @data 2021-07-15 10:16:19
 */
@Component
public class DistributorDataDownloadSupport {
    
    public void addRow(HSSFRow row, DownloadInventory record, HSSFCellStyle defaultStyle) {
        // 货号（型号）
        HSSFCell skuCell = row.createCell(0);
        skuCell.setCellStyle(defaultStyle);
        skuCell.setCellValue(record.getSku());
        // 品牌
        HSSFCell brandNameCell = row.createCell(1);
        brandNameCell.setCellStyle(defaultStyle);
        brandNameCell.setCellValue(record.getBrandShortName());
        // 产品名称
        HSSFCell productNameCell = row.createCell(2);
        productNameCell.setCellStyle(defaultStyle);
        productNameCell.setCellValue(record.getProductName());
        // 系列
        HSSFCell productSeriesCell = row.createCell(3);
        productSeriesCell.setCellStyle(defaultStyle);
        productSeriesCell.setCellValue(record.getProductSeries());
        // 大类
        HSSFCell goodsCategoryCell = row.createCell(4);
        goodsCategoryCell.setCellStyle(defaultStyle);
        GoodsCategory goodsCategory = GoodsCategory.getByCode(record.getGoodsCategory());
        goodsCategoryCell.setCellValue(goodsCategory == null ? null : goodsCategory.getName());
        // 年份
        HSSFCell yearCell = row.createCell(5);
        yearCell.setCellStyle(defaultStyle);
        yearCell.setCellValue(record.getYear());
        // 季节
        HSSFCell seasonCell = row.createCell(6);
        seasonCell.setCellStyle(defaultStyle);
        seasonCell.setCellValue(record.getSeason());
        // 性别
        HSSFCell goodsSexCell = row.createCell(7);
        goodsSexCell.setCellStyle(defaultStyle);
        GoodsSex goodsSex = GoodsSex.getByCode(record.getGoodsSex());
        goodsSexCell.setCellValue(goodsSex == null ? null : goodsSex.getName());
        // 颜色
        HSSFCell colorCell = row.createCell(8);
        colorCell.setCellStyle(defaultStyle);
        colorCell.setCellValue(record.getColor());
        // 尺码（尺寸、规格）
        HSSFCell normalizedSizeCell = row.createCell(9);
        normalizedSizeCell.setCellStyle(defaultStyle);
        normalizedSizeCell.setCellValue(record.getNormalizedSize());
        // 重量
        HSSFCell weightCell = row.createCell(10);
        weightCell.setCellStyle(defaultStyle);
        weightCell.setCellValue(record.getWeight() == null ? null : record.getWeight().toString());
        // 吊牌价
        HSSFCell productMarketPriceCell = row.createCell(11);
        productMarketPriceCell.setCellStyle(defaultStyle);
        productMarketPriceCell.setCellValue(record.getProductMarketPrice() == null ? null : record.getProductMarketPrice().toString());
        // 数量
        HSSFCell qtyCell = row.createCell(12);
        qtyCell.setCellStyle(defaultStyle);
        qtyCell.setCellValue(record.getQty());
        // 卖点
        HSSFCell salePropsCell = row.createCell(14);
        salePropsCell.setCellStyle(defaultStyle);
        salePropsCell.setCellValue(record.getSaleProps());
        // 材质
        HSSFCell materialCell = row.createCell(15);
        materialCell.setCellStyle(defaultStyle);
        materialCell.setCellValue(record.getMaterial());
        // 细节备注
        HSSFCell remarkCell = row.createCell(16);
        remarkCell.setCellStyle(defaultStyle);
        remarkCell.setCellValue(record.getRemark());
        // 原尺码
        HSSFCell originalSizeCell = row.createCell(17);
        originalSizeCell.setCellStyle(defaultStyle);
        originalSizeCell.setCellValue(record.getOriginalSize());
        // 销售折扣
        HSSFCell salesDiscountCell = row.createCell(18);
        salesDiscountCell.setCellStyle(defaultStyle);
        salesDiscountCell.setCellValue(record.getSalesDiscount() == null ? null : record.getSalesDiscount().toString());
        // 仓库（渠道）名称
        HSSFCell supplierChannelNameCell = row.createCell(19);
        supplierChannelNameCell.setCellStyle(defaultStyle);
        supplierChannelNameCell.setCellValue(record.getSupplierChannelShortName());
    }

}
