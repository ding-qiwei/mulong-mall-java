package com.mulong.mall.service.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorGoodsService;

/**
 * GoodsServiceImpl
 * 
 * @author mulong
 * @data 2021-05-29 21:37:07
 */
@Service
public class DistributorGoodsServiceImpl implements DistributorGoodsService {

    /**
     * @author mulong
     */
    @Override
    public GetGoodsNewByPageResult getNewByPage(GetGoodsNewByPageParam param) {
        // TODO Auto-generated method stub
        GetGoodsNewByPageResult getGoodsNewByPageResult = new GetGoodsNewByPageResult(0, 0, 0);
        return getGoodsNewByPageResult;
    }

    /**
     * @author mulong
     */
    @Override
    public ResponseEntity<byte[]> downloadNew() {
        // TODO Auto-generated method stub
        return null;
    }

}
