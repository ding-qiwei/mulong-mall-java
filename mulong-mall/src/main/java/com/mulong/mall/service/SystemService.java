package com.mulong.mall.service;

import java.util.*;

import org.springframework.web.multipart.MultipartFile;

import com.mulong.mall.domain.bo.*;
import com.mulong.mall.domain.param.*;
import com.mulong.mall.domain.result.*;

/**
 * SystemService
 * 
 * @author mulong
 * @data 2021-03-30 21:58:16
 */
public interface SystemService {

    /**
     * 查询网关账号
     * 
     * @author mulong
     */
    GetGatewayAccountByPageResult getGatewayAccountByPage(GetGatewayAccountByPageParam param);

    /**
     * 添加网关账号
     * 
     * @author mulong 
     */
    GatewayAccount addGatewayAccount(GatewayAccount gatewayAccount);

    /**
     * 查询网关账号
     * 
     * @author mulong 
     */
    GatewayAccount getGatewayAccountById(Integer id);

    /**
     * 更新网关账号
     * 
     * @author mulong 
     */
    GatewayAccount putGatewayAccountById(Integer id, GatewayAccount gatewayAccount);

    /**
     * 删除网关账号
     * 
     * @author mulong 
     */
    GatewayAccount deleteGatewayAccountById(Integer id);

    /**
     * 查询数据字典
     * 
     * @author mulong
     */
    GetDataDictByPageResult getDataDictByPage(GetDataDictByPageParam param);

    /**
     * 添加数据字典
     * 
     * @author mulong 
     */
    DataDict addDataDict(DataDict dataDict);

    /**
     * 查询数据字典
     * 
     * @author mulong 
     */
    DataDict getDataDictById(Integer id);

    /**
     * 更新数据字典
     * 
     * @author mulong 
     */
    DataDict putDataDictById(Integer id, DataDict dataDict);

    /**
     * 删除数据字典
     * 
     * @author mulong 
     */
    DataDict deleteDataDictById(Integer id);

    /**
     * 获取数据字典
     * 
     * @author mulong
     */
    List<DataDict> fetchDataDicts(List<String> dataDictCodes);

    /**
     * 获取网关账号权限信息
     * 
     * @author mulong
     */
    GetUserInfoResult getInfo();

    /**
     * 更新网关账号密码
     * 
     * @author mulong
     */
    void updatePassword(UpdateGetwayAccountPasswordParam param);

    /**
     * 上传文件
     * 
     * @author mulong
     */
    UploadFileResult uploadFile(MultipartFile file);

    /**
     * 上传图片
     * 
     * @author mulong
     */
    UploadImgResult uploadImg(MultipartFile file);

    /**
     * 获取枚举
     * 
     * @author mulong
     */
    List<SystemEnum> fetchEnum(List<String> enumCodes);

    /**
     * EnumBrand
     * 
     * @author mulong
     */
    List<EnumBrand> getEnumBrand();

    /**
     * EnumDeliveryType
     * 
     * @author mulong
     */
    List<EnumDeliveryType> getEnumDeliveryType();

    /**
     * EnumSupplierChannel
     * 
     * @author mulong
     */
    List<EnumSupplierChannel> getEnumSupplierChannel();

}
