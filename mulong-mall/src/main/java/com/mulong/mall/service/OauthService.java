package com.mulong.mall.service;

import com.mulong.mall.domain.param.OauthTokenParam;
import com.mulong.mall.domain.result.OauthTokenResult;

/**
 * OauthService
 * 
 * @author mulong
 * @data 2021-08-10 18:00:37
 */
public interface OauthService {

    /**
     * 获取token
     * 
     * @author mulong
     */
    OauthTokenResult token(OauthTokenParam param);

}
