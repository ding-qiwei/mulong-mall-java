package com.mulong.mall.service;

import org.springframework.http.ResponseEntity;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;

/**
 * DataDownloadService
 * 
 * @author mulong
 * @data 2021-05-29 21:21:20
 */
public interface DistributorDataDownloadService {

    /**
     * 库存渠道列表
     * 
     * @author mulong
     */
    GetDataDownloadInventoryChannelByPageResult getInventoryChannelByPage(GetDataDownloadInventoryChannelByPageParam param);

    /**
     * 库存下载
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadInventory(DownloadDataDownloadInventoryParam param);

    /**
     * 新增款式推荐包列表
     * 
     * @author mulong
     */
    GetDataDownloadRecommendationByPageResult getRecommendationByPage(GetDataDownloadRecommendationByPageParam param);

    /**
     * 新增款式推荐包下载
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadRecommendation(DownloadDataDownloadRecommendationParam param);

    /**
     * 数据包列表
     * 
     * @author mulong
     */
    GetDataDownloadDataPackageByPageResult getDataPackageByPage(GetDataDownloadDataPackageByPageParam param);

    /**
     * 数据包下载
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadDataPackage(DownloadDataDownloadDataPackageParam param);

    /**
     * 商品数据列表
     * 
     * @author mulong
     */
    GetDataDownloadGoodsByPageResult getGoodsByPage(GetDataDownloadGoodsByPageParam param);

    /**
     * 商品数据下载
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadGoods(DownloadDataDownloadGoodsParam param);

}
