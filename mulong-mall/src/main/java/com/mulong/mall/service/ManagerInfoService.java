package com.mulong.mall.service;

import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;

/**
 * ManagerInfoService
 * 
 * @author mulong
 * @data 2021-06-21 14:06:51
 */
public interface ManagerInfoService {

    /**
     * 查询帮助
     * 
     * @author mulong
     */
    GetHelpByPageResult getHelpByPage(GetHelpByPageParam param);

    /**
     * 添加帮助
     * 
     * @author mulong 
     */
    HelpCenterRecord addHelp(HelpCenterRecord help);

    /**
     * 查询帮助
     * 
     * @author mulong 
     */
    HelpCenterRecord getHelpById(Integer id);

    /**
     * 更新帮助
     * 
     * @author mulong 
     */
    HelpCenterRecord putHelpById(Integer id, HelpCenterRecord help);

    /**
     * 删除帮助
     * 
     * @author mulong 
     */
    HelpCenterRecord deleteHelpById(Integer id);

}
