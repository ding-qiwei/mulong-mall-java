package com.mulong.mall.service.support;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.springframework.stereotype.Component;

import com.mulong.common.domain.pojo.mall.custom.CustomerOrderDetailInfo;
import com.mulong.common.enums.CustomerOrderStatus;
import com.mulong.common.enums.GoodsCategory;
import com.mulong.common.enums.GoodsSex;
import com.mulong.common.util.MulongUtil;

import cn.hutool.core.date.DateUtil;

/**
 * DistributorDistributionSupport
 * 
 * @author mulong
 * @data 2021-08-19 13:40:37
 */
@Component
public class DistributorDistributionSupport {

    public void addRow(HSSFRow row, CustomerOrderDetailInfo record, HSSFCellStyle defaultStyle) {
        // 订单日期
        HSSFCell createTimeCell = row.createCell(0);
        createTimeCell.setCellStyle(defaultStyle);
        createTimeCell.setCellValue(DateUtil.format(record.getCreateTime(), "yyyy/MM/dd"));
        // 订单状态
        CustomerOrderStatus customerOrderStatus = CustomerOrderStatus.getByCode(record.getStatus());
        HSSFCell statusCell = row.createCell(1);
        statusCell.setCellStyle(defaultStyle);
        statusCell.setCellValue(customerOrderStatus.getName());
        // 系统订单号
        HSSFCell orderIdCell = row.createCell(2);
        orderIdCell.setCellStyle(defaultStyle);
        orderIdCell.setCellValue(record.getOrderId());
        // 平台订单号（自定义）
        HSSFCell customizedOrderIdCell = row.createCell(3);
        customizedOrderIdCell.setCellStyle(defaultStyle);
        customizedOrderIdCell.setCellValue(record.getCustomizedOrderId());
        // 店铺（客户）名称
        HSSFCell gatewayAccountCell = row.createCell(4);
        gatewayAccountCell.setCellStyle(defaultStyle);
        gatewayAccountCell.setCellValue(record.getGatewayAccountName());
        // 品牌
        HSSFCell brandCell = row.createCell(5);
        brandCell.setCellStyle(defaultStyle);
        brandCell.setCellValue(record.getBrandName());
        // 产品名称
        HSSFCell productNameCell = row.createCell(6);
        productNameCell.setCellStyle(defaultStyle);
        productNameCell.setCellValue(record.getProductName());
        // 大类
        GoodsCategory goodsCategory = GoodsCategory.getByCode(record.getGoodsCategory());
        HSSFCell goodsCategoryCell = row.createCell(7);
        goodsCategoryCell.setCellStyle(defaultStyle);
        goodsCategoryCell.setCellValue(goodsCategory.getName());
        // 性别
        GoodsSex goodsSex = GoodsSex.getByCode(record.getGoodsSex());
        HSSFCell goodsSexCell = row.createCell(8);
        goodsSexCell.setCellStyle(defaultStyle);
        goodsSexCell.setCellValue(goodsSex.getName());
        // 颜色
        HSSFCell colorCell = row.createCell(9);
        colorCell.setCellStyle(defaultStyle);
        colorCell.setCellValue(record.getProductColor());
        // 货号（型号）
        HSSFCell skuCell = row.createCell(10);
        skuCell.setCellStyle(defaultStyle);
        skuCell.setCellValue(record.getSku());
        // 尺码（尺寸、规格）
        HSSFCell sizeCell = row.createCell(11);
        sizeCell.setCellStyle(defaultStyle);
        sizeCell.setCellValue(record.getNormalizedSize());
        // 收货人
        HSSFCell receiverNameCell = row.createCell(13);
        receiverNameCell.setCellStyle(defaultStyle);
        receiverNameCell.setCellValue(record.getReceiverName());
        // 联系电话
        HSSFCell receiverPhoneCell = row.createCell(14);
        receiverPhoneCell.setCellStyle(defaultStyle);
        receiverPhoneCell.setCellValue(record.getReceiverMobilephone());
        // 省
        HSSFCell receiverProvinceCell = row.createCell(15);
        receiverProvinceCell.setCellStyle(defaultStyle);
        receiverProvinceCell.setCellValue(record.getReceiverProvince());
        // 市
        HSSFCell receiverCityCell = row.createCell(16);
        receiverCityCell.setCellStyle(defaultStyle);
        receiverCityCell.setCellValue(record.getReceiverCity());
        // 区
        HSSFCell receiverDistrictCell = row.createCell(17);
        receiverDistrictCell.setCellStyle(defaultStyle);
        receiverDistrictCell.setCellValue(record.getReceiverDistrict());
        // 详细地址
        HSSFCell receiverAddressCell = row.createCell(18);
        receiverAddressCell.setCellStyle(defaultStyle);
        receiverAddressCell.setCellValue(record.getReceiverAddress());
        // 订单备注
        HSSFCell remarkCell = row.createCell(19);
        remarkCell.setCellStyle(defaultStyle);
        remarkCell.setCellValue(record.getRemark());
        // 反馈日期
        HSSFCell feedbackTimeCell = row.createCell(20);
        feedbackTimeCell.setCellStyle(defaultStyle);
        if (record.getFeedbackTime() != null) {
            feedbackTimeCell.setCellValue(DateUtil.format(record.getFeedbackTime(), "yyyy/MM/dd"));
        }
        // 发货快递
        HSSFCell actualDeliveryTypeCell = row.createCell(21);
        actualDeliveryTypeCell.setCellStyle(defaultStyle);
        actualDeliveryTypeCell.setCellValue(record.getActualDeliveryTypeName());
        // 快递单号
        HSSFCell expressNoCell = row.createCell(22);
        expressNoCell.setCellStyle(defaultStyle);
        expressNoCell.setCellValue(record.getExpressNo());
        // 是否安装
        HSSFCell anzhuangCell = row.createCell(23);
        anzhuangCell.setCellStyle(defaultStyle);
        anzhuangCell.setCellValue(StringUtils.EMPTY);
        // 客户下单渠道
        HSSFCell supplierChannelCell = row.createCell(24);
        supplierChannelCell.setCellStyle(defaultStyle);
        supplierChannelCell.setCellValue(record.getSupplierChannelShortName());
        // 下单数量
        HSSFCell qtyCell = row.createCell(25);
        qtyCell.setCellStyle(defaultStyle);
        qtyCell.setCellValue(record.getQty());
        // 实发数量
        HSSFCell actualQtyCell = row.createCell(26);
        actualQtyCell.setCellStyle(defaultStyle);
        actualQtyCell.setCellValue(StringUtils.EMPTY);
        // 吊牌价
        HSSFCell marketPriceCell = row.createCell(27);
        marketPriceCell.setCellStyle(defaultStyle);
        marketPriceCell.setCellValue(MulongUtil.formatMoney(record.getMarketPrice()));
        // 销售折扣
        HSSFCell discountCell = row.createCell(28);
        discountCell.setCellStyle(defaultStyle);
        discountCell.setCellValue(StringUtils.EMPTY);
        // 销售额
        HSSFCell salePriceCell = row.createCell(29);
        salePriceCell.setCellStyle(defaultStyle);
        salePriceCell.setCellValue(MulongUtil.formatMoney(record.getDiscountPrice()));
        // 预收快递费
        HSSFCell deliveryPriceCell = row.createCell(30);
        deliveryPriceCell.setCellStyle(defaultStyle);
        deliveryPriceCell.setCellValue(StringUtils.EMPTY);
        // 实收快递费
        HSSFCell actualDeliveryPriceCell = row.createCell(31);
        actualDeliveryPriceCell.setCellStyle(defaultStyle);
        actualDeliveryPriceCell.setCellValue(StringUtils.EMPTY);
        // 销售总额
        HSSFCell totalPriceCell = row.createCell(32);
        totalPriceCell.setCellStyle(defaultStyle);
        totalPriceCell.setCellValue(MulongUtil.formatMoney(record.getPayPrice()));
    }

}
