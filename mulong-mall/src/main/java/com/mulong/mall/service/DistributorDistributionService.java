package com.mulong.mall.service;

import org.springframework.http.ResponseEntity;

import com.mulong.mall.domain.bo.distributor.*;
import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;

/**
 * DistributionService
 * 
 * @author mulong
 * @data 2021-05-29 21:34:50
 */
public interface DistributorDistributionService {

    /**
     * 查询商品库存
     * 
     * @author mulong 
     */
    DistributionProductInventoryResult getProductInventory(DistributionProductInventoryParam param);

    /**
     * 下单
     * 
     * @author mulong 
     */
    DistributionBookingResult booking(DistributionBookingParam param);

    /**
     * @author mulong 
     */
    GetDistributionGrouponByPageResult getGrouponByPage(GetDistributionGrouponByPageParam param);

    /**
     * @author mulong 
     */
    DistributionGroupon getGrouponById(Long id);

    /**
     * 订单列表
     * 
     * @author mulong 
     */
    GetDistributionOrderByPageResult getOrderByPage(GetDistributionOrderByPageParam param);

    /**
     * 下载订单
     * 
     * @author mulong 
     */
    ResponseEntity<byte[]> downloadOrder(DownloadOrderParam param);

    /**
     * 付款全部未付款订单
     * 
     * @author mulong 
     */
    DistributionPayAllOrderResult payAllOrder();

    /**
     * 付款选中订单
     * 
     * @author mulong 
     */
    DistributionPaySelectedOrderResult paySelectedOrder(DistributionPaySelectedOrderParam param);

    /**
     * @author mulong 
     */
    DistributionOrder getOrderById(Long id);

    /**
     * @author mulong 
     */
    DistributionOrder putOrderById(Long id, DistributionOrder order);

    /**
     * @author mulong 
     */
    GetDistributionTradeByPageResult getTradeByPage(GetDistributionTradeByPageParam param);

    /**
     * @author mulong 
     */
    DistributionTrade getTradeById(Long id);

    /**
     * @author mulong 
     */
    DistributionTrade putTradeById(Long id, DistributionTrade trade);

    /**
     * @author mulong 
     */
    GetDistributionDiscountByPageResult getDiscountByPage(GetDistributionDiscountByPageParam param);

    /**
     * @author mulong 
     */
    GetDistributionDeliveryChargeByPageResult getDeliveryChargeByPage(GetDistributionDeliveryChargeByPageParam param);

    /**
     * @author mulong 
     */
    GetDistributionGoodsDemandByPageResult getGoodsDemandByPage(GetDistributionGoodsDemandByPageParam param);

    /**
     * @author mulong 
     */
    DistributionGoodsDemand addGoodsDemand(DistributionGoodsDemand goodsDemand);

    /**
     * @author mulong 
     */
    DistributionGoodsDemand getGoodsDemandById(Long id);

    /**
     * @author mulong 
     */
    DistributionGoodsDemand putGoodsDemandById(Long id, DistributionGoodsDemand goodsDemand);

    /**
     * @author mulong 
     */
    GetDistributionMyFavoriteByPageResult getMyFavoriteByPage(GetDistributionMyFavoriteByPageParam param);

    /**
     * @author mulong 
     */
    DistributionMyFavorite addMyFavorite(DistributionMyFavorite myFavorite);

    /**
     * @author mulong 
     */
    DistributionMyFavorite deleteMyFavoriteById(Long id);

}
