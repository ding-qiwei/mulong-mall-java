package com.mulong.mall.service.impl;

import org.springframework.stereotype.Service;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorPresaleService;

/**
 * PresaleServiceImpl
 * 
 * @author mulong
 * @data 2021-05-29 21:39:26
 */
@Service
public class DistributorPresaleServiceImpl implements DistributorPresaleService {

    /**
     * 预售订单
     * 
     * @author mulong
     */
    @Override
    public GetPresaleOrderByPageResult getOrderByPage(GetPresaleOrderByPageParam param) {
        // TODO Auto-generated method stub
        GetPresaleOrderByPageResult getPresaleOrderByPageResult = new GetPresaleOrderByPageResult(0, 0, 0);
        return getPresaleOrderByPageResult;
    }

    /**
     * 预售下单
     * 
     * @author mulong
     */
    @Override
    public GetPresaleBookingByPageResult getBookingByPage(GetPresaleBookingByPageParam param) {
        // TODO Auto-generated method stub
        GetPresaleBookingByPageResult getPresaleBookingByPageResult = new GetPresaleBookingByPageResult(0, 0, 0);
        return getPresaleBookingByPageResult;
    }

}
