package com.mulong.mall.service;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;

/**
 * PresaleService
 * 
 * @author mulong
 * @data 2021-05-29 21:39:06
 */
public interface DistributorPresaleService {

    /**
     * 预售订单
     * 
     * @author mulong
     */
    GetPresaleOrderByPageResult getOrderByPage(GetPresaleOrderByPageParam param);

    /**
     * 预售下单
     * 
     * @author mulong
     */
    GetPresaleBookingByPageResult getBookingByPage(GetPresaleBookingByPageParam param);

}
