package com.mulong.mall.service;

import com.mulong.mall.domain.bo.manager.FinanceAccount;
import com.mulong.mall.domain.param.manager.GetFinanceAccountByPageParam;
import com.mulong.mall.domain.result.manager.GetFinanceAccountByPageResult;

/**
 * ManagerFinanceService
 * 
 * @author mulong
 * @data 2021-06-21 14:07:29
 */
public interface ManagerFinanceService {

    /**
     * 查询资金账户
     * 
     * @author mulong
     */
    GetFinanceAccountByPageResult getFinanceAccountByPage(GetFinanceAccountByPageParam param);

    /**
     * 添加资金账户
     * 
     * @author mulong 
     */
    FinanceAccount addFinanceAccount(FinanceAccount account);

    /**
     * 查询资金账户
     * 
     * @author mulong 
     */
    FinanceAccount getFinanceAccountById(Integer id);

    /**
     * 更新资金账户
     * 
     * @author mulong 
     */
    FinanceAccount putFinanceAccountById(Integer id, FinanceAccount account);

    /**
     * 删除资金账户
     * 
     * @author mulong 
     */
    FinanceAccount deleteFinanceAccountById(Integer id);

}
