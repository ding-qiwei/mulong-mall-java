package com.mulong.mall.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mulong.mall.constants.Constants;
import com.mulong.mall.domain.bo.distributor.*;
import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;
import com.mulong.mall.service.DistributorHelpCenterService;
import com.mulong.common.dao.mall.HelpCenterDao;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;

/**
 * HelpCenterServiceImpl
 * 
 * @author mulong
 * @data 2021-05-29 21:38:06
 */
@Service
public class DistributorHelpCenterServiceImpl implements DistributorHelpCenterService {
    @Autowired
    private HelpCenterDao helpCenterDao;

    /**
     * 说明文档
     * 
     * @author mulong
     */
    @Override
    public PlatformDocument getPlatformDocument() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * 系统帮助
     * 
     * @author mulong
     */
    @Override
    public GetSystemHelpByPageResult getSystemHelpByPage(GetSystemHelpByPageParam param) {
        int pageNo = param.getPageNo() == null ? Constants.DEFAULT_PAGE_NO : param.getPageNo();
        int pageSize = param.getPageSize() == null ? Constants.DEFAULT_PAGE_SIZE : param.getPageSize();
        PageHelper.startPage(pageNo, pageSize, true);
        Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord> page = (Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord>) helpCenterDao.queryPublishedSystemHelp(param.getTitleKeyword());
        GetSystemHelpByPageResult getHelpCenterSystemHelpByPageResult = new GetSystemHelpByPageResult(page.getPageNum(), page.getPageSize(), (int)page.getTotal());
        List<com.mulong.common.domain.pojo.mall.HelpCenterRecord> list = page.getResult();
        if (!CollectionUtils.isEmpty(list)) {
            getHelpCenterSystemHelpByPageResult.setResult(list.stream().map(SystemHelp::new).collect(Collectors.toList()));
        }
        return getHelpCenterSystemHelpByPageResult;
    }

    /**
     * 系统帮助
     * 
     * @author mulong
     */
    @Override
    public SystemHelp getSystemHelpById(Integer id) {
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = helpCenterDao.queryHelpById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        return new SystemHelp(record);
    }

    /**
     * 销售帮助
     * 
     * @author mulong
     */
    @Override
    public GetSalesHelpByPageResult getSalesHelpByPage(GetSalesHelpByPageParam param) {
        int pageNo = param.getPageNo() == null ? Constants.DEFAULT_PAGE_NO : param.getPageNo();
        int pageSize = param.getPageSize() == null ? Constants.DEFAULT_PAGE_SIZE : param.getPageSize();
        PageHelper.startPage(pageNo, pageSize, true);
        Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord> page = (Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord>) helpCenterDao.queryPublishedSalesHelp(param.getTitleKeyword());
        GetSalesHelpByPageResult getHelpCenterSalesHelpByPageResult = new GetSalesHelpByPageResult(page.getPageNum(), page.getPageSize(), (int)page.getTotal());
        List<com.mulong.common.domain.pojo.mall.HelpCenterRecord> list = page.getResult();
        if (!CollectionUtils.isEmpty(list)) {
            getHelpCenterSalesHelpByPageResult.setResult(list.stream().map(SalesHelp::new).collect(Collectors.toList()));
        }
        return getHelpCenterSalesHelpByPageResult;
    }

    /**
     * 销售帮助
     * 
     * @author mulong
     */
    @Override
    public SalesHelp getSalesHelpById(Integer id) {
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = helpCenterDao.queryHelpById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        return new SalesHelp(record);
    }

    /**
     * 其他帮助
     * 
     * @author mulong
     */
    @Override
    public GetOthersHelpByPageResult getOthersHelpByPage(GetOthersHelpByPageParam param) {
        int pageNo = param.getPageNo() == null ? Constants.DEFAULT_PAGE_NO : param.getPageNo();
        int pageSize = param.getPageSize() == null ? Constants.DEFAULT_PAGE_SIZE : param.getPageSize();
        PageHelper.startPage(pageNo, pageSize, true);
        Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord> page = (Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord>) helpCenterDao.queryPublishedOthersHelp(param.getTitleKeyword());
        GetOthersHelpByPageResult getHelpCenterOthersHelpByPageResult = new GetOthersHelpByPageResult(page.getPageNum(), page.getPageSize(), (int)page.getTotal());
        List<com.mulong.common.domain.pojo.mall.HelpCenterRecord> list = page.getResult();
        if (!CollectionUtils.isEmpty(list)) {
            getHelpCenterOthersHelpByPageResult.setResult(list.stream().map(OthersHelp::new).collect(Collectors.toList()));
        }
        return getHelpCenterOthersHelpByPageResult;
    }

    /**
     * 其他帮助
     * 
     * @author mulong
     */
    @Override
    public OthersHelp getOthersHelpById(Integer id) {
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = helpCenterDao.queryHelpById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        return new OthersHelp(record);
    }

    /**
     * 平台策略
     * 
     * @author mulong
     */
    @Override
    public GetPlatformStrategyByPageResult getPlatformStrategyByPage(
            GetPlatformStrategyByPageParam param) {
        int pageNo = param.getPageNo() == null ? Constants.DEFAULT_PAGE_NO : param.getPageNo();
        int pageSize = param.getPageSize() == null ? Constants.DEFAULT_PAGE_SIZE : param.getPageSize();
        PageHelper.startPage(pageNo, pageSize, true);
        Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord> page = (Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord>) helpCenterDao.queryPublishedPlatformStrategy(param.getTitleKeyword());
        GetPlatformStrategyByPageResult getHelpCenterPlatformStrategyByPageResult = new GetPlatformStrategyByPageResult(page.getPageNum(), page.getPageSize(), (int)page.getTotal());
        List<com.mulong.common.domain.pojo.mall.HelpCenterRecord> list = page.getResult();
        if (!CollectionUtils.isEmpty(list)) {
            getHelpCenterPlatformStrategyByPageResult.setResult(list.stream().map(PlatformStrategy::new).collect(Collectors.toList()));
        }
        return getHelpCenterPlatformStrategyByPageResult;
    }

    /**
     * 平台策略
     * 
     * @author mulong
     */
    @Override
    public PlatformStrategy getPlatformStrategyById(Integer id) {
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = helpCenterDao.queryHelpById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        return new PlatformStrategy(record);
    }

}
