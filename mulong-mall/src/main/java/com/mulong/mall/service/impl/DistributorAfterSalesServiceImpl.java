package com.mulong.mall.service.impl;

import org.springframework.stereotype.Service;

import com.mulong.mall.domain.bo.*;
import com.mulong.mall.domain.param.*;
import com.mulong.mall.domain.result.*;
import com.mulong.mall.service.DistributorAfterSalesService;

import lombok.extern.slf4j.Slf4j;

/**
 * DistributionAfterSalesServiceImpl
 * 
 * @author mulong
 * @data 2021-05-29 21:34:05
 */
@Slf4j
@Service
public class DistributorAfterSalesServiceImpl implements DistributorAfterSalesService {

}
