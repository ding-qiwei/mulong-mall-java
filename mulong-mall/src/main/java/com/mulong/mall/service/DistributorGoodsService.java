package com.mulong.mall.service;

import org.springframework.http.ResponseEntity;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;

/**
 * GoodsService
 * 
 * @author mulong
 * @data 2021-05-29 21:36:49
 */
public interface DistributorGoodsService {

    /**
     * @author mulong
     */
    GetGoodsNewByPageResult getNewByPage(GetGoodsNewByPageParam param);

    /**
     * @author mulong
     */
    ResponseEntity<byte[]> downloadNew();

}
