package com.mulong.mall.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mulong.common.dao.mall.HelpCenterDao;
import com.mulong.common.domain.pojo.mall.custom.HelpCenterRecordQuery;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;
import com.mulong.mall.constants.Constants;
import com.mulong.mall.domain.bo.manager.HelpCenterRecord;
import com.mulong.mall.domain.param.manager.GetHelpByPageParam;
import com.mulong.mall.domain.result.manager.GetHelpByPageResult;
import com.mulong.mall.service.ManagerInfoService;
import com.mulong.mall.util.MallRequestContext;

import lombok.extern.slf4j.Slf4j;

/**
 * ManagerInfoServiceImpl
 * 
 * @author mulong
 * @data 2021-06-21 14:10:33
 */
@Slf4j
@Service
public class ManagerInfoServiceImpl implements ManagerInfoService {
    @Autowired
    private HelpCenterDao helpCenterDao;

    /**
     * 查询帮助
     * 
     * @author mulong
     */
    @Override
    public GetHelpByPageResult getHelpByPage(GetHelpByPageParam param) {
        HelpCenterRecordQuery query = new HelpCenterRecordQuery();
        query.setCategory(param.getCategory());
        query.setStatus(param.getStatus());
        query.setTitleKeyword(param.getTitleKeyword());
        query.setOrderBy("`update_time` DESC");
        int pageNo = param.getPageNo() == null ? Constants.DEFAULT_PAGE_NO : param.getPageNo();
        int pageSize = param.getPageSize() == null ? Constants.DEFAULT_PAGE_SIZE : param.getPageSize();
        PageHelper.startPage(pageNo, pageSize, true);
        Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord> page = (Page<com.mulong.common.domain.pojo.mall.HelpCenterRecord>) helpCenterDao.queryHelp(query);
        GetHelpByPageResult getHelpByPageResult = new GetHelpByPageResult(page.getPageNum(), page.getPageSize(), (int)page.getTotal());
        List<com.mulong.common.domain.pojo.mall.HelpCenterRecord> list = page.getResult();
        if (!CollectionUtils.isEmpty(list)) {
            getHelpByPageResult.setResult(list.stream().map(HelpCenterRecord::new).collect(Collectors.toList()));
        }
        return getHelpByPageResult;
    }

    /**
     * 添加帮助
     * 
     * @author mulong 
     */
    @Override
    public HelpCenterRecord addHelp(HelpCenterRecord help) {
        help.setId(null);
        com.mulong.common.domain.pojo.mall.HelpCenterRecord newRecord = help.buildHelpCenterRecord();
        newRecord = helpCenterDao.addHelp(newRecord, MallRequestContext.getUsername());
        return new HelpCenterRecord(newRecord);
    }

    /**
     * 查询帮助
     * 
     * @author mulong 
     */
    @Override
    public HelpCenterRecord getHelpById(Integer id) {
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = helpCenterDao.queryHelpById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        return new HelpCenterRecord(record);
    }

    /**
     * 更新帮助
     * 
     * @author mulong 
     */
    @Override
    public HelpCenterRecord putHelpById(Integer id, HelpCenterRecord help) {
        if (!id.equals(help.getId())) {
            log.error("id mismatch");
            throw new MulongException(ErrorEnums.ILLEGAL_PARAM);
        }
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = helpCenterDao.queryHelpById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        com.mulong.common.domain.pojo.mall.HelpCenterRecord newRecord = help.buildHelpCenterRecord();
        record.setCategory(newRecord.getCategory());
        record.setStatus(newRecord.getStatus());
        record.setTitle(newRecord.getTitle());
        record.setContent(newRecord.getContent());
        record.setPublishedTime(newRecord.getPublishedTime());
        helpCenterDao.putHelp(record, MallRequestContext.getUsername());
        return new HelpCenterRecord(record);
    }

    /**
     * 删除帮助
     * 
     * @author mulong 
     */
    @Override
    public HelpCenterRecord deleteHelpById(Integer id) {
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = helpCenterDao.queryHelpById(id);
        if (record == null) {
            throw new MulongException(ErrorEnums.RESOURCE_NOT_EXISTS);
        }
        helpCenterDao.deleteHelp(record, MallRequestContext.getUsername());
        return new HelpCenterRecord(record);
    }

}
