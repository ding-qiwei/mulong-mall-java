package com.mulong.mall.service.support;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mulong.common.dao.mall.SizeConversionDao;
import com.mulong.common.domain.pojo.mall.SizeConversion;
import com.mulong.common.util.SizeConverter;

/**
 * MallSupport
 * 
 * @author mulong
 * @data 2021-07-12 14:43:03
 */
@Component
public class MallSupport {
    @Autowired
    private SizeConversionDao sizeConversionDao;

    public SizeConverter getSizeConverter() {
        List<SizeConversion> all = sizeConversionDao.queryAll();        
        return new SizeConverter(all);
    }

}
