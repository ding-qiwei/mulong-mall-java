package com.mulong.mall.service;

import org.springframework.http.ResponseEntity;

import com.mulong.mall.domain.param.distributor.*;
import com.mulong.mall.domain.result.distributor.*;

/**
 * FinanceService
 * 
 * @author mulong
 * @data 2021-05-29 21:35:50
 */
public interface DistributorFinanceService {

    /**
     * @author mulong
     */
    GetFinanceAccountRechargeByPageResult getAccountRechargeByPage(GetFinanceAccountRechargeByPageParam param);

    /**
     * @author mulong
     */
    ResponseEntity<byte[]> downloadAccountRecharge();

    /**
     * @author mulong
     */
    GetFinanceAccountTransactionByPageResult getAccountTransactionByPage(GetFinanceAccountTransactionByPageParam param);

    /**
     * @author mulong
     */
    ResponseEntity<byte[]> downloadAccountTransaction();

}
