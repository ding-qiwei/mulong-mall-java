package com.mulong.mall.service;

import org.springframework.http.ResponseEntity;

import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;

/**
 * ManagerOrderService
 * 
 * @author mulong
 * @data 2021-06-21 14:07:42
 */
public interface ManagerOrderService {

    /**
     * 客户订单列表
     * 
     * @author mulong
     */
    GetCustomerOrderByPageResult getCustomerOrderByPage(GetCustomerOrderByPageParam param);

    /**
     * 下载客户订单
     * 
     * @author mulong 
     */
    ResponseEntity<byte[]> downloadCustomerOrder(DownloadCustomerOrderParam param);

    /**
     * 下载客户已付款订单
     * 
     * @author mulong 
     */
    ResponseEntity<byte[]> downloadCustomerPaidOrder();

}
