package com.mulong.mall.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;

/**
 * ManagerGoodsService
 * 
 * @author mulong
 * @data 2021-06-21 14:01:58
 */
public interface ManagerGoodsService {

    /**
     * 上传库存
     * 
     * @author mulong
     */
    UploadInventoryResult uploadInventory(Integer supplierChannelId, MultipartFile file);

    /**
     * 下载库存导入模板
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadInventoryTemplate();

    /**
     * 下载库存
     * 
     * @author mulong 
     */
    ResponseEntity<byte[]> downloadInventory(DownloadInventoryParam param);

    /**
     * 查询商品库存
     * 
     * @author mulong
     */
    GetProductInventoryResult getProductInventory(GetProductInventoryParam param);

    /**
     * 查询商品
     * 
     * @author mulong
     */
    GetProductByPageResult getProductByPage(GetProductByPageParam param);

    /**
     * 添加商品
     * 
     * @author mulong 
     */
    Product addProduct(Product product);

    /**
     * 查询商品
     * 
     * @author mulong 
     */
    Product getProductById(Long id);

    /**
     * 更新商品
     * 
     * @author mulong 
     */
    Product putProductById(Long id, Product product);

    /**
     * 删除商品
     * 
     * @author mulong 
     */
    Product deleteProductById(Long id);

    /**
     * 下载商品
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadProduct(DownloadProductParam param);

    /**
     * 上传商品
     * 
     * @author mulong
     */
    UploadProductResult uploadProduct(MultipartFile file);

    /**
     * 下载产品信息模板
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadProductTemplate();

    /**
     * 查询尺码
     * 
     * @author mulong
     */
    GetSizeConversionByPageResult getSizeByPage(GetSizeConversionByPageParam param);

    /**
     * 添加尺码
     * 
     * @author mulong 
     */
    SizeConversion addSize(SizeConversion sizeConversion);

    /**
     * 查询尺码
     * 
     * @author mulong 
     */
    SizeConversion getSizeById(Integer id);

    /**
     * 更新尺码
     * 
     * @author mulong 
     */
    SizeConversion putSizeById(Integer id, SizeConversion sizeConversion);

    /**
     * 删除尺码
     * 
     * @author mulong 
     */
    SizeConversion deleteSizeById(Integer id);

    /**
     * 下载尺码
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadSize(DownloadSizeConversionParam param);

    /**
     * 上传尺码
     * 
     * @author mulong
     */
    UploadSizeResult uploadSize(MultipartFile file);

    /**
     * 下载尺码转换模板
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadSizeTemplate();

    /**
     * 查询条形码
     * 
     * @author mulong
     */
    GetBarcodeByPageResult getBarcodeByPage(GetBarcodeByPageParam param);

    /**
     * 添加条形码
     * 
     * @author mulong 
     */
    Barcode addBarcode(Barcode barcode);

    /**
     * 查询条形码
     * 
     * @author mulong 
     */
    Barcode getBarcodeById(Long id);

    /**
     * 更新条形码
     * 
     * @author mulong 
     */
    Barcode putBarcodeById(Long id, Barcode barcode);

    /**
     * 删除条形码
     * 
     * @author mulong 
     */
    Barcode deleteBarcodeById(Long id);

    /**
     * 下载条形码
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadBarcode(DownloadBarcodeParam param);

    /**
     * 上传条形码
     * 
     * @author mulong
     */
    UploadBarcodeResult uploadBarcode(MultipartFile file);

    /**
     * 下载条形码信息模板
     * 
     * @author mulong
     */
    ResponseEntity<byte[]> downloadBarcodeTemplate();

}
