package com.mulong.mall.service;

import com.mulong.mall.domain.bo.manager.*;
import com.mulong.mall.domain.param.manager.*;
import com.mulong.mall.domain.result.manager.*;

/**
 * ManagerBusinessService
 * 
 * @author mulong
 * @data 2021-06-21 14:03:38
 */
public interface ManagerBusinessService {

    /**
     * 查询快递方式
     * 
     * @author mulong
     */
    GetDeliveryTypeByPageResult getDeliveryTypeByPage(GetDeliveryTypeByPageParam param);

    /**
     * 添加快递方式
     * 
     * @author mulong 
     */
    DeliveryType addDeliveryType(DeliveryType deliveryType);

    /**
     * 查询快递方式
     * 
     * @author mulong 
     */
    DeliveryType getDeliveryTypeById(Integer id);

    /**
     * 更新快递方式
     * 
     * @author mulong 
     */
    DeliveryType putDeliveryTypeById(Integer id, DeliveryType deliveryType);

    /**
     * 删除快递方式
     * 
     * @author mulong 
     */
    DeliveryType deleteDeliveryTypeById(Integer id);

    /**
     * 查询邮费
     * 
     * @author mulong
     */
    GetDeliveryChargeByPageResult getDeliveryChargeByPage(GetDeliveryChargeByPageParam param);

    /**
     * 添加邮费
     * 
     * @author mulong 
     */
    DeliveryCharge addDeliveryCharge(DeliveryCharge deliveryCharge);

    /**
     * 查询邮费
     * 
     * @author mulong 
     */
    DeliveryCharge getDeliveryChargeById(Long id);

    /**
     * 更新邮费
     * 
     * @author mulong 
     */
    DeliveryCharge putDeliveryChargeById(Long id, DeliveryCharge deliveryCharge);

    /**
     * 删除邮费
     * 
     * @author mulong 
     */
    DeliveryCharge deleteDeliveryChargeById(Long id);

    /**
     * 批量添加邮费
     * 
     * @author mulong 
     */
    AddBatchDeliveryChargeResult addBatchDeliveryCharge(AddBatchDeliveryChargeParam param);

    /**
     * 查询品牌
     * 
     * @author mulong
     */
    GetBrandByPageResult getBrandByPage(GetBrandByPageParam param);

    /**
     * 添加品牌
     * 
     * @author mulong 
     */
    Brand addBrand(Brand brand);

    /**
     * 查询品牌
     * 
     * @author mulong 
     */
    Brand getBrandById(Integer id);

    /**
     * 更新品牌
     * 
     * @author mulong 
     */
    Brand putBrandById(Integer id, Brand brand);

    /**
     * 删除品牌
     * 
     * @author mulong 
     */
    Brand deleteBrandById(Integer id);

}
