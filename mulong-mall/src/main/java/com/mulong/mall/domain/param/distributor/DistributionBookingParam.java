package com.mulong.mall.domain.param.distributor;

import java.math.BigDecimal;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionBookingParam
 * 
 * @author mulong
 * @data 2021-05-29 21:58:51
 */
@Getter
@Setter
public class DistributionBookingParam {
    private String name;
    private String mobilephone;
    private String telephone;
    private String province;
    private String city;
    private String district;
    private String zipcode;
    private String customizedOrderId;
    private String address;
    private String remark;
    private List<Detail> details;

    @Getter
    @Setter
    public static class Detail {
        private String sku;
        private Integer supplierChannelId;
        private String normalizedSize;
        private Integer qty;
        private BigDecimal marketPrice;
        private BigDecimal discountPrice;
        private BigDecimal deliveryChargePreEach;
        private BigDecimal pricePreEach;
        private BigDecimal priceTotal;
        private Long supplierChannelDeliveryChargeId;
    }

}
