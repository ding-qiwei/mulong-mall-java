package com.mulong.mall.domain.result.distributor;

import com.mulong.mall.domain.bo.distributor.DataDownloadGoods;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDataDownloadGoodsByPageResult
 * 
 * @author mulong
 * @data 2021-06-13 23:18:31
 */
public class GetDataDownloadGoodsByPageResult extends PageResult<DataDownloadGoods> {

    public GetDataDownloadGoodsByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
