package com.mulong.mall.domain.result.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadSizeResult
 * 
 * @author mulong
 * @data 2021-06-30 16:34:11
 */
@Getter
@Setter
public class UploadSizeResult {
    private Integer sizeCount; 
}
