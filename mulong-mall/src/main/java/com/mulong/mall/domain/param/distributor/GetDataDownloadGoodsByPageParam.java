package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDataDownloadGoodsByPageParam
 * 
 * @author mulong
 * @data 2021-06-13 23:17:49
 */
@Getter
@Setter
public class GetDataDownloadGoodsByPageParam extends PageParam {
    private String artNo;
    private Integer brandId;
    private Integer seasonId;
    private Integer categoryId;
}
