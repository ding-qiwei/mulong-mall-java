package com.mulong.mall.domain.bo.manager;

import com.mulong.common.domain.pojo.mall.Brand;
import com.mulong.common.enums.GoodsCategory;
import com.mulong.common.enums.GoodsSex;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadSizeConversionRecord
 * 
 * @author etiger
 * @data 2021-11-25 16:09:26
 */
@Getter
@Setter
public class UploadSizeConversionRecord {
    private String brandValue;
    private Brand brand;
    private String goodsCategoryValue;
    private GoodsCategory goodsCategory; 
    private String goodsSexValue;
    private GoodsSex goodsSex;
    private String originalSizeValue;
    private String normalizedSizeValue;
    private String remarkValue;
}
