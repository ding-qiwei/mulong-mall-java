package com.mulong.mall.domain.result.distributor;

import com.mulong.mall.domain.bo.distributor.DataDownloadRecommendation;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDataDownloadRecommendationByPageResult
 * 
 * @author mulong
 * @data 2021-06-13 23:26:20
 */
public class GetDataDownloadRecommendationByPageResult extends PageResult<DataDownloadRecommendation> {

    public GetDataDownloadRecommendationByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
