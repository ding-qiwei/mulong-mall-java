package com.mulong.mall.domain.bo.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadBarcodeRecord
 * 
 * @author etiger
 * @data 2021-11-26 13:36:32
 */
@Getter
@Setter
public class UploadBarcodeRecord {
    private String sku;
    private String normalizedSize;
    private String barcode;
}
