package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDeliveryTypeByPageParam
 * 
 * @author mulong
 * @data 2021-05-31 16:23:29
 */
@Getter
@Setter
public class GetDeliveryTypeByPageParam extends PageParam {
    private String nameKeyword;
}
