package com.mulong.mall.domain.param.distributor;

import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetFinanceAccountTransactionByPageParam
 * 
 * @author mulong
 * @data 2021-05-30 16:36:00
 */
@Getter
@Setter
public class GetFinanceAccountTransactionByPageParam extends PageParam {
    private String transactionNo;
    private Integer transactionCategory;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date transactionDateFrom;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date transactionDateTo;
}
