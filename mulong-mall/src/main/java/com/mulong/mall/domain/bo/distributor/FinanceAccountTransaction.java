package com.mulong.mall.domain.bo.distributor;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mulong.common.domain.pojo.mall.AccountTransaction;
import com.mulong.common.enums.AccountTransactionCategory;

import lombok.Getter;
import lombok.Setter;

/**
 * FinanceAccountTransaction
 * 
 * @author mulong
 * @data 2021-05-30 16:34:33
 */
@Getter
@Setter
public class FinanceAccountTransaction {
    private Long id;
    /** 账户id */
    private Integer accountId;
    /** 账户名字 */
    private String accountName;
    /** 交易流水号 */
    private String transactionNo;
    /** 交易分类 */
    private Integer transactionCategory;
    private String transactionCategoryTitle;
    /** 交易金额 */
    private BigDecimal transactionAmount;
    /** 交易时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date transactionTime;
    /** 备注 */
    private String remark;

    public FinanceAccountTransaction(AccountTransaction record) {
        this.id = record.getId();
        this.accountId = record.getGatewayAccountId();
        this.accountName = null; // TODO
        this.transactionNo = record.getTransactionNo();
        this.transactionCategory = record.getTransactionCategory();
        AccountTransactionCategory accountTransactionCategory = AccountTransactionCategory.getByCode(this.transactionCategory);
        this.transactionCategoryTitle = accountTransactionCategory == null ? null : accountTransactionCategory.getName();
        this.transactionAmount = record.getTransactionAmount();
        this.transactionTime = record.getTransactionTime();
        this.remark = record.getRemark();
    }

}
