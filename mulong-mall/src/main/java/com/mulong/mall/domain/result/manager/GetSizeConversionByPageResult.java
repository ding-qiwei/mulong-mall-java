package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.SizeConversion;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetSizeConversionByPageResult
 * 
 * @author mulong
 * @data 2021-06-25 22:16:07
 */
public class GetSizeConversionByPageResult extends PageResult<List<SizeConversion>> {

    public GetSizeConversionByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
