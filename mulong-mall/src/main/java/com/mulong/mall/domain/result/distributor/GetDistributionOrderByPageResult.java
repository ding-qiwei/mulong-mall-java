package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DistributionOrder;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDistributionOrderByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 22:15:35
 */
public class GetDistributionOrderByPageResult extends PageResult<List<DistributionOrder>> {

    public GetDistributionOrderByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
