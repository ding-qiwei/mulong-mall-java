package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.ChannelSupplierSkuDiscount;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetChannelSupplierSkuDiscountByPageResult
 * 
 * @author etiger
 * @data 2021-12-03 17:28:37
 */
public class GetChannelSupplierSkuDiscountByPageResult extends PageResult<List<ChannelSupplierSkuDiscount>> {

    public GetChannelSupplierSkuDiscountByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
