package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetHelpCenterSalesHelpByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 23:31:39
 */
@Getter
@Setter
public class GetSalesHelpByPageParam extends PageParam {
    private String titleKeyword;
}
