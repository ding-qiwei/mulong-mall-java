package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDiscount;
import com.mulong.common.enums.GoodsCategory;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SupplierChannelDiscount
 * 
 * @author mulong
 * @data 2021-06-01 14:29:02
 */
@Getter
@Setter
@NoArgsConstructor
public class ChannelSupplierDiscount {
    private Long id;
    private Integer channelId;
    private String channelTitle;
    private Integer brandId;
    private String brandTitle;
    private Integer goodsCategory;
    private String goodsCategoryTitle;
    private BigDecimal discount;

    public ChannelSupplierDiscount(com.mulong.common.domain.pojo.mall.SupplierChannelDiscount record) {
        this.id = record.getId();
        this.channelId = record.getChannelId();
        this.brandId = record.getBrandId();
        this.goodsCategory = record.getGoodsCategory();
        this.discount = record.getDiscount();
    }

    public ChannelSupplierDiscount(CustomSupplierChannelDiscount record) {
        this.id = record.getId();
        this.channelId = record.getChannelId();
        this.channelTitle = record.getChannelTitle();
        this.brandId = record.getBrandId();
        this.brandTitle = record.getBrandTitle();
        this.goodsCategory = record.getGoodsCategory();
        GoodsCategory goodsCategory = GoodsCategory.getByCode(this.goodsCategory);
        this.goodsCategoryTitle = goodsCategory == null ? null : goodsCategory.getName();
        this.discount = record.getDiscount();
    }

    public com.mulong.common.domain.pojo.mall.SupplierChannelDiscount buildSupplierChannelDiscount() {
        com.mulong.common.domain.pojo.mall.SupplierChannelDiscount record = new com.mulong.common.domain.pojo.mall.SupplierChannelDiscount();
        record.setId(id);
        record.setChannelId(channelId);
        record.setBrandId(brandId);
        record.setGoodsCategory(goodsCategory);
        record.setDiscount(discount);
        return record;
    }

}
