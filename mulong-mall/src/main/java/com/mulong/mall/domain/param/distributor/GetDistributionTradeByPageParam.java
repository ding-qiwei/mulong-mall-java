package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDistributionTradeByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 22:16:53
 */
@Getter
@Setter
public class GetDistributionTradeByPageParam extends PageParam {

}
