package com.mulong.mall.domain.param.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadProductParam
 * 
 * @author etiger
 * @data 2021-12-13 15:52:43
 */
@Getter
@Setter
public class DownloadProductParam {
    private String sku;
    private Integer brandId;
    private Integer goodsCategory;
    private String year;
    private String season;
    private Integer goodsSex;
}
