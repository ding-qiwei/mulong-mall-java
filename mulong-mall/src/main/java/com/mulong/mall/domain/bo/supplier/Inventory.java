package com.mulong.mall.domain.bo.supplier;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Inventory
 * 
 * @author mulong
 * @data 2021-07-09 13:34:37
 */
@Getter
@Setter
@NoArgsConstructor
public class Inventory {
    private Long id;
    /** 货号 */
    private String sku;
    /** 尺码 */
    private String originalSize;
    /** 数量 */
    private Integer qty;
    /** 吊牌价 */
    private BigDecimal marketPrice;
    /** 成本折扣 */
    private BigDecimal discount;
    /** 创建时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public Inventory(com.mulong.common.domain.pojo.mall.Inventory record) {
        this.id = record.getId();
        this.sku = record.getSku();
        this.originalSize = record.getOriginalSize();
        this.qty = record.getQty();
        this.marketPrice = record.getMarketPrice();
        this.discount = record.getDiscount();
        this.createTime = record.getCreateTime();
    }

    public com.mulong.common.domain.pojo.mall.Inventory buildInventory() {
        com.mulong.common.domain.pojo.mall.Inventory record = new com.mulong.common.domain.pojo.mall.Inventory();
        record.setId(id);
        record.setSku(sku);
        record.setOriginalSize(originalSize);
        record.setQty(qty);
        record.setMarketPrice(marketPrice);
        record.setDiscount(discount);
        record.setCreateTime(createTime);
        return record;
    }

}
