package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDeliveryChargeByPageParam
 * 
 * @author mulong
 * @data 2021-06-23 10:46:54
 */
@Getter
@Setter
public class GetDeliveryChargeByPageParam extends PageParam {
    private Integer deliveryTypeId;
    private String province;
}
