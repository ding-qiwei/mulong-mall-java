package com.mulong.mall.domain.result.distributor;

import com.mulong.mall.domain.bo.distributor.PresaleOrder;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetPresaleOrderByPageResult
 * 
 * @author mulong
 * @data 2021-06-14 08:57:56
 */
public class GetPresaleOrderByPageResult extends PageResult<PresaleOrder> {

    public GetPresaleOrderByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
