package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DistributionGoodsDemand;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDistributionGoodsDemandByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 22:33:03
 */
public class GetDistributionGoodsDemandByPageResult extends PageResult<List<DistributionGoodsDemand>> {

    public GetDistributionGoodsDemandByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
