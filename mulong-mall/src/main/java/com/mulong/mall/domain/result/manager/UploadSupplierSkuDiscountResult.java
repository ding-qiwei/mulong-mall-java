package com.mulong.mall.domain.result.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadSupplierSkuDiscountResult
 * 
 * @author etiger
 * @data 2021-12-03 20:30:51
 */
@Getter
@Setter
public class UploadSupplierSkuDiscountResult {
    private Integer supplierSkuDiscountCount;
}
