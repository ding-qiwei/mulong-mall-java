package com.mulong.mall.domain.param;

import lombok.Getter;
import lombok.Setter;

/**
 * PageParam
 * 
 * @author mulong
 * @data 2021-03-30 14:05:54
 */
@Getter
@Setter
public class PageParam {
    private Integer pageNo;
    private Integer pageSize;
}
