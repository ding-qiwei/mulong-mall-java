package com.mulong.mall.domain.bo.manager;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Help
 * 
 * @author mulong
 * @data 2021-06-21 15:34:46
 */
@Getter
@Setter
@NoArgsConstructor
public class HelpCenterRecord {
    private Integer id;
    /** 帮助分类 */
    private Integer category;
    /** 状态 */
    private Integer status;
    /** 标题 */
    private String title;
    /** 正文 */
    private String content;
    /** 发布时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publishedTime;

    public HelpCenterRecord(com.mulong.common.domain.pojo.mall.HelpCenterRecord record) {
        this.id = record.getId();
        this.category = record.getCategory();
        this.status = record.getStatus();
        this.title = record.getTitle();
        this.content = record.getContent();
        this.publishedTime = record.getPublishedTime();
    }

    public com.mulong.common.domain.pojo.mall.HelpCenterRecord buildHelpCenterRecord() {
        com.mulong.common.domain.pojo.mall.HelpCenterRecord record = new com.mulong.common.domain.pojo.mall.HelpCenterRecord();
        record.setId(id);
        record.setCategory(category);
        record.setStatus(status);
        record.setTitle(title);
        record.setContent(content);
        record.setPublishedTime(publishedTime);
        return record;
    }

}
