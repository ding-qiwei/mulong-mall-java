package com.mulong.mall.domain.bo.distributor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DistributionMyFavorite
 * 
 * @author mulong
 * @data 2021-05-29 22:04:28
 */
@Getter
@Setter
@NoArgsConstructor
public class DistributionMyFavorite {
    private Long id;
    private String artNo;
    private String productName;
    private String imgUrl;

    public DistributionMyFavorite(com.mulong.common.domain.pojo.mall.MyFavorite record) {
        this.id = record.getId();
        this.artNo = record.getArtNo();
    }

    public DistributionMyFavorite(com.mulong.common.domain.pojo.mall.custom.DistributionMyFavorite record) {
        this.id = record.getId();
        this.artNo = record.getArtNo();
        this.productName = record.getProductName();
        this.imgUrl = record.getImgUrl();
    }

    public com.mulong.common.domain.pojo.mall.MyFavorite buildMyFavorite() {
        com.mulong.common.domain.pojo.mall.MyFavorite record = new com.mulong.common.domain.pojo.mall.MyFavorite();
        record.setId(id);
        record.setArtNo(artNo);
        return record;
    }

}
