package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Product
 * 
 * @author mulong
 * @data 2021-06-25 20:48:32
 */
@Getter
@Setter
@NoArgsConstructor
public class Product {
    private Long id;
    private String sku;
    private Integer brandId;
    private String name;
    private String series;
    private Integer goodsCategory;
    private String year;
    private String season;
    private Integer goodsSex;
    private String color;
    private BigDecimal weight;
    private BigDecimal marketPrice;
    private String saleProps;
    private String material;
    private String remark;
    
    public Product(com.mulong.common.domain.pojo.mall.Product record) {
        this.id = record.getId();
        this.sku = record.getSku();
        this.brandId = record.getBrandId();
        this.name = record.getName();
        this.series = record.getSeries();
        this.goodsCategory = record.getGoodsCategory();
        this.year = record.getYear();
        this.season = record.getSeason();
        this.goodsSex = record.getGoodsSex();
        this.color = record.getColor();
        this.weight = record.getWeight();
        this.marketPrice = record.getMarketPrice();
        this.saleProps = record.getSaleProps();
        this.material = record.getMaterial();
        this.remark = record.getRemark();
    }

    public com.mulong.common.domain.pojo.mall.Product buildProduct() {
        com.mulong.common.domain.pojo.mall.Product record = new com.mulong.common.domain.pojo.mall.Product();
        record.setId(id);
        record.setSku(sku);
        record.setBrandId(brandId);
        record.setName(name);
        record.setSeries(series);
        record.setGoodsCategory(goodsCategory);
        record.setYear(year);
        record.setSeason(season);
        record.setGoodsSex(goodsSex);
        record.setColor(color);
        record.setWeight(weight);
        record.setMarketPrice(marketPrice);
        record.setSaleProps(saleProps);
        record.setMaterial(material);
        record.setRemark(remark);
        return record;
    }

}
