package com.mulong.mall.domain.bo.distributor;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DistributionGoodsDemand
 * 
 * @author mulong
 * @data 2021-05-29 22:03:04
 */
@Getter
@Setter
@NoArgsConstructor
public class DistributionGoodsDemand {
    private Long id;
    /** 货号 */
    private String artNo;
    /** 状态 */
    private Integer status;
    /** 尺码 */
    private String size;
    /** 数量 */
    private Integer qty;
    /** 可接受最大折扣 */
    private BigDecimal maxDiscount;
    /** 可接受调货日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date expectDate;
    /** 是否到货提醒 */
    private Integer needNotice;
    /** 备注 */
    private String remark;

    public DistributionGoodsDemand(com.mulong.common.domain.pojo.mall.GoodsDemand record) {
        this.id = record.getId();
        this.artNo = record.getArtNo();
        this.status = record.getStatus();
        this.size = record.getSize();
        this.qty = record.getQty();
        this.maxDiscount = record.getMaxDiscount();
        this.expectDate = record.getExpectDate();
        this.needNotice = record.getNeedNotice();
        this.remark = record.getRemark();
    }

    public com.mulong.common.domain.pojo.mall.GoodsDemand buildGoodsDemand() {
        com.mulong.common.domain.pojo.mall.GoodsDemand record = new com.mulong.common.domain.pojo.mall.GoodsDemand();
        record.setId(id);
        record.setArtNo(artNo);
        record.setStatus(status);
        record.setSize(size);
        record.setQty(qty);
        record.setMaxDiscount(maxDiscount);
        record.setExpectDate(expectDate);
        record.setNeedNotice(needNotice);
        record.setRemark(remark);
        return record;
    }

}
