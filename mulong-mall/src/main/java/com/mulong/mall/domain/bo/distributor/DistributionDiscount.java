package com.mulong.mall.domain.bo.distributor;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDiscount;
import com.mulong.common.enums.GoodsCategory;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DistributionDiscount
 * 
 * @author mulong
 * @data 2021-05-29 22:01:41
 */
@Getter
@Setter
@NoArgsConstructor
public class DistributionDiscount {
    private Long id;
    private Integer channelId;
    private String channelTitle;
    private Integer brandId;
    private String brandTitle;
    private Integer goodsCategory;
    private String goodsCategoryTitle;
    private BigDecimal discount;

    public DistributionDiscount(CustomSupplierChannelDiscount record) {
        this.id = record.getId();
        this.channelId = record.getChannelId();
        this.channelTitle = record.getChannelTitle();
        this.brandId = record.getBrandId();
        this.brandTitle = record.getBrandTitle();
        this.goodsCategory = record.getGoodsCategory();
        GoodsCategory goodsCategory = GoodsCategory.getByCode(this.goodsCategory);
        this.goodsCategoryTitle = goodsCategory == null ? null : goodsCategory.getName();
        this.discount = record.getDiscount();
    }

}
