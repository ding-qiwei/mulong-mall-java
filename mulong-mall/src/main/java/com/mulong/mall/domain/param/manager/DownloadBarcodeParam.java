package com.mulong.mall.domain.param.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadBarcodeParam
 * 
 * @author etiger
 * @data 2021-11-26 16:07:37
 */
@Getter
@Setter
public class DownloadBarcodeParam {
    private String sku;
    private String barcode;
}
