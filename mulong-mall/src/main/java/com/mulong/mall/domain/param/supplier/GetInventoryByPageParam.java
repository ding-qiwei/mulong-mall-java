package com.mulong.mall.domain.param.supplier;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetInventoryByPageParam
 * 
 * @author mulong
 * @data 2021-07-09 13:42:32
 */
@Getter
@Setter
public class GetInventoryByPageParam extends PageParam {
    private String sku;
}
