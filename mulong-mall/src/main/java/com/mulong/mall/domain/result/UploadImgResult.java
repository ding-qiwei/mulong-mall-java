package com.mulong.mall.domain.result;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadImgResult
 * 
 * @author mulong
 * @data 2021-04-06 10:19:26
 */
@Getter
@Setter
public class UploadImgResult {
    private String url;
}
