package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelSkuDiscount;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * ChannelSupplierSkuDiscount
 * 
 * @author etiger
 * @data 2021-12-03 17:18:37
 */
@Getter
@Setter
@NoArgsConstructor
public class ChannelSupplierSkuDiscount {
    private Long id;
    private Integer channelId;
    private String channelTitle;
    private String sku;
    private BigDecimal discount;

    public ChannelSupplierSkuDiscount(com.mulong.common.domain.pojo.mall.SupplierChannelSkuDiscount record) {
        this.id = record.getId();
        this.channelId = record.getChannelId();
        this.sku = record.getSku();
        this.discount = record.getDiscount();
    }

    public ChannelSupplierSkuDiscount(CustomSupplierChannelSkuDiscount record) {
        this.id = record.getId();
        this.channelId = record.getChannelId();
        this.channelTitle = record.getChannelTitle();
        this.sku = record.getSku();
        this.discount = record.getDiscount();
    }

    public com.mulong.common.domain.pojo.mall.SupplierChannelSkuDiscount buildSupplierChannelSkuDiscount() {
        com.mulong.common.domain.pojo.mall.SupplierChannelSkuDiscount record = new com.mulong.common.domain.pojo.mall.SupplierChannelSkuDiscount();
        record.setId(id);
        record.setChannelId(channelId);
        record.setSku(sku);
        record.setDiscount(discount);
        return record;
    }

}
