package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.Product;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetProductByPageResult
 * 
 * @author mulong
 * @data 2021-06-25 22:08:27
 */
public class GetProductByPageResult extends PageResult<List<Product>> {

    public GetProductByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
