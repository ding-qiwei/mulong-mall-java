package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.Brand;
import com.mulong.common.enums.GoodsCategory;
import com.mulong.common.enums.GoodsSex;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadProductRecord
 * 
 * @author etiger
 * @data 2021-11-25 16:18:33
 */
@Getter
@Setter
public class UploadProductRecord {
    private String sku;
    private String brandValue;
    private Brand brand;
    private String name;
    private String goodsCategoryValue;
    private GoodsCategory goodsCategory; 
    private String series;
    private String year;
    private String season;
    private String goodsSexValue;
    private GoodsSex goodsSex;
    private String color;
    private BigDecimal weight;
    private BigDecimal marketPrice;
    private String saleProps;
    private String material;
    private String remark;
}
