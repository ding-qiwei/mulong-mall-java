package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.Barcode;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetBarcodeByPageResult
 * 
 * @author etiger
 * @data 2021-11-26 16:08:50
 */
public class GetBarcodeByPageResult extends PageResult<List<Barcode>> {

    public GetBarcodeByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
