package com.mulong.mall.domain.bo.distributor;

import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mulong.common.domain.pojo.mall.HelpCenterRecord;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * HelpCenterSalesHelp
 * 
 * @author mulong
 * @data 2021-05-29 23:27:39
 */
@Getter
@Setter
@NoArgsConstructor
public class SalesHelp {
    private Integer id;
    /** 标题 */
    private String title;
    /** 正文 */
    private String content;
    /** 发布时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publishedTime;

    public SalesHelp(HelpCenterRecord record) {
        this.id = record.getId();
        this.title = record.getTitle();
        this.content = record.getContent();
        this.publishedTime = record.getPublishedTime();
    }

}
