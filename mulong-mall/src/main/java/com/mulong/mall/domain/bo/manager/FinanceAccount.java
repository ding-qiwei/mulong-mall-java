package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mulong.common.domain.pojo.mall.custom.CustomAccountFinancial;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * FinanceAccount
 * 
 * @author etiger
 * @data 2022-01-09 17:51:16
 */
@Getter
@Setter
@NoArgsConstructor
public class FinanceAccount {
    private Integer id;
    private Integer gatewayAccountId;
    private BigDecimal accountBalance;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private String gatewayAccountName;
    private String gatewayAccountType;
    private Integer gatewayAccountStatus;
    private String gatewayAccountRemark;

    public FinanceAccount(com.mulong.common.domain.pojo.mall.AccountFinancial record) {
        this.id = record.getId();
        this.gatewayAccountId = record.getGatewayAccountId();
        this.accountBalance = record.getAccountBalance();
        this.updateTime = record.getUpdateTime();
    }

    public FinanceAccount(CustomAccountFinancial record) {
        this.id = record.getId();
        this.gatewayAccountId = record.getGatewayAccountId();
        this.accountBalance = record.getAccountBalance();
        this.updateTime = record.getUpdateTime();
        this.gatewayAccountName = record.getGatewayAccountName();
        this.gatewayAccountType = record.getGatewayAccountType();
        this.gatewayAccountStatus = record.getGatewayAccountStatus();
        this.gatewayAccountRemark = record.getGatewayAccountRemark();
    }

    public com.mulong.common.domain.pojo.mall.AccountFinancial buildAccountFinancial() {
        com.mulong.common.domain.pojo.mall.AccountFinancial record = new com.mulong.common.domain.pojo.mall.AccountFinancial();
        record.setId(id);
        record.setGatewayAccountId(gatewayAccountId);
        record.setAccountBalance(accountBalance);
        return record;
    }

}
