package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetGoodsNewByPageParam
 * 
 * @author mulong
 * @data 2021-06-14 09:21:49
 */
@Getter
@Setter
public class GetGoodsNewByPageParam extends PageParam {

}
