package com.mulong.mall.domain.bo.manager;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DeliveryType
 * 
 * @author mulong
 * @data 2021-05-31 15:50:22
 */
@Getter
@Setter
@NoArgsConstructor
public class DeliveryType {
    private Integer id;
    private String name;
    private String description;
    private Integer deliveryTypeOrder;

    public DeliveryType(com.mulong.common.domain.pojo.mall.DeliveryType record) {
        this.id = record.getId();
        this.name = record.getName();
        this.description = record.getDescription();
        this.deliveryTypeOrder = record.getDeliveryTypeOrder();
    }

    public com.mulong.common.domain.pojo.mall.DeliveryType buildDeliveryType() {
        com.mulong.common.domain.pojo.mall.DeliveryType record = new com.mulong.common.domain.pojo.mall.DeliveryType();
        record.setId(id);
        record.setName(name);
        record.setDescription(description);;
        record.setDeliveryTypeOrder(deliveryTypeOrder);
        return record;
    }

}
