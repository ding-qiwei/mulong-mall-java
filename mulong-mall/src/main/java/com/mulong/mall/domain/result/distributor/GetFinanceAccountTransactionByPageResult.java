package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.FinanceAccountTransaction;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetFinanceAccountTransactionByPageResult
 * 
 * @author mulong
 * @data 2021-05-30 17:01:37
 */
public class GetFinanceAccountTransactionByPageResult extends PageResult<List<FinanceAccountTransaction>> {

    public GetFinanceAccountTransactionByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
