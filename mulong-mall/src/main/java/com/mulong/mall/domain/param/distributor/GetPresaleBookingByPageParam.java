package com.mulong.mall.domain.param.distributor;

import java.util.Date;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetPresaleBookingByPageParam
 * 
 * @author mulong
 * @data 2021-06-14 08:55:11
 */
@Getter
@Setter
public class GetPresaleBookingByPageParam extends PageParam {
    private String artNo;
    private Integer brandId;
    private String titleKeyword;
    private Date expectRecivedDateFrom;
    private Date expectRecivedDateTo;
}
