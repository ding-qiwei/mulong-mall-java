package com.mulong.mall.domain.param.manager;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadInventoryParam
 * 
 * @author mulong
 * @data 2021-07-13 09:52:21
 */
@Getter
@Setter
public class DownloadInventoryParam {
    private List<Integer> supplierChannelIdList;
}
