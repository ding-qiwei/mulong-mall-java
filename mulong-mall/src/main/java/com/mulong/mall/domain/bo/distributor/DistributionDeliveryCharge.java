package com.mulong.mall.domain.bo.distributor;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDeliveryCharge;
import com.mulong.common.enums.Province;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DistributionDeliveryCharge
 * 
 * @author mulong
 * @data 2021-05-29 22:02:27
 */
@Getter
@Setter
@NoArgsConstructor
public class DistributionDeliveryCharge {
    private Long id;
    private Integer deliveryTypeId;
    private String deliveryTypeTitle;
    private String province;
    private String provinceTitle;
    private BigDecimal firstPrice;
    private BigDecimal addPrice;

    public DistributionDeliveryCharge(CustomSupplierChannelDeliveryCharge record) {
        if (record.getChannelId() >= 0) {
            throw new MulongException(ErrorEnums.UNKNOWN_ERROR);
        }
        this.id = record.getId();
        this.deliveryTypeId = record.getDeliveryTypeId();
        this.deliveryTypeTitle = record.getDeliveryTypeTitle();
        this.province = record.getProvince();
        Province province = Province.getByCode(this.province);
        this.provinceTitle = province == null ? null : province.getName();
        this.firstPrice = record.getFirstPrice();
        this.addPrice = record.getAddPrice();
    }

}
