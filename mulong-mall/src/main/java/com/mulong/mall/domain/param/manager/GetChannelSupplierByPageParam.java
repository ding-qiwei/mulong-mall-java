package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetChannelByPageParam
 * 
 * @author mulong
 * @data 2021-05-31 16:25:41
 */
@Getter
@Setter
public class GetChannelSupplierByPageParam extends PageParam {
    private String nameKeyword;
}
