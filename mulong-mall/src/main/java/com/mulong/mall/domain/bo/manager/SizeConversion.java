package com.mulong.mall.domain.bo.manager;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SizeConversion
 * 
 * @author mulong
 * @data 2021-06-25 20:48:00
 */
@Getter
@Setter
@NoArgsConstructor
public class SizeConversion {
    private Integer id;
    private Integer brandId;
    private Integer goodsCategory;
    private Integer goodsSex;
    private String originalSize;
    private String normalizedSize;
    private String remark;

    public SizeConversion(com.mulong.common.domain.pojo.mall.SizeConversion record) {
        this.id = record.getId();
        this.brandId = record.getBrandId();
        this.goodsCategory = record.getGoodsCategory();
        this.goodsSex = record.getGoodsSex();
        this.originalSize = record.getOriginalSize();
        this.normalizedSize = record.getNormalizedSize();
        this.remark = record.getRemark();
    }

    public com.mulong.common.domain.pojo.mall.SizeConversion buildSizeConversion() {
        com.mulong.common.domain.pojo.mall.SizeConversion record = new com.mulong.common.domain.pojo.mall.SizeConversion();
        record.setId(id);
        record.setBrandId(brandId);
        record.setGoodsCategory(goodsCategory);
        record.setGoodsSex(goodsSex);
        record.setOriginalSize(originalSize);
        record.setNormalizedSize(normalizedSize);
        record.setRemark(remark);
        return record;
    }

}
