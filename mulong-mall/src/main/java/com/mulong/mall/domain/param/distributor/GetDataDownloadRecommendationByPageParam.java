package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDataDownloadRecommendationByPageParam
 * 
 * @author mulong
 * @data 2021-06-13 23:24:12
 */
@Getter
@Setter
public class GetDataDownloadRecommendationByPageParam extends PageParam {

}
