package com.mulong.mall.domain.bo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * EnumBrand
 * 
 * @author mulong
 * @data 2021-05-31 09:31:00
 */
@Getter
@Setter
@NoArgsConstructor
public class EnumBrand {
    private Integer id;
    private String name;
    private String shortName;

    public EnumBrand(com.mulong.common.domain.pojo.mall.Brand record) {
        this.id = record.getId();
        this.name = record.getName();
        this.shortName = record.getShortName();
    }

}
