package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.PlatformStrategy;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetHelpCenterPlatformStrategyByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 23:35:29
 */
public class GetPlatformStrategyByPageResult extends PageResult<List<PlatformStrategy>> {

    public GetPlatformStrategyByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
