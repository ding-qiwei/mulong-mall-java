package com.mulong.mall.domain.param.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadCustomerOrderParam
 * 
 * @author mulong
 * @data 2021-08-26 15:15:26
 */
@Getter
@Setter
public class DownloadCustomerOrderParam {
    private String customizedOrderId;
    private String sku;
    private Integer supplierChannelId;
    private String receiverName;
    private String expressNo;
    private Integer status;
    private String createTimeBegin;
    private String createTimeEnd;
    private String feedbackTimeBegin;
    private String feedbackTimeEnd;
}
