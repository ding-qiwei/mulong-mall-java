package com.mulong.mall.domain.bo.distributor;

import java.math.BigDecimal;
import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mulong.common.domain.pojo.mall.AccountRecharge;
import com.mulong.common.enums.AccountRechargeChannel;
import com.mulong.common.enums.AccountRechargeStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * FinanceAccountRecharge
 * 
 * @author mulong
 * @data 2021-05-30 16:33:50
 */
@Getter
@Setter
public class FinanceAccountRecharge {
    private Long id;
    /** 交易流水号 */
    private String transactionSerialNo;
    /** 充值金额 */
    private BigDecimal amount;
    /** 充值方式 */
    private Integer rechargeChannel;
    private String rechargeChannelTitle;
    /** 充值时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date rechargeTime;
    /** 处理时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date handleTime;
    /** 处理状态 */
    private Integer status;
    private String statusTitle;
    /** 申请人id */
    private Integer applyAccountId;
    /** 申请人名字 */
    private String applyAccountName;
    /** 充值备注 */
    private String remark;
    /** 提示信息 */
    private String tips;

    public FinanceAccountRecharge(AccountRecharge record) {
        this.id = record.getId();
        this.transactionSerialNo = record.getTransactionSerialNo();
        this.amount = record.getAmount();
        this.rechargeChannel = record.getRechargeChannel();
        AccountRechargeChannel financeRechargeChannel = AccountRechargeChannel.getByCode(this.rechargeChannel);
        this.rechargeChannelTitle = financeRechargeChannel == null ? null : financeRechargeChannel.getName();
        this.rechargeTime = record.getRechargeTime();
        this.handleTime = record.getHandleTime();
        this.status = record.getStatus();
        AccountRechargeStatus financeRechargeStatus = AccountRechargeStatus.getByCode(this.status);
        this.statusTitle = financeRechargeStatus == null ? null : financeRechargeStatus.getName();
        this.applyAccountId = record.getApplyGatewayAccountId();
        this.applyAccountName = null; // TODO
        this.remark = record.getRemark();
        this.tips = record.getTips();
    }

}
