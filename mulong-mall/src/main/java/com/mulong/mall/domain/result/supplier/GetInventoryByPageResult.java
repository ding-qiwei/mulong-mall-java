package com.mulong.mall.domain.result.supplier;

import java.util.*;

import com.mulong.mall.domain.bo.supplier.Inventory;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetInventoryByPageResult
 * 
 * @author mulong
 * @data 2021-07-09 13:43:32
 */
public class GetInventoryByPageResult extends PageResult<List<Inventory>> {

    public GetInventoryByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
