package com.mulong.mall.domain.result.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * BookingResult
 * 
 * @author mulong
 * @data 2021-07-29 17:44:31
 */
@Getter
@Setter
public class BookingResult {

}
