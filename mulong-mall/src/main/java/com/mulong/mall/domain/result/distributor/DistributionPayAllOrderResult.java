package com.mulong.mall.domain.result.distributor;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionPayAllOrderResult
 * 
 * @author mulong
 * @data 2021-08-19 21:36:14
 */
@Getter
@Setter
public class DistributionPayAllOrderResult {
    private int payOrderCount;
    private BigDecimal totalPay;
}
