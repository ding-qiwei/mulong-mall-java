package com.mulong.mall.domain.param.distributor;

import java.util.*;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetPresaleOrderByPageParam
 * 
 * @author mulong
 * @data 2021-06-14 08:57:32
 */
@Getter
@Setter
public class GetPresaleOrderByPageParam extends PageParam {
    
}
