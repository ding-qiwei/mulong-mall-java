package com.mulong.mall.domain.bo.distributor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * HelpCenterPlatformDocument
 * 
 * @author mulong
 * @data 2021-05-29 23:26:15
 */
@Getter
@Setter
@NoArgsConstructor
public class PlatformDocument {

}
