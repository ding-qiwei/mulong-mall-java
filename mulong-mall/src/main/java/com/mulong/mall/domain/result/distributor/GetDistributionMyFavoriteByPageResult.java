package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DistributionMyFavorite;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDistributionMyFavoriteByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 22:37:04
 */
public class GetDistributionMyFavoriteByPageResult extends PageResult<List<DistributionMyFavorite>> {

    public GetDistributionMyFavoriteByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
