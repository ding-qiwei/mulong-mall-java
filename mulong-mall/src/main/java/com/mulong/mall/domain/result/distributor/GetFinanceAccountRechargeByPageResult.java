package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.FinanceAccountRecharge;
import com.mulong.mall.domain.result.PageResult;

import lombok.Getter;
import lombok.Setter;

/**
 * GetAccountRechargeByPageResult
 * 
 * @author mulong
 * @data 2021-05-30 16:57:01
 */
@Getter
@Setter
public class GetFinanceAccountRechargeByPageResult extends PageResult<List<FinanceAccountRecharge>> {

    public GetFinanceAccountRechargeByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
