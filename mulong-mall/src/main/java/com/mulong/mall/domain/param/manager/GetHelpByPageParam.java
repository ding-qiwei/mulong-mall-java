package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetHelpByPageParam
 * 
 * @author mulong
 * @data 2021-06-21 15:38:50
 */
@Getter
@Setter
public class GetHelpByPageParam extends PageParam {
    private Integer category;
    private Integer status;
    private String titleKeyword;
}
