package com.mulong.mall.domain.result.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadProductResult
 * 
 * @author mulong
 * @data 2021-06-30 16:19:13
 */
@Getter
@Setter
public class UploadProductResult {
    private Integer productCount; 
}
