package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DistributionTrade;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDistributionTradeByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 22:17:37
 */
public class GetDistributionTradeByPageResult extends PageResult<List<DistributionTrade>> {

    public GetDistributionTradeByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
