package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.DeliveryType;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDeliveryTypeByPageResult
 * 
 * @author mulong
 * @data 2021-05-31 16:24:21
 */
public class GetDeliveryTypeByPageResult extends PageResult<List<DeliveryType>> {

    public GetDeliveryTypeByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
