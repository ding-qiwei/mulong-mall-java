package com.mulong.mall.domain.bo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * EnumSupplierChannel
 * 
 * @author mulong
 * @data 2021-05-31 15:58:56
 */
@Getter
@Setter
@NoArgsConstructor
public class EnumSupplierChannel {
    private Integer id;
    private String name;
    private String shortName;

    public EnumSupplierChannel(com.mulong.common.domain.pojo.mall.SupplierChannel record) {
        this.id = record.getId();
        this.name = record.getName();
        this.shortName = record.getShortName();
    }

}
