package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.OthersHelp;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetHelpCenterOthersHelpByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 23:32:53
 */
public class GetOthersHelpByPageResult extends PageResult<List<OthersHelp>> {

    public GetOthersHelpByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
