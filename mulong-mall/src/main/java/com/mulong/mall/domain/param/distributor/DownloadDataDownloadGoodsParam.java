package com.mulong.mall.domain.param.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadDataDownloadGoodsParam
 * 
 * @author mulong
 * @data 2021-06-13 23:35:32
 */
@Getter
@Setter
public class DownloadDataDownloadGoodsParam {

}
