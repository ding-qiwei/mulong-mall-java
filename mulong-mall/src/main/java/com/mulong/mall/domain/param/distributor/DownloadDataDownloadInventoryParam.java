package com.mulong.mall.domain.param.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadDataDownloadInventoryParam
 * 
 * @author mulong
 * @data 2021-06-13 23:32:45
 */
@Getter
@Setter
public class DownloadDataDownloadInventoryParam {
    private Integer supplierChannelId;
}
