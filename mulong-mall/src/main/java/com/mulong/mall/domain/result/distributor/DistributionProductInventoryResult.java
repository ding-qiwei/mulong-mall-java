package com.mulong.mall.domain.result.distributor;

import java.util.*;

import org.springframework.util.CollectionUtils;

import com.mulong.mall.domain.bo.ProductInventory;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionProductInventoryResult
 * 
 * @author mulong
 * @data 2021-08-18 23:35:54
 */
@Getter
@Setter
public class DistributionProductInventoryResult {
    private Integer count;
    private List<ProductInventory> list;

    public DistributionProductInventoryResult() {
        this.count = 0;
    }

    public DistributionProductInventoryResult(List<ProductInventory> list) {
        this.setList(list);
    }

    public void setList(List<ProductInventory> list) {
        this.list = list;
        if (CollectionUtils.isEmpty(list)) {
            this.count = 0;
        } else {
            this.count = this.list.size();
        }
    }

}
