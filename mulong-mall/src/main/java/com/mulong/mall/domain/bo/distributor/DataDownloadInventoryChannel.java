package com.mulong.mall.domain.bo.distributor;

import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mulong.common.domain.pojo.mall.SupplierChannel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DataDownloadInventoryChannel
 * 
 * @author mulong
 * @data 2021-06-13 23:20:34
 */
@Getter
@Setter
@NoArgsConstructor
public class DataDownloadInventoryChannel {
    private Integer supplierChannelId;
    /** 名称 */
    private String name;
    /** 简称 */
    private String shortName;
    /** 库存更新时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date inventoryUpdateTime;
    /** 库存更新备注 */
    private String inventoryUpdateRemark;

    public DataDownloadInventoryChannel(SupplierChannel record) {
        this.supplierChannelId = record.getId();
        this.name = record.getName();
        this.shortName = record.getShortName();
        this.inventoryUpdateTime = record.getInventoryUpdateTime();
        this.inventoryUpdateRemark = record.getInventoryUpdateRemark();
    }

}
