package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetFinanceAccountRechargeByPageParam
 * 
 * @author mulong
 * @data 2021-05-30 16:35:26
 */
@Getter
@Setter
public class GetFinanceAccountRechargeByPageParam extends PageParam {
    private String transactionSerialNo;
    private Integer status;
}
