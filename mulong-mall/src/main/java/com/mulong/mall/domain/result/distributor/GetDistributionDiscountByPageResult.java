package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DistributionDiscount;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDistributionDiscountByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 22:30:02
 */
public class GetDistributionDiscountByPageResult extends PageResult<List<DistributionDiscount>> {

    public GetDistributionDiscountByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
