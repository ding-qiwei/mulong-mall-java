package com.mulong.mall.domain.bo;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.mulong.common.domain.pojo.mall.custom.CustomDataDictionary;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DataDict
 * 
 * @author mulong
 * @data 2021-03-31 12:14:05
 */
@Getter
@Setter
@NoArgsConstructor
public class DataDict {
    private Integer id;
    private String code;
    private String name;
    private List<Detail> details;

    public DataDict(CustomDataDictionary record) {
        this.id = record.getId();
        this.code = record.getCode();
        this.name = record.getName();
        List<CustomDataDictionary.Detail> recordDetails = record.getContentObject();
        if (!CollectionUtils.isEmpty(recordDetails)) {
            this.details = recordDetails.stream().map(Detail::new).collect(Collectors.toList());
            Collections.sort(this.details, new Comparator<Detail>() {
                @Override
                public int compare(Detail o1, Detail o2) {
                    return o1.index.compareTo(o2.index);
                }
            });
        }
    }

    public CustomDataDictionary buildCustomDataDictionary() {
        CustomDataDictionary record = new CustomDataDictionary();
        record.setId(id);
        record.setCode(code);
        record.setName(name);
        if (!CollectionUtils.isEmpty(details)) {
            record.setContentObject(details.stream().map(item -> item.buildCustomDataDictionaryDetail()).collect(Collectors.toList()));
        }
        return record;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class Detail {
        private String code;
        private String name;
        private String description;
        private Integer index;

        public Detail(CustomDataDictionary.Detail record) {
            this.code = record.getCode();
            this.name = record.getName();
            this.description = record.getDescription();
            this.index = record.getIndex();
        }

        public CustomDataDictionary.Detail buildCustomDataDictionaryDetail() {
            CustomDataDictionary.Detail record = new CustomDataDictionary.Detail();
            record.setCode(code);
            record.setName(name);
            record.setDescription(description);
            record.setIndex(index);
            return record;
        }

    }

}
