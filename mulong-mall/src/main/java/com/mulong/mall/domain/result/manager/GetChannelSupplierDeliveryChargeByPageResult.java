package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.ChannelSupplierDeliveryCharge;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetChannelDeliveryChargeByPageResult
 * 
 * @author mulong
 * @data 2021-06-01 14:36:54
 */
public class GetChannelSupplierDeliveryChargeByPageResult extends PageResult<List<ChannelSupplierDeliveryCharge>> {

    public GetChannelSupplierDeliveryChargeByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
