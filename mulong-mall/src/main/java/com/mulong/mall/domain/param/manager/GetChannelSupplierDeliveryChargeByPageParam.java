package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetChannelDeliveryChargeByPageParam
 * 
 * @author mulong
 * @data 2021-06-01 14:36:27
 */
@Getter
@Setter
public class GetChannelSupplierDeliveryChargeByPageParam extends PageParam {
    private Integer channelId;
    private Integer deliveryTypeId;
    private String province;
}
