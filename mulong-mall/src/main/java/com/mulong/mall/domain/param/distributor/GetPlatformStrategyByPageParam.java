package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetHelpCenterPlatformStrategyByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 23:31:06
 */
@Getter
@Setter
public class GetPlatformStrategyByPageParam extends PageParam {
    private String titleKeyword;
}
