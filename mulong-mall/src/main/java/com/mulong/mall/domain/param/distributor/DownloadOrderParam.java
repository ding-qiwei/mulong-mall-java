package com.mulong.mall.domain.param.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadOrderParam
 * 
 * @author mulong
 * @data 2021-08-26 16:54:27
 */
@Getter
@Setter
public class DownloadOrderParam {
    private String customizedOrderId;
    private String sku;
    private Integer supplierChannelId;
    private String receiverName;
    private String expressNo;
    private Integer status;
    private String createTimeBegin;
    private String createTimeEnd;
    private String feedbackTimeBegin;
    private String feedbackTimeEnd;
}
