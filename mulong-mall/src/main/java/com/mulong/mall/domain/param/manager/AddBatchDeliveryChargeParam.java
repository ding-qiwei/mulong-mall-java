package com.mulong.mall.domain.param.manager;

import java.math.BigDecimal;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * AddBatchDeliveryChargeParam
 * 
 * @author mulong
 * @data 2021-10-14 14:40:32
 */
@Getter
@Setter
public class AddBatchDeliveryChargeParam {
    private List<String> provinces;
    private List<Integer> deliveryTypeIds;
    private BigDecimal firstPrice;
    private BigDecimal addPrice;
}
