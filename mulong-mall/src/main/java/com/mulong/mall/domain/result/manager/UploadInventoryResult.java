package com.mulong.mall.domain.result.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadInventoryResult
 * 
 * @author mulong
 * @data 2021-07-14 15:06:48
 */
@Getter
@Setter
public class UploadInventoryResult {
    private Integer inventoryCount;
}
