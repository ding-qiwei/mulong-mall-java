package com.mulong.mall.domain.param.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadDataDownloadRecommendationParam
 * 
 * @author mulong
 * @data 2021-06-13 23:34:51
 */
@Getter
@Setter
public class DownloadDataDownloadRecommendationParam {

}
