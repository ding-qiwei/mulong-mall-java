package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.ChannelSupplierDiscount;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetChannelDiscountByPageResult
 * 
 * @author mulong
 * @data 2021-06-01 14:30:45
 */
public class GetChannelSupplierDiscountByPageResult extends PageResult<List<ChannelSupplierDiscount>> {

    public GetChannelSupplierDiscountByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
