package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.SystemHelp;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetHelpCenterSystemHelpByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 23:37:32
 */
public class GetSystemHelpByPageResult extends PageResult<List<SystemHelp>> {

    public GetSystemHelpByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
