package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetFinanceAccountByPageParam
 * 
 * @author etiger
 * @data 2022-01-11 11:12:14
 */
@Getter
@Setter
public class GetFinanceAccountByPageParam extends PageParam {
    private Integer gatewayAccountId;
    private String gatewayAccountType;
    private Integer gatewayAccountStatus; 
}
