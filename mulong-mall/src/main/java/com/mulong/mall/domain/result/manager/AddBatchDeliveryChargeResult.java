package com.mulong.mall.domain.result.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * AddBatchDeliveryChargeResult
 * 
 * @author mulong
 * @data 2021-10-14 14:39:33
 */
@Getter
@Setter
public class AddBatchDeliveryChargeResult {
    private Integer count;
}
