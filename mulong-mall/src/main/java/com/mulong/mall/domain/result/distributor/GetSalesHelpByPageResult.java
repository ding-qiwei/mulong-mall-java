package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.SalesHelp;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetHelpCenterSalesHelpByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 23:36:19
 */
public class GetSalesHelpByPageResult extends PageResult<List<SalesHelp>> {

    public GetSalesHelpByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
