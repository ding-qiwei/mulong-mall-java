package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetHelpCenterSystemHelpByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 23:32:11
 */
@Getter
@Setter
public class GetSystemHelpByPageParam extends PageParam {
    private String titleKeyword;
}
