package com.mulong.mall.domain.param;

import lombok.Getter;
import lombok.Setter;

/**
 * GetGatewayAccountByPageParam
 * 
 * @author mulong
 * @data 2021-03-31 12:46:23
 */
@Getter
@Setter
public class GetGatewayAccountByPageParam extends PageParam {
    private String usernameKeyword;
    private String type;
    private Integer status;
}
