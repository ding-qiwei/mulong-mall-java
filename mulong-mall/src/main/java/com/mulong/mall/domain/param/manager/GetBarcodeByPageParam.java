package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetBarcodeByPageParam
 * 
 * @author etiger
 * @data 2021-11-26 16:08:16
 */
@Getter
@Setter
public class GetBarcodeByPageParam extends PageParam {
    private String sku;
    private String barcode;
}
