package com.mulong.mall.domain.bo.distributor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DistributionGroupon
 * 
 * @author mulong
 * @data 2021-05-29 21:59:28
 */
@Getter
@Setter
@NoArgsConstructor
public class DistributionGroupon {
    private Long id;
}
