package com.mulong.mall.domain.param;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDataDictByPageParam
 * 
 * @author mulong
 * @data 2021-03-31 12:54:45
 */
@Getter
@Setter
public class GetDataDictByPageParam extends PageParam {
    private String codeKeyword;
    private String nameKeyword;
}
