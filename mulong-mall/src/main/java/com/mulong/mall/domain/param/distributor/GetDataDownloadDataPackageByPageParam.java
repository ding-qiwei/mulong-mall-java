package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDataDownloadDataPackageByPageParam
 * 
 * @author mulong
 * @data 2021-06-13 23:24:40
 */
@Getter
@Setter
public class GetDataDownloadDataPackageByPageParam extends PageParam {

}
