package com.mulong.mall.domain.result;

import java.util.*;

import com.mulong.mall.domain.bo.DataDict;

/**
 * GetDataDictByPageResult
 * 
 * @author mulong
 * @data 2021-03-31 12:55:41
 */
public class GetDataDictByPageResult extends PageResult<List<DataDict>> {

    public GetDataDictByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
