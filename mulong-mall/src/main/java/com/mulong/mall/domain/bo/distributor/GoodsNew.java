package com.mulong.mall.domain.bo.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * GoodsNew
 * 
 * @author mulong
 * @data 2021-06-14 09:22:35
 */
@Getter
@Setter
public class GoodsNew {
    private Long id;
}
