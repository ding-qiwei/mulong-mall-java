package com.mulong.mall.domain.result.distributor;

import com.mulong.mall.domain.bo.distributor.DataDownloadDataPackage;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDataDownloadDataPackageByPageResult
 * 
 * @author mulong
 * @data 2021-06-13 23:27:05
 */
public class GetDataDownloadDataPackageByPageResult extends PageResult<DataDownloadDataPackage> {

    public GetDataDownloadDataPackageByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
