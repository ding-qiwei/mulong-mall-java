package com.mulong.mall.domain.param.manager;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * BookingParam
 * 
 * @author mulong
 * @data 2021-07-29 17:44:12
 */
@Getter
@Setter
public class BookingParam {
    private String name;
    private String mobilephone;
    private String telephone;
    private String province;
    private String city;
    private String district;
    private String zipcode;
    private String orderId;
    private String address;
    private String remark;
    private List<Detail> details;

    @Getter
    @Setter
    public static class Detail {
        private Long inventoryId;
        private Integer qty;
    }

}
