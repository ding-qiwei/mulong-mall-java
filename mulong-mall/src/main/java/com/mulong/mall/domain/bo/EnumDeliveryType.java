package com.mulong.mall.domain.bo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * EnumDeliveryType
 * 
 * @author mulong
 * @data 2021-05-31 15:59:06
 */
@Getter
@Setter
@NoArgsConstructor
public class EnumDeliveryType {
    private Integer id;
    private String name;

    public EnumDeliveryType(com.mulong.common.domain.pojo.mall.DeliveryType record) {
        this.id = record.getId();
        this.name = record.getName();
    }

}
