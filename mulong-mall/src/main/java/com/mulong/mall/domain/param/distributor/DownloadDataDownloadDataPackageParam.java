package com.mulong.mall.domain.param.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadDataDownloadDataPackageParam
 * 
 * @author mulong
 * @data 2021-06-13 23:35:12
 */
@Getter
@Setter
public class DownloadDataDownloadDataPackageParam {

}
