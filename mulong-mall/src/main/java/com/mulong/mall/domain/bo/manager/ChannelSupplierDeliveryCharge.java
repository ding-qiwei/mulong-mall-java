package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDeliveryCharge;
import com.mulong.common.enums.Province;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SupplierChannelDeliveryCharge
 * 
 * @author mulong
 * @data 2021-06-01 14:29:20
 */
@Getter
@Setter
@NoArgsConstructor
public class ChannelSupplierDeliveryCharge {
    private Long id;
    private Integer channelId;
    private Integer deliveryTypeId;
    private String deliveryTypeTitle;
    private String province;
    private String provinceTitle;
    private BigDecimal firstPrice;
    private BigDecimal addPrice;

    public ChannelSupplierDeliveryCharge(com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge record) {
        this.id = record.getId();
        this.channelId = record.getChannelId();
        this.deliveryTypeId = record.getDeliveryTypeId();
        this.province = record.getProvince();
        Province province = Province.getByCode(this.province);
        this.provinceTitle = province == null ? null : province.getName();
        this.firstPrice = record.getFirstPrice();
        this.addPrice = record.getAddPrice();
    }

    public ChannelSupplierDeliveryCharge(CustomSupplierChannelDeliveryCharge record) {
        this.id = record.getId();
        this.channelId = record.getChannelId();
        this.deliveryTypeId = record.getDeliveryTypeId();
        this.deliveryTypeTitle = record.getDeliveryTypeTitle();
        this.province = record.getProvince();
        Province province = Province.getByCode(this.province);
        this.provinceTitle = province == null ? null : province.getName();
        this.firstPrice = record.getFirstPrice();
        this.addPrice = record.getAddPrice();
    }

    public com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge buildSupplierChannelDeliveryCharge() {
        com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge record = new com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge();
        record.setId(id);
        record.setChannelId(channelId);
        record.setDeliveryTypeId(deliveryTypeId);
        record.setProvince(province);
        record.setFirstPrice(firstPrice);
        record.setAddPrice(addPrice);
        return record;
    }

}
