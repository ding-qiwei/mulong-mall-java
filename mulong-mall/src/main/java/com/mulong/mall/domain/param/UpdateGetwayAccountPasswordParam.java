package com.mulong.mall.domain.param;

import jakarta.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

/**
 * UpdateGetwayAccountPasswordParam
 * 
 * @author mulong
 * @data 2021-03-31 13:13:30
 */
@Getter
@Setter
public class UpdateGetwayAccountPasswordParam {
    @NotBlank
    private String username;
    @NotBlank
    private String oldPassword;
    @NotBlank
    private String newPassword;
    @NotBlank
    private String reNewPassword;
}
