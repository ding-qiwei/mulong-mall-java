package com.mulong.mall.domain.bo.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * PresaleOrder
 * 
 * @author mulong
 * @data 2021-06-14 08:56:11
 */
@Getter
@Setter
public class PresaleOrder {
    private Long id;
}
