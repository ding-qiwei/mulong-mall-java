package com.mulong.mall.domain.result.supplier;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadInventoryResult
 * 
 * @author mulong
 * @data 2021-07-09 13:44:41
 */
@Getter
@Setter
public class UploadInventoryResult {
    private Integer inventoryCount; 
}
