package com.mulong.mall.domain.param.distributor;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionProductInventoryParam
 * 
 * @author mulong
 * @data 2021-08-18 23:35:35
 */
@Getter
@Setter
public class DistributionProductInventoryParam {
    private String skuPrefix;
}
