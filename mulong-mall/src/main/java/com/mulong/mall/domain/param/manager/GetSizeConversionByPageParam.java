package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetSizeConversionByPageParam
 * 
 * @author mulong
 * @data 2021-06-25 22:15:16
 */
@Getter
@Setter
public class GetSizeConversionByPageParam extends PageParam {
    private Integer brandId;
    private Integer goodsCategory;
    private Integer goodsSex;
}
