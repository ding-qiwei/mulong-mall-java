package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetCustomerOrderByPageParam
 * 
 * @author mulong
 * @data 2021-08-20 10:11:20
 */
@Getter
@Setter
public class GetCustomerOrderByPageParam extends PageParam {
    private String customizedOrderId;
    private String sku;
    private Integer supplierChannelId;
    private String receiverName;
    private String expressNo;
    private Integer status;
    private String createTimeBegin;
    private String createTimeEnd;
    private String feedbackTimeBegin;
    private String feedbackTimeEnd;
}
