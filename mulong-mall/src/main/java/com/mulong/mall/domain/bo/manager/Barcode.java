package com.mulong.mall.domain.bo.manager;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Barcode
 * 
 * @author etiger
 * @data 2021-11-26 13:36:15
 */
@Getter
@Setter
@NoArgsConstructor
public class Barcode {
    private Long id;
    private String sku;
    private String normalizedSize;
    private String barcode;

    public Barcode(com.mulong.common.domain.pojo.mall.ProductBarcode record) {
        this.id = record.getId();
        this.sku = record.getSku();
        this.normalizedSize = record.getNormalizedSize();
        this.barcode = record.getBarcode();
    }

    public com.mulong.common.domain.pojo.mall.ProductBarcode buildProductBarcode() {
        com.mulong.common.domain.pojo.mall.ProductBarcode record = new com.mulong.common.domain.pojo.mall.ProductBarcode();
        record.setId(id);
        record.setSku(sku);
        record.setNormalizedSize(normalizedSize);
        record.setBarcode(barcode);
        return record;
    }

}
