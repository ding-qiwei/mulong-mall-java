package com.mulong.mall.domain.bo;

import com.mulong.common.domain.pojo.mall.custom.CustomGatewayAccount;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * GatewayAccount
 * 
 * @author mulong
 * @data 2021-03-31 12:12:09
 */
@Getter
@Setter
@NoArgsConstructor
public class GatewayAccount {
    private Integer id;
    private String username;
    private String password;
    private String type;
    private Integer status;
    private String remark;

    public GatewayAccount(CustomGatewayAccount record) {
        this.id = record.getId();
        this.username = record.getUsername();
        this.password = record.getPassword();
        this.type = record.getType();
        this.status = record.getStatus();
        this.remark = record.getRemark();
    }

    public CustomGatewayAccount buildCustomGatewayAccount() {
        CustomGatewayAccount record = new CustomGatewayAccount();
        record.setId(id);
        record.setUsername(username);
        record.setPassword(password);
        record.setType(type);
        record.setStatus(status);
        record.setRemark(remark);
        return record;
    }

}
