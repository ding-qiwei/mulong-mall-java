package com.mulong.mall.domain.result.manager;

import java.util.*;

import org.springframework.util.CollectionUtils;

import com.mulong.mall.domain.bo.ProductInventory;

import lombok.Getter;
import lombok.Setter;

/**
 * GetProductInventoryResult
 * 
 * @author mulong
 * @data 2021-07-12 14:02:13
 */
@Getter
@Setter
public class GetProductInventoryResult {
    private Integer count;
    private List<ProductInventory> list;

    public GetProductInventoryResult() {
        this.count = 0;
    }

    public GetProductInventoryResult(List<ProductInventory> list) {
        this.setList(list);
    }

    public void setList(List<ProductInventory> list) {
        this.list = list;
        if (CollectionUtils.isEmpty(list)) {
            this.count = 0;
        } else {
            this.count = this.list.size();
        }
    }

}
