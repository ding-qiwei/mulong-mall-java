package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDistributionMyFavoriteByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 22:26:53
 */
@Getter
@Setter
public class GetDistributionMyFavoriteByPageParam extends PageParam {
    private String artNo;
    private String productName;
}
