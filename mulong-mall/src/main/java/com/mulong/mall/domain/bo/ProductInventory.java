package com.mulong.mall.domain.bo;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mulong.common.domain.pojo.mall.custom.ProductInventoryInventory;
import com.mulong.common.domain.pojo.mall.custom.ProductInventoryProduct;
import com.mulong.common.enums.GoodsCategory;
import com.mulong.common.enums.GoodsSex;

import lombok.Getter;
import lombok.Setter;

/**
 * ProductInventory
 * 
 * @author mulong
 * @data 2021-06-25 20:49:55
 */
@Getter
@Setter
public class ProductInventory {
    private Long id;
    private String sku;
    private Integer brandId;
    private String brandName;
    private String productName;
    private String productSeries;
    private Integer goodsCategory;
    private String goodsCategoryName;
    private String year;
    private String season;
    private Integer goodsSex;
    private String goodsSexName;
    private String color;
    private BigDecimal weight;
    private BigDecimal marketPrice;
    private String saleProps;
    private String material;
    private String remark;
    private List<String> extendColList;
    private List<SupplierInventory> supplierInventoryList;

    public ProductInventory(ProductInventoryProduct record) {
        this.id = record.getId();
        this.sku = record.getSku();
        this.brandId = record.getBrandId();
        this.brandName = record.getBrandShortName();
        this.productName = record.getName();
        this.productSeries = record.getSeries();
        this.goodsCategory = record.getGoodsCategory();
        GoodsCategory goodsCategory = GoodsCategory.getByCode(this.goodsCategory);
        this.goodsCategoryName = goodsCategory == null ? null : goodsCategory.getName();
        this.year = record.getYear();
        this.season = record.getSeason();
        this.goodsSex = record.getGoodsSex();
        GoodsSex goodsSex = GoodsSex.getByCode(this.goodsSex);
        this.goodsSexName = goodsSex == null ? null : goodsSex.getName();
        this.color = record.getColor();
        this.weight = record.getWeight();
        this.marketPrice = record.getMarketPrice();
        this.saleProps = record.getSaleProps();
        this.material = record.getMaterial();
        this.remark = record.getRemark();
        if (!CollectionUtils.isEmpty(record.getInventoryList())) {
            Set<String> extendColSet = new HashSet<>();
            Map<Integer, SupplierInventory> supplierInventoryMap = new HashMap<>();
            for (ProductInventoryInventory inventory : record.getInventoryList()) {
                Integer supplierChannelId = inventory.getSupplierChannelId();
                String normalizedSize = inventory.getNormalizedSize();
                Integer qty = inventory.getQty();
                extendColSet.add(normalizedSize);
                SupplierInventory supplierInventory = supplierInventoryMap.get(supplierChannelId);
                if (supplierInventory == null) {
                    supplierInventory = new SupplierInventory();
                    supplierInventory.setSupplierChannelId(supplierChannelId);
                    supplierInventory.setSupplierChannelName(inventory.getSupplierChannelShortName());
                    supplierInventory.setSku(inventory.getSku());
                    supplierInventory.setMarketPrice(inventory.getMarketPrice());
                    supplierInventory.setDiscount(inventory.getDiscount());
                    BigDecimal discountPrice = inventory.getMarketPrice().multiply(inventory.getDiscount());
                    supplierInventory.setDiscountPrice(discountPrice);
                    supplierInventory.setCreateTime(inventory.getCreateTime());
                    supplierInventoryMap.put(supplierChannelId, supplierInventory);
                }
                supplierInventory.addInventory(normalizedSize, qty);
            }
            this.extendColList = extendColSet.stream().map(Function.identity()).collect(Collectors.toList());
            Collections.sort(this.extendColList);
            this.supplierInventoryList = supplierInventoryMap.values().stream().map(Function.identity()).collect(Collectors.toList());
        }
    }

    @Getter
    @Setter
    public static class SupplierInventory {
        private Integer supplierChannelId;
        private String supplierChannelName;
        private String sku;
        private Map<String, Integer> extendValues = new HashMap<>();
        private int total = 0;
        private BigDecimal marketPrice;
        private BigDecimal discount;
        private BigDecimal discountPrice;
        @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
        @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
        private Date createTime;

        public void addInventory(String normalizedSize, Integer qty) {
            Integer value = extendValues.get(normalizedSize);
            if (value != null) {
                value += qty;
            } else {
                value = qty;
            }
            extendValues.put(normalizedSize, value);
            this.total += qty;
        }

    }

}
