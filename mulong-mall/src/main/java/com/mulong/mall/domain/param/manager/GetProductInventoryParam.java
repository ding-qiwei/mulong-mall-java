package com.mulong.mall.domain.param.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * GetInventoryByPageParam
 * 
 * @author mulong
 * @data 2021-07-12 14:01:38
 */
@Getter
@Setter
public class GetProductInventoryParam {
    private String skuPrefix;
}
