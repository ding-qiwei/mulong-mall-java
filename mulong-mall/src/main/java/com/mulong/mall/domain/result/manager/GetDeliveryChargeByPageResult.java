package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.DeliveryCharge;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDeliveryChargeByPageResult
 * 
 * @author mulong
 * @data 2021-06-23 10:47:51
 */
public class GetDeliveryChargeByPageResult extends PageResult<List<DeliveryCharge>> {

    public GetDeliveryChargeByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
