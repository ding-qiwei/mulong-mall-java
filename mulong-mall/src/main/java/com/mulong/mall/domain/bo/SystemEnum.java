package com.mulong.mall.domain.bo;

import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * SystemEnum
 * 
 * @author mulong
 * @data 2021-06-01 20:39:58
 */
@Getter
@Setter
public class SystemEnum {
    private String code;
    private List<?> options;
}
