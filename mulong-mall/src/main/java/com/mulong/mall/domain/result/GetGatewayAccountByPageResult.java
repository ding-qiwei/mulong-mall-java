package com.mulong.mall.domain.result;

import java.util.*;

import com.mulong.mall.domain.bo.GatewayAccount;

/**
 * GetGatewayAccountByPageResult
 * 
 * @author mulong
 * @data 2021-03-31 12:47:22
 */
public class GetGatewayAccountByPageResult extends PageResult<List<GatewayAccount>> {

    public GetGatewayAccountByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
