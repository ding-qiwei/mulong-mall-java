package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DistributionGroupon;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetGrouponByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 22:07:26
 */
public class GetDistributionGrouponByPageResult extends PageResult<List<DistributionGroupon>> {

    public GetDistributionGrouponByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
