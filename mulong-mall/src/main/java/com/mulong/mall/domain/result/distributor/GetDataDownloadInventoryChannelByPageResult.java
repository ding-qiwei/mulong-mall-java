package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DataDownloadInventoryChannel;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDataDownloadInventoryChannelByPageResult
 * 
 * @author mulong
 * @data 2021-06-13 23:25:39
 */
public class GetDataDownloadInventoryChannelByPageResult extends PageResult<List<DataDownloadInventoryChannel>> {

    public GetDataDownloadInventoryChannelByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
