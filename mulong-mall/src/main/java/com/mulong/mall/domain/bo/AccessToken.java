package com.mulong.mall.domain.bo;

import java.util.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * AccessToken
 * 
 * @author mulong
 * @data 2021-08-10 18:18:27
 */
@Getter
@Setter
@NoArgsConstructor
public class AccessToken {
    private Integer gatewayAccountId;
    private String username;
    private String usertype; 
    private Date expiredTime;
}
