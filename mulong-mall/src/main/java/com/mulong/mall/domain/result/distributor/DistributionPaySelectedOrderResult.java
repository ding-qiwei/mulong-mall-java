package com.mulong.mall.domain.result.distributor;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionPaySelectedOrderResult
 * 
 * @author mulong
 * @data 2021-08-19 21:38:29
 */
@Getter
@Setter
public class DistributionPaySelectedOrderResult {
    private int payOrderCount;
    private BigDecimal totalPay;
}
