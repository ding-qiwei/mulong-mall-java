package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDistributionDiscountByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 22:19:10
 */
@Getter
@Setter
public class GetDistributionDiscountByPageParam extends PageParam {
    private Integer channelId;
    private Integer brandId;
    private Integer goodsCategory;
}
