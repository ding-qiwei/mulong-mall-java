package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetProductByPageParam
 * 
 * @author mulong
 * @data 2021-06-25 22:07:44
 */
@Getter
@Setter
public class GetProductByPageParam extends PageParam {
    private String sku;
    private Integer brandId;
    private Integer goodsCategory;
    private String year;
    private String season;
    private Integer goodsSex;
}
