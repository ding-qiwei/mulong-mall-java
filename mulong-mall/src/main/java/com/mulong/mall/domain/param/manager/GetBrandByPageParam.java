package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetBrandByPageParam
 * 
 * @author mulong
 * @data 2021-06-21 14:26:40
 */
@Getter
@Setter
public class GetBrandByPageParam extends PageParam {
    private String nameKeyword;
}
