package com.mulong.mall.domain.bo.distributor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DistributionTrade
 * 
 * @author mulong
 * @data 2021-05-29 22:01:04
 */
@Getter
@Setter
@NoArgsConstructor
public class DistributionTrade {
    private Long id;
}
