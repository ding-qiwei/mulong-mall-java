package com.mulong.mall.domain.result.manager;

import java.util.List;

import com.mulong.mall.domain.bo.manager.HelpCenterRecord;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetHelpByPageResult
 * 
 * @author mulong
 * @data 2021-06-21 15:39:03
 */
public class GetHelpByPageResult extends PageResult<List<HelpCenterRecord>> {

    public GetHelpByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
