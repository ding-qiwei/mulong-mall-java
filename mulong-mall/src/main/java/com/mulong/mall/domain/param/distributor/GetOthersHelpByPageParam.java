package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetHelpCenterOthersHelpByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 23:30:33
 */
@Getter
@Setter
public class GetOthersHelpByPageParam extends PageParam {
    private String titleKeyword;
}
