package com.mulong.mall.domain.bo.distributor;

import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * PresaleBooking
 * 
 * @author mulong
 * @data 2021-06-14 08:55:43
 */
@Getter
@Setter
public class PresaleBooking {
    private Long id;
    /** 预计到货日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date expectRecivedDate;
    /** 预售单据 */
    private String title;
    /** 其它概况 */
    private String remark;
}
