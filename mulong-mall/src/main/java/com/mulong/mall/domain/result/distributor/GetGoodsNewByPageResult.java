package com.mulong.mall.domain.result.distributor;

import com.mulong.mall.domain.bo.distributor.GoodsNew;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetGoodsNewByPageResult
 * 
 * @author mulong
 * @data 2021-06-14 09:23:12
 */
public class GetGoodsNewByPageResult extends PageResult<GoodsNew> {

    public GetGoodsNewByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
