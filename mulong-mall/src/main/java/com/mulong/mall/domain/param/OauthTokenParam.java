package com.mulong.mall.domain.param;

import jakarta.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

/**
 * OauthTokenParam
 * 
 * @author mulong
 * @data 2021-08-10 18:01:51
 */
@Getter
@Setter
public class OauthTokenParam {
    /** username */
    @NotBlank(message="username不能为空")
    private String username;
    /** password */
    @NotBlank(message="password不能为空")
    private String password;
}
