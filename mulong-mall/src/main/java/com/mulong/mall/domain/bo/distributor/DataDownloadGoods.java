package com.mulong.mall.domain.bo.distributor;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * DataDownloadGoods
 * 
 * @author mulong
 * @data 2021-06-13 23:18:50
 */
@Getter
@Setter
public class DataDownloadGoods {
    private Long id;
    /** 产品图片 */
    private String imgUrl;
    /** 货号 */
    private String artNo;
    /** 品牌 */
    private Integer brandId;
    /** 分类 */
    private Integer categoryId;
    /** 子分类 */
    private Integer subCategoryId;
    /** 性别 */
    private Integer sexId;
    /** 市场价 */
    private BigDecimal marketPrice;
    /** 季节 */
    private Integer seasonId;
    /** 上市时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date listingDate;
    /** 产品名称 */
    private String name;
    /** 下载地址 */
    private String downloadUrl;
}
