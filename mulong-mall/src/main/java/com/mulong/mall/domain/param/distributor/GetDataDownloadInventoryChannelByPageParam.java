package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDataDownloadInventoryChannelByPageParam
 * 
 * @author mulong
 * @data 2021-06-13 23:23:42
 */
@Getter
@Setter
public class GetDataDownloadInventoryChannelByPageParam extends PageParam {
    private Integer supplierChannelId;
}
