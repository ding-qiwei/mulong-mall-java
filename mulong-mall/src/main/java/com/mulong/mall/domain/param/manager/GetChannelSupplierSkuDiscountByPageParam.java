package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetChannelSupplierSkuDiscountByPageParam
 * 
 * @author etiger
 * @data 2021-12-03 17:27:56
 */
@Getter
@Setter
public class GetChannelSupplierSkuDiscountByPageParam extends PageParam {
    private Integer channelId;
    private String sku;
}
