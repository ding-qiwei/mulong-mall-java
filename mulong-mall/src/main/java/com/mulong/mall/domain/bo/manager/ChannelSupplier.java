package com.mulong.mall.domain.bo.manager;

import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SupplierChannel
 * 
 * @author mulong
 * @data 2021-05-31 15:50:31
 */
@Getter
@Setter
@NoArgsConstructor
public class ChannelSupplier {
    private Integer id;
    /** 名称 */
    private String name;
    /** 简称 */
    private String shortName;
    /** 说明 */
    private String description;
    /** 关联网关账号 */
    private Integer relatedGatewayAccountId;
    /** 库存更新时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date inventoryUpdateTime;
    /** 库存更新备注 */
    private String inventoryUpdateRemark;

    public ChannelSupplier(com.mulong.common.domain.pojo.mall.SupplierChannel record) {
        this.id = record.getId();
        this.name = record.getName();
        this.shortName = record.getShortName();
        this.description = record.getDescription();
        this.relatedGatewayAccountId = record.getRelatedGatewayAccountId();
        this.inventoryUpdateTime = record.getInventoryUpdateTime();
        this.inventoryUpdateRemark = record.getInventoryUpdateRemark();
    }

    public com.mulong.common.domain.pojo.mall.SupplierChannel buildSupplierChannel() {
        com.mulong.common.domain.pojo.mall.SupplierChannel record = new com.mulong.common.domain.pojo.mall.SupplierChannel();
        record.setId(id);
        record.setName(name);
        record.setShortName(shortName);
        record.setDescription(description);
        record.setRelatedGatewayAccountId(relatedGatewayAccountId);
        record.setInventoryUpdateTime(inventoryUpdateTime);
        record.setInventoryUpdateRemark(inventoryUpdateRemark);
        return record;
    }

}
