package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDistributionGoodsByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 22:26:03
 */
@Getter
@Setter
public class GetDistributionGoodsDemandByPageParam extends PageParam {
    private String artNo;
    private Integer status;
    private Integer needNotice;
}
