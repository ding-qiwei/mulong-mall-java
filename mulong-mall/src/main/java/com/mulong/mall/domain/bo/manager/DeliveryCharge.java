package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.custom.CustomSupplierChannelDeliveryCharge;
import com.mulong.common.enums.Province;
import com.mulong.common.exception.ErrorEnums;
import com.mulong.common.exception.MulongException;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DeliveryCharge
 * 
 * @author mulong
 * @data 2021-06-23 10:40:40
 */
@Getter
@Setter
@NoArgsConstructor
public class DeliveryCharge {
    private Long id;
    private Integer deliveryTypeId;
    private String deliveryTypeTitle;
    private String province;
    private String provinceTitle;
    private BigDecimal firstPrice;
    private BigDecimal addPrice;

    public DeliveryCharge(com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge record) {
        if (record.getChannelId() >= 0) {
            throw new MulongException(ErrorEnums.UNKNOWN_ERROR);
        }
        this.id = record.getId();
        this.deliveryTypeId = record.getDeliveryTypeId();
        this.province = record.getProvince();
        Province province = Province.getByCode(this.province);
        this.provinceTitle = province == null ? null : province.getName();
        this.firstPrice = record.getFirstPrice();
        this.addPrice = record.getAddPrice();
    }

    public DeliveryCharge(CustomSupplierChannelDeliveryCharge record) {
        if (record.getChannelId() >= 0) {
            throw new MulongException(ErrorEnums.UNKNOWN_ERROR);
        }
        this.id = record.getId();
        this.deliveryTypeId = record.getDeliveryTypeId();
        this.deliveryTypeTitle = record.getDeliveryTypeTitle();
        this.province = record.getProvince();
        Province province = Province.getByCode(this.province);
        this.provinceTitle = province == null ? null : province.getName();
        this.firstPrice = record.getFirstPrice();
        this.addPrice = record.getAddPrice();
    }

    public com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge buildSupplierChannelDeliveryCharge() {
        com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge record = new com.mulong.common.domain.pojo.mall.SupplierChannelDeliveryCharge();
        record.setId(id);
        record.setChannelId(-1);
        record.setDeliveryTypeId(deliveryTypeId);
        record.setProvince(province);
        record.setFirstPrice(firstPrice);
        record.setAddPrice(addPrice);
        return record;
    }

}
