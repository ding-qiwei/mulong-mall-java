package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDistributionOrderByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 22:14:32
 */
@Getter
@Setter
public class GetDistributionOrderByPageParam extends PageParam {
    private String customizedOrderId;
    private String sku;
    private Integer supplierChannelId;
    private String receiverName;
    private String expressNo;
    private Integer status;
    private String createTimeBegin;
    private String createTimeEnd;
    private String feedbackTimeBegin;
    private String feedbackTimeEnd;
}
