package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;

import com.mulong.common.domain.pojo.mall.SupplierChannel;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadSupplierSkuDiscountRecord
 * 
 * @author etiger
 * @data 2021-12-04 16:06:01
 */
@Getter
@Setter
public class UploadSupplierSkuDiscountRecord {
    private String supplierChannelValue;
    private SupplierChannel supplierChannel;
    private String sku;
    private BigDecimal discount;
}
