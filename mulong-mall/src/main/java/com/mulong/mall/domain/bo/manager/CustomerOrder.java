package com.mulong.mall.domain.bo.manager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mulong.common.domain.pojo.mall.custom.CustomerOrderDetailInfo;
import com.mulong.common.enums.CustomerOrderStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * CustomerOrder
 * 
 * @author mulong
 * @data 2021-08-20 10:09:02
 */
@Getter
@Setter
public class CustomerOrder {
    private String orderId;
    private String customizedOrderId;
    private String sku;
    private String normalizedSize;
    private BigDecimal discountPrice;
    private BigDecimal payPrice;
    private BigDecimal deliveryCharge;
    private Integer qty;
    private String receiverName;
    private String receiverAddress;
    private String receiverZipcode;
    private String receiverMobilephone;
    private String receiverTelephone;
    private String expressNo;
    private Integer status;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date createTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date feedbackTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date onShipmentDate;
    private String remark;
    private String gatewayAccountName;
    private String supplierChannelName;
    private String supplierChannelShortName;
    private BigDecimal marketPrice;
    private BigDecimal discount;
    private String expectDeliveryTypeName;
    private String actualDeliveryTypeName;
    private String statusName;

    public CustomerOrder(CustomerOrderDetailInfo record) {
        this.orderId = record.getOrderId();
        this.customizedOrderId = record.getCustomizedOrderId();
        this.sku = record.getSku();
        this.normalizedSize = record.getNormalizedSize();
        this.discountPrice = record.getDiscountPrice();
        this.payPrice = record.getPayPrice();
        this.deliveryCharge = record.getDeliveryCharge();
        this.qty = record.getQty();
        this.receiverName = record.getReceiverName();
        this.receiverAddress = record.getReceiverAddress();
        this.receiverZipcode = record.getReceiverZipcode();
        this.receiverMobilephone = record.getReceiverMobilephone();
        this.receiverTelephone = record.getReceiverTelephone();
        this.expressNo = record.getExpressNo();
        this.status = record.getStatus();
        this.createTime = record.getCreateTime();
        this.feedbackTime = record.getFeedbackTime();
        this.onShipmentDate = record.getOnShipmentDate();
        this.remark = record.getRemark();
        this.gatewayAccountName = record.getGatewayAccountName();
        this.supplierChannelName = record.getSupplierChannelName();
        this.supplierChannelShortName = record.getSupplierChannelShortName();
        this.marketPrice = record.getMarketPrice();
        this.discount = getDiscount(record.getDiscountPrice(), record.getMarketPrice());
        this.expectDeliveryTypeName = record.getExpectDeliveryTypeName();
        this.actualDeliveryTypeName = record.getActualDeliveryTypeName();
        CustomerOrderStatus customerOrderStatus = CustomerOrderStatus.getByCode(this.status);
        if (customerOrderStatus != null) {
            this.statusName = customerOrderStatus.getName();
        }
    }

    private BigDecimal getDiscount(BigDecimal discountPrice, BigDecimal marketPrice) {
        double discount = (discountPrice.doubleValue() + 0.0)/marketPrice.doubleValue();
        return BigDecimal.valueOf(discount).setScale(3, RoundingMode.HALF_UP);
    }

}
