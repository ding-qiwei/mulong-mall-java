package com.mulong.mall.domain.result.distributor;

import com.mulong.mall.domain.bo.distributor.PresaleBooking;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetPresaleBookingByPageResult
 * 
 * @author mulong
 * @data 2021-06-14 08:54:53
 */
public class GetPresaleBookingByPageResult extends PageResult<PresaleBooking> {

    public GetPresaleBookingByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
