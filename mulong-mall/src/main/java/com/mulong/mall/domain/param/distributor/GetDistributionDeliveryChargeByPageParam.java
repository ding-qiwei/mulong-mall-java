package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetDistributionExpressChargeByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 22:24:56
 */
@Getter
@Setter
public class GetDistributionDeliveryChargeByPageParam extends PageParam {
    private Integer deliveryTypeId;
    private String province;
}
