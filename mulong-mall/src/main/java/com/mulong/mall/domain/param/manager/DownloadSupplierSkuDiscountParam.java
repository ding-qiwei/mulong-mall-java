package com.mulong.mall.domain.param.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadSupplierSkuDiscountParam
 * 
 * @author etiger
 * @data 2021-12-03 20:29:19
 */
@Getter
@Setter
public class DownloadSupplierSkuDiscountParam {
    private Integer channelId;
    private String sku;
}
