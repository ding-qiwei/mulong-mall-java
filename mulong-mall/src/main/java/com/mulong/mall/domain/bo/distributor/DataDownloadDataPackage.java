package com.mulong.mall.domain.bo.distributor;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * DataDownloadDataPackage
 * 
 * @author mulong
 * @data 2021-06-13 23:22:12
 */
@Getter
@Setter
public class DataDownloadDataPackage {
    private Long id;
    /** 数据包名称 */
    private String title;
    /** 时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** 数据说明 */
    private String description;
    /** 下载次数 */
    private Integer downloadCount;
    /** 下载地址 */
    private Integer downloadUrl;
}
