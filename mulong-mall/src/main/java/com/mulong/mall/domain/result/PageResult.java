package com.mulong.mall.domain.result;

import lombok.Getter;
import lombok.Setter;

/**
 * PageResult
 * 
 * @author mulong
 * @data 2021-03-30 14:06:41
 */
public class PageResult<T> {
    @Getter
    @Setter
    private T result;
    @Getter
    private int pageSize;
    @Getter
    private int pageNo;
    @Getter
    private int totalCount;

    public PageResult(int pageNo, int pageSize, int totalCount) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
    }

    public PageResult(int pageNo, int pageSize, int totalCount, T result) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.result = result;
    }

    public int getPageCount() {
        if (totalCount <= 0) {
            return 0;
        } else {
            return totalCount % pageSize == 0 ? totalCount / pageSize : (totalCount / pageSize) + 1;
        }
    }

}
