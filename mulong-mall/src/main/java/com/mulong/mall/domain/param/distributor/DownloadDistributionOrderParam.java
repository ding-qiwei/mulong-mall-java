package com.mulong.mall.domain.param.distributor;

import java.util.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadDistributionOrderParam
 * 
 * @author mulong
 * @data 2021-08-19 11:50:10
 */
@Getter
@Setter
public class DownloadDistributionOrderParam {
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date createTimeBegin;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date createTimeEnd;
}
