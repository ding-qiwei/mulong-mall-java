package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.ChannelSupplier;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetChannelByPageResult
 * 
 * @author mulong
 * @data 2021-05-31 16:26:28
 */
public class GetChannelSupplierByPageResult extends PageResult<List<ChannelSupplier>> {

    public GetChannelSupplierByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
