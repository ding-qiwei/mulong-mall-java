package com.mulong.mall.domain.bo.manager;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Brand
 * 
 * @author mulong
 * @data 2021-06-21 14:24:23
 */
@Getter
@Setter
@NoArgsConstructor
public class Brand {
    private Integer id;
    private String name;
    private String shortName;
    private Integer brandOrder;
    private String description;

    public Brand(com.mulong.common.domain.pojo.mall.Brand record) {
        this.id = record.getId();
        this.name = record.getName();
        this.shortName = record.getShortName();
        this.brandOrder = record.getBrandOrder();
        this.description = record.getDescription();
    }

    public com.mulong.common.domain.pojo.mall.Brand buildBrand() {
        com.mulong.common.domain.pojo.mall.Brand record = new com.mulong.common.domain.pojo.mall.Brand();
        record.setId(id);
        record.setName(name);
        record.setShortName(shortName);
        record.setBrandOrder(brandOrder);
        record.setDescription(description);
        return record;
    }

}
