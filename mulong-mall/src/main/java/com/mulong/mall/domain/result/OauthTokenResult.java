package com.mulong.mall.domain.result;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * OauthTokenResult
 * 
 * @author mulong
 * @data 2021-08-10 18:02:04
 */
@Getter
@Setter
public class OauthTokenResult {
    /** token */
    private String accessToken;
    /** token过期时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expiredTime;
}
