package com.mulong.mall.domain.result.distributor;

import java.util.*;

import com.mulong.mall.domain.bo.distributor.DistributionDeliveryCharge;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetDistributionExpressChargeByPageResult
 * 
 * @author mulong
 * @data 2021-05-29 22:31:37
 */
public class GetDistributionDeliveryChargeByPageResult extends PageResult<List<DistributionDeliveryCharge>> {

    public GetDistributionDeliveryChargeByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
