package com.mulong.mall.domain.param.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * DownloadSizeConversionParam
 * 
 * @author etiger
 * @data 2021-11-25 11:37:12
 */
@Getter
@Setter
public class DownloadSizeConversionParam {
    private Integer brandId;
    private Integer goodsCategory;
    private Integer goodsSex;
}
