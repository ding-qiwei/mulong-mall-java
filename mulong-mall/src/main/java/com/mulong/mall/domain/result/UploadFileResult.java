package com.mulong.mall.domain.result;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadFileResult
 * 
 * @author mulong
 * @data 2021-04-06 10:18:22
 */
@Getter
@Setter
public class UploadFileResult {
    private String url;
}
