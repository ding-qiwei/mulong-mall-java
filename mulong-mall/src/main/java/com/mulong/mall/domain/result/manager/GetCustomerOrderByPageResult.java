package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.CustomerOrder;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetCustomerOrderByPageResult
 * 
 * @author mulong
 * @data 2021-08-20 10:13:18
 */
public class GetCustomerOrderByPageResult extends PageResult<List<CustomerOrder>> {

    public GetCustomerOrderByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
