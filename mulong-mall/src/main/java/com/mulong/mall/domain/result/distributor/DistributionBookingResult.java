package com.mulong.mall.domain.result.distributor;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionBookingResult
 * 
 * @author mulong
 * @data 2021-05-29 21:59:02
 */
@Getter
@Setter
public class DistributionBookingResult {
    private BigDecimal needPayPrice;
}
