package com.mulong.mall.domain.param.distributor;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetGrouponByPageParam
 * 
 * @author mulong
 * @data 2021-05-29 22:05:48
 */
@Getter
@Setter
public class GetDistributionGrouponByPageParam extends PageParam {

}
