package com.mulong.mall.domain.result.manager;

import java.util.*;

import com.mulong.mall.domain.bo.manager.Brand;
import com.mulong.mall.domain.result.PageResult;

/**
 * GetBrandByPageResult
 * 
 * @author mulong
 * @data 2021-06-21 14:26:54
 */
public class GetBrandByPageResult extends PageResult<List<Brand>> {

    public GetBrandByPageResult(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

}
