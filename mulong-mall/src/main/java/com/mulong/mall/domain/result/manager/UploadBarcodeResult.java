package com.mulong.mall.domain.result.manager;

import lombok.Getter;
import lombok.Setter;

/**
 * UploadBarcodeResult
 * 
 * @author etiger
 * @data 2021-11-26 16:06:36
 */
@Getter
@Setter
public class UploadBarcodeResult {
    private Integer barcodeCount; 
}
