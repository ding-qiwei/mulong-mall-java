package com.mulong.mall.domain.result;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * GetUserInfoResult
 * 
 * @author mulong
 * @data 2021-03-31 13:11:29
 */
@Getter
@Setter
public class GetUserInfoResult {
    private List<String> roles;
    private String introduction;
    private String avatar;
    private String name;
}
