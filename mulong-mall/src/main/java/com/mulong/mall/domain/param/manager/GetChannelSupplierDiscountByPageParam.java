package com.mulong.mall.domain.param.manager;

import com.mulong.mall.domain.param.PageParam;

import lombok.Getter;
import lombok.Setter;

/**
 * GetChannelDiscountByPageParam
 * 
 * @author mulong
 * @data 2021-06-01 14:30:23
 */
@Getter
@Setter
public class GetChannelSupplierDiscountByPageParam extends PageParam {
    private Integer channelId;
    private Integer brandId;
    private Integer goodsCategory;
}
