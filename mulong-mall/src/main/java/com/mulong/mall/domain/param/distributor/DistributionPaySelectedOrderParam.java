package com.mulong.mall.domain.param.distributor;

import java.math.BigDecimal;
import java.util.*;

import lombok.Getter;
import lombok.Setter;

/**
 * DistributionPaySelectedOrderParam
 * 
 * @author mulong
 * @data 2021-08-19 21:39:17
 */
@Getter
@Setter
public class DistributionPaySelectedOrderParam {
    private List<Detail> details;

    @Getter
    @Setter
    public static class Detail {
        private String orderId;
        private BigDecimal payPrice;
    }

}
