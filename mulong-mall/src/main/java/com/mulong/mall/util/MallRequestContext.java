package com.mulong.mall.util;

import org.apache.commons.lang3.StringUtils;
import com.mulong.mall.domain.bo.AccessToken;
import com.mulong.common.enums.GatewayAccountType;
import com.mulong.common.util.ThreadContext;

/**
 * AdminRequestContext
 * 
 * @author mulong
 * @data 2020-12-13 22:44:20
 */
public class MallRequestContext {
    private static final String GATEWAY_ACCOUNT_ID_KEY = "GatewayAccountId";
    private static final String USERNAME_KEY = "Username";
    private static final String USERTYPE_KEY = "Usertype";

    private MallRequestContext() {
        throw new IllegalStateException("Utility class");
    }

    public static void init(AccessToken accessToken) {
        Integer gatewayAccountId = accessToken.getGatewayAccountId();
        if (gatewayAccountId != null) {
            ThreadContext.put(GATEWAY_ACCOUNT_ID_KEY, gatewayAccountId);
        }
        String username = accessToken.getUsername();
        if (StringUtils.isNotBlank(username)) {
            ThreadContext.put(USERNAME_KEY, username);
        }
        String usertype = accessToken.getUsertype();
        if (StringUtils.isNotBlank(usertype)) {
            GatewayAccountType gatewayAccountType = GatewayAccountType.getByCode(usertype);
            ThreadContext.put(USERTYPE_KEY, gatewayAccountType);
        }
    }

    public static Integer getGatewayAccountId() {
        return ThreadContext.getInteger(GATEWAY_ACCOUNT_ID_KEY);
    }

    public static String getUsername() {
        return ThreadContext.getString(USERNAME_KEY);
    }

    public static GatewayAccountType getUsertype() {
        return (GatewayAccountType) ThreadContext.get(USERTYPE_KEY);
    }

}
