package com.mulong.mall.config;

import java.io.File;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

/**
 * VariableConfig
 * 
 * @author mulong
 * @data 2021-06-24 09:18:20
 */
@Configuration
public class InitMallConfig {
    @Getter
    @Value("${mulong.mall.upload-tmp-directory}")
    private String uploadTmpDirectory;

    @PostConstruct
    public void init() {
        // upload tmp directory
        File uploadTmpDirectoryFile = new File(uploadTmpDirectory);
        uploadTmpDirectoryFile.deleteOnExit();
        uploadTmpDirectoryFile.mkdirs();
    }

    @PreDestroy
    public void destroy() {
        // upload tmp directory
        File uploadTmpDirectoryFile = new File(uploadTmpDirectory);
        uploadTmpDirectoryFile.deleteOnExit();
    }

}
