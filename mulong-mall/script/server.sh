#!/bin/bash

SERVICE_NAME="mulong-mall"
PORT=9001
SERVICE_HEALTH_URL="http://127.0.0.1:$PORT/mulong/mall/actuator/health"
ACTIVE_PROFILE="prod"
BASE_DIR="/home/mulong/mulong-mall-java/mulong-mall"
JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"
JAVA_OPT="-Xms256m -Xmx512m -Xmn256m -jar $SERVICE_NAME.jar --spring.profiles.active=$ACTIVE_PROFILE"
SHUTDOWN_URL="http://127.0.0.1:$PORT/mulong/mall/actuator/shutdown"
BACKUP_DIR="/home/mulong/mulong-mall-java/mulong-mall/backup"

function check_port() {
    echo -e "\033[32mChecking if service port:$PORT is UP...\033[0m"
    is_pass=0
    for timer in {1..6};do
        check=`netstat -lunpt|grep $PORT|wc -l`
        if [ $check -gt 0 ];then
            is_pass=1
            break;
        fi
        echo -e "\033[33mTry $timer times: $SERVICE_NAME:$PORT...\033[0m"
        timer=$[timer-1]
        sleep 10
    done
    if [ $is_pass -eq 0 ];then
        echo -e "\033[31mFail: $SERVICE_NAME:$PORT is not started successfully, pls check the startup logs for details!\033[0m"
        return 255
    else
        echo -e "\033[32mPASS: $SERVICE_NAME:$PORT is started sucessfully!\033[0m"
        fi
}

function check_service_health(){
    is_pass=0
    echo -e "\033[32mChecking service health via $SERVICE_HEALTH_URL...\033[0m"
    for timer in {1..10};do
        check=`curl -s $SERVICE_HEALTH_URL -m 5 -w %{http_code} -o /dev/null`
        if [ $check == "200" ];then
            is_pass=1
            break;
        fi
        echo -e "\033[33mTry $timer times: $SERVICE_NAME:$PORT...\033[0m"
        timer=$[timer-1]
        sleep 10
    done
    if [ $is_pass -eq 0 ];then
        echo -e "\033[31mFail: $SERVICE_NAME:$PORT is not started successfully, pls check the startup logs for details!\033[0m"
        return 255
    else
        echo -e "\033[32mPASS: $SERVICE_NAME:$PORT is started sucessfully!\033[0m"
    fi
}

function start(){
    cd $BASE_DIR
        echo "Starting $SERVICE_NAME ..."
        nohup $JAVA_HOME/bin/java $JAVA_OPT >/dev/null 2>&1 &

       if [[ '$SERVICE_HEALTH_URL' == '' ]];then
           check_port
       else
           check_service_health
       fi
}

function stop(){
    echo "Stopping Service: $SERVICE_NAME..."   
    curl -X POST $SHUTDOWN_URL -m 5

    is_stop=0
    echo -e "\033[32mChecking service health via $SERVICE_HEALTH_URL...\033[0m"
    for timer in {1..6};do
        check=`curl -s $SERVICE_HEALTH_URL -m 5 -w %{http_code} -o /dev/null`
        if [ $check -ne "200" ];then
            is_stop=1
            break;
        fi
        echo -e "\033[33mTry $timer times: $SERVICE_NAME:$PORT...\033[0m"
        timer=$[timer-1]
        sleep 5
    done
    ps aux|grep $SERVICE_NAME.jar|grep -v grep| awk '{print $2}'|xargs kill -9
    echo -e "\033[32mPASS: $SERVICE_NAME:$PORT is stopped sucessfully!\033[0m"
}

function restart(){
    stop
    sleep 3
    start
}

function status(){
    count=`netstat -lunpt|grep $PORT|wc -l`
    p_id=`ps -ef |grep java|grep $SERVICE_NAME.jar|grep -v grep|awk '{print $2}'`
    if [ $count -gt 0 ];then
        echo "$SERVICE_NAME is running(pid:$p_id)..."
    else
        echo "$SERVICE_NAME is not running..."
    fi

}

function backup() {
    cd $BASE_DIR
    if [ -f $SERVICE_NAME.jar ];then
        suffix=`date +%s`
        mv $SERVICE_NAME.jar $BACKUP_DIR/$SERVICE_NAME.jar-$suffix
        echo "$BASE_DIR/$SERVICE_NAME.jar move to $BACKUP_DIR/$SERVICE_NAME.jar-$suffix"
    fi
}

#############################################################
case $1 in
    start)
      start
      ;;
    stop)
      stop
      ;;
    restart)
      restart
      ;;
    status)
      status
      ;;
    backup)
      backup
      ;;
    *)
    echo -e "\033[0;31m Usage: \033[0m  \033[0;34m  $COMMAND  {start|stop|restart}   \033[0m
\033[0;31m Example: \033[0m
      \033[0;33m $COMMAND  start  \033[0m"
esac
