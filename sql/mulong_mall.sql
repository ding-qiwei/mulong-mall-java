/*
Navicat MySQL Data Transfer

Source Server         : localhost_root
Source Server Version : 80023
Source Host           : localhost:3306
Source Database       : mulong_mall

Target Server Type    : MYSQL
Target Server Version : 80023
File Encoding         : 65001

Date: 2021-10-18 21:10:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account_recharge
-- ----------------------------
DROP TABLE IF EXISTS `account_recharge`;
CREATE TABLE `account_recharge` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `transaction_serial_no` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '交易流水号',
  `amount` decimal(10,2) NOT NULL COMMENT '充值金额',
  `recharge_channel` tinyint NOT NULL COMMENT '充值渠道 0-银行打款充值 1-支付宝转账 2-微信转账',
  `recharge_time` timestamp NULL DEFAULT NULL COMMENT '充值时间',
  `handle_time` timestamp NULL DEFAULT NULL COMMENT '处理时间',
  `status` tinyint NOT NULL COMMENT '处理状态 0-待处理 1-充值完成',
  `apply_gateway_account_id` int NOT NULL COMMENT '申请人id',
  `remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '充值备注',
  `tips` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '提示信息',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of account_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for account_transaction
-- ----------------------------
DROP TABLE IF EXISTS `account_transaction`;
CREATE TABLE `account_transaction` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gateway_account_id` int NOT NULL,
  `transaction_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '交易流水号',
  `transaction_category` tinyint NOT NULL COMMENT '交易分类',
  `transaction_amount` decimal(10,2) NOT NULL COMMENT '交易金额',
  `transaction_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '交易时间',
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of account_transaction
-- ----------------------------

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '品牌名称',
  `short_name` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '品牌简称',
  `brand_order` int NOT NULL COMMENT '品牌排序',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '品牌说明',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  UNIQUE KEY `uk_short_name` (`short_name`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', '阿迪达斯（ADIDAS）', '阿迪达斯', '1', 0xE998BFE8BFAAE8BEBEE696AFEFBC88414449444153EFBC89, '2021-05-31 11:20:49', 'admin', '2021-05-31 11:20:49', 'admin');
INSERT INTO `brand` VALUES ('2', '阿迪生活（ADIDAS NEO）', '阿迪生活', '2', 0xE998BFE8BFAAE7949FE6B4BBEFBC88414449444153204E454FEFBC89, '2021-05-31 11:21:13', 'admin', '2021-05-31 11:21:13', 'admin');
INSERT INTO `brand` VALUES ('3', '阿迪三叶草（ADIDAS ORIGINALS）', '阿迪三叶草', '3', 0xE998BFE8BFAAE4B889E58FB6E88D89EFBC88414449444153204F524947494E414C53EFBC89, '2021-05-31 11:21:49', 'admin', '2021-05-31 11:21:49', 'admin');
INSERT INTO `brand` VALUES ('4', '阿迪儿童（ADIDAS KIDS）', '阿迪儿童', '4', 0xE998BFE8BFAAE584BFE7ABA5EFBC88414449444153204B494453EFBC89, '2021-05-31 11:23:34', 'admin', '2021-05-31 11:23:34', 'admin');
INSERT INTO `brand` VALUES ('5', '耐克（NIKE）', '耐克', '5', 0xE88090E5858BEFBC884E494B45EFBC89, '2021-05-31 11:23:44', 'admin', '2021-05-31 11:23:44', 'admin');
INSERT INTO `brand` VALUES ('6', '耐克360（NIKE360）', '耐克360', '6', 0xE88090E5858B333630EFBC884E494B45333630EFBC89, '2021-05-31 11:23:57', 'admin', '2021-05-31 11:23:57', 'admin');
INSERT INTO `brand` VALUES ('9', '耐克儿童（NIKE KIDS）', '耐克儿童', '7', 0xE88090E5858BE584BFE7ABA5EFBC884E494B45204B494453EFBC89, '2021-10-12 15:00:05', 'manager', '2021-10-12 15:00:05', 'manager');
INSERT INTO `brand` VALUES ('10', '耐克乔丹（NIKE JORDAN）', '耐克乔丹', '8', 0xE88090E5858BE4B994E4B8B9EFBC884E494B45204A4F5244414EEFBC89, '2021-10-12 15:01:11', 'manager', '2021-10-12 15:01:11', 'manager');
INSERT INTO `brand` VALUES ('11', '亚瑟士（ASICS）', '亚瑟士', '9', 0xE4BA9AE7919FE5A3ABEFBC884153494353EFBC89, '2021-10-12 15:01:30', 'manager', '2021-10-12 15:01:30', 'manager');
INSERT INTO `brand` VALUES ('12', '鬼塚虎（ONITSUKA TIGER）', '鬼塚虎', '10', 0xE9ACBCE5A19AE8998EEFBC884F4E495453554B41205449474552EFBC89, '2021-10-12 15:01:56', 'manager', '2021-10-12 15:01:56', 'manager');
INSERT INTO `brand` VALUES ('13', '彪马（PUMA）', '彪马', '11', 0xE5BDAAE9A9ACEFBC8850554D41EFBC89, '2021-10-12 15:02:38', 'manager', '2021-10-12 15:02:38', 'manager');
INSERT INTO `brand` VALUES ('14', '锐步（REEBOK）', '锐步', '12', 0xE99490E6ADA5EFBC88524545424F4BEFBC89, '2021-10-12 15:02:57', 'manager', '2021-10-12 15:02:57', 'manager');
INSERT INTO `brand` VALUES ('15', '匡威（CONVERSE）', '匡威', '13', 0xE58CA1E5A881EFBC88434F4E5645525345EFBC89, '2021-10-12 15:03:12', 'manager', '2021-10-12 15:03:12', 'manager');
INSERT INTO `brand` VALUES ('16', '卡帕（KAPPA）', '卡帕', '14', 0xE58DA1E5B895EFBC884B41505041EFBC89, '2021-10-12 15:03:26', 'manager', '2021-10-12 15:03:26', 'manager');
INSERT INTO `brand` VALUES ('17', 'NEW BALANCE', 'NEW BALANCE', '15', 0x4E45572042414C414E4345, '2021-10-12 15:03:51', 'manager', '2021-10-12 15:03:51', 'manager');
INSERT INTO `brand` VALUES ('18', 'OUTDOOR', 'OUTDOOR', '45', 0x4F5554444F4F52, '2021-10-12 15:04:21', 'manager', '2021-10-12 15:04:21', 'manager');
INSERT INTO `brand` VALUES ('19', '波尼（PONY）', '波尼', '44', 0xE6B3A2E5B0BCEFBC88504F4E59EFBC89, '2021-10-12 15:04:35', 'manager', '2021-10-12 15:04:35', 'manager');
INSERT INTO `brand` VALUES ('20', '冠军(CHAMPION)', '冠军', '43', 0xE586A0E5869B284348414D50494F4E29, '2021-10-12 15:04:57', 'manager', '2021-10-12 15:04:57', 'manager');
INSERT INTO `brand` VALUES ('21', 'DCshoes', 'DCshoes', '42', 0x444373686F6573, '2021-10-12 15:05:09', 'manager', '2021-10-12 15:05:09', 'manager');
INSERT INTO `brand` VALUES ('22', '雅品（IPURE）', '雅品', '41', 0xE99B85E59381EFBC884950555245EFBC89, '2021-10-12 15:05:24', 'manager', '2021-10-12 15:05:24', 'manager');
INSERT INTO `brand` VALUES ('23', '斯伯丁(SPALDING)', '斯伯丁', '40', 0xE696AFE4BCAFE4B881285350414C44494E4729, '2021-10-12 15:06:00', 'manager', '2021-10-12 15:06:00', 'manager');
INSERT INTO `brand` VALUES ('24', '乐飞叶（LAFUMA）', '乐飞叶', '39', 0xE4B990E9A39EE58FB6EFBC884C4146554D41EFBC89, '2021-10-12 15:06:15', 'manager', '2021-10-12 15:06:15', 'manager');
INSERT INTO `brand` VALUES ('25', '斯凯奇（SKECHERS）', '斯凯奇', '38', 0xE696AFE587AFE5A587EFBC88534B454348455253EFBC89, '2021-10-12 15:06:27', 'manager', '2021-10-12 15:06:27', 'manager');
INSERT INTO `brand` VALUES ('26', 'DCSHOECOUSA', 'DCSHOECOUSA', '37', 0x444353484F45434F555341, '2021-10-12 15:06:39', 'manager', '2021-10-12 15:06:39', 'manager');
INSERT INTO `brand` VALUES ('27', 'KUHL', 'KUHL', '36', 0x4B55484C, '2021-10-12 15:06:49', 'manager', '2021-10-12 15:06:49', 'manager');
INSERT INTO `brand` VALUES ('28', '伊海诗（EX2）', '伊海诗', '35', 0xE4BC8AE6B5B7E8AF97EFBC88455832EFBC89, '2021-10-12 15:07:08', 'manager', '2021-10-12 15:07:08', 'manager');
INSERT INTO `brand` VALUES ('29', '快乐狐狸（ACTION FOX）', '快乐狐狸', '34', 0xE5BFABE4B990E78B90E78BB8EFBC88414354494F4E20464F58EFBC89, '2021-10-12 15:07:20', 'manager', '2021-10-12 15:07:20', 'manager');
INSERT INTO `brand` VALUES ('30', '北极狐（FJALLRAVEN）', '北极狐', '33', 0xE58C97E69E81E78B90EFBC88464A414C4C524156454EEFBC89, '2021-10-12 15:07:35', 'manager', '2021-10-12 15:07:35', 'manager');
INSERT INTO `brand` VALUES ('31', '圣弗莱（SEVLAE）', '圣弗莱', '32', 0xE59CA3E5BC97E88EB1EFBC885345564C4145EFBC89, '2021-10-12 15:07:46', 'manager', '2021-10-12 15:07:46', 'manager');
INSERT INTO `brand` VALUES ('32', '肯拓普（CANTORP）', '肯拓普', '31', 0xE882AFE68B93E699AEEFBC8843414E544F5250EFBC89, '2021-10-12 15:07:58', 'manager', '2021-10-12 15:07:58', 'manager');
INSERT INTO `brand` VALUES ('33', '凯乐石（KAILAS）', '凯乐石', '30', 0xE587AFE4B990E79FB3EFBC884B41494C4153EFBC89, '2021-10-12 15:13:31', 'manager', '2021-10-12 15:13:31', 'manager');
INSERT INTO `brand` VALUES ('34', '阿肯诺（ACANU）', '阿肯诺', '29', 0xE998BFE882AFE8AFBAEFBC884143414E55EFBC89, '2021-10-12 15:13:51', 'manager', '2021-10-12 15:13:51', 'manager');
INSERT INTO `brand` VALUES ('35', 'DISCOVERY EXPEDITION', 'DISCOVERY EXPEDITION', '28', 0x444953434F564552592045585045444954494F4E, '2021-10-12 15:14:07', 'manager', '2021-10-12 15:14:07', 'manager');
INSERT INTO `brand` VALUES ('36', '探路者（TOREAD）', '探路者', '27', 0xE68EA2E8B7AFE88085EFBC88544F52454144EFBC89, '2021-10-12 15:14:28', 'manager', '2021-10-12 15:14:28', 'manager');
INSERT INTO `brand` VALUES ('37', '科诺修思（KROCEUS）', '科诺修思', '26', 0xE7A791E8AFBAE4BFAEE6809DEFBC884B524F43455553EFBC89, '2021-10-12 15:14:42', 'manager', '2021-10-12 15:14:42', 'manager');
INSERT INTO `brand` VALUES ('38', '添柏岚（TIMBERLAND）', '添柏岚', '25', 0xE6B7BBE69F8FE5B29AEFBC8854494D4245524C414E44EFBC89, '2021-10-12 15:14:55', 'manager', '2021-10-12 15:14:55', 'manager');
INSERT INTO `brand` VALUES ('39', 'LEKI', 'LEKI', '24', 0x4C454B49, '2021-10-12 15:15:05', 'manager', '2021-10-12 15:15:05', 'manager');
INSERT INTO `brand` VALUES ('40', 'LOWA', 'LOWA', '23', 0x4C4F5741, '2021-10-12 15:15:13', 'manager', '2021-10-12 15:15:13', 'manager');
INSERT INTO `brand` VALUES ('41', '诺诗兰（NORTHLAND）', '诺诗兰', '22', 0xE8AFBAE8AF97E585B0EFBC884E4F5254484C414E44EFBC89, '2021-10-12 15:15:57', 'manager', '2021-10-12 15:15:57', 'manager');
INSERT INTO `brand` VALUES ('42', '狼爪（JACK WOLFSKIN）', '狼爪', '21', 0xE78BBCE788AAEFBC884A41434B20574F4C46534B494EEFBC89, '2021-10-12 15:16:07', 'manager', '2021-10-12 15:16:07', 'manager');
INSERT INTO `brand` VALUES ('43', '乐斯菲斯（THE NORTH FACE）', '乐斯菲斯', '20', 0xE4B990E696AFE88FB2E696AFEFBC88544845204E4F5254482046414345EFBC89, '2021-10-12 15:16:19', 'manager', '2021-10-12 15:16:19', 'manager');
INSERT INTO `brand` VALUES ('44', '哥伦比亚（COLUMBIA）', '哥伦比亚', '19', 0xE593A5E4BCA6E6AF94E4BA9AEFBC88434F4C554D424941EFBC89, '2021-10-12 15:16:30', 'manager', '2021-10-12 15:16:30', 'manager');
INSERT INTO `brand` VALUES ('45', '特步（XTEP）', '特步', '18', 0xE789B9E6ADA5EFBC8858544550EFBC89, '2021-10-12 15:16:43', 'manager', '2021-10-12 15:16:43', 'manager');
INSERT INTO `brand` VALUES ('46', '安踏（ANTA）', '安踏', '17', 0xE5AE89E8B88FEFBC88414E5441EFBC89, '2021-10-12 15:17:22', 'manager', '2021-10-12 15:17:22', 'manager');
INSERT INTO `brand` VALUES ('47', '李宁（LI-NING）', '李宁', '16', 0xE69D8EE5AE81EFBC884C492D4E494E47EFBC89, '2021-10-12 15:17:34', 'manager', '2021-10-12 15:17:34', 'manager');

-- ----------------------------
-- Table structure for customer_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `customer_order_detail`;
CREATE TABLE `customer_order_detail` (
  `order_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `gateway_account_id` int NOT NULL,
  `customized_order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '自定义订单号',
  `sku` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `supplier_channel_id` int NOT NULL COMMENT '供应商渠道id',
  `normalized_size` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '尺码',
  `market_price` decimal(10,2) NOT NULL,
  `discount_price` decimal(10,2) NOT NULL COMMENT '折扣价',
  `delivery_charge` decimal(10,2) NOT NULL COMMENT '运费',
  `pay_price` decimal(10,2) NOT NULL COMMENT '付款金额',
  `actual_delivery_charge` decimal(10,0) DEFAULT NULL,
  `qty` int NOT NULL DEFAULT '1',
  `receiver_name` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '收件人',
  `receiver_province` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '省',
  `receiver_city` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '市',
  `receiver_district` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '区',
  `receiver_address` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '收件人地址',
  `receiver_zipcode` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `receiver_mobilephone` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `receiver_telephone` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `expect_delivery_type_id` int NOT NULL COMMENT '下单快递',
  `actual_delivery_type_id` int DEFAULT NULL COMMENT '实发快递',
  `express_no` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '快递单号',
  `status` tinyint NOT NULL COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `feedback_time` timestamp NULL DEFAULT NULL COMMENT '反馈时间',
  `on_shipment_date` date DEFAULT NULL COMMENT '预计发货日期',
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of customer_order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `data_dictionary`;
CREATE TABLE `data_dictionary` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '字典代码',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字典名称',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '字典内容',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='数据字典';

-- ----------------------------
-- Records of data_dictionary
-- ----------------------------
INSERT INTO `data_dictionary` VALUES ('12', 'YEAR', '年份', 0x5B7B22636F6465223A2232303232222C226465736372697074696F6E223A2232303232E5B9B4222C22696E646578223A35382C226E616D65223A2232303232227D2C7B22636F6465223A2232303231222C226465736372697074696F6E223A2232303231E5B9B4222C22696E646578223A35392C226E616D65223A2232303231227D2C7B22636F6465223A2232303230222C226465736372697074696F6E223A2232303230E5B9B4222C22696E646578223A36302C226E616D65223A2232303230227D2C7B22636F6465223A2232303139222C226465736372697074696F6E223A2232303139E5B9B4222C22696E646578223A36312C226E616D65223A2232303139227D2C7B22636F6465223A2232303138222C226465736372697074696F6E223A2232303138E5B9B4222C22696E646578223A36322C226E616D65223A2232303138227D2C7B22636F6465223A2232303131222C226465736372697074696F6E223A2232303131E5B9B4222C22696E646578223A36392C226E616D65223A2232303131227D2C7B22636F6465223A2232303130222C226465736372697074696F6E223A2232303130E5B9B4222C22696E646578223A37302C226E616D65223A2232303130227D5D, '2021-06-30 14:29:41', 'admin', '2021-06-30 14:52:02', 'admin');

-- ----------------------------
-- Table structure for delivery_type
-- ----------------------------
DROP TABLE IF EXISTS `delivery_type`;
CREATE TABLE `delivery_type` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `description` text COLLATE utf8mb4_bin,
  `delivery_type_order` int NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of delivery_type
-- ----------------------------
INSERT INTO `delivery_type` VALUES ('1', '普通快递', 0xE699AEE9809AE5BFABE9809228E58C85E68BACEFBC9AE794B3E9809AE38081E4B8ADE9809AE38081E99FB5E8BEBEE38081E6B187E9809A29, '1', '2021-05-31 21:59:03', 'admin', '2021-05-31 22:17:33', 'admin');
INSERT INTO `delivery_type` VALUES ('2', '顺丰', 0xE9A1BAE4B8B0, '5', '2021-05-31 21:59:32', 'admin', '2021-05-31 21:59:32', 'admin');
INSERT INTO `delivery_type` VALUES ('3', '自提', 0xE887AAE68F90, '45', '2021-05-31 21:59:43', 'admin', '2021-05-31 21:59:43', 'admin');
INSERT INTO `delivery_type` VALUES ('4', '顺丰到付', 0xE9A1BAE4B8B0E588B0E4BB98, '40', '2021-05-31 21:59:55', 'admin', '2021-05-31 21:59:55', 'admin');
INSERT INTO `delivery_type` VALUES ('5', '德邦', 0xE5BEB7E982A6, '10', '2021-05-31 22:00:35', 'admin', '2021-05-31 22:00:35', 'admin');
INSERT INTO `delivery_type` VALUES ('6', 'EMS', 0x454D53, '15', '2021-05-31 22:00:45', 'admin', '2021-05-31 22:00:45', 'admin');
INSERT INTO `delivery_type` VALUES ('7', '辽宁申通', 0xE8BEBDE5AE81E794B3E9809A, '20', '2021-05-31 22:00:56', 'admin', '2021-05-31 22:00:56', 'admin');
INSERT INTO `delivery_type` VALUES ('8', '辽宁圆通', 0xE8BEBDE5AE81E59C86E9809A, '25', '2021-05-31 22:01:08', 'admin', '2021-05-31 22:01:08', 'admin');
INSERT INTO `delivery_type` VALUES ('9', '上探申通', 0xE4B88AE68EA2E794B3E9809A, '30', '2021-05-31 22:01:27', 'admin', '2021-05-31 22:01:27', 'admin');
INSERT INTO `delivery_type` VALUES ('10', '德邦到付', 0xE5BEB7E982A6E588B0E4BB98, '35', '2021-05-31 22:01:39', 'admin', '2021-05-31 22:01:39', 'admin');

-- ----------------------------
-- Table structure for financial_account
-- ----------------------------
DROP TABLE IF EXISTS `financial_account`;
CREATE TABLE `financial_account` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gateway_account_id` int NOT NULL,
  `account_balance` decimal(10,2) NOT NULL COMMENT '账户余额',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_gateway_account_id` (`gateway_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of financial_account
-- ----------------------------

-- ----------------------------
-- Table structure for gateway_account
-- ----------------------------
DROP TABLE IF EXISTS `gateway_account`;
CREATE TABLE `gateway_account` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'guest' COMMENT '网关账号类型 admin-管理员 editor-编辑 user-用户 guest-访客',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '状态, 0-关闭 1-开启',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='网关账号';

-- ----------------------------
-- Records of gateway_account
-- ----------------------------
INSERT INTO `gateway_account` VALUES ('1', 'admin', '123456', 'admin', '1', '超级管理员账号', '2021-04-06 10:33:04', 'admin', '2021-09-22 16:00:57', 'admin');
INSERT INTO `gateway_account` VALUES ('5', 'manager', '123456', 'manager', '1', '后台管理员测试账号', '2021-06-21 12:16:23', 'admin', '2021-09-22 16:01:01', 'admin');
INSERT INTO `gateway_account` VALUES ('7', 'goods', '123456', 'goods', '1', '现货仓管理员测试账号\r\n', '2021-06-21 11:44:55', 'admin', '2021-09-22 16:02:19', 'admin');
INSERT INTO `gateway_account` VALUES ('8', 'retail', '123456', 'retail', '1', '零售测试账号', '2021-06-21 11:47:02', 'admin', '2021-09-22 16:02:06', 'admin');
INSERT INTO `gateway_account` VALUES ('9', 'service', '123456', 'service', '1', '客服测试账号', '2021-06-21 11:47:16', 'admin', '2021-09-22 16:01:38', 'admin');
INSERT INTO `gateway_account` VALUES ('10', 'distributor', '123456', 'distributor', '1', '分销商测试账号', '2021-06-21 11:47:33', 'admin', '2021-09-22 16:01:50', 'admin');
INSERT INTO `gateway_account` VALUES ('11', 'supplier', '123456', 'supplier', '1', '供应商测试账号', '2021-06-21 11:47:40', 'admin', '2021-09-22 16:02:31', 'admin');
INSERT INTO `gateway_account` VALUES ('13', 'supplier001', '123456', 'supplier', '1', '供应商测试账号', '2021-07-09 18:14:26', 'admin', '2021-09-22 16:02:32', 'admin');
INSERT INTO `gateway_account` VALUES ('14', 'supplier003', '1234561', 'supplier', '1', '供应商测试账号', '2021-09-22 15:45:39', 'admin', '2021-09-22 16:02:35', 'admin');

-- ----------------------------
-- Table structure for goods_demand
-- ----------------------------
DROP TABLE IF EXISTS `goods_demand`;
CREATE TABLE `goods_demand` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `apply_gateway_account_id` int NOT NULL,
  `art_no` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `size` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `qty` int NOT NULL,
  `max_discount` decimal(10,2) NOT NULL,
  `expect_date` date NOT NULL,
  `need_notice` tinyint DEFAULT NULL,
  `status` tinyint NOT NULL,
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of goods_demand
-- ----------------------------
INSERT INTO `goods_demand` VALUES ('1', '1', '123456', '40', '10', '1.00', '2021-06-16', '0', '0', '111', '2021-06-15 13:42:43', '2021-06-15 13:42:43');
INSERT INTO `goods_demand` VALUES ('2', '1', '654321', '41', '100', '5.11', '2021-06-26', '1', '0', '加急', '2021-06-15 13:49:07', '2021-06-15 13:49:07');

-- ----------------------------
-- Table structure for help_center_record
-- ----------------------------
DROP TABLE IF EXISTS `help_center_record`;
CREATE TABLE `help_center_record` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `category` tinyint NOT NULL DEFAULT '99' COMMENT '帮助分类 0-系统帮助 1-销售帮助 2-平台策略 99-其它帮助',
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态 0-已发布 1-待编辑 2-已编辑完毕 3-撤销发布',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '标题',
  `content` text COLLATE utf8mb4_bin COMMENT '正文',
  `published_time` timestamp NULL DEFAULT NULL COMMENT '发布时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of help_center_record
-- ----------------------------

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `supplier_channel_id` int NOT NULL,
  `sku` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `original_size` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `normalized_size` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `qty` int NOT NULL,
  `market_price` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of inventory
-- ----------------------------
INSERT INTO `inventory` VALUES ('23', '1', '123', 'AAA', '001', '10', '100.00', '0.90', '2021-07-15 16:04:37', 'manager');
INSERT INTO `inventory` VALUES ('24', '1', '123', 'BBB', '002', '20', '200.00', '0.80', '2021-07-15 16:04:37', 'manager');
INSERT INTO `inventory` VALUES ('25', '1', '666', '666', null, '666', '777.00', '888.00', '2021-07-15 16:04:37', 'manager');
INSERT INTO `inventory` VALUES ('26', '4', '123', 'AAA', '001', '10', '100.00', '0.90', '2021-07-15 16:41:55', 'manager');
INSERT INTO `inventory` VALUES ('27', '4', '123', 'BBB', '002', '20', '200.00', '0.80', '2021-07-15 16:41:55', 'manager');
INSERT INTO `inventory` VALUES ('28', '4', '666', '666', null, '666', '777.00', '888.00', '2021-07-15 16:41:55', 'manager');
INSERT INTO `inventory` VALUES ('29', '4', '777', '777', null, '888', '888.00', '888.00', '2021-07-15 16:41:55', 'manager');
INSERT INTO `inventory` VALUES ('30', '2', '123', 'AAA', '001', '10', '100.00', '0.90', '2021-07-15 16:48:03', 'supplier');
INSERT INTO `inventory` VALUES ('31', '2', '123', 'BBB', '002', '20', '200.00', '0.80', '2021-07-15 16:48:03', 'supplier');
INSERT INTO `inventory` VALUES ('32', '2', '666', '666', null, '666', '777.00', '888.00', '2021-07-15 16:48:03', 'supplier');
INSERT INTO `inventory` VALUES ('33', '2', '777', '777', null, '888', '888.00', '888.00', '2021-07-15 16:48:03', 'supplier');

-- ----------------------------
-- Table structure for my_favorite
-- ----------------------------
DROP TABLE IF EXISTS `my_favorite`;
CREATE TABLE `my_favorite` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `gateway_account_id` int NOT NULL,
  `art_no` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of my_favorite
-- ----------------------------
INSERT INTO `my_favorite` VALUES ('2', '1', '456', null, null);
INSERT INTO `my_favorite` VALUES ('3', '2', '111', null, null);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `brand_id` int NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `series` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `goods_category` tinyint NOT NULL,
  `year` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `season` char(2) COLLATE utf8mb4_bin NOT NULL,
  `goods_sex` tinyint NOT NULL,
  `color` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `weight` decimal(6,2) NOT NULL,
  `market_price` decimal(10,2) NOT NULL,
  `barcode` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `sale_props` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `material` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_art_no` (`sku`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('9', '123', '5', 'ceshi1', 'ceshi2', '3', '2023', 'Q2', '0', '颜色', '1.00', '20.00', '111', '222', '333', '333', '2021-06-30 23:51:09', 'manager', '2021-06-30 23:51:09', 'manager');

-- ----------------------------
-- Table structure for size_conversion
-- ----------------------------
DROP TABLE IF EXISTS `size_conversion`;
CREATE TABLE `size_conversion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `goods_category` tinyint NOT NULL,
  `goods_sex` tinyint NOT NULL,
  `original_size` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `normalized_size` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uk` (`brand_id`,`goods_category`,`goods_sex`,`original_size`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of size_conversion
-- ----------------------------
INSERT INTO `size_conversion` VALUES ('3', '5', '3', '0', 'AAA', '001', '测试', null, null, null, null);
INSERT INTO `size_conversion` VALUES ('4', '5', '3', '0', 'BBB', '002', '测试', null, null, '2021-07-09 18:54:56', null);

-- ----------------------------
-- Table structure for supplier_channel
-- ----------------------------
DROP TABLE IF EXISTS `supplier_channel`;
CREATE TABLE `supplier_channel` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `short_name` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `description` text COLLATE utf8mb4_bin,
  `related_gateway_account_id` int DEFAULT NULL,
  `inventory_update_time` timestamp NULL DEFAULT NULL,
  `inventory_update_remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  UNIQUE KEY `uk_short_name` (`short_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of supplier_channel
-- ----------------------------
INSERT INTO `supplier_channel` VALUES ('1', '渠道4', '渠道4', 0xE6B8A0E9819334, null, '2021-07-15 16:04:37', '4', '2021-05-31 22:38:46', 'admin', '2021-07-15 16:04:37', 'manager');
INSERT INTO `supplier_channel` VALUES ('2', '渠道2', '渠道2', 0xE6B8A0E9819332, '11', '2021-07-15 16:48:03', '5', '2021-05-31 22:38:54', 'admin', '2021-07-15 16:48:02', 'manager');
INSERT INTO `supplier_channel` VALUES ('4', '渠道5', '渠道5', 0xE6B8A0E9819335, '13', '2021-07-15 16:41:55', '3', '2021-07-09 18:15:39', 'manager', '2021-07-15 16:41:54', 'manager');

-- ----------------------------
-- Table structure for supplier_channel_delivery_charge
-- ----------------------------
DROP TABLE IF EXISTS `supplier_channel_delivery_charge`;
CREATE TABLE `supplier_channel_delivery_charge` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `channel_id` int NOT NULL,
  `delivery_type_id` int NOT NULL,
  `province` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `first_price` decimal(10,2) NOT NULL,
  `add_price` decimal(10,2) NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_channel_delivery_type_province` (`channel_id`,`delivery_type_id`,`province`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=351 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of supplier_channel_delivery_charge
-- ----------------------------
INSERT INTO `supplier_channel_delivery_charge` VALUES ('3', '1', '7', '北京', '3.00', '4.00', '2021-06-23 10:24:38', 'manager', '2021-06-23 10:25:20', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('7', '-1', '6', '台湾', '99.00', '99.00', '2021-10-12 16:48:44', 'manager', '2021-10-12 16:48:44', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('8', '-1', '6', '香港', '99.00', '99.00', '2021-10-12 16:52:06', 'manager', '2021-10-12 16:52:06', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('9', '-1', '6', '澳门', '99.00', '99.00', '2021-10-12 16:52:21', 'manager', '2021-10-12 16:52:21', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('10', '-1', '6', '上海', '28.00', '24.00', '2021-10-12 16:56:14', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('11', '-1', '6', '浙江', '28.00', '24.00', '2021-10-12 16:57:18', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('20', '-1', '6', '江苏', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('21', '-1', '6', '安徽', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('22', '-1', '6', '山东', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('23', '-1', '6', '北京', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('24', '-1', '6', '天津', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('25', '-1', '6', '广东', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('26', '-1', '6', '福建', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('27', '-1', '6', '河南', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('28', '-1', '6', '湖北', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('29', '-1', '6', '河北', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('30', '-1', '6', '甘肃', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('31', '-1', '6', '山西', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('32', '-1', '6', '重庆', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('33', '-1', '6', '陕西', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('34', '-1', '6', '四川', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('35', '-1', '6', '海南', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('36', '-1', '6', '云南', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('37', '-1', '6', '吉林', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('38', '-1', '6', '辽宁', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('39', '-1', '6', '贵州', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('40', '-1', '6', '黑龙江', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('41', '-1', '6', '青海', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('42', '-1', '6', '广西', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('43', '-1', '6', '内蒙', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('44', '-1', '6', '宁夏', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('45', '-1', '6', '西藏', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('46', '-1', '6', '新疆', '28.00', '24.00', '2021-10-14 16:39:45', 'manager', '2021-10-14 16:39:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('49', '-1', '5', '西藏', '50.00', '45.00', '2021-10-14 16:47:03', 'manager', '2021-10-14 16:47:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('50', '-1', '5', '新疆', '50.00', '45.00', '2021-10-14 16:47:03', 'manager', '2021-10-14 16:47:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('51', '-1', '5', '青海', '50.00', '45.00', '2021-10-14 16:47:03', 'manager', '2021-10-14 16:47:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('52', '-1', '5', '广西', '50.00', '45.00', '2021-10-14 16:47:03', 'manager', '2021-10-14 16:47:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('53', '-1', '5', '内蒙', '50.00', '45.00', '2021-10-14 16:47:03', 'manager', '2021-10-14 16:47:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('54', '-1', '5', '宁夏', '50.00', '45.00', '2021-10-14 16:47:03', 'manager', '2021-10-14 16:47:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('55', '-1', '5', '四川', '35.00', '30.00', '2021-10-14 16:54:27', 'manager', '2021-10-14 16:54:27', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('56', '-1', '5', '吉林', '35.00', '30.00', '2021-10-14 16:54:27', 'manager', '2021-10-14 16:54:27', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('57', '-1', '5', '辽宁', '35.00', '30.00', '2021-10-14 16:54:27', 'manager', '2021-10-14 16:54:27', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('58', '-1', '5', '贵州', '35.00', '30.00', '2021-10-14 16:54:27', 'manager', '2021-10-14 16:54:27', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('59', '-1', '5', '海南', '35.00', '30.00', '2021-10-14 16:54:27', 'manager', '2021-10-14 16:54:27', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('60', '-1', '5', '甘肃', '35.00', '30.00', '2021-10-14 16:54:27', 'manager', '2021-10-14 16:54:27', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('61', '-1', '5', '黑龙江', '35.00', '30.00', '2021-10-14 16:54:27', 'manager', '2021-10-14 16:54:27', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('62', '-1', '5', '上海', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('63', '-1', '5', '浙江', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('64', '-1', '5', '江苏', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('65', '-1', '5', '安徽', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('66', '-1', '5', '山东', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('67', '-1', '5', '北京', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('68', '-1', '5', '天津', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('69', '-1', '5', '广东', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('70', '-1', '5', '福建', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('71', '-1', '5', '江西', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('72', '-1', '5', '湖南', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('73', '-1', '5', '河南', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('74', '-1', '5', '湖北', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('75', '-1', '5', '河北', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('76', '-1', '5', '山西', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('77', '-1', '5', '云南', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('78', '-1', '5', '重庆', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('79', '-1', '5', '陕西', '30.00', '25.00', '2021-10-14 16:57:13', 'manager', '2021-10-14 16:57:13', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('80', '-1', '7', '辽宁', '12.00', '10.00', '2021-10-14 21:38:37', 'manager', '2021-10-14 21:45:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('82', '-1', '7', '吉林', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('83', '-1', '7', '黑龙江', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('84', '-1', '7', '北京', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('85', '-1', '7', '天津', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('86', '-1', '7', '内蒙', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('87', '-1', '7', '山西', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('88', '-1', '7', '河北', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('89', '-1', '7', '山东', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('90', '-1', '7', '河南', '15.00', '10.00', '2021-10-14 21:48:48', 'manager', '2021-10-14 21:49:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('100', '-1', '7', '上海', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('101', '-1', '7', '浙江', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('102', '-1', '7', '江苏', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('103', '-1', '7', '安徽', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('104', '-1', '7', '福建', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('105', '-1', '7', '湖南', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('106', '-1', '7', '湖北', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('107', '-1', '7', '广东', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('108', '-1', '7', '江西', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('109', '-1', '7', '重庆', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('110', '-1', '7', '陕西', '16.00', '12.00', '2021-10-14 21:50:34', 'manager', '2021-10-14 21:50:34', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('111', '-1', '7', '青海', '20.00', '15.00', '2021-10-14 21:51:22', 'manager', '2021-10-14 21:51:22', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('112', '-1', '7', '甘肃', '20.00', '15.00', '2021-10-14 21:51:22', 'manager', '2021-10-14 21:51:22', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('113', '-1', '7', '宁夏', '20.00', '15.00', '2021-10-14 21:51:22', 'manager', '2021-10-14 21:51:22', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('114', '-1', '7', '贵州', '20.00', '15.00', '2021-10-14 21:51:22', 'manager', '2021-10-14 21:51:22', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('115', '-1', '7', '四川', '20.00', '15.00', '2021-10-14 21:51:22', 'manager', '2021-10-14 21:51:22', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('116', '-1', '7', '西藏', '25.00', '22.00', '2021-10-14 21:52:23', 'manager', '2021-10-14 21:52:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('117', '-1', '7', '新疆', '25.00', '22.00', '2021-10-14 21:52:23', 'manager', '2021-10-14 21:52:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('118', '-1', '7', '云南', '25.00', '22.00', '2021-10-14 21:52:23', 'manager', '2021-10-14 21:52:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('119', '-1', '7', '海南', '25.00', '22.00', '2021-10-14 21:52:23', 'manager', '2021-10-14 21:52:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('120', '-1', '7', '广西', '25.00', '22.00', '2021-10-14 21:52:23', 'manager', '2021-10-14 21:52:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('121', '-1', '10', '上海', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('122', '-1', '10', '浙江', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('123', '-1', '10', '江苏', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('124', '-1', '10', '安徽', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('125', '-1', '10', '山东', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('126', '-1', '10', '北京', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('127', '-1', '10', '天津', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('128', '-1', '10', '广东', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('129', '-1', '10', '福建', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('130', '-1', '10', '江西', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('131', '-1', '10', '湖南', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('132', '-1', '10', '河南', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('133', '-1', '10', '湖北', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('134', '-1', '10', '河北', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('135', '-1', '10', '甘肃', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('136', '-1', '10', '山西', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('137', '-1', '10', '重庆', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('138', '-1', '10', '陕西', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('139', '-1', '10', '四川', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('140', '-1', '10', '海南', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('141', '-1', '10', '云南', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('142', '-1', '10', '吉林', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('143', '-1', '10', '辽宁', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('144', '-1', '10', '贵州', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('145', '-1', '10', '黑龙江', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('146', '-1', '10', '青海', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('147', '-1', '10', '广西', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('148', '-1', '10', '内蒙', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('149', '-1', '10', '宁夏', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('150', '-1', '10', '西藏', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('151', '-1', '10', '新疆', '0.00', '0.00', '2021-10-14 21:55:46', 'manager', '2021-10-14 21:55:46', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('152', '-1', '8', '辽宁', '12.00', '10.00', '2021-10-14 22:07:53', 'manager', '2021-10-14 22:07:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('153', '-1', '8', '吉林', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('154', '-1', '8', '黑龙江', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('155', '-1', '8', '北京', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('156', '-1', '8', '天津', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('157', '-1', '8', '内蒙', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('158', '-1', '8', '山西', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('159', '-1', '8', '河北', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('160', '-1', '8', '山东', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('161', '-1', '8', '河南', '15.00', '10.00', '2021-10-14 22:08:18', 'manager', '2021-10-14 22:08:18', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('162', '-1', '8', '上海', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('163', '-1', '8', '浙江', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('164', '-1', '8', '江苏', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('165', '-1', '8', '安徽', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('166', '-1', '8', '福建', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('167', '-1', '8', '湖南', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('168', '-1', '8', '湖北', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('169', '-1', '8', '广东', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('170', '-1', '8', '江西', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('171', '-1', '8', '重庆', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('172', '-1', '8', '陕西', '16.00', '12.00', '2021-10-14 22:08:55', 'manager', '2021-10-14 22:08:55', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('173', '-1', '8', '青海', '20.00', '15.00', '2021-10-14 22:09:33', 'manager', '2021-10-14 22:09:33', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('174', '-1', '8', '甘肃', '20.00', '15.00', '2021-10-14 22:09:33', 'manager', '2021-10-14 22:09:33', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('175', '-1', '8', '宁夏', '20.00', '15.00', '2021-10-14 22:09:33', 'manager', '2021-10-14 22:09:33', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('176', '-1', '8', '贵州', '20.00', '15.00', '2021-10-14 22:09:33', 'manager', '2021-10-14 22:09:33', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('177', '-1', '8', '四川', '20.00', '15.00', '2021-10-14 22:09:33', 'manager', '2021-10-14 22:09:33', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('178', '-1', '8', '西藏', '25.00', '22.00', '2021-10-14 22:10:09', 'manager', '2021-10-14 22:10:09', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('179', '-1', '8', '新疆', '25.00', '22.00', '2021-10-14 22:10:09', 'manager', '2021-10-14 22:10:09', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('180', '-1', '8', '云南', '25.00', '22.00', '2021-10-14 22:10:09', 'manager', '2021-10-14 22:10:09', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('181', '-1', '8', '海南', '25.00', '22.00', '2021-10-14 22:10:09', 'manager', '2021-10-14 22:10:09', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('182', '-1', '8', '广西', '25.00', '22.00', '2021-10-14 22:10:09', 'manager', '2021-10-14 22:10:09', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('183', '-1', '1', '江苏', '10.00', '8.00', '2021-10-14 22:11:38', 'manager', '2021-10-14 22:11:38', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('184', '-1', '1', '浙江', '10.00', '8.00', '2021-10-14 22:11:38', 'manager', '2021-10-14 22:11:38', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('185', '-1', '1', '上海', '10.00', '8.00', '2021-10-14 22:11:38', 'manager', '2021-10-14 22:11:38', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('186', '-1', '1', '福建', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('187', '-1', '1', '江西', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('188', '-1', '1', '山西', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('189', '-1', '1', '河北', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('190', '-1', '1', '山东', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('191', '-1', '1', '河南', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('192', '-1', '1', '安徽', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('193', '-1', '1', '广东', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('194', '-1', '1', '湖南', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('195', '-1', '1', '湖北', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('196', '-1', '1', '北京', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('197', '-1', '1', '天津', '12.00', '10.00', '2021-10-14 22:12:45', 'manager', '2021-10-14 22:12:45', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('198', '-1', '1', '云南', '15.00', '12.00', '2021-10-14 22:13:25', 'manager', '2021-10-14 22:13:25', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('199', '-1', '1', '重庆', '15.00', '12.00', '2021-10-14 22:13:25', 'manager', '2021-10-14 22:13:25', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('200', '-1', '1', '四川', '15.00', '12.00', '2021-10-14 22:13:25', 'manager', '2021-10-14 22:13:25', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('201', '-1', '1', '贵州', '15.00', '12.00', '2021-10-14 22:13:25', 'manager', '2021-10-14 22:13:25', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('202', '-1', '1', '陕西', '15.00', '12.00', '2021-10-14 22:13:25', 'manager', '2021-10-14 22:13:25', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('203', '-1', '1', '吉林', '16.00', '14.00', '2021-10-14 22:14:03', 'manager', '2021-10-14 22:14:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('204', '-1', '1', '辽宁', '16.00', '14.00', '2021-10-14 22:14:03', 'manager', '2021-10-14 22:14:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('205', '-1', '1', '广西', '16.00', '14.00', '2021-10-14 22:14:03', 'manager', '2021-10-14 22:14:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('206', '-1', '1', '黑龙江', '16.00', '14.00', '2021-10-14 22:14:03', 'manager', '2021-10-14 22:14:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('207', '-1', '1', '甘肃', '16.00', '14.00', '2021-10-14 22:14:03', 'manager', '2021-10-14 22:14:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('208', '-1', '1', '海南', '16.00', '14.00', '2021-10-14 22:14:03', 'manager', '2021-10-14 22:14:03', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('209', '-1', '1', '内蒙', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('210', '-1', '1', '新疆', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('211', '-1', '1', '宁夏', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('212', '-1', '1', '青海', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('213', '-1', '1', '西藏', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('214', '-1', '1', '台湾', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('215', '-1', '1', '香港', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('216', '-1', '1', '澳门', '26.00', '22.00', '2021-10-14 22:14:39', 'manager', '2021-10-14 22:14:39', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('217', '-1', '4', '上海', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('218', '-1', '4', '浙江', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('219', '-1', '4', '江苏', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('220', '-1', '4', '安徽', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('221', '-1', '4', '山东', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('222', '-1', '4', '北京', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('223', '-1', '4', '天津', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('224', '-1', '4', '广东', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('225', '-1', '4', '福建', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('226', '-1', '4', '江西', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('227', '-1', '4', '湖南', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('228', '-1', '4', '河南', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('229', '-1', '4', '湖北', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('230', '-1', '4', '河北', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('231', '-1', '4', '甘肃', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('232', '-1', '4', '山西', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('233', '-1', '4', '重庆', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('234', '-1', '4', '陕西', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('235', '-1', '4', '四川', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('236', '-1', '4', '海南', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('237', '-1', '4', '云南', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('238', '-1', '4', '吉林', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('239', '-1', '4', '辽宁', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('240', '-1', '4', '贵州', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('241', '-1', '4', '黑龙江', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('242', '-1', '4', '青海', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('243', '-1', '4', '广西', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('244', '-1', '4', '内蒙', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('245', '-1', '4', '宁夏', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('246', '-1', '4', '西藏', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('247', '-1', '4', '新疆', '0.00', '0.00', '2021-10-14 22:16:16', 'manager', '2021-10-14 22:16:16', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('248', '-1', '3', '上海', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('249', '-1', '3', '浙江', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('250', '-1', '3', '江苏', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('251', '-1', '3', '安徽', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('252', '-1', '3', '山东', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('253', '-1', '3', '北京', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('254', '-1', '3', '天津', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('255', '-1', '3', '广东', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('256', '-1', '3', '福建', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('257', '-1', '3', '江西', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('258', '-1', '3', '湖南', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('259', '-1', '3', '河南', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('260', '-1', '3', '湖北', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('261', '-1', '3', '河北', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('262', '-1', '3', '山西', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('263', '-1', '3', '云南', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('264', '-1', '3', '重庆', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('265', '-1', '3', '陕西', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('266', '-1', '3', '四川', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('267', '-1', '3', '吉林', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('268', '-1', '3', '辽宁', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('269', '-1', '3', '贵州', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('270', '-1', '3', '海南', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('271', '-1', '3', '甘肃', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('272', '-1', '3', '黑龙江', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('273', '-1', '3', '青海', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('274', '-1', '3', '广西', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('275', '-1', '3', '内蒙', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('276', '-1', '3', '宁夏', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('277', '-1', '3', '西藏', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('278', '-1', '3', '新疆', '0.00', '0.00', '2021-10-14 22:16:58', 'manager', '2021-10-14 22:16:58', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('279', '-1', '9', '上海', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('280', '-1', '9', '浙江', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('281', '-1', '9', '江苏', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('282', '-1', '9', '安徽', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('283', '-1', '9', '广东', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('284', '-1', '9', '湖南', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('285', '-1', '9', '湖北', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('286', '-1', '9', '河南', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('287', '-1', '9', '北京', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('288', '-1', '9', '天津', '12.00', '10.00', '2021-10-14 22:20:53', 'manager', '2021-10-14 22:20:53', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('289', '-1', '9', '福建', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('290', '-1', '9', '江西', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('291', '-1', '9', '山西', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('292', '-1', '9', '云南', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('293', '-1', '9', '重庆', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('294', '-1', '9', '陕西', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('295', '-1', '9', '四川', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('296', '-1', '9', '河北', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('297', '-1', '9', '山东', '15.00', '12.00', '2021-10-14 22:21:23', 'manager', '2021-10-14 22:21:23', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('298', '-1', '9', '吉林', '18.00', '14.00', '2021-10-14 22:22:00', 'manager', '2021-10-14 22:22:00', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('299', '-1', '9', '辽宁', '18.00', '14.00', '2021-10-14 22:22:00', 'manager', '2021-10-14 22:22:00', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('300', '-1', '9', '贵州', '18.00', '14.00', '2021-10-14 22:22:00', 'manager', '2021-10-14 22:22:00', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('301', '-1', '9', '广西', '18.00', '14.00', '2021-10-14 22:22:00', 'manager', '2021-10-14 22:22:00', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('302', '-1', '9', '黑龙江', '18.00', '14.00', '2021-10-14 22:22:00', 'manager', '2021-10-14 22:22:00', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('303', '-1', '9', '甘肃', '18.00', '14.00', '2021-10-14 22:22:00', 'manager', '2021-10-14 22:22:00', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('304', '-1', '9', '内蒙', '28.00', '20.00', '2021-10-14 22:22:32', 'manager', '2021-10-14 22:22:32', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('305', '-1', '9', '宁夏', '28.00', '20.00', '2021-10-14 22:22:32', 'manager', '2021-10-14 22:22:32', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('306', '-1', '9', '西藏', '28.00', '20.00', '2021-10-14 22:22:32', 'manager', '2021-10-14 22:22:32', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('307', '-1', '9', '新疆', '28.00', '20.00', '2021-10-14 22:22:32', 'manager', '2021-10-14 22:22:32', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('308', '-1', '9', '海南', '28.00', '20.00', '2021-10-14 22:22:32', 'manager', '2021-10-14 22:22:32', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('309', '-1', '9', '青海', '28.00', '20.00', '2021-10-14 22:22:32', 'manager', '2021-10-14 22:22:32', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('310', '-1', '2', '上海', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('311', '-1', '2', '浙江', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('312', '-1', '2', '江苏', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('313', '-1', '2', '安徽', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('314', '-1', '2', '山东', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('315', '-1', '2', '北京', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('316', '-1', '2', '天津', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('317', '-1', '2', '广东', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('318', '-1', '2', '福建', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('319', '-1', '2', '江西', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('320', '-1', '2', '湖南', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('321', '-1', '2', '河南', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('322', '-1', '2', '湖北', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('323', '-1', '2', '河北', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('324', '-1', '2', '山西', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('325', '-1', '2', '云南', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('326', '-1', '2', '重庆', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('327', '-1', '2', '陕西', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('328', '-1', '2', '四川', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('329', '-1', '2', '吉林', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('330', '-1', '2', '辽宁', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('331', '-1', '2', '贵州', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('332', '-1', '2', '黑龙江', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('333', '-1', '2', '甘肃', '22.00', '16.00', '2021-10-14 22:26:35', 'manager', '2021-10-14 22:26:35', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('334', '-1', '2', '青海', '28.00', '24.00', '2021-10-14 22:27:10', 'manager', '2021-10-14 22:27:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('335', '-1', '2', '广西', '28.00', '24.00', '2021-10-14 22:27:10', 'manager', '2021-10-14 22:27:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('336', '-1', '2', '内蒙', '28.00', '24.00', '2021-10-14 22:27:10', 'manager', '2021-10-14 22:27:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('337', '-1', '2', '宁夏', '28.00', '24.00', '2021-10-14 22:27:10', 'manager', '2021-10-14 22:27:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('338', '-1', '2', '西藏', '28.00', '24.00', '2021-10-14 22:27:10', 'manager', '2021-10-14 22:27:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('339', '-1', '2', '新疆', '28.00', '24.00', '2021-10-14 22:27:10', 'manager', '2021-10-14 22:27:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('340', '-1', '2', '海南', '28.00', '24.00', '2021-10-14 22:27:10', 'manager', '2021-10-14 22:27:28', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('348', '-1', '2', '台湾', '99.00', '99.00', '2021-10-14 22:27:44', 'manager', '2021-10-14 22:27:44', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('349', '-1', '2', '香港', '99.00', '99.00', '2021-10-14 22:27:44', 'manager', '2021-10-14 22:27:44', 'manager');
INSERT INTO `supplier_channel_delivery_charge` VALUES ('350', '-1', '2', '澳门', '99.00', '99.00', '2021-10-14 22:27:44', 'manager', '2021-10-14 22:27:44', 'manager');

-- ----------------------------
-- Table structure for supplier_channel_discount
-- ----------------------------
DROP TABLE IF EXISTS `supplier_channel_discount`;
CREATE TABLE `supplier_channel_discount` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `channel_id` int NOT NULL,
  `brand_id` int NOT NULL,
  `goods_category` tinyint NOT NULL,
  `year` char(4) COLLATE utf8mb4_bin NOT NULL,
  `season` char(2) COLLATE utf8mb4_bin NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of supplier_channel_discount
-- ----------------------------
INSERT INTO `supplier_channel_discount` VALUES ('4', '1', '6', '1', '', 'Q4', '3.50', '2021-09-30 15:56:47', 'manager', '2021-09-30 15:56:47', 'manager');
